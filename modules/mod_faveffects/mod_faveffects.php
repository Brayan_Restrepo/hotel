<?php

/**
*   FavEffects
*
*   Responsive and customizable Joomla!3 module
*
*   @version        1.5
*   @link           http://extensions.favthemes.com/faveffects
*   @author         FavThemes - http://www.favthemes.com
*   @copyright      Copyright (C) 2015 FavThemes.com. All Rights Reserved.
*   @license        Licensed under GNU/GPLv3, see http://www.gnu.org/licenses/gpl-3.0.html
*/

// no direct access

defined('_JEXEC') or die;

$jquery_load                            = $params->get('jquery_load');
$layout_effect                          = $params->get('layout_effect');
$icon_width                             = $params->get('icon_width');
$icon_border_radius                     = $params->get('icon_border_radius');
$icon_border_type                       = $params->get('icon_border_type');
$icon_border_width                      = $params->get('icon_border_width');
$title_google_font                      = $params->get('title_google_font');
$title_font_size                        = $params->get('title_font_size');

for ($j=1;$j<7;$j++) {

${'show_icon'.$j}                       = $params->get('show_icon'.$j);
${'icon_effect'.$j}                     = $params->get('icon_effect'.$j);
${'icon_name'.$j}                       = $params->get('icon_name'.$j);
${'icon_color'.$j}                      = $params->get('icon_color'.$j);
${'icon_bg_color'.$j}                   = $params->get('icon_bg_color'.$j);
${'show_icon_link'.$j}                  = $params->get('show_icon_link'.$j);
${'icon_link'.$j}                       = $params->get('icon_link'.$j);
${'icon_link_target'.$j}                = $params->get('icon_link_target'.$j);
${'icon_font_size'.$j}                  = $params->get('icon_font_size'.$j);
${'icon_border_color'.$j}               = $params->get('icon_border_color'.$j);
${'title_text'.$j}                      = $params->get('title_text'.$j);
${'title_color'.$j}                     = $params->get('title_color'.$j);
${'show_title_link'.$j}                 = $params->get('show_title_link'.$j);
${'title_link'.$j}                      = $params->get('title_link'.$j);

}

$custom_id = rand(10000,20000);

// Load Bootstrap
JHtml::_('bootstrap.framework');
JHTML::stylesheet('media/jui/css/bootstrap.min.css');
JHTML::stylesheet('media/jui/css/bootstrap-responsive.css');
// Module CSS
JHTML::stylesheet('modules/mod_faveffects/theme/css/faveffects.css');
JHTML::stylesheet('modules/mod_faveffects/theme/icons/FontAwesome/css/font-awesome.css');
// Google Font
JHTML::stylesheet('//fonts.googleapis.com/css?family='.$title_google_font);
JHTML::stylesheet('//fonts.googleapis.com/css?family=Open+Sans:400');
// Scripts
if ($jquery_load) {JHtml::_('jquery.framework'); }
JHTML::script('modules/mod_faveffects/theme/js/viewportchecker/viewportchecker.js');

?>

<?php if ($layout_effect != 'layout-effect-none') { ?>

  <script type="text/javascript">
    jQuery(document).ready(function() {
    jQuery('#faveffects-<?php echo $custom_id; ?> .layout-effect').addClass("favhide").viewportChecker({
      classToAdd: 'favshow <?php echo $layout_effect; ?>', // Class to add to the elements when they are visible
      offset: 100
      });
    });
  </script>

<?php } ?>

<div id="faveffects-<?php echo $custom_id; ?>" class="row-fluid">

  <?php
  $span_class = '';
  $active_columns = array($show_icon1,$show_icon2,$show_icon3,$show_icon4,$show_icon5,$show_icon6);
  $columns_check = 0; foreach ($active_columns as $active_column) { if ($active_column == 1) { $columns_check++; } }

    if ($columns_check > 0) { $span_class = 'span'.(str_replace(".","-",12/$columns_check)); }

    for ($i=1;$i<7;$i++) {

    if ((${'show_icon'.$i}) !=0) { ?>

    <div id="faveffects-box<?php echo $i; ?>"
      class="<?php echo $span_class; ?> faveffects<?php echo $i; ?> layout-effect">

        <div id="faveffects-icon<?php echo $i; ?>"
          class="faveffects-<?php echo ${'icon_effect'.$i}; ?>"
          style="background-color: #<?php echo ${'icon_bg_color'.$i}; ?>;
                  width: <?php echo $icon_width; ?>;
                  border: <?php echo $icon_border_width; ?> <?php echo $icon_border_type; ?> #<?php echo ${'icon_border_color'.$i}; ?>;
                  -webkit-border-radius: <?php echo $icon_border_radius; ?>;
                  -moz-border-radius: <?php echo $icon_border_radius; ?>;
                  border-radius: <?php echo $icon_border_radius; ?>;">

          <?php if (${'show_icon_link'.$i} == 1) { ?>

            <a href="<?php echo ${'icon_link'.$i}; ?>" target="_<?php echo ${'icon_link_target'.$i}; ?>">

          <?php } ?>

              <i class="fa <?php echo ${'icon_name'.$i}; ?>"
                  style="color: #<?php echo ${'icon_color'.$i}; ?>;
                        font-size: <?php echo ${'icon_font_size'.$i}; ?>;">
              </i>

          <?php if (${'show_icon_link'.$i} == 1) { ?>

            </a>

          <?php } ?>

        </div>

        <p id="faveffects-title<?php echo $i; ?>"
            style="color: #<?php echo ${'title_color'.$i}; ?>;
                  font-family: <?php echo $title_google_font; ?>;
                  font-size: <?php echo $title_font_size; ?>;">

          <?php if (${'show_title_link'.$i} == 1) { ?>

            <a href="<?php echo ${'title_link'.$i}; ?>" target="_<?php echo ${'icon_link_target'.$i}; ?>"
              style="color: #<?php echo ${'title_color'.$i}; ?>;">

          <?php } ?>

              <?php echo ${'title_text'.$i}; ?>

          <?php if (${'show_title_link'.$i} == 1) { ?>

            </a>

          <?php } ?>

        </p>

    </div>

  <?php } } ?>

</div>