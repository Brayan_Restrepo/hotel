
DROP TABLE IF EXISTS `#__assets`;
CREATE TABLE IF NOT EXISTS `#__assets` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `parent_id` int(11) NOT NULL DEFAULT '0' COMMENT 'Nested set parent.',
  `lft` int(11) NOT NULL DEFAULT '0' COMMENT 'Nested set lft.',
  `rgt` int(11) NOT NULL DEFAULT '0' COMMENT 'Nested set rgt.',
  `level` int(10) unsigned NOT NULL COMMENT 'The cached level in the nested tree.',
  `name` varchar(50) NOT NULL COMMENT 'The unique name for the asset.\n',
  `title` varchar(100) NOT NULL COMMENT 'The descriptive title for the asset.',
  `rules` varchar(5120) NOT NULL COMMENT 'JSON encoded access control.',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_asset_name` (`name`),
  KEY `idx_lft_rgt` (`lft`,`rgt`),
  KEY `idx_parent_id` (`parent_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=512 ;

-- --------------------------------------------------------

--
-- Table structure for table `#__associations`
--

DROP TABLE IF EXISTS `#__associations`;
CREATE TABLE IF NOT EXISTS `#__associations` (
  `id` int(11) NOT NULL COMMENT 'A reference to the associated item.',
  `context` varchar(50) NOT NULL COMMENT 'The context of the associated item.',
  `key` char(32) NOT NULL COMMENT 'The key for the association computed from an md5 on associated ids.',
  PRIMARY KEY (`context`,`id`),
  KEY `idx_key` (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `#__associations`
--

INSERT INTO `#__associations` (`id`, `context`, `key`) VALUES
(208, 'com_menus.item', 'd8de03296c20dc0e00a095d1bdf9d908'),
(209, 'com_menus.item', 'd8de03296c20dc0e00a095d1bdf9d908'),
(214, 'com_menus.item', 'd8de03296c20dc0e00a095d1bdf9d908');

-- --------------------------------------------------------

--
-- Table structure for table `#__banners`
--

DROP TABLE IF EXISTS `#__banners`;
CREATE TABLE IF NOT EXISTS `#__banners` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cid` int(11) NOT NULL DEFAULT '0',
  `type` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL DEFAULT '',
  `alias` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `imptotal` int(11) NOT NULL DEFAULT '0',
  `impmade` int(11) NOT NULL DEFAULT '0',
  `clicks` int(11) NOT NULL DEFAULT '0',
  `clickurl` varchar(200) NOT NULL DEFAULT '',
  `state` tinyint(3) NOT NULL DEFAULT '0',
  `catid` int(10) unsigned NOT NULL DEFAULT '0',
  `description` text NOT NULL,
  `custombannercode` varchar(2048) NOT NULL,
  `sticky` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `metakey` text NOT NULL,
  `params` text NOT NULL,
  `own_prefix` tinyint(1) NOT NULL DEFAULT '0',
  `metakey_prefix` varchar(255) NOT NULL DEFAULT '',
  `purchase_type` tinyint(4) NOT NULL DEFAULT '-1',
  `track_clicks` tinyint(4) NOT NULL DEFAULT '-1',
  `track_impressions` tinyint(4) NOT NULL DEFAULT '-1',
  `checked_out` int(10) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `reset` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `language` char(7) NOT NULL DEFAULT '',
  `created_by` int(10) unsigned NOT NULL DEFAULT '0',
  `created_by_alias` varchar(255) NOT NULL DEFAULT '',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(10) unsigned NOT NULL DEFAULT '0',
  `version` int(10) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `idx_state` (`state`),
  KEY `idx_own_prefix` (`own_prefix`),
  KEY `idx_metakey_prefix` (`metakey_prefix`),
  KEY `idx_banner_catid` (`catid`),
  KEY `idx_language` (`language`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `#__banner_clients`
--

DROP TABLE IF EXISTS `#__banner_clients`;
CREATE TABLE IF NOT EXISTS `#__banner_clients` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `contact` varchar(255) NOT NULL DEFAULT '',
  `email` varchar(255) NOT NULL DEFAULT '',
  `extrainfo` text NOT NULL,
  `state` tinyint(3) NOT NULL DEFAULT '0',
  `checked_out` int(10) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `metakey` text NOT NULL,
  `own_prefix` tinyint(4) NOT NULL DEFAULT '0',
  `metakey_prefix` varchar(255) NOT NULL DEFAULT '',
  `purchase_type` tinyint(4) NOT NULL DEFAULT '-1',
  `track_clicks` tinyint(4) NOT NULL DEFAULT '-1',
  `track_impressions` tinyint(4) NOT NULL DEFAULT '-1',
  PRIMARY KEY (`id`),
  KEY `idx_own_prefix` (`own_prefix`),
  KEY `idx_metakey_prefix` (`metakey_prefix`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `#__banner_tracks`
--

DROP TABLE IF EXISTS `#__banner_tracks`;
CREATE TABLE IF NOT EXISTS `#__banner_tracks` (
  `track_date` datetime NOT NULL,
  `track_type` int(10) unsigned NOT NULL,
  `banner_id` int(10) unsigned NOT NULL,
  `count` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`track_date`,`track_type`,`banner_id`),
  KEY `idx_track_date` (`track_date`),
  KEY `idx_track_type` (`track_type`),
  KEY `idx_banner_id` (`banner_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `#__categories`
--

DROP TABLE IF EXISTS `#__categories`;
CREATE TABLE IF NOT EXISTS `#__categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `asset_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'FK to the #__assets table.',
  `parent_id` int(10) unsigned NOT NULL DEFAULT '0',
  `lft` int(11) NOT NULL DEFAULT '0',
  `rgt` int(11) NOT NULL DEFAULT '0',
  `level` int(10) unsigned NOT NULL DEFAULT '0',
  `path` varchar(255) NOT NULL DEFAULT '',
  `extension` varchar(50) NOT NULL DEFAULT '',
  `title` varchar(255) NOT NULL,
  `alias` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `note` varchar(255) NOT NULL DEFAULT '',
  `description` mediumtext NOT NULL,
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `checked_out` int(11) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `access` int(10) unsigned NOT NULL DEFAULT '0',
  `params` text NOT NULL,
  `metadesc` varchar(1024) NOT NULL COMMENT 'The meta description for the page.',
  `metakey` varchar(1024) NOT NULL COMMENT 'The meta keywords for the page.',
  `metadata` varchar(2048) NOT NULL COMMENT 'JSON encoded metadata properties.',
  `created_user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `created_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `modified_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `hits` int(10) unsigned NOT NULL DEFAULT '0',
  `language` char(7) NOT NULL,
  `version` int(10) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `cat_idx` (`extension`,`published`,`access`),
  KEY `idx_access` (`access`),
  KEY `idx_checkout` (`checked_out`),
  KEY `idx_path` (`path`),
  KEY `idx_left_right` (`lft`,`rgt`),
  KEY `idx_alias` (`alias`),
  KEY `idx_language` (`language`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=25 ;

--
-- Dumping data for table `#__categories`
--

INSERT INTO `#__categories` (`id`, `asset_id`, `parent_id`, `lft`, `rgt`, `level`, `path`, `extension`, `title`, `alias`, `note`, `description`, `published`, `checked_out`, `checked_out_time`, `access`, `params`, `metadesc`, `metakey`, `metadata`, `created_user_id`, `created_time`, `modified_user_id`, `modified_time`, `hits`, `language`, `version`) VALUES
(1, 0, 0, 0, 45, 0, '', 'system', 'ROOT', 'root', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{}', '', '', '{}', 851, '2011-01-01 00:00:01', 0, '0000-00-00 00:00:00', 0, '*', 1),
(2, 27, 1, 1, 2, 1, 'uncategorised', 'com_content', 'Uncategorised', 'uncategorised', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{"category_layout":"","image":""}', '', '', '{"author":"","robots":""}', 851, '2011-01-01 00:00:01', 0, '0000-00-00 00:00:00', 0, '*', 1),
(3, 28, 1, 37, 38, 1, 'uncategorised', 'com_banners', 'Uncategorised', 'uncategorised', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{"category_layout":"","image":""}', '', '', '{"author":"","robots":""}', 851, '2011-01-01 00:00:01', 0, '0000-00-00 00:00:00', 0, '*', 1),
(4, 29, 1, 39, 40, 1, 'uncategorised', 'com_contact', 'Uncategorised', 'uncategorised', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{"category_layout":"","image":""}', '', '', '{"author":"","robots":""}', 851, '2011-01-01 00:00:01', 0, '0000-00-00 00:00:00', 0, '*', 1),
(5, 30, 1, 41, 42, 1, 'uncategorised', 'com_newsfeeds', 'Uncategorised', 'uncategorised', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{"category_layout":"","image":""}', '', '', '{"author":"","robots":""}', 851, '2011-01-01 00:00:01', 0, '0000-00-00 00:00:00', 0, '*', 1),
(7, 32, 1, 43, 44, 1, 'uncategorised', 'com_users', 'Uncategorised', 'uncategorised', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{"category_layout":"","image":""}', '', '', '{"author":"","robots":""}', 851, '2011-01-01 00:00:01', 0, '0000-00-00 00:00:00', 0, '*', 1),
(8, 63, 1, 3, 4, 1, 'home', 'com_content', 'Home', 'home', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{"category_layout":"","image":"","image_alt":""}', '', '', '{"author":"","robots":""}', 851, '2015-04-12 12:45:31', 0, '2015-04-12 12:45:31', 0, '*', 1),
(9, 64, 1, 5, 6, 1, 'store', 'com_content', 'Store', 'store', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{"category_layout":"","image":"","image_alt":""}', '', '', '{"author":"","robots":""}', 851, '2015-04-12 12:45:44', 0, '2015-04-12 12:45:44', 0, '*', 1),
(10, 65, 1, 7, 20, 1, 'features', 'com_content', 'Features', 'features', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{"category_layout":"","image":"","image_alt":""}', '', '', '{"author":"","robots":""}', 851, '2015-04-12 12:46:00', 0, '2015-04-12 12:46:00', 0, '*', 1),
(11, 66, 1, 21, 28, 1, 'joomla', 'com_content', 'Joomla', 'joomla', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{"category_layout":"","image":"","image_alt":""}', '', '', '{"author":"","robots":""}', 851, '2015-04-12 12:46:14', 0, '2015-04-12 12:46:14', 0, '*', 1),
(12, 67, 1, 29, 32, 1, 'k2', 'com_content', 'K2 ', 'k2', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{"category_layout":"","image":"","image_alt":""}', '', '', '{"author":"","robots":""}', 851, '2015-04-12 13:36:22', 0, '2015-04-12 13:36:22', 0, '*', 1),
(13, 68, 1, 33, 34, 1, 'extensions', 'com_content', 'Extensions', 'extensions', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{"category_layout":"","image":"","image_alt":""}', '', '', '{"author":"","robots":""}', 851, '2015-04-12 13:36:39', 0, '2015-04-12 13:36:39', 0, '*', 1),
(14, 69, 1, 35, 36, 1, 'docs', 'com_content', 'Docs', 'docs', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{"category_layout":"","image":"","image_alt":""}', '', '', '{"author":"","robots":""}', 851, '2015-04-12 13:36:52', 851, '2015-06-05 11:15:55', 0, '*', 1),
(15, 70, 10, 8, 9, 2, 'features/module-positions', 'com_content', 'Module Positions', 'module-positions', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{"category_layout":"","image":"","image_alt":""}', '', '', '{"author":"","robots":""}', 851, '2015-04-12 13:38:40', 0, '2015-04-12 13:38:40', 0, '*', 1),
(16, 71, 10, 10, 11, 2, 'features/module-variations', 'com_content', 'Module Variations', 'module-variations', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{"category_layout":"","image":"","image_alt":""}', '', '', '{"author":"","robots":""}', 851, '2015-04-12 13:38:56', 0, '2015-04-12 13:38:56', 0, '*', 1),
(17, 72, 10, 12, 13, 2, 'features/navigation-styles', 'com_content', 'Navigation Styles', 'navigation-styles', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{"category_layout":"","image":"","image_alt":""}', '', '', '{"author":"","robots":""}', 851, '2015-04-12 13:39:13', 0, '2015-04-12 13:39:13', 0, '*', 1),
(18, 73, 10, 14, 15, 2, 'features/typography', 'com_content', 'Typography', 'typography', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{"category_layout":"","image":"","image_alt":""}', '', '', '{"author":"","robots":""}', 851, '2015-04-12 13:39:34', 0, '2015-04-12 13:39:34', 0, '*', 1),
(19, 74, 11, 22, 23, 2, 'joomla/joomla-content', 'com_content', 'Joomla Content', 'joomla-content', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{"category_layout":"","image":"","image_alt":""}', '', '', '{"author":"","robots":""}', 851, '2015-04-12 13:39:57', 0, '2015-04-12 13:39:57', 0, '*', 1),
(20, 75, 11, 24, 25, 2, 'joomla/joomla-modules', 'com_content', 'Joomla Modules', 'joomla-modules', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{"category_layout":"","image":"","image_alt":""}', '', '', '{"author":"","robots":""}', 851, '2015-04-12 13:40:17', 0, '2015-04-12 13:40:17', 0, '*', 1),
(21, 76, 10, 16, 17, 2, 'features/404-page', 'com_content', '404 Page', '404-page', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{"category_layout":"","image":"","image_alt":""}', '', '', '{"author":"","robots":""}', 851, '2015-04-12 13:40:31', 851, '2015-08-04 21:55:53', 0, '*', 1),
(22, 77, 11, 26, 27, 2, 'joomla/newsflash', 'com_content', 'Newsflash', 'newsflash', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{"category_layout":"","image":"","image_alt":""}', '', '', '{"author":"","robots":""}', 851, '2015-04-12 13:40:49', 0, '2015-04-12 13:40:49', 0, '*', 1),
(23, 78, 12, 30, 31, 2, 'k2/k2-modules', 'com_content', 'K2 Modules', 'k2-modules', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{"category_layout":"","image":"","image_alt":""}', '', '', '{"author":"","robots":""}', 851, '2015-04-12 13:41:06', 0, '2015-04-12 13:41:06', 0, '*', 1),
(24, 478, 10, 18, 19, 2, 'features/offline-page', 'com_content', 'Offline Page', 'offline-page', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{"category_layout":"","image":"","image_alt":""}', '', '', '{"author":"","robots":""}', 851, '2015-08-04 21:55:26', 851, '2015-08-04 21:55:34', 0, '*', 1);

-- --------------------------------------------------------

--
-- Table structure for table `#__contact_details`
--

DROP TABLE IF EXISTS `#__contact_details`;
CREATE TABLE IF NOT EXISTS `#__contact_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `alias` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `con_position` varchar(255) DEFAULT NULL,
  `address` text,
  `suburb` varchar(100) DEFAULT NULL,
  `state` varchar(100) DEFAULT NULL,
  `country` varchar(100) DEFAULT NULL,
  `postcode` varchar(100) DEFAULT NULL,
  `telephone` varchar(255) DEFAULT NULL,
  `fax` varchar(255) DEFAULT NULL,
  `misc` mediumtext,
  `image` varchar(255) DEFAULT NULL,
  `email_to` varchar(255) DEFAULT NULL,
  `default_con` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `checked_out` int(10) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `params` text NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `catid` int(11) NOT NULL DEFAULT '0',
  `access` int(10) unsigned NOT NULL DEFAULT '0',
  `mobile` varchar(255) NOT NULL DEFAULT '',
  `webpage` varchar(255) NOT NULL DEFAULT '',
  `sortname1` varchar(255) NOT NULL,
  `sortname2` varchar(255) NOT NULL,
  `sortname3` varchar(255) NOT NULL,
  `language` char(7) NOT NULL,
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(10) unsigned NOT NULL DEFAULT '0',
  `created_by_alias` varchar(255) NOT NULL DEFAULT '',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(10) unsigned NOT NULL DEFAULT '0',
  `metakey` text NOT NULL,
  `metadesc` text NOT NULL,
  `metadata` text NOT NULL,
  `featured` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT 'Set if article is featured.',
  `xreference` varchar(50) NOT NULL COMMENT 'A reference to enable linkages to external data sets.',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `version` int(10) unsigned NOT NULL DEFAULT '1',
  `hits` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_access` (`access`),
  KEY `idx_checkout` (`checked_out`),
  KEY `idx_state` (`published`),
  KEY `idx_catid` (`catid`),
  KEY `idx_createdby` (`created_by`),
  KEY `idx_featured_catid` (`featured`,`catid`),
  KEY `idx_language` (`language`),
  KEY `idx_xreference` (`xreference`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `#__contact_details`
--

INSERT INTO `#__contact_details` (`id`, `name`, `alias`, `con_position`, `address`, `suburb`, `state`, `country`, `postcode`, `telephone`, `fax`, `misc`, `image`, `email_to`, `default_con`, `published`, `checked_out`, `checked_out_time`, `ordering`, `params`, `user_id`, `catid`, `access`, `mobile`, `webpage`, `sortname1`, `sortname2`, `sortname3`, `language`, `created`, `created_by`, `created_by_alias`, `modified`, `modified_by`, `metakey`, `metadesc`, `metadata`, `featured`, `xreference`, `publish_up`, `publish_down`, `version`, `hits`) VALUES
(1, 'Demo Contact', 'demo-contact', '', 'Address', '', '', '', '', '', 'Fax', '', '', 'hello@favthemes.com', 0, 1, 851, '2015-08-05 01:03:18', 1, '{"show_contact_category":"","show_contact_list":"","presentation_style":"","show_tags":"","show_name":"","show_position":"","show_email":"","show_street_address":"","show_suburb":"","show_state":"","show_postcode":"","show_country":"","show_telephone":"","show_mobile":"","show_fax":"","show_webpage":"","show_misc":"","show_image":"","allow_vcard":"","show_articles":"","articles_display_num":"","show_profile":"","show_links":"","linka_name":"","linka":false,"linkb_name":"","linkb":false,"linkc_name":"","linkc":false,"linkd_name":"","linkd":false,"linke_name":"","linke":false,"contact_layout":"","show_email_form":"","show_email_copy":"","banned_email":"","banned_subject":"","banned_text":"","validate_session":"","custom_reply":"","redirect":""}', 0, 4, 1, 'Mobile', 'http://www.favthemes.com', '', '', '', '*', '2015-04-12 14:31:23', 851, '', '2015-08-05 01:03:18', 851, '', '', '{"robots":"","rights":""}', 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 2, 32);

-- --------------------------------------------------------

--
-- Table structure for table `#__content`
--

DROP TABLE IF EXISTS `#__content`;
CREATE TABLE IF NOT EXISTS `#__content` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `asset_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'FK to the #__assets table.',
  `title` varchar(255) NOT NULL DEFAULT '',
  `alias` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `introtext` mediumtext NOT NULL,
  `fulltext` mediumtext NOT NULL,
  `state` tinyint(3) NOT NULL DEFAULT '0',
  `catid` int(10) unsigned NOT NULL DEFAULT '0',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(10) unsigned NOT NULL DEFAULT '0',
  `created_by_alias` varchar(255) NOT NULL DEFAULT '',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(10) unsigned NOT NULL DEFAULT '0',
  `checked_out` int(10) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `images` text NOT NULL,
  `urls` text NOT NULL,
  `attribs` varchar(5120) NOT NULL,
  `version` int(10) unsigned NOT NULL DEFAULT '1',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `metakey` text NOT NULL,
  `metadesc` text NOT NULL,
  `access` int(10) unsigned NOT NULL DEFAULT '0',
  `hits` int(10) unsigned NOT NULL DEFAULT '0',
  `metadata` text NOT NULL,
  `featured` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT 'Set if article is featured.',
  `language` char(7) NOT NULL COMMENT 'The language code for the article.',
  `xreference` varchar(50) NOT NULL COMMENT 'A reference to enable linkages to external data sets.',
  PRIMARY KEY (`id`),
  KEY `idx_access` (`access`),
  KEY `idx_checkout` (`checked_out`),
  KEY `idx_state` (`state`),
  KEY `idx_catid` (`catid`),
  KEY `idx_createdby` (`created_by`),
  KEY `idx_featured_catid` (`featured`,`catid`),
  KEY `idx_language` (`language`),
  KEY `idx_xreference` (`xreference`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `#__content`
--

INSERT INTO `#__content` (`id`, `asset_id`, `title`, `alias`, `introtext`, `fulltext`, `state`, `catid`, `created`, `created_by`, `created_by_alias`, `modified`, `modified_by`, `checked_out`, `checked_out_time`, `publish_up`, `publish_down`, `images`, `urls`, `attribs`, `version`, `ordering`, `metakey`, `metadesc`, `access`, `hits`, `metadata`, `featured`, `language`, `xreference`) VALUES
(2, 79, 'Say hello to Favourite!  ', 'say-hello-to-favourite', '<p>We have created this awesome responsive template with over 200 settings, so that you can customize it quickly and easily for any project!</p>\r\n<p>Our themes are built using Bootstrap, HTML5, CSS3, Google Fonts and Font Awesome, while adding support for K2. Being responsive, this is a smart template that adapts and resizes itself to desktop and mobile devices.</p>\r\n<p>This free template can be used for unlimited personal or commercial websites, just don''t resell it.</p>\r\n', '\r\n<p>This Premium product also has extensive <a href="http://favthemes.com/tutorials.html" target="_blank">tutorials</a> for downloading, setting up and using the parameters of the template.</p>', 1, 19, '2015-04-12 13:49:58', 851, '', '2015-11-27 15:37:44', 851, 0, '0000-00-00 00:00:00', '2015-04-12 13:49:58', '0000-00-00 00:00:00', '{"image_intro":"images\\/demo\\/img\\/demo-img-1.jpg","float_intro":"","image_intro_alt":"","image_intro_caption":"","image_fulltext":"images\\/demo\\/img\\/demo-img-1.jpg","float_fulltext":"","image_fulltext_alt":"","image_fulltext_caption":""}', '{"urla":false,"urlatext":"","targeta":"","urlb":false,"urlbtext":"","targetb":"","urlc":false,"urlctext":"","targetc":""}', '{"show_title":"","link_titles":"","show_tags":"","show_intro":"","info_block_position":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_vote":"","show_hits":"","show_noauth":"","urls_position":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 18, 1, '', '', 1, 67, '{"robots":"","author":"","rights":"","xreference":""}', 0, '*', ''),
(3, 80, '404', '404', '<h3 class="fav404">404</h3>\r\n<p class="fav404">Oops! The page you requested was not found!</p>\r\n<p class="fav404"><a class="btn" href="index.php/">Back to Home Page</a></p>', '', 1, 21, '2015-04-12 15:30:02', 851, '', '2015-08-04 23:01:21', 851, 0, '0000-00-00 00:00:00', '2015-04-12 15:30:02', '0000-00-00 00:00:00', '{"image_intro":"","float_intro":"","image_intro_alt":"","image_intro_caption":"","image_fulltext":"","float_fulltext":"","image_fulltext_alt":"","image_fulltext_caption":""}', '{"urla":false,"urlatext":"","targeta":"","urlb":false,"urlbtext":"","targetb":"","urlc":false,"urlctext":"","targetc":""}', '{"show_title":"0","link_titles":"0","show_tags":"0","show_intro":"0","info_block_position":"","show_category":"0","link_category":"0","show_parent_category":"0","link_parent_category":"0","show_author":"0","link_author":"0","show_create_date":"0","show_modify_date":"0","show_publish_date":"0","show_item_navigation":"0","show_icons":"0","show_print_icon":"0","show_email_icon":"0","show_vote":"0","show_hits":"0","show_noauth":"0","urls_position":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 5, 1, '', '', 1, 176, '{"robots":"","author":"","rights":"","xreference":""}', 0, '*', ''),
(4, 89, 'Customize everything', 'customize-everything', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Elit mus aliquet quisquam aute numquam assumenda unde libero dolores nostrud reprehenderit odit?</p>\r\n<p>Vivamus sit amet libero turpis, non venenatis urna. In blandit, odio convallis suscipit venenatis, ante ipsum cursus augue.</p>\r\n', '\r\n<p>Elit mus aliquet quisquam aute numquam assumenda unde libero dolores nostrud reprehenderit odit? Proin facilisi molestiae dolorem duis, consequuntur lectus, vestibulum risus eleifend provident, ultricies perspiciatis hac molestie conubia eius, aute, consequatur, pellentesque.</p>', 1, 19, '2015-04-12 16:30:42', 851, '', '2015-06-06 12:42:28', 851, 0, '0000-00-00 00:00:00', '2015-04-12 16:30:42', '0000-00-00 00:00:00', '{"image_intro":"images\\/demo\\/img\\/demo-img-3.jpg","float_intro":"","image_intro_alt":"","image_intro_caption":"","image_fulltext":"images\\/demo\\/img\\/demo-img-3.jpg","float_fulltext":"","image_fulltext_alt":"","image_fulltext_caption":""}', '{"urla":false,"urlatext":"","targeta":"","urlb":false,"urlbtext":"","targetb":"","urlc":false,"urlctext":"","targetc":""}', '{"show_title":"","link_titles":"","show_tags":"","show_intro":"","info_block_position":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_vote":"","show_hits":"","show_noauth":"","urls_position":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 4, 3, '', '', 1, 0, '{"robots":"","author":"","rights":"","xreference":""}', 0, '*', ''),
(5, 90, 'Responsive theme', 'responsive-theme', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Elit mus aliquet quisquam aute numquam assumenda unde libero dolores nostrud reprehenderit odit?</p>\r\n<p>Vivamus sit amet libero turpis, non venenatis urna. In blandit, odio convallis suscipit venenatis, ante ipsum cursus augue.</p>\r\n', '\r\n<p>Elit mus aliquet quisquam aute numquam assumenda unde libero dolores nostrud reprehenderit odit? Proin facilisi molestiae dolorem duis, consequuntur lectus, vestibulum risus eleifend provident, ultricies perspiciatis hac molestie conubia eius, aute, consequatur, pellentesque.</p>', 1, 19, '2015-04-12 16:32:09', 851, '', '2015-06-06 12:42:08', 851, 0, '0000-00-00 00:00:00', '2015-04-12 16:32:09', '0000-00-00 00:00:00', '{"image_intro":"images\\/demo\\/img\\/demo-img-5.jpg","float_intro":"","image_intro_alt":"","image_intro_caption":"","image_fulltext":"images\\/demo\\/img\\/demo-img-5.jpg","float_fulltext":"","image_fulltext_alt":"","image_fulltext_caption":""}', '{"urla":false,"urlatext":"","targeta":"","urlb":false,"urlbtext":"","targetb":"","urlc":false,"urlctext":"","targetc":""}', '{"show_title":"","link_titles":"","show_tags":"","show_intro":"","info_block_position":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_vote":"","show_hits":"","show_noauth":"","urls_position":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 4, 2, '', '', 1, 2, '{"robots":"","author":"","rights":"","xreference":""}', 0, '*', ''),
(6, 91, 'Ready for mobile devices', 'ready-for-mobile-devices', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Elit mus aliquet quisquam aute numquam assumenda unde libero dolores nostrud reprehenderit odit?</p>\r\n<p>Vivamus sit amet libero turpis, non venenatis urna. In blandit, odio convallis suscipit venenatis, ante ipsum cursus augue.</p>\r\n', '\r\n<p>Elit mus aliquet quisquam aute numquam assumenda unde libero dolores nostrud reprehenderit odit? Proin facilisi molestiae dolorem duis, consequuntur lectus, vestibulum risus eleifend provident, ultricies perspiciatis hac molestie conubia eius, aute, consequatur, pellentesque.</p>', 1, 19, '2015-04-12 16:33:34', 851, '', '2015-06-06 12:41:48', 851, 0, '0000-00-00 00:00:00', '2015-04-12 16:33:34', '0000-00-00 00:00:00', '{"image_intro":"images\\/demo\\/img\\/demo-img-2.jpg","float_intro":"","image_intro_alt":"","image_intro_caption":"","image_fulltext":"images\\/demo\\/img\\/demo-img-2.jpg","float_fulltext":"","image_fulltext_alt":"","image_fulltext_caption":""}', '{"urla":false,"urlatext":"","targeta":"","urlb":false,"urlbtext":"","targetb":"","urlc":false,"urlctext":"","targetc":""}', '{"show_title":"","link_titles":"","show_tags":"","show_intro":"","info_block_position":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_vote":"","show_hits":"","show_noauth":"","urls_position":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 2, 5, '', '', 1, 1, '{"robots":"","author":"","rights":"","xreference":""}', 0, '*', ''),
(7, 92, 'A brand new store', 'a-brand-new-store', '<p><img class="img-center img-polaroid" src="images/demo/img/demo-img-4.jpg" alt="" /></p>\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>', '', 1, 22, '2015-04-12 16:34:02', 851, '', '2015-06-06 12:40:54', 851, 0, '0000-00-00 00:00:00', '2015-04-12 16:34:02', '0000-00-00 00:00:00', '{"image_intro":"","float_intro":"","image_intro_alt":"","image_intro_caption":"","image_fulltext":"","float_fulltext":"","image_fulltext_alt":"","image_fulltext_caption":""}', '{"urla":false,"urlatext":"","targeta":"","urlb":false,"urlbtext":"","targetb":"","urlc":false,"urlctext":"","targetc":""}', '{"show_title":"","link_titles":"","show_tags":"","show_intro":"","info_block_position":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_vote":"","show_hits":"","show_noauth":"","urls_position":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 6, 0, '', '', 1, 2, '{"robots":"","author":"","rights":"","xreference":""}', 0, '*', ''),
(8, 93, 'Article Title', 'article-title', '<p>This is the <code>article</code>. Add content to your articles such as text, images and links and structure your articles into categories. All the article and component content is placed here.</p>\r\n', '', 1, 15, '2015-04-12 16:35:01', 851, '', '2015-09-10 19:04:56', 851, 0, '0000-00-00 00:00:00', '2015-04-12 16:35:01', '0000-00-00 00:00:00', '{"image_intro":"","float_intro":"","image_intro_alt":"","image_intro_caption":"","image_fulltext":"","float_fulltext":"","image_fulltext_alt":"","image_fulltext_caption":""}', '{"urla":false,"urlatext":"","targeta":"","urlb":false,"urlbtext":"","targetb":"","urlc":false,"urlctext":"","targetc":""}', '{"show_title":"","link_titles":"","show_tags":"","show_intro":"","info_block_position":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_vote":"","show_hits":"","show_noauth":"","urls_position":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 126, 0, '', '', 1, 0, '{"robots":"","author":"","rights":"","xreference":""}', 0, '*', ''),
(9, 94, 'Archived Article', 'archived-article', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Elit mus aliquet quisquam aute numquam assumenda unde libero dolores nostrud reprehenderit odit? Vivamus sit amet libero turpis, non venenatis urna. In blandit, odio convallis suscipit venenatis, ante ipsum cursus augue.</p>\r\n<p>Elit mus aliquet quisquam aute numquam assumenda unde libero dolores nostrud reprehenderit odit? Proin facilisi molestiae dolorem duis, consequuntur lectus, vestibulum risus eleifend provident, ultricies perspiciatis hac molestie conubia eius, aute, consequatur, pellentesque.</p>', '', 2, 19, '2015-04-12 16:36:10', 851, '', '2015-04-12 17:23:18', 851, 0, '0000-00-00 00:00:00', '2015-04-12 16:36:10', '0000-00-00 00:00:00', '{"image_intro":"","float_intro":"","image_intro_alt":"","image_intro_caption":"","image_fulltext":"","float_fulltext":"","image_fulltext_alt":"","image_fulltext_caption":""}', '{"urla":false,"urlatext":"","targeta":"","urlb":false,"urlbtext":"","targetb":"","urlc":false,"urlctext":"","targetc":""}', '{"show_title":"","link_titles":"","show_tags":"","show_intro":"","info_block_position":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_vote":"","show_hits":"","show_noauth":"","urls_position":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 3, 4, '', '', 1, 0, '{"robots":"","author":"","rights":"","xreference":""}', 0, '*', ''),
(10, 491, 'Say hello to Favourite! ', 'say-hello-to-favourite', '<p>We have created this awesome responsive template with over 200 settings, so that you can customize it quickly and easily for any project! Being responsive, this is a smart template that adapts and resizes itself to desktop and mobile devices.</p>\r\n<p>Our themes are built using Bootstrap, HTML5, CSS3, Google Fonts and Font Awesome, while adding support for K2. Take a look at the <a href="index.php/documentation" target="_self">docs page</a> and explore the demo to discover the amazing features of this template!</p>\r\n<p>This free template can be used for unlimited personal or commercial websites, just don''t resell it.</p>\r\n', '\r\n<p>This Premium product also has extensive <a href="http://favthemes.com/tutorials.html" target="_blank">tutorials</a> for downloading, setting up and using the parameters of the template.</p>', 1, 8, '0000-00-00 00:00:00', 851, '', '2015-11-27 15:37:36', 851, 0, '0000-00-00 00:00:00', '2015-08-08 12:05:18', '0000-00-00 00:00:00', '{"image_intro":"images\\/demo\\/img\\/demo-img-home.jpg","float_intro":"left","image_intro_alt":"","image_intro_caption":"","image_fulltext":"images\\/demo\\/img\\/demo-img-home.jpg","float_fulltext":"left","image_fulltext_alt":"","image_fulltext_caption":""}', '{"urla":false,"urlatext":"","targeta":"","urlb":false,"urlbtext":"","targetb":"","urlc":false,"urlctext":"","targetc":""}', '{"show_title":"","link_titles":"","show_tags":"","show_intro":"","info_block_position":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_vote":"","show_hits":"","show_noauth":"","urls_position":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 31, 0, '', '', 1, 5, '{"robots":"","author":"","rights":"","xreference":""}', 1, '*', '');

-- --------------------------------------------------------

--
-- Table structure for table `#__contentitem_tag_map`
--

DROP TABLE IF EXISTS `#__contentitem_tag_map`;
CREATE TABLE IF NOT EXISTS `#__contentitem_tag_map` (
  `type_alias` varchar(255) NOT NULL DEFAULT '',
  `core_content_id` int(10) unsigned NOT NULL COMMENT 'PK from the core content table',
  `content_item_id` int(11) NOT NULL COMMENT 'PK from the content type table',
  `tag_id` int(10) unsigned NOT NULL COMMENT 'PK from the tag table',
  `tag_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Date of most recent save for this tag-item',
  `type_id` mediumint(8) NOT NULL COMMENT 'PK from the content_type table',
  UNIQUE KEY `uc_ItemnameTagid` (`type_id`,`content_item_id`,`tag_id`),
  KEY `idx_tag_type` (`tag_id`,`type_id`),
  KEY `idx_date_id` (`tag_date`,`tag_id`),
  KEY `idx_tag` (`tag_id`),
  KEY `idx_type` (`type_id`),
  KEY `idx_core_content_id` (`core_content_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Maps items from content tables to tags';

--
-- Dumping data for table `#__contentitem_tag_map`
--

INSERT INTO `#__contentitem_tag_map` (`type_alias`, `core_content_id`, `content_item_id`, `tag_id`, `tag_date`, `type_id`) VALUES
('com_content.article', 3, 4, 2, '2015-06-06 03:42:28', 1),
('com_content.article', 3, 4, 3, '2015-06-06 03:42:28', 1),
('com_content.article', 2, 5, 2, '2015-06-06 03:42:08', 1),
('com_content.article', 2, 5, 3, '2015-06-06 03:42:08', 1),
('com_content.article', 2, 5, 4, '2015-06-06 03:42:08', 1);

-- --------------------------------------------------------

--
-- Table structure for table `#__content_frontpage`
--

DROP TABLE IF EXISTS `#__content_frontpage`;
CREATE TABLE IF NOT EXISTS `#__content_frontpage` (
  `content_id` int(11) NOT NULL DEFAULT '0',
  `ordering` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`content_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `#__content_frontpage`
--

INSERT INTO `#__content_frontpage` (`content_id`, `ordering`) VALUES
(10, 1);

-- --------------------------------------------------------

--
-- Table structure for table `#__content_rating`
--

DROP TABLE IF EXISTS `#__content_rating`;
CREATE TABLE IF NOT EXISTS `#__content_rating` (
  `content_id` int(11) NOT NULL DEFAULT '0',
  `rating_sum` int(10) unsigned NOT NULL DEFAULT '0',
  `rating_count` int(10) unsigned NOT NULL DEFAULT '0',
  `lastip` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`content_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `#__content_types`
--

DROP TABLE IF EXISTS `#__content_types`;
CREATE TABLE IF NOT EXISTS `#__content_types` (
  `type_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type_title` varchar(255) NOT NULL DEFAULT '',
  `type_alias` varchar(255) NOT NULL DEFAULT '',
  `table` varchar(255) NOT NULL DEFAULT '',
  `rules` text NOT NULL,
  `field_mappings` text NOT NULL,
  `router` varchar(255) NOT NULL DEFAULT '',
  `content_history_options` varchar(5120) DEFAULT NULL COMMENT 'JSON string for com_contenthistory options',
  PRIMARY KEY (`type_id`),
  KEY `idx_alias` (`type_alias`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `#__content_types`
--

INSERT INTO `#__content_types` (`type_id`, `type_title`, `type_alias`, `table`, `rules`, `field_mappings`, `router`, `content_history_options`) VALUES
(1, 'Article', 'com_content.article', '{"special":{"dbtable":"#__content","key":"id","type":"Content","prefix":"JTable","config":"array()"},"common":{"dbtable":"#__ucm_content","key":"ucm_id","type":"Corecontent","prefix":"JTable","config":"array()"}}', '', '{"common":{"core_content_item_id":"id","core_title":"title","core_state":"state","core_alias":"alias","core_created_time":"created","core_modified_time":"modified","core_body":"introtext", "core_hits":"hits","core_publish_up":"publish_up","core_publish_down":"publish_down","core_access":"access", "core_params":"attribs", "core_featured":"featured", "core_metadata":"metadata", "core_language":"language", "core_images":"images", "core_urls":"urls", "core_version":"version", "core_ordering":"ordering", "core_metakey":"metakey", "core_metadesc":"metadesc", "core_catid":"catid", "core_xreference":"xreference", "asset_id":"asset_id"}, "special":{"fulltext":"fulltext"}}', 'ContentHelperRoute::getArticleRoute', '{"formFile":"administrator\\/components\\/com_content\\/models\\/forms\\/article.xml", "hideFields":["asset_id","checked_out","checked_out_time","version"],"ignoreChanges":["modified_by", "modified", "checked_out", "checked_out_time", "version", "hits"],"convertToInt":["publish_up", "publish_down", "featured", "ordering"],"displayLookup":[{"sourceColumn":"catid","targetTable":"#__categories","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"created_by","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"access","targetTable":"#__viewlevels","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"modified_by","targetTable":"#__users","targetColumn":"id","displayColumn":"name"} ]}'),
(2, 'Contact', 'com_contact.contact', '{"special":{"dbtable":"#__contact_details","key":"id","type":"Contact","prefix":"ContactTable","config":"array()"},"common":{"dbtable":"#__ucm_content","key":"ucm_id","type":"Corecontent","prefix":"JTable","config":"array()"}}', '', '{"common":{"core_content_item_id":"id","core_title":"name","core_state":"published","core_alias":"alias","core_created_time":"created","core_modified_time":"modified","core_body":"address", "core_hits":"hits","core_publish_up":"publish_up","core_publish_down":"publish_down","core_access":"access", "core_params":"params", "core_featured":"featured", "core_metadata":"metadata", "core_language":"language", "core_images":"image", "core_urls":"webpage", "core_version":"version", "core_ordering":"ordering", "core_metakey":"metakey", "core_metadesc":"metadesc", "core_catid":"catid", "core_xreference":"xreference", "asset_id":"null"}, "special":{"con_position":"con_position","suburb":"suburb","state":"state","country":"country","postcode":"postcode","telephone":"telephone","fax":"fax","misc":"misc","email_to":"email_to","default_con":"default_con","user_id":"user_id","mobile":"mobile","sortname1":"sortname1","sortname2":"sortname2","sortname3":"sortname3"}}', 'ContactHelperRoute::getContactRoute', '{"formFile":"administrator\\/components\\/com_contact\\/models\\/forms\\/contact.xml","hideFields":["default_con","checked_out","checked_out_time","version","xreference"],"ignoreChanges":["modified_by", "modified", "checked_out", "checked_out_time", "version", "hits"],"convertToInt":["publish_up", "publish_down", "featured", "ordering"], "displayLookup":[ {"sourceColumn":"created_by","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"catid","targetTable":"#__categories","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"modified_by","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"access","targetTable":"#__viewlevels","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"user_id","targetTable":"#__users","targetColumn":"id","displayColumn":"name"} ] }'),
(3, 'Newsfeed', 'com_newsfeeds.newsfeed', '{"special":{"dbtable":"#__newsfeeds","key":"id","type":"Newsfeed","prefix":"NewsfeedsTable","config":"array()"},"common":{"dbtable":"#__ucm_content","key":"ucm_id","type":"Corecontent","prefix":"JTable","config":"array()"}}', '', '{"common":{"core_content_item_id":"id","core_title":"name","core_state":"published","core_alias":"alias","core_created_time":"created","core_modified_time":"modified","core_body":"description", "core_hits":"hits","core_publish_up":"publish_up","core_publish_down":"publish_down","core_access":"access", "core_params":"params", "core_featured":"featured", "core_metadata":"metadata", "core_language":"language", "core_images":"images", "core_urls":"link", "core_version":"version", "core_ordering":"ordering", "core_metakey":"metakey", "core_metadesc":"metadesc", "core_catid":"catid", "core_xreference":"xreference", "asset_id":"null"}, "special":{"numarticles":"numarticles","cache_time":"cache_time","rtl":"rtl"}}', 'NewsfeedsHelperRoute::getNewsfeedRoute', '{"formFile":"administrator\\/components\\/com_newsfeeds\\/models\\/forms\\/newsfeed.xml","hideFields":["asset_id","checked_out","checked_out_time","version"],"ignoreChanges":["modified_by", "modified", "checked_out", "checked_out_time", "version", "hits"],"convertToInt":["publish_up", "publish_down", "featured", "ordering"],"displayLookup":[{"sourceColumn":"catid","targetTable":"#__categories","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"created_by","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"access","targetTable":"#__viewlevels","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"modified_by","targetTable":"#__users","targetColumn":"id","displayColumn":"name"} ]}'),
(4, 'User', 'com_users.user', '{"special":{"dbtable":"#__users","key":"id","type":"User","prefix":"JTable","config":"array()"},"common":{"dbtable":"#__ucm_content","key":"ucm_id","type":"Corecontent","prefix":"JTable","config":"array()"}}', '', '{"common":{"core_content_item_id":"id","core_title":"name","core_state":"null","core_alias":"username","core_created_time":"registerdate","core_modified_time":"lastvisitDate","core_body":"null", "core_hits":"null","core_publish_up":"null","core_publish_down":"null","access":"null", "core_params":"params", "core_featured":"null", "core_metadata":"null", "core_language":"null", "core_images":"null", "core_urls":"null", "core_version":"null", "core_ordering":"null", "core_metakey":"null", "core_metadesc":"null", "core_catid":"null", "core_xreference":"null", "asset_id":"null"}, "special":{}}', 'UsersHelperRoute::getUserRoute', ''),
(5, 'Article Category', 'com_content.category', '{"special":{"dbtable":"#__categories","key":"id","type":"Category","prefix":"JTable","config":"array()"},"common":{"dbtable":"#__ucm_content","key":"ucm_id","type":"Corecontent","prefix":"JTable","config":"array()"}}', '', '{"common":{"core_content_item_id":"id","core_title":"title","core_state":"published","core_alias":"alias","core_created_time":"created_time","core_modified_time":"modified_time","core_body":"description", "core_hits":"hits","core_publish_up":"null","core_publish_down":"null","core_access":"access", "core_params":"params", "core_featured":"null", "core_metadata":"metadata", "core_language":"language", "core_images":"null", "core_urls":"null", "core_version":"version", "core_ordering":"null", "core_metakey":"metakey", "core_metadesc":"metadesc", "core_catid":"parent_id", "core_xreference":"null", "asset_id":"asset_id"}, "special":{"parent_id":"parent_id","lft":"lft","rgt":"rgt","level":"level","path":"path","extension":"extension","note":"note"}}', 'ContentHelperRoute::getCategoryRoute', '{"formFile":"administrator\\/components\\/com_categories\\/models\\/forms\\/category.xml", "hideFields":["asset_id","checked_out","checked_out_time","version","lft","rgt","level","path","extension"], "ignoreChanges":["modified_user_id", "modified_time", "checked_out", "checked_out_time", "version", "hits", "path"],"convertToInt":["publish_up", "publish_down"], "displayLookup":[{"sourceColumn":"created_user_id","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"access","targetTable":"#__viewlevels","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"modified_user_id","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"parent_id","targetTable":"#__categories","targetColumn":"id","displayColumn":"title"}]}'),
(6, 'Contact Category', 'com_contact.category', '{"special":{"dbtable":"#__categories","key":"id","type":"Category","prefix":"JTable","config":"array()"},"common":{"dbtable":"#__ucm_content","key":"ucm_id","type":"Corecontent","prefix":"JTable","config":"array()"}}', '', '{"common":{"core_content_item_id":"id","core_title":"title","core_state":"published","core_alias":"alias","core_created_time":"created_time","core_modified_time":"modified_time","core_body":"description", "core_hits":"hits","core_publish_up":"null","core_publish_down":"null","core_access":"access", "core_params":"params", "core_featured":"null", "core_metadata":"metadata", "core_language":"language", "core_images":"null", "core_urls":"null", "core_version":"version", "core_ordering":"null", "core_metakey":"metakey", "core_metadesc":"metadesc", "core_catid":"parent_id", "core_xreference":"null", "asset_id":"asset_id"}, "special":{"parent_id":"parent_id","lft":"lft","rgt":"rgt","level":"level","path":"path","extension":"extension","note":"note"}}', 'ContactHelperRoute::getCategoryRoute', '{"formFile":"administrator\\/components\\/com_categories\\/models\\/forms\\/category.xml", "hideFields":["asset_id","checked_out","checked_out_time","version","lft","rgt","level","path","extension"], "ignoreChanges":["modified_user_id", "modified_time", "checked_out", "checked_out_time", "version", "hits", "path"],"convertToInt":["publish_up", "publish_down"], "displayLookup":[{"sourceColumn":"created_user_id","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"access","targetTable":"#__viewlevels","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"modified_user_id","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"parent_id","targetTable":"#__categories","targetColumn":"id","displayColumn":"title"}]}'),
(7, 'Newsfeeds Category', 'com_newsfeeds.category', '{"special":{"dbtable":"#__categories","key":"id","type":"Category","prefix":"JTable","config":"array()"},"common":{"dbtable":"#__ucm_content","key":"ucm_id","type":"Corecontent","prefix":"JTable","config":"array()"}}', '', '{"common":{"core_content_item_id":"id","core_title":"title","core_state":"published","core_alias":"alias","core_created_time":"created_time","core_modified_time":"modified_time","core_body":"description", "core_hits":"hits","core_publish_up":"null","core_publish_down":"null","core_access":"access", "core_params":"params", "core_featured":"null", "core_metadata":"metadata", "core_language":"language", "core_images":"null", "core_urls":"null", "core_version":"version", "core_ordering":"null", "core_metakey":"metakey", "core_metadesc":"metadesc", "core_catid":"parent_id", "core_xreference":"null", "asset_id":"asset_id"}, "special":{"parent_id":"parent_id","lft":"lft","rgt":"rgt","level":"level","path":"path","extension":"extension","note":"note"}}', 'NewsfeedsHelperRoute::getCategoryRoute', '{"formFile":"administrator\\/components\\/com_categories\\/models\\/forms\\/category.xml", "hideFields":["asset_id","checked_out","checked_out_time","version","lft","rgt","level","path","extension"], "ignoreChanges":["modified_user_id", "modified_time", "checked_out", "checked_out_time", "version", "hits", "path"],"convertToInt":["publish_up", "publish_down"], "displayLookup":[{"sourceColumn":"created_user_id","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"access","targetTable":"#__viewlevels","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"modified_user_id","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"parent_id","targetTable":"#__categories","targetColumn":"id","displayColumn":"title"}]}'),
(8, 'Tag', 'com_tags.tag', '{"special":{"dbtable":"#__tags","key":"tag_id","type":"Tag","prefix":"TagsTable","config":"array()"},"common":{"dbtable":"#__ucm_content","key":"ucm_id","type":"Corecontent","prefix":"JTable","config":"array()"}}', '', '{"common":{"core_content_item_id":"id","core_title":"title","core_state":"published","core_alias":"alias","core_created_time":"created_time","core_modified_time":"modified_time","core_body":"description", "core_hits":"hits","core_publish_up":"null","core_publish_down":"null","core_access":"access", "core_params":"params", "core_featured":"featured", "core_metadata":"metadata", "core_language":"language", "core_images":"images", "core_urls":"urls", "core_version":"version", "core_ordering":"null", "core_metakey":"metakey", "core_metadesc":"metadesc", "core_catid":"null", "core_xreference":"null", "asset_id":"null"}, "special":{"parent_id":"parent_id","lft":"lft","rgt":"rgt","level":"level","path":"path"}}', 'TagsHelperRoute::getTagRoute', '{"formFile":"administrator\\/components\\/com_tags\\/models\\/forms\\/tag.xml", "hideFields":["checked_out","checked_out_time","version", "lft", "rgt", "level", "path", "urls", "publish_up", "publish_down"],"ignoreChanges":["modified_user_id", "modified_time", "checked_out", "checked_out_time", "version", "hits", "path"],"convertToInt":["publish_up", "publish_down"], "displayLookup":[{"sourceColumn":"created_user_id","targetTable":"#__users","targetColumn":"id","displayColumn":"name"}, {"sourceColumn":"access","targetTable":"#__viewlevels","targetColumn":"id","displayColumn":"title"}, {"sourceColumn":"modified_user_id","targetTable":"#__users","targetColumn":"id","displayColumn":"name"}]}'),
(9, 'Banner', 'com_banners.banner', '{"special":{"dbtable":"#__banners","key":"id","type":"Banner","prefix":"BannersTable","config":"array()"},"common":{"dbtable":"#__ucm_content","key":"ucm_id","type":"Corecontent","prefix":"JTable","config":"array()"}}', '', '{"common":{"core_content_item_id":"id","core_title":"name","core_state":"published","core_alias":"alias","core_created_time":"created","core_modified_time":"modified","core_body":"description", "core_hits":"null","core_publish_up":"publish_up","core_publish_down":"publish_down","core_access":"access", "core_params":"params", "core_featured":"null", "core_metadata":"metadata", "core_language":"language", "core_images":"images", "core_urls":"link", "core_version":"version", "core_ordering":"ordering", "core_metakey":"metakey", "core_metadesc":"metadesc", "core_catid":"catid", "core_xreference":"null", "asset_id":"null"}, "special":{"imptotal":"imptotal", "impmade":"impmade", "clicks":"clicks", "clickurl":"clickurl", "custombannercode":"custombannercode", "cid":"cid", "purchase_type":"purchase_type", "track_impressions":"track_impressions", "track_clicks":"track_clicks"}}', '', '{"formFile":"administrator\\/components\\/com_banners\\/models\\/forms\\/banner.xml", "hideFields":["checked_out","checked_out_time","version", "reset"],"ignoreChanges":["modified_by", "modified", "checked_out", "checked_out_time", "version", "imptotal", "impmade", "reset"], "convertToInt":["publish_up", "publish_down", "ordering"], "displayLookup":[{"sourceColumn":"catid","targetTable":"#__categories","targetColumn":"id","displayColumn":"title"}, {"sourceColumn":"cid","targetTable":"#__banner_clients","targetColumn":"id","displayColumn":"name"}, {"sourceColumn":"created_by","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"modified_by","targetTable":"#__users","targetColumn":"id","displayColumn":"name"} ]}'),
(10, 'Banners Category', 'com_banners.category', '{"special":{"dbtable":"#__categories","key":"id","type":"Category","prefix":"JTable","config":"array()"},"common":{"dbtable":"#__ucm_content","key":"ucm_id","type":"Corecontent","prefix":"JTable","config":"array()"}}', '', '{"common":{"core_content_item_id":"id","core_title":"title","core_state":"published","core_alias":"alias","core_created_time":"created_time","core_modified_time":"modified_time","core_body":"description", "core_hits":"hits","core_publish_up":"null","core_publish_down":"null","core_access":"access", "core_params":"params", "core_featured":"null", "core_metadata":"metadata", "core_language":"language", "core_images":"null", "core_urls":"null", "core_version":"version", "core_ordering":"null", "core_metakey":"metakey", "core_metadesc":"metadesc", "core_catid":"parent_id", "core_xreference":"null", "asset_id":"asset_id"}, "special": {"parent_id":"parent_id","lft":"lft","rgt":"rgt","level":"level","path":"path","extension":"extension","note":"note"}}', '', '{"formFile":"administrator\\/components\\/com_categories\\/models\\/forms\\/category.xml", "hideFields":["asset_id","checked_out","checked_out_time","version","lft","rgt","level","path","extension"], "ignoreChanges":["modified_user_id", "modified_time", "checked_out", "checked_out_time", "version", "hits", "path"], "convertToInt":["publish_up", "publish_down"], "displayLookup":[{"sourceColumn":"created_user_id","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"access","targetTable":"#__viewlevels","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"modified_user_id","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"parent_id","targetTable":"#__categories","targetColumn":"id","displayColumn":"title"}]}'),
(11, 'Banner Client', 'com_banners.client', '{"special":{"dbtable":"#__banner_clients","key":"id","type":"Client","prefix":"BannersTable"}}', '', '', '', '{"formFile":"administrator\\/components\\/com_banners\\/models\\/forms\\/client.xml", "hideFields":["checked_out","checked_out_time"], "ignoreChanges":["checked_out", "checked_out_time"], "convertToInt":[], "displayLookup":[]}'),
(12, 'User Notes', 'com_users.note', '{"special":{"dbtable":"#__user_notes","key":"id","type":"Note","prefix":"UsersTable"}}', '', '', '', '{"formFile":"administrator\\/components\\/com_users\\/models\\/forms\\/note.xml", "hideFields":["checked_out","checked_out_time", "publish_up", "publish_down"],"ignoreChanges":["modified_user_id", "modified_time", "checked_out", "checked_out_time"], "convertToInt":["publish_up", "publish_down"],"displayLookup":[{"sourceColumn":"catid","targetTable":"#__categories","targetColumn":"id","displayColumn":"title"}, {"sourceColumn":"created_user_id","targetTable":"#__users","targetColumn":"id","displayColumn":"name"}, {"sourceColumn":"user_id","targetTable":"#__users","targetColumn":"id","displayColumn":"name"}, {"sourceColumn":"modified_user_id","targetTable":"#__users","targetColumn":"id","displayColumn":"name"}]}'),
(13, 'User Notes Category', 'com_users.category', '{"special":{"dbtable":"#__categories","key":"id","type":"Category","prefix":"JTable","config":"array()"},"common":{"dbtable":"#__ucm_content","key":"ucm_id","type":"Corecontent","prefix":"JTable","config":"array()"}}', '', '{"common":{"core_content_item_id":"id","core_title":"title","core_state":"published","core_alias":"alias","core_created_time":"created_time","core_modified_time":"modified_time","core_body":"description", "core_hits":"hits","core_publish_up":"null","core_publish_down":"null","core_access":"access", "core_params":"params", "core_featured":"null", "core_metadata":"metadata", "core_language":"language", "core_images":"null", "core_urls":"null", "core_version":"version", "core_ordering":"null", "core_metakey":"metakey", "core_metadesc":"metadesc", "core_catid":"parent_id", "core_xreference":"null", "asset_id":"asset_id"}, "special":{"parent_id":"parent_id","lft":"lft","rgt":"rgt","level":"level","path":"path","extension":"extension","note":"note"}}', '', '{"formFile":"administrator\\/components\\/com_categories\\/models\\/forms\\/category.xml", "hideFields":["checked_out","checked_out_time","version","lft","rgt","level","path","extension"], "ignoreChanges":["modified_user_id", "modified_time", "checked_out", "checked_out_time", "version", "hits", "path"], "convertToInt":["publish_up", "publish_down"], "displayLookup":[{"sourceColumn":"created_user_id","targetTable":"#__users","targetColumn":"id","displayColumn":"name"}, {"sourceColumn":"access","targetTable":"#__viewlevels","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"modified_user_id","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"parent_id","targetTable":"#__categories","targetColumn":"id","displayColumn":"title"}]}');

-- --------------------------------------------------------

--
-- Table structure for table `#__core_log_searches`
--

DROP TABLE IF EXISTS `#__core_log_searches`;
CREATE TABLE IF NOT EXISTS `#__core_log_searches` (
  `search_term` varchar(128) NOT NULL DEFAULT '',
  `hits` int(10) unsigned NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `#__extensions`
--

DROP TABLE IF EXISTS `#__extensions`;
CREATE TABLE IF NOT EXISTS `#__extensions` (
  `extension_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `type` varchar(20) NOT NULL,
  `element` varchar(100) NOT NULL,
  `folder` varchar(100) NOT NULL,
  `client_id` tinyint(3) NOT NULL,
  `enabled` tinyint(3) NOT NULL DEFAULT '1',
  `access` int(10) unsigned NOT NULL DEFAULT '1',
  `protected` tinyint(3) NOT NULL DEFAULT '0',
  `manifest_cache` text NOT NULL,
  `params` text NOT NULL,
  `custom_data` text NOT NULL,
  `system_data` text NOT NULL,
  `checked_out` int(10) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ordering` int(11) DEFAULT '0',
  `state` int(11) DEFAULT '0',
  PRIMARY KEY (`extension_id`),
  KEY `element_clientid` (`element`,`client_id`),
  KEY `element_folder_clientid` (`element`,`folder`,`client_id`),
  KEY `extension` (`type`,`element`,`folder`,`client_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10187 ;

--
-- Dumping data for table `#__extensions`
--

INSERT INTO `#__extensions` (`extension_id`, `name`, `type`, `element`, `folder`, `client_id`, `enabled`, `access`, `protected`, `manifest_cache`, `params`, `custom_data`, `system_data`, `checked_out`, `checked_out_time`, `ordering`, `state`) VALUES
(1, 'com_mailto', 'component', 'com_mailto', '', 0, 1, 1, 1, '{"name":"com_mailto","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.\\t","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_MAILTO_XML_DESCRIPTION","group":"","filename":"mailto"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(2, 'com_wrapper', 'component', 'com_wrapper', '', 0, 1, 1, 1, '{"name":"com_wrapper","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.\\n\\t","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_WRAPPER_XML_DESCRIPTION","group":"","filename":"wrapper"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(3, 'com_admin', 'component', 'com_admin', '', 1, 1, 1, 1, '{"name":"com_admin","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_ADMIN_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(4, 'com_banners', 'component', 'com_banners', '', 1, 1, 1, 0, '{"name":"com_banners","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_BANNERS_XML_DESCRIPTION","group":"","filename":"banners"}', '{"purchase_type":"3","track_impressions":"0","track_clicks":"0","metakey_prefix":"","save_history":"1","history_limit":10}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(5, 'com_cache', 'component', 'com_cache', '', 1, 1, 1, 1, '{"name":"com_cache","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_CACHE_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(6, 'com_categories', 'component', 'com_categories', '', 1, 1, 1, 1, '{"name":"com_categories","type":"component","creationDate":"December 2007","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_CATEGORIES_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(7, 'com_checkin', 'component', 'com_checkin', '', 1, 1, 1, 1, '{"name":"com_checkin","type":"component","creationDate":"Unknown","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_CHECKIN_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(8, 'com_contact', 'component', 'com_contact', '', 1, 1, 1, 0, '{"name":"com_contact","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_CONTACT_XML_DESCRIPTION","group":"","filename":"contact"}', '{"show_contact_category":"hide","save_history":"1","history_limit":10,"show_contact_list":"0","presentation_style":"sliders","show_name":"1","show_position":"1","show_email":"0","show_street_address":"1","show_suburb":"1","show_state":"1","show_postcode":"1","show_country":"1","show_telephone":"1","show_mobile":"1","show_fax":"1","show_webpage":"1","show_misc":"1","show_image":"1","image":"","allow_vcard":"0","show_articles":"0","show_profile":"0","show_links":"0","linka_name":"","linkb_name":"","linkc_name":"","linkd_name":"","linke_name":"","contact_icons":"0","icon_address":"","icon_email":"","icon_telephone":"","icon_mobile":"","icon_fax":"","icon_misc":"","show_headings":"1","show_position_headings":"1","show_email_headings":"0","show_telephone_headings":"1","show_mobile_headings":"0","show_fax_headings":"0","allow_vcard_headings":"0","show_suburb_headings":"1","show_state_headings":"1","show_country_headings":"1","show_email_form":"1","show_email_copy":"1","banned_email":"","banned_subject":"","banned_text":"","validate_session":"1","custom_reply":"0","redirect":"","show_category_crumb":"0","metakey":"","metadesc":"","robots":"","author":"","rights":"","xreference":""}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(9, 'com_cpanel', 'component', 'com_cpanel', '', 1, 1, 1, 1, '{"name":"com_cpanel","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_CPANEL_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10, 'com_installer', 'component', 'com_installer', '', 1, 1, 1, 1, '{"name":"com_installer","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_INSTALLER_XML_DESCRIPTION","group":""}', '{"show_jed_info":"0","cachetimeout":"6","minimum_stability":"4"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(11, 'com_languages', 'component', 'com_languages', '', 1, 1, 1, 1, '{"name":"com_languages","type":"component","creationDate":"2006","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_LANGUAGES_XML_DESCRIPTION","group":""}', '{"administrator":"en-GB","site":"en-GB"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(12, 'com_login', 'component', 'com_login', '', 1, 1, 1, 1, '{"name":"com_login","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_LOGIN_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(13, 'com_media', 'component', 'com_media', '', 1, 1, 0, 1, '{"name":"com_media","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_MEDIA_XML_DESCRIPTION","group":"","filename":"media"}', '{"upload_extensions":"bmp,csv,doc,gif,ico,jpg,jpeg,odg,odp,ods,odt,pdf,png,ppt,swf,txt,xcf,xls,BMP,CSV,DOC,GIF,ICO,JPG,JPEG,ODG,ODP,ODS,ODT,PDF,PNG,PPT,SWF,TXT,XCF,XLS","upload_maxsize":"10","file_path":"images","image_path":"images","restrict_uploads":"1","allowed_media_usergroup":"3","check_mime":"1","image_extensions":"bmp,gif,jpg,png","ignore_extensions":"","upload_mime":"image\\/jpeg,image\\/gif,image\\/png,image\\/bmp,application\\/x-shockwave-flash,application\\/msword,application\\/excel,application\\/pdf,application\\/powerpoint,text\\/plain,application\\/x-zip","upload_mime_illegal":"text\\/html"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(14, 'com_menus', 'component', 'com_menus', '', 1, 1, 1, 1, '{"name":"com_menus","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_MENUS_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(15, 'com_messages', 'component', 'com_messages', '', 1, 1, 1, 1, '{"name":"com_messages","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_MESSAGES_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(16, 'com_modules', 'component', 'com_modules', '', 1, 1, 1, 1, '{"name":"com_modules","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_MODULES_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(17, 'com_newsfeeds', 'component', 'com_newsfeeds', '', 1, 1, 1, 0, '{"name":"com_newsfeeds","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_NEWSFEEDS_XML_DESCRIPTION","group":"","filename":"newsfeeds"}', '{"newsfeed_layout":"_:default","save_history":"1","history_limit":5,"show_feed_image":"1","show_feed_description":"1","show_item_description":"1","feed_character_count":"0","feed_display_order":"des","float_first":"right","float_second":"right","show_tags":"1","category_layout":"_:default","show_category_title":"1","show_description":"1","show_description_image":"1","maxLevel":"-1","show_empty_categories":"0","show_subcat_desc":"1","show_cat_items":"1","show_cat_tags":"1","show_base_description":"1","maxLevelcat":"-1","show_empty_categories_cat":"0","show_subcat_desc_cat":"1","show_cat_items_cat":"1","filter_field":"1","show_pagination_limit":"1","show_headings":"1","show_articles":"0","show_link":"1","show_pagination":"1","show_pagination_results":"1"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(18, 'com_plugins', 'component', 'com_plugins', '', 1, 1, 1, 1, '{"name":"com_plugins","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_PLUGINS_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(19, 'com_search', 'component', 'com_search', '', 1, 1, 1, 0, '{"name":"com_search","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_SEARCH_XML_DESCRIPTION","group":"","filename":"search"}', '{"enabled":"0","show_date":"1"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(20, 'com_templates', 'component', 'com_templates', '', 1, 1, 1, 1, '{"name":"com_templates","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_TEMPLATES_XML_DESCRIPTION","group":""}', '{"template_positions_display":"0","upload_limit":"2","image_formats":"gif,bmp,jpg,jpeg,png","source_formats":"txt,less,ini,xml,js,php,css","font_formats":"woff,ttf,otf","compressed_formats":"zip"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(22, 'com_content', 'component', 'com_content', '', 1, 1, 0, 1, '{"name":"com_content","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_CONTENT_XML_DESCRIPTION","group":"","filename":"content"}', '{"article_layout":"_:default","show_title":"1","link_titles":"1","show_intro":"1","info_block_position":"2","show_category":"0","link_category":"0","show_parent_category":"0","link_parent_category":"0","show_author":"0","link_author":"0","show_create_date":"0","show_modify_date":"0","show_publish_date":"0","show_item_navigation":"1","show_vote":"0","show_readmore":"1","show_readmore_title":"0","readmore_limit":"100","show_tags":"1","show_icons":"0","show_print_icon":"0","show_email_icon":"0","show_hits":"0","show_noauth":"0","urls_position":"0","show_publishing_options":"1","show_article_options":"1","save_history":"1","history_limit":10,"show_urls_images_frontend":"0","show_urls_images_backend":"1","targeta":0,"targetb":0,"targetc":0,"float_intro":"none","float_fulltext":"none","category_layout":"_:blog","show_category_heading_title_text":"0","show_category_title":"0","show_description":"0","show_description_image":"0","maxLevel":"0","show_empty_categories":"0","show_no_articles":"0","show_subcat_desc":"0","show_cat_num_articles":"0","show_cat_tags":"0","show_base_description":"0","maxLevelcat":"-1","show_empty_categories_cat":"0","show_subcat_desc_cat":"0","show_cat_num_articles_cat":"0","num_leading_articles":"1","num_intro_articles":"2","num_columns":"2","num_links":"1","multi_column_order":"0","show_subcategory_content":"0","show_pagination_limit":"1","filter_field":"hide","show_headings":"1","list_show_date":"0","date_format":"","list_show_hits":"1","list_show_author":"1","orderby_pri":"order","orderby_sec":"order","order_date":"published","show_pagination":"2","show_pagination_results":"1","show_featured":"show","show_feed_link":"1","feed_summary":"0","feed_show_readmore":"0"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(23, 'com_config', 'component', 'com_config', '', 1, 1, 0, 1, '{"name":"com_config","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_CONFIG_XML_DESCRIPTION","group":""}', '{"filters":{"1":{"filter_type":"NH","filter_tags":"","filter_attributes":""},"9":{"filter_type":"BL","filter_tags":"","filter_attributes":""},"6":{"filter_type":"BL","filter_tags":"","filter_attributes":""},"7":{"filter_type":"NONE","filter_tags":"","filter_attributes":""},"2":{"filter_type":"NH","filter_tags":"","filter_attributes":""},"3":{"filter_type":"BL","filter_tags":"","filter_attributes":""},"4":{"filter_type":"BL","filter_tags":"","filter_attributes":""},"5":{"filter_type":"BL","filter_tags":"","filter_attributes":""},"8":{"filter_type":"NONE","filter_tags":"","filter_attributes":""}}}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(24, 'com_redirect', 'component', 'com_redirect', '', 1, 1, 0, 1, '{"name":"com_redirect","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_REDIRECT_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(25, 'com_users', 'component', 'com_users', '', 1, 1, 0, 1, '{"name":"com_users","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_USERS_XML_DESCRIPTION","group":"","filename":"users"}', '{"allowUserRegistration":"0","new_usertype":"2","guest_usergroup":"9","sendpassword":"1","useractivation":"1","mail_to_admin":"0","captcha":"","frontend_userparams":"1","site_language":"0","change_login_name":"0","reset_count":"10","reset_time":"1","minimum_length":"4","minimum_integers":"0","minimum_symbols":"0","minimum_uppercase":"0","save_history":"1","history_limit":5,"mailSubjectPrefix":"","mailBodySuffix":""}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(27, 'com_finder', 'component', 'com_finder', '', 1, 1, 0, 0, '{"name":"com_finder","type":"component","creationDate":"August 2011","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_FINDER_XML_DESCRIPTION","group":"","filename":"finder"}', '{"enabled":"0","show_description":"1","description_length":255,"allow_empty_query":"0","show_url":"1","show_autosuggest":"1","show_suggested_query":"1","show_explained_query":"1","show_advanced":"1","show_advanced_tips":"1","expand_advanced":"0","show_date_filters":"1","sort_order":"relevance","sort_direction":"desc","highlight_terms":"1","opensearch_name":"","opensearch_description":"","batch_size":"50","memory_table_limit":30000,"title_multiplier":"1.7","text_multiplier":"0.7","meta_multiplier":"1.2","path_multiplier":"2.0","misc_multiplier":"0.3","stem":"1","stemmer":"snowball","enable_logging":"0"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(28, 'com_joomlaupdate', 'component', 'com_joomlaupdate', '', 1, 1, 0, 1, '{"name":"com_joomlaupdate","type":"component","creationDate":"February 2012","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_JOOMLAUPDATE_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(29, 'com_tags', 'component', 'com_tags', '', 1, 1, 1, 1, '{"name":"com_tags","type":"component","creationDate":"December 2013","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.1.0","description":"COM_TAGS_XML_DESCRIPTION","group":"","filename":"tags"}', '{"tag_layout":"_:default","save_history":"1","history_limit":5,"show_tag_title":"0","tag_list_show_tag_image":"0","tag_list_show_tag_description":"0","tag_list_image":"","show_tag_num_items":"0","tag_list_orderby":"title","tag_list_orderby_direction":"ASC","show_headings":"0","tag_list_show_date":"0","tag_list_show_item_image":"0","tag_list_show_item_description":"0","tag_list_item_maximum_characters":0,"return_any_or_all":"1","include_children":"0","maximum":200,"tag_list_language_filter":"all","tags_layout":"_:default","all_tags_orderby":"title","all_tags_orderby_direction":"ASC","all_tags_show_tag_image":"0","all_tags_show_tag_descripion":"0","all_tags_tag_maximum_characters":20,"all_tags_show_tag_hits":"0","filter_field":"1","show_pagination_limit":"1","show_pagination":"2","show_pagination_results":"1","tag_field_ajax_mode":"1","show_feed_link":"1"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(30, 'com_contenthistory', 'component', 'com_contenthistory', '', 1, 1, 1, 0, '{"name":"com_contenthistory","type":"component","creationDate":"May 2013","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.2.0","description":"COM_CONTENTHISTORY_XML_DESCRIPTION","group":"","filename":"contenthistory"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(31, 'com_ajax', 'component', 'com_ajax', '', 1, 1, 1, 0, '{"name":"com_ajax","type":"component","creationDate":"August 2013","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.2.0","description":"COM_AJAX_XML_DESCRIPTION","group":"","filename":"ajax"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(32, 'com_postinstall', 'component', 'com_postinstall', '', 1, 1, 1, 1, '{"name":"com_postinstall","type":"component","creationDate":"September 2013","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.2.0","description":"COM_POSTINSTALL_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(101, 'SimplePie', 'library', 'simplepie', '', 0, 1, 1, 1, '{"name":"SimplePie","type":"library","creationDate":"2004","author":"SimplePie","copyright":"Copyright (c) 2004-2009, Ryan Parman and Geoffrey Sneddon","authorEmail":"","authorUrl":"http:\\/\\/simplepie.org\\/","version":"1.2","description":"LIB_SIMPLEPIE_XML_DESCRIPTION","group":"","filename":"simplepie"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(102, 'phputf8', 'library', 'phputf8', '', 0, 1, 1, 1, '{"name":"phputf8","type":"library","creationDate":"2006","author":"Harry Fuecks","copyright":"Copyright various authors","authorEmail":"hfuecks@gmail.com","authorUrl":"http:\\/\\/sourceforge.net\\/projects\\/phputf8","version":"0.5","description":"LIB_PHPUTF8_XML_DESCRIPTION","group":"","filename":"phputf8"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(103, 'Joomla! Platform', 'library', 'joomla', '', 0, 1, 1, 1, '{"name":"Joomla! Platform","type":"library","creationDate":"2008","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"http:\\/\\/www.joomla.org","version":"13.1","description":"LIB_JOOMLA_XML_DESCRIPTION","group":"","filename":"joomla"}', '{"mediaversion":"e62271400c3fe719e2212a3a1613619d"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(104, 'IDNA Convert', 'library', 'idna_convert', '', 0, 1, 1, 1, '{"name":"IDNA Convert","type":"library","creationDate":"2004","author":"phlyLabs","copyright":"2004-2011 phlyLabs Berlin, http:\\/\\/phlylabs.de","authorEmail":"phlymail@phlylabs.de","authorUrl":"http:\\/\\/phlylabs.de","version":"0.8.0","description":"LIB_IDNA_XML_DESCRIPTION","group":"","filename":"idna_convert"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(105, 'FOF', 'library', 'fof', '', 0, 1, 1, 1, '{"name":"FOF","type":"library","creationDate":"2015-04-22 13:15:32","author":"Nicholas K. Dionysopoulos \\/ Akeeba Ltd","copyright":"(C)2011-2015 Nicholas K. Dionysopoulos","authorEmail":"nicholas@akeebabackup.com","authorUrl":"https:\\/\\/www.akeebabackup.com","version":"2.4.3","description":"LIB_FOF_XML_DESCRIPTION","group":"","filename":"fof"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(106, 'PHPass', 'library', 'phpass', '', 0, 1, 1, 1, '{"name":"PHPass","type":"library","creationDate":"2004-2006","author":"Solar Designer","copyright":"","authorEmail":"solar@openwall.com","authorUrl":"http:\\/\\/www.openwall.com\\/phpass\\/","version":"0.3","description":"LIB_PHPASS_XML_DESCRIPTION","group":"","filename":"phpass"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(200, 'mod_articles_archive', 'module', 'mod_articles_archive', '', 0, 1, 1, 0, '{"name":"mod_articles_archive","type":"module","creationDate":"July 2006","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_ARTICLES_ARCHIVE_XML_DESCRIPTION","group":"","filename":"mod_articles_archive"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(201, 'mod_articles_latest', 'module', 'mod_articles_latest', '', 0, 1, 1, 0, '{"name":"mod_articles_latest","type":"module","creationDate":"July 2004","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_LATEST_NEWS_XML_DESCRIPTION","group":"","filename":"mod_articles_latest"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(202, 'mod_articles_popular', 'module', 'mod_articles_popular', '', 0, 1, 1, 0, '{"name":"mod_articles_popular","type":"module","creationDate":"July 2006","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_POPULAR_XML_DESCRIPTION","group":"","filename":"mod_articles_popular"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(203, 'mod_banners', 'module', 'mod_banners', '', 0, 1, 1, 0, '{"name":"mod_banners","type":"module","creationDate":"July 2006","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_BANNERS_XML_DESCRIPTION","group":"","filename":"mod_banners"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(204, 'mod_breadcrumbs', 'module', 'mod_breadcrumbs', '', 0, 1, 1, 1, '{"name":"mod_breadcrumbs","type":"module","creationDate":"July 2006","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_BREADCRUMBS_XML_DESCRIPTION","group":"","filename":"mod_breadcrumbs"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(205, 'mod_custom', 'module', 'mod_custom', '', 0, 1, 1, 1, '{"name":"mod_custom","type":"module","creationDate":"July 2004","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_CUSTOM_XML_DESCRIPTION","group":"","filename":"mod_custom"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(206, 'mod_feed', 'module', 'mod_feed', '', 0, 1, 1, 0, '{"name":"mod_feed","type":"module","creationDate":"July 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_FEED_XML_DESCRIPTION","group":"","filename":"mod_feed"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(207, 'mod_footer', 'module', 'mod_footer', '', 0, 1, 1, 0, '{"name":"mod_footer","type":"module","creationDate":"July 2006","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_FOOTER_XML_DESCRIPTION","group":"","filename":"mod_footer"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(208, 'mod_login', 'module', 'mod_login', '', 0, 1, 1, 1, '{"name":"mod_login","type":"module","creationDate":"July 2006","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_LOGIN_XML_DESCRIPTION","group":"","filename":"mod_login"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(209, 'mod_menu', 'module', 'mod_menu', '', 0, 1, 1, 1, '{"name":"mod_menu","type":"module","creationDate":"July 2004","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_MENU_XML_DESCRIPTION","group":"","filename":"mod_menu"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(210, 'mod_articles_news', 'module', 'mod_articles_news', '', 0, 1, 1, 0, '{"name":"mod_articles_news","type":"module","creationDate":"July 2006","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_ARTICLES_NEWS_XML_DESCRIPTION","group":"","filename":"mod_articles_news"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(211, 'mod_random_image', 'module', 'mod_random_image', '', 0, 1, 1, 0, '{"name":"mod_random_image","type":"module","creationDate":"July 2006","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_RANDOM_IMAGE_XML_DESCRIPTION","group":"","filename":"mod_random_image"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(212, 'mod_related_items', 'module', 'mod_related_items', '', 0, 1, 1, 0, '{"name":"mod_related_items","type":"module","creationDate":"July 2004","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_RELATED_XML_DESCRIPTION","group":"","filename":"mod_related_items"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(213, 'mod_search', 'module', 'mod_search', '', 0, 1, 1, 0, '{"name":"mod_search","type":"module","creationDate":"July 2004","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_SEARCH_XML_DESCRIPTION","group":"","filename":"mod_search"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(214, 'mod_stats', 'module', 'mod_stats', '', 0, 1, 1, 0, '{"name":"mod_stats","type":"module","creationDate":"July 2004","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_STATS_XML_DESCRIPTION","group":"","filename":"mod_stats"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(215, 'mod_syndicate', 'module', 'mod_syndicate', '', 0, 1, 1, 1, '{"name":"mod_syndicate","type":"module","creationDate":"May 2006","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_SYNDICATE_XML_DESCRIPTION","group":"","filename":"mod_syndicate"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(216, 'mod_users_latest', 'module', 'mod_users_latest', '', 0, 1, 1, 0, '{"name":"mod_users_latest","type":"module","creationDate":"December 2009","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_USERS_LATEST_XML_DESCRIPTION","group":"","filename":"mod_users_latest"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(218, 'mod_whosonline', 'module', 'mod_whosonline', '', 0, 1, 1, 0, '{"name":"mod_whosonline","type":"module","creationDate":"July 2004","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_WHOSONLINE_XML_DESCRIPTION","group":"","filename":"mod_whosonline"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(219, 'mod_wrapper', 'module', 'mod_wrapper', '', 0, 1, 1, 0, '{"name":"mod_wrapper","type":"module","creationDate":"October 2004","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_WRAPPER_XML_DESCRIPTION","group":"","filename":"mod_wrapper"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(220, 'mod_articles_category', 'module', 'mod_articles_category', '', 0, 1, 1, 0, '{"name":"mod_articles_category","type":"module","creationDate":"February 2010","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_ARTICLES_CATEGORY_XML_DESCRIPTION","group":"","filename":"mod_articles_category"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(221, 'mod_articles_categories', 'module', 'mod_articles_categories', '', 0, 1, 1, 0, '{"name":"mod_articles_categories","type":"module","creationDate":"February 2010","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_ARTICLES_CATEGORIES_XML_DESCRIPTION","group":"","filename":"mod_articles_categories"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(222, 'mod_languages', 'module', 'mod_languages', '', 0, 1, 1, 1, '{"name":"mod_languages","type":"module","creationDate":"February 2010","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_LANGUAGES_XML_DESCRIPTION","group":"","filename":"mod_languages"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(223, 'mod_finder', 'module', 'mod_finder', '', 0, 1, 0, 0, '{"name":"mod_finder","type":"module","creationDate":"August 2011","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_FINDER_XML_DESCRIPTION","group":"","filename":"mod_finder"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(300, 'mod_custom', 'module', 'mod_custom', '', 1, 1, 1, 1, '{"name":"mod_custom","type":"module","creationDate":"July 2004","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_CUSTOM_XML_DESCRIPTION","group":"","filename":"mod_custom"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(301, 'mod_feed', 'module', 'mod_feed', '', 1, 1, 1, 0, '{"name":"mod_feed","type":"module","creationDate":"July 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_FEED_XML_DESCRIPTION","group":"","filename":"mod_feed"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(302, 'mod_latest', 'module', 'mod_latest', '', 1, 1, 1, 0, '{"name":"mod_latest","type":"module","creationDate":"July 2004","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_LATEST_XML_DESCRIPTION","group":"","filename":"mod_latest"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(303, 'mod_logged', 'module', 'mod_logged', '', 1, 1, 1, 0, '{"name":"mod_logged","type":"module","creationDate":"January 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_LOGGED_XML_DESCRIPTION","group":"","filename":"mod_logged"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(304, 'mod_login', 'module', 'mod_login', '', 1, 1, 1, 1, '{"name":"mod_login","type":"module","creationDate":"March 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_LOGIN_XML_DESCRIPTION","group":"","filename":"mod_login"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(305, 'mod_menu', 'module', 'mod_menu', '', 1, 1, 1, 0, '{"name":"mod_menu","type":"module","creationDate":"March 2006","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_MENU_XML_DESCRIPTION","group":"","filename":"mod_menu"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(307, 'mod_popular', 'module', 'mod_popular', '', 1, 1, 1, 0, '{"name":"mod_popular","type":"module","creationDate":"July 2004","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_POPULAR_XML_DESCRIPTION","group":"","filename":"mod_popular"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(308, 'mod_quickicon', 'module', 'mod_quickicon', '', 1, 1, 1, 1, '{"name":"mod_quickicon","type":"module","creationDate":"Nov 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_QUICKICON_XML_DESCRIPTION","group":"","filename":"mod_quickicon"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(309, 'mod_status', 'module', 'mod_status', '', 1, 1, 1, 0, '{"name":"mod_status","type":"module","creationDate":"Feb 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_STATUS_XML_DESCRIPTION","group":"","filename":"mod_status"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(310, 'mod_submenu', 'module', 'mod_submenu', '', 1, 1, 1, 0, '{"name":"mod_submenu","type":"module","creationDate":"Feb 2006","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_SUBMENU_XML_DESCRIPTION","group":"","filename":"mod_submenu"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(311, 'mod_title', 'module', 'mod_title', '', 1, 1, 1, 0, '{"name":"mod_title","type":"module","creationDate":"Nov 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_TITLE_XML_DESCRIPTION","group":"","filename":"mod_title"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(312, 'mod_toolbar', 'module', 'mod_toolbar', '', 1, 1, 1, 1, '{"name":"mod_toolbar","type":"module","creationDate":"Nov 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_TOOLBAR_XML_DESCRIPTION","group":"","filename":"mod_toolbar"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(313, 'mod_multilangstatus', 'module', 'mod_multilangstatus', '', 1, 1, 1, 0, '{"name":"mod_multilangstatus","type":"module","creationDate":"September 2011","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_MULTILANGSTATUS_XML_DESCRIPTION","group":"","filename":"mod_multilangstatus"}', '{"cache":"0"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(314, 'mod_version', 'module', 'mod_version', '', 1, 1, 1, 0, '{"name":"mod_version","type":"module","creationDate":"January 2012","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_VERSION_XML_DESCRIPTION","group":"","filename":"mod_version"}', '{"format":"short","product":"1","cache":"0"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(315, 'mod_stats_admin', 'module', 'mod_stats_admin', '', 1, 1, 1, 0, '{"name":"mod_stats_admin","type":"module","creationDate":"July 2004","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_STATS_XML_DESCRIPTION","group":"","filename":"mod_stats_admin"}', '{"serverinfo":"0","siteinfo":"0","counter":"0","increase":"0","cache":"1","cache_time":"900","cachemode":"static"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(316, 'mod_tags_popular', 'module', 'mod_tags_popular', '', 0, 1, 1, 0, '{"name":"mod_tags_popular","type":"module","creationDate":"January 2013","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.1.0","description":"MOD_TAGS_POPULAR_XML_DESCRIPTION","group":"","filename":"mod_tags_popular"}', '{"maximum":"5","timeframe":"alltime","owncache":"1"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(317, 'mod_tags_similar', 'module', 'mod_tags_similar', '', 0, 1, 1, 0, '{"name":"mod_tags_similar","type":"module","creationDate":"January 2013","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.1.0","description":"MOD_TAGS_SIMILAR_XML_DESCRIPTION","group":"","filename":"mod_tags_similar"}', '{"maximum":"5","matchtype":"any","owncache":"1"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(400, 'plg_authentication_gmail', 'plugin', 'gmail', 'authentication', 0, 0, 1, 0, '{"name":"plg_authentication_gmail","type":"plugin","creationDate":"February 2006","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_GMAIL_XML_DESCRIPTION","group":"","filename":"gmail"}', '{"applysuffix":"0","suffix":"","verifypeer":"1","user_blacklist":""}', '', '', 0, '0000-00-00 00:00:00', 1, 0),
(401, 'plg_authentication_joomla', 'plugin', 'joomla', 'authentication', 0, 1, 1, 1, '{"name":"plg_authentication_joomla","type":"plugin","creationDate":"November 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_AUTH_JOOMLA_XML_DESCRIPTION","group":"","filename":"joomla"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(402, 'plg_authentication_ldap', 'plugin', 'ldap', 'authentication', 0, 0, 1, 0, '{"name":"plg_authentication_ldap","type":"plugin","creationDate":"November 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_LDAP_XML_DESCRIPTION","group":"","filename":"ldap"}', '{"host":"","port":"389","use_ldapV3":"0","negotiate_tls":"0","no_referrals":"0","auth_method":"bind","base_dn":"","search_string":"","users_dn":"","username":"admin","password":"bobby7","ldap_fullname":"fullName","ldap_email":"mail","ldap_uid":"uid"}', '', '', 0, '0000-00-00 00:00:00', 3, 0),
(403, 'plg_content_contact', 'plugin', 'contact', 'content', 0, 1, 1, 0, '{"name":"plg_content_contact","type":"plugin","creationDate":"January 2014","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.2.2","description":"PLG_CONTENT_CONTACT_XML_DESCRIPTION","group":"","filename":"contact"}', '', '', '', 0, '0000-00-00 00:00:00', 1, 0),
(404, 'plg_content_emailcloak', 'plugin', 'emailcloak', 'content', 0, 1, 1, 0, '{"name":"plg_content_emailcloak","type":"plugin","creationDate":"November 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_CONTENT_EMAILCLOAK_XML_DESCRIPTION","group":"","filename":"emailcloak"}', '{"mode":"1"}', '', '', 0, '0000-00-00 00:00:00', 1, 0),
(406, 'plg_content_loadmodule', 'plugin', 'loadmodule', 'content', 0, 1, 1, 0, '{"name":"plg_content_loadmodule","type":"plugin","creationDate":"November 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_LOADMODULE_XML_DESCRIPTION","group":"","filename":"loadmodule"}', '{"style":"xhtml"}', '', '', 0, '2011-09-18 15:22:50', 0, 0),
(407, 'plg_content_pagebreak', 'plugin', 'pagebreak', 'content', 0, 1, 1, 0, '{"name":"plg_content_pagebreak","type":"plugin","creationDate":"November 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_CONTENT_PAGEBREAK_XML_DESCRIPTION","group":"","filename":"pagebreak"}', '{"title":"1","multipage_toc":"1","showall":"1"}', '', '', 0, '0000-00-00 00:00:00', 4, 0),
(408, 'plg_content_pagenavigation', 'plugin', 'pagenavigation', 'content', 0, 1, 1, 0, '{"name":"plg_content_pagenavigation","type":"plugin","creationDate":"January 2006","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_PAGENAVIGATION_XML_DESCRIPTION","group":"","filename":"pagenavigation"}', '{"position":"1"}', '', '', 0, '0000-00-00 00:00:00', 5, 0),
(409, 'plg_content_vote', 'plugin', 'vote', 'content', 0, 1, 1, 0, '{"name":"plg_content_vote","type":"plugin","creationDate":"November 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_VOTE_XML_DESCRIPTION","group":"","filename":"vote"}', '', '', '', 0, '0000-00-00 00:00:00', 6, 0),
(410, 'plg_editors_codemirror', 'plugin', 'codemirror', 'editors', 0, 1, 1, 1, '{"name":"plg_editors_codemirror","type":"plugin","creationDate":"28 March 2011","author":"Marijn Haverbeke","copyright":"Copyright (C) 2014 by Marijn Haverbeke <marijnh@gmail.com> and others","authorEmail":"marijnh@gmail.com","authorUrl":"http:\\/\\/codemirror.net\\/","version":"5.6","description":"PLG_CODEMIRROR_XML_DESCRIPTION","group":"","filename":"codemirror"}', '{"lineNumbers":"1","lineWrapping":"1","matchTags":"1","matchBrackets":"1","marker-gutter":"1","autoCloseTags":"1","autoCloseBrackets":"1","autoFocus":"1","theme":"default","tabmode":"indent"}', '', '', 0, '0000-00-00 00:00:00', 1, 0),
(411, 'plg_editors_none', 'plugin', 'none', 'editors', 0, 1, 1, 1, '{"name":"plg_editors_none","type":"plugin","creationDate":"September 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_NONE_XML_DESCRIPTION","group":"","filename":"none"}', '', '', '', 0, '0000-00-00 00:00:00', 2, 0),
(412, 'plg_editors_tinymce', 'plugin', 'tinymce', 'editors', 0, 1, 1, 0, '{"name":"plg_editors_tinymce","type":"plugin","creationDate":"2005-2014","author":"Moxiecode Systems AB","copyright":"Moxiecode Systems AB","authorEmail":"N\\/A","authorUrl":"tinymce.moxiecode.com","version":"4.1.7","description":"PLG_TINY_XML_DESCRIPTION","group":"","filename":"tinymce"}', '{"mode":"1","skin":"0","mobile":"0","entity_encoding":"raw","lang_mode":"1","text_direction":"ltr","content_css":"1","content_css_custom":"","relative_urls":"1","newlines":"0","invalid_elements":"script,applet,iframe","extended_elements":"","html_height":"550","html_width":"750","resizing":"1","element_path":"1","fonts":"1","paste":"1","searchreplace":"1","insertdate":"1","colors":"1","table":"1","smilies":"1","hr":"1","link":"1","media":"1","print":"1","directionality":"1","fullscreen":"1","alignment":"1","visualchars":"1","visualblocks":"1","nonbreaking":"1","template":"1","blockquote":"1","wordcount":"1","advlist":"1","autosave":"1","contextmenu":"1","inlinepopups":"1","custom_plugin":"","custom_button":""}', '', '', 0, '0000-00-00 00:00:00', 3, 0),
(413, 'plg_editors-xtd_article', 'plugin', 'article', 'editors-xtd', 0, 1, 1, 0, '{"name":"plg_editors-xtd_article","type":"plugin","creationDate":"October 2009","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_ARTICLE_XML_DESCRIPTION","group":"","filename":"article"}', '', '', '', 0, '0000-00-00 00:00:00', 1, 0),
(414, 'plg_editors-xtd_image', 'plugin', 'image', 'editors-xtd', 0, 1, 1, 0, '{"name":"plg_editors-xtd_image","type":"plugin","creationDate":"August 2004","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_IMAGE_XML_DESCRIPTION","group":"","filename":"image"}', '', '', '', 0, '0000-00-00 00:00:00', 2, 0),
(415, 'plg_editors-xtd_pagebreak', 'plugin', 'pagebreak', 'editors-xtd', 0, 1, 1, 0, '{"name":"plg_editors-xtd_pagebreak","type":"plugin","creationDate":"August 2004","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_EDITORSXTD_PAGEBREAK_XML_DESCRIPTION","group":"","filename":"pagebreak"}', '', '', '', 0, '0000-00-00 00:00:00', 3, 0),
(416, 'plg_editors-xtd_readmore', 'plugin', 'readmore', 'editors-xtd', 0, 1, 1, 0, '{"name":"plg_editors-xtd_readmore","type":"plugin","creationDate":"March 2006","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_READMORE_XML_DESCRIPTION","group":"","filename":"readmore"}', '', '', '', 0, '0000-00-00 00:00:00', 4, 0);
INSERT INTO `#__extensions` (`extension_id`, `name`, `type`, `element`, `folder`, `client_id`, `enabled`, `access`, `protected`, `manifest_cache`, `params`, `custom_data`, `system_data`, `checked_out`, `checked_out_time`, `ordering`, `state`) VALUES
(417, 'plg_search_categories', 'plugin', 'categories', 'search', 0, 1, 1, 0, '{"name":"plg_search_categories","type":"plugin","creationDate":"November 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_SEARCH_CATEGORIES_XML_DESCRIPTION","group":"","filename":"categories"}', '{"search_limit":"50","search_content":"1","search_archived":"1"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(418, 'plg_search_contacts', 'plugin', 'contacts', 'search', 0, 1, 1, 0, '{"name":"plg_search_contacts","type":"plugin","creationDate":"November 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_SEARCH_CONTACTS_XML_DESCRIPTION","group":"","filename":"contacts"}', '{"search_limit":"50","search_content":"1","search_archived":"1"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(419, 'plg_search_content', 'plugin', 'content', 'search', 0, 1, 1, 0, '{"name":"plg_search_content","type":"plugin","creationDate":"November 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_SEARCH_CONTENT_XML_DESCRIPTION","group":"","filename":"content"}', '{"search_limit":"50","search_content":"1","search_archived":"1"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(420, 'plg_search_newsfeeds', 'plugin', 'newsfeeds', 'search', 0, 1, 1, 0, '{"name":"plg_search_newsfeeds","type":"plugin","creationDate":"November 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_SEARCH_NEWSFEEDS_XML_DESCRIPTION","group":"","filename":"newsfeeds"}', '{"search_limit":"50","search_content":"1","search_archived":"1"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(422, 'plg_system_languagefilter', 'plugin', 'languagefilter', 'system', 0, 1, 1, 1, '{"name":"plg_system_languagefilter","type":"plugin","creationDate":"July 2010","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_SYSTEM_LANGUAGEFILTER_XML_DESCRIPTION","group":"","filename":"languagefilter"}', '{"detect_browser":"0","automatic_change":"1","item_associations":"1","remove_default_prefix":"1","lang_cookie":"0","alternate_meta":"0"}', '', '', 0, '0000-00-00 00:00:00', 1, 0),
(423, 'plg_system_p3p', 'plugin', 'p3p', 'system', 0, 0, 1, 0, '{"name":"plg_system_p3p","type":"plugin","creationDate":"September 2010","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_P3P_XML_DESCRIPTION","group":"","filename":"p3p"}', '{"headers":"NOI ADM DEV PSAi COM NAV OUR OTRo STP IND DEM"}', '', '', 0, '0000-00-00 00:00:00', 2, 0),
(424, 'plg_system_cache', 'plugin', 'cache', 'system', 0, 0, 1, 1, '{"name":"plg_system_cache","type":"plugin","creationDate":"February 2007","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_CACHE_XML_DESCRIPTION","group":"","filename":"cache"}', '{"browsercache":"0","cachetime":"15"}', '', '', 0, '0000-00-00 00:00:00', 9, 0),
(425, 'plg_system_debug', 'plugin', 'debug', 'system', 0, 1, 1, 0, '{"name":"plg_system_debug","type":"plugin","creationDate":"December 2006","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_DEBUG_XML_DESCRIPTION","group":"","filename":"debug"}', '{"profile":"1","queries":"1","memory":"1","language_files":"1","language_strings":"1","strip-first":"1","strip-prefix":"","strip-suffix":""}', '', '', 0, '0000-00-00 00:00:00', 4, 0),
(426, 'plg_system_log', 'plugin', 'log', 'system', 0, 1, 1, 1, '{"name":"plg_system_log","type":"plugin","creationDate":"April 2007","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_LOG_XML_DESCRIPTION","group":"","filename":"log"}', '', '', '', 0, '0000-00-00 00:00:00', 5, 0),
(427, 'plg_system_redirect', 'plugin', 'redirect', 'system', 0, 0, 1, 1, '{"name":"plg_system_redirect","type":"plugin","creationDate":"April 2009","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_SYSTEM_REDIRECT_XML_DESCRIPTION","group":"","filename":"redirect"}', '', '', '', 0, '0000-00-00 00:00:00', 6, 0),
(428, 'plg_system_remember', 'plugin', 'remember', 'system', 0, 1, 1, 1, '{"name":"plg_system_remember","type":"plugin","creationDate":"April 2007","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_REMEMBER_XML_DESCRIPTION","group":"","filename":"remember"}', '', '', '', 0, '0000-00-00 00:00:00', 7, 0),
(429, 'plg_system_sef', 'plugin', 'sef', 'system', 0, 1, 1, 0, '{"name":"plg_system_sef","type":"plugin","creationDate":"December 2007","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_SEF_XML_DESCRIPTION","group":"","filename":"sef"}', '', '', '', 0, '0000-00-00 00:00:00', 8, 0),
(430, 'plg_system_logout', 'plugin', 'logout', 'system', 0, 1, 1, 1, '{"name":"plg_system_logout","type":"plugin","creationDate":"April 2009","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_SYSTEM_LOGOUT_XML_DESCRIPTION","group":"","filename":"logout"}', '', '', '', 0, '0000-00-00 00:00:00', 3, 0),
(431, 'plg_user_contactcreator', 'plugin', 'contactcreator', 'user', 0, 0, 1, 0, '{"name":"plg_user_contactcreator","type":"plugin","creationDate":"August 2009","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_CONTACTCREATOR_XML_DESCRIPTION","group":"","filename":"contactcreator"}', '{"autowebpage":"","category":"34","autopublish":"0"}', '', '', 0, '0000-00-00 00:00:00', 1, 0),
(432, 'plg_user_joomla', 'plugin', 'joomla', 'user', 0, 1, 1, 0, '{"name":"plg_user_joomla","type":"plugin","creationDate":"December 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_USER_JOOMLA_XML_DESCRIPTION","group":"","filename":"joomla"}', '{"autoregister":"1","mail_to_user":"1","forceLogout":"1"}', '', '', 0, '0000-00-00 00:00:00', 2, 0),
(433, 'plg_user_profile', 'plugin', 'profile', 'user', 0, 0, 1, 0, '{"name":"plg_user_profile","type":"plugin","creationDate":"January 2008","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_USER_PROFILE_XML_DESCRIPTION","group":"","filename":"profile"}', '{"register-require_address1":"1","register-require_address2":"1","register-require_city":"1","register-require_region":"1","register-require_country":"1","register-require_postal_code":"1","register-require_phone":"1","register-require_website":"1","register-require_favoritebook":"1","register-require_aboutme":"1","register-require_tos":"2","register_tos_article":"","register-require_dob":"1","profile-require_address1":"1","profile-require_address2":"1","profile-require_city":"1","profile-require_region":"1","profile-require_country":"1","profile-require_postal_code":"1","profile-require_phone":"1","profile-require_website":"1","profile-require_favoritebook":"1","profile-require_aboutme":"1","profile-require_dob":"1"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(434, 'plg_extension_joomla', 'plugin', 'joomla', 'extension', 0, 1, 1, 1, '{"name":"plg_extension_joomla","type":"plugin","creationDate":"May 2010","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_EXTENSION_JOOMLA_XML_DESCRIPTION","group":"","filename":"joomla"}', '', '', '', 0, '0000-00-00 00:00:00', 1, 0),
(435, 'plg_content_joomla', 'plugin', 'joomla', 'content', 0, 1, 1, 0, '{"name":"plg_content_joomla","type":"plugin","creationDate":"November 2010","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_CONTENT_JOOMLA_XML_DESCRIPTION","group":"","filename":"joomla"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(436, 'plg_system_languagecode', 'plugin', 'languagecode', 'system', 0, 0, 1, 0, '{"name":"plg_system_languagecode","type":"plugin","creationDate":"November 2011","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_SYSTEM_LANGUAGECODE_XML_DESCRIPTION","group":"","filename":"languagecode"}', '', '', '', 0, '0000-00-00 00:00:00', 10, 0),
(437, 'plg_quickicon_joomlaupdate', 'plugin', 'joomlaupdate', 'quickicon', 0, 1, 1, 1, '{"name":"plg_quickicon_joomlaupdate","type":"plugin","creationDate":"August 2011","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_QUICKICON_JOOMLAUPDATE_XML_DESCRIPTION","group":"","filename":"joomlaupdate"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(438, 'plg_quickicon_extensionupdate', 'plugin', 'extensionupdate', 'quickicon', 0, 1, 1, 1, '{"name":"plg_quickicon_extensionupdate","type":"plugin","creationDate":"August 2011","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_QUICKICON_EXTENSIONUPDATE_XML_DESCRIPTION","group":"","filename":"extensionupdate"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(439, 'plg_captcha_recaptcha', 'plugin', 'recaptcha', 'captcha', 0, 0, 1, 0, '{"name":"plg_captcha_recaptcha","type":"plugin","creationDate":"December 2011","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.4.0","description":"PLG_CAPTCHA_RECAPTCHA_XML_DESCRIPTION","group":"","filename":"recaptcha"}', '{"public_key":"","private_key":"","theme":"clean"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(440, 'plg_system_highlight', 'plugin', 'highlight', 'system', 0, 1, 1, 0, '{"name":"plg_system_highlight","type":"plugin","creationDate":"August 2011","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_SYSTEM_HIGHLIGHT_XML_DESCRIPTION","group":"","filename":"highlight"}', '', '', '', 0, '0000-00-00 00:00:00', 7, 0),
(441, 'plg_content_finder', 'plugin', 'finder', 'content', 0, 1, 1, 0, '{"name":"plg_content_finder","type":"plugin","creationDate":"December 2011","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_CONTENT_FINDER_XML_DESCRIPTION","group":"","filename":"finder"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(442, 'plg_finder_categories', 'plugin', 'categories', 'finder', 0, 1, 1, 0, '{"name":"plg_finder_categories","type":"plugin","creationDate":"August 2011","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_FINDER_CATEGORIES_XML_DESCRIPTION","group":"","filename":"categories"}', '', '', '', 0, '0000-00-00 00:00:00', 1, 0),
(443, 'plg_finder_contacts', 'plugin', 'contacts', 'finder', 0, 1, 1, 0, '{"name":"plg_finder_contacts","type":"plugin","creationDate":"August 2011","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_FINDER_CONTACTS_XML_DESCRIPTION","group":"","filename":"contacts"}', '', '', '', 0, '0000-00-00 00:00:00', 2, 0),
(444, 'plg_finder_content', 'plugin', 'content', 'finder', 0, 1, 1, 0, '{"name":"plg_finder_content","type":"plugin","creationDate":"August 2011","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_FINDER_CONTENT_XML_DESCRIPTION","group":"","filename":"content"}', '', '', '', 0, '0000-00-00 00:00:00', 3, 0),
(445, 'plg_finder_newsfeeds', 'plugin', 'newsfeeds', 'finder', 0, 1, 1, 0, '{"name":"plg_finder_newsfeeds","type":"plugin","creationDate":"August 2011","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_FINDER_NEWSFEEDS_XML_DESCRIPTION","group":"","filename":"newsfeeds"}', '', '', '', 0, '0000-00-00 00:00:00', 4, 0),
(447, 'plg_finder_tags', 'plugin', 'tags', 'finder', 0, 1, 1, 0, '{"name":"plg_finder_tags","type":"plugin","creationDate":"February 2013","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_FINDER_TAGS_XML_DESCRIPTION","group":"","filename":"tags"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(448, 'plg_twofactorauth_totp', 'plugin', 'totp', 'twofactorauth', 0, 0, 1, 0, '{"name":"plg_twofactorauth_totp","type":"plugin","creationDate":"August 2013","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.2.0","description":"PLG_TWOFACTORAUTH_TOTP_XML_DESCRIPTION","group":"","filename":"totp"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(449, 'plg_authentication_cookie', 'plugin', 'cookie', 'authentication', 0, 1, 1, 0, '{"name":"plg_authentication_cookie","type":"plugin","creationDate":"July 2013","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_AUTH_COOKIE_XML_DESCRIPTION","group":"","filename":"cookie"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(450, 'plg_twofactorauth_yubikey', 'plugin', 'yubikey', 'twofactorauth', 0, 0, 1, 0, '{"name":"plg_twofactorauth_yubikey","type":"plugin","creationDate":"September 2013","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.2.0","description":"PLG_TWOFACTORAUTH_YUBIKEY_XML_DESCRIPTION","group":"","filename":"yubikey"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(451, 'plg_search_tags', 'plugin', 'tags', 'search', 0, 1, 1, 0, '{"name":"plg_search_tags","type":"plugin","creationDate":"March 2014","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_SEARCH_TAGS_XML_DESCRIPTION","group":"","filename":"tags"}', '{"search_limit":"50","show_tagged_items":"1"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(503, 'beez3', 'template', 'beez3', '', 0, 1, 1, 0, '{"name":"beez3","type":"template","creationDate":"25 November 2009","author":"Angie Radtke","copyright":"Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.","authorEmail":"a.radtke@derauftritt.de","authorUrl":"http:\\/\\/www.der-auftritt.de","version":"3.1.0","description":"TPL_BEEZ3_XML_DESCRIPTION","group":"","filename":"templateDetails"}', '{"wrapperSmall":"53","wrapperLarge":"72","sitetitle":"","sitedescription":"","navposition":"center","templatecolor":"nature"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(504, 'hathor', 'template', 'hathor', '', 1, 1, 1, 0, '{"name":"hathor","type":"template","creationDate":"May 2010","author":"Andrea Tarr","copyright":"Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.","authorEmail":"hathor@tarrconsulting.com","authorUrl":"http:\\/\\/www.tarrconsulting.com","version":"3.0.0","description":"TPL_HATHOR_XML_DESCRIPTION","group":"","filename":"templateDetails"}', '{"showSiteName":"0","colourChoice":"0","boldText":"0"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(506, 'protostar', 'template', 'protostar', '', 0, 1, 1, 0, '{"name":"protostar","type":"template","creationDate":"4\\/30\\/2012","author":"Kyle Ledbetter","copyright":"Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"","version":"1.0","description":"TPL_PROTOSTAR_XML_DESCRIPTION","group":"","filename":"templateDetails"}', '{"templateColor":"","logoFile":"","googleFont":"1","googleFontName":"Open+Sans","fluidContainer":"0"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(507, 'isis', 'template', 'isis', '', 1, 1, 1, 0, '{"name":"isis","type":"template","creationDate":"3\\/30\\/2012","author":"Kyle Ledbetter","copyright":"Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"","version":"1.0","description":"TPL_ISIS_XML_DESCRIPTION","group":"","filename":"templateDetails"}', '{"templateColor":"","logoFile":""}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(600, 'English (en-GB)', 'language', 'en-GB', '', 0, 1, 1, 1, '{"name":"English (en-GB)","type":"language","creationDate":"2013-03-07","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.4.3","description":"en-GB site language","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(601, 'English (en-GB)', 'language', 'en-GB', '', 1, 1, 1, 1, '{"name":"English (en-GB)","type":"language","creationDate":"2013-03-07","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.4.3","description":"en-GB administrator language","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(700, 'files_joomla', 'file', 'joomla', '', 0, 1, 1, 1, '{"name":"files_joomla","type":"file","creationDate":"September 2015","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.4.4","description":"FILES_JOOMLA_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10013, 'COM_K2', 'component', 'com_k2', '', 1, 1, 0, 0, '{"name":"COM_K2","type":"component","creationDate":"December 8th, 2014","author":"JoomlaWorks","copyright":"Copyright (c) 2006 - 2014 JoomlaWorks Ltd. All rights reserved.","authorEmail":"please-use-the-contact-form@joomlaworks.net","authorUrl":"www.joomlaworks.net","version":"2.6.9","description":"Thank you for installing K2 by JoomlaWorks, the powerful content extension for Joomla!","group":"","filename":"k2"}', '{"enable_css":"1","jQueryHandling":"1.8remote","backendJQueryHandling":"remote","userName":"1","userImage":"1","userDescription":"1","userURL":"1","userEmail":"0","userFeedLink":"0","userFeedIcon":"1","userItemCount":"10","userItemTitle":"1","userItemTitleLinked":"1","userItemDateCreated":"1","userItemImage":"1","userItemIntroText":"1","userItemCategory":"1","userItemTags":"1","userItemCommentsAnchor":"1","userItemReadMore":"1","userItemK2Plugins":"1","tagItemCount":"10","tagItemTitle":"1","tagItemTitleLinked":"1","tagItemDateCreated":"1","tagItemImage":"1","tagItemIntroText":"1","tagItemCategory":"1","tagItemReadMore":"1","tagItemExtraFields":"0","tagOrdering":"","tagFeedLink":"0","tagFeedIcon":"0","genericItemCount":"10","genericItemTitle":"1","genericItemTitleLinked":"1","genericItemDateCreated":"1","genericItemImage":"1","genericItemIntroText":"1","genericItemCategory":"1","genericItemReadMore":"1","genericItemExtraFields":"0","genericFeedLink":"0","genericFeedIcon":"0","feedLimit":"10","feedItemImage":"1","feedImgSize":"S","feedItemIntroText":"1","feedTextWordLimit":"","feedItemFullText":"1","feedItemTags":"0","feedItemVideo":"0","feedItemGallery":"0","feedItemAttachments":"0","feedBogusEmail":"","introTextCleanup":"0","introTextCleanupExcludeTags":"","introTextCleanupTagAttr":"","fullTextCleanup":"0","fullTextCleanupExcludeTags":"","fullTextCleanupTagAttr":"","xssFiltering":"0","linkPopupWidth":"900","linkPopupHeight":"600","imagesQuality":"100","itemImageXS":"100","itemImageS":"200","itemImageM":"400","itemImageL":"600","itemImageXL":"800","itemImageGeneric":"300","catImageWidth":"100","catImageDefault":"1","userImageWidth":"100","userImageDefault":"1","commenterImgWidth":"48","onlineImageEditor":"splashup","imageTimestamp":"0","imageMemoryLimit":"","socialButtonCode":"","twitterUsername":"","facebookImage":"Medium","comments":"0","commentsOrdering":"DESC","commentsLimit":"10","commentsFormPosition":"below","commentsPublishing":"0","commentsReporting":"2","commentsReportRecipient":"","inlineCommentsModeration":"0","gravatar":"1","antispam":"0","recaptchaForRegistered":"1","akismetForRegistered":"1","commentsFormNotes":"1","commentsFormNotesText":"","frontendEditing":"1","showImageTab":"1","showImageGalleryTab":"1","showVideoTab":"1","showExtraFieldsTab":"1","showAttachmentsTab":"1","showK2Plugins":"1","sideBarDisplayFrontend":"0","mergeEditors":"1","sideBarDisplay":"1","attachmentsFolder":"","hideImportButton":"0","googleSearch":"0","googleSearchContainer":"k2GoogleSearchContainer","K2UserProfile":"1","K2UserGroup":"3","redirect":"123","adminSearch":"simple","cookieDomain":"","taggingSystem":"1","lockTags":"0","showTagFilter":"0","k2TagNorm":"0","k2TagNormCase":"lower","k2TagNormAdditionalReplacements":"","recaptcha_public_key":"","recaptcha_private_key":"","recaptcha_theme":"clean","recaptchaOnRegistration":"0","akismetApiKey":"","stopForumSpam":"0","stopForumSpamApiKey":"","showItemsCounterAdmin":"1","showChildCatItems":"1","disableCompactOrdering":"0","metaDescLimit":"150","enforceSEFReplacements":"0","SEFReplacements":"À|A, Á|A, Â|A, Ã|A, Ä|A, Å|A, à|a, á|a, â|a, ã|a, ä|a, å|a, Ā|A, ā|a, Ă|A, ă|a, Ą|A, ą|a, Ç|C, ç|c, Ć|C, ć|c, Ĉ|C, ĉ|c, Ċ|C, ċ|c, Č|C, č|c, Ð|D, ð|d, Ď|D, ď|d, Đ|D, đ|d, È|E, É|E, Ê|E, Ë|E, è|e, é|e, ê|e, ë|e, Ē|E, ē|e, Ĕ|E, ĕ|e, Ė|E, ė|e, Ę|E, ę|e, Ě|E, ě|e, Ĝ|G, ĝ|g, Ğ|G, ğ|g, Ġ|G, ġ|g, Ģ|G, ģ|g, Ĥ|H, ĥ|h, Ħ|H, ħ|h, Ì|I, Í|I, Î|I, Ï|I, ì|i, í|i, î|i, ï|i, Ĩ|I, ĩ|i, Ī|I, ī|i, Ĭ|I, ĭ|i, Į|I, į|i, İ|I, ı|i, Ĵ|J, ĵ|j, Ķ|K, ķ|k, ĸ|k, Ĺ|L, ĺ|l, Ļ|L, ļ|l, Ľ|L, ľ|l, Ŀ|L, ŀ|l, Ł|L, ł|l, Ñ|N, ñ|n, Ń|N, ń|n, Ņ|N, ņ|n, Ň|N, ň|n, ŉ|n, Ŋ|N, ŋ|n, Ò|O, Ó|O, Ô|O, Õ|O, Ö|O, Ø|O, ò|o, ó|o, ô|o, õ|o, ö|o, ø|o, Ō|O, ō|o, Ŏ|O, ŏ|o, Ő|O, ő|o, Ŕ|R, ŕ|r, Ŗ|R, ŗ|r, Ř|R, ř|r, Ś|S, ś|s, Ŝ|S, ŝ|s, Ş|S, ş|s, Š|S, š|s, ſ|s, Ţ|T, ţ|t, Ť|T, ť|t, Ŧ|T, ŧ|t, Ù|U, Ú|U, Û|U, Ü|U, ù|u, ú|u, û|u, ü|u, Ũ|U, ũ|u, Ū|U, ū|u, Ŭ|U, ŭ|u, Ů|U, ů|u, Ű|U, ű|u, Ų|U, ų|u, Ŵ|W, ŵ|w, Ý|Y, ý|y, ÿ|y, Ŷ|Y, ŷ|y, Ÿ|Y, Ź|Z, ź|z, Ż|Z, ż|z, Ž|Z, ž|z, α|a, β|b, γ|g, δ|d, ε|e, ζ|z, η|h, θ|th, ι|i, κ|k, λ|l, μ|m, ν|n, ξ|x, ο|o, π|p, ρ|r, σ|s, τ|t, υ|y, φ|f, χ|ch, ψ|ps, ω|w, Α|A, Β|B, Γ|G, Δ|D, Ε|E, Ζ|Z, Η|H, Θ|Th, Ι|I, Κ|K, Λ|L, Μ|M, Ξ|X, Ο|O, Π|P, Ρ|R, Σ|S, Τ|T, Υ|Y, Φ|F, Χ|Ch, Ψ|Ps, Ω|W, ά|a, έ|e, ή|h, ί|i, ό|o, ύ|y, ώ|w, Ά|A, Έ|E, Ή|H, Ί|I, Ό|O, Ύ|Y, Ώ|W, ϊ|i, ΐ|i, ϋ|y, ς|s, А|A, Ӑ|A, Ӓ|A, Ә|E, Ӛ|E, Ӕ|E, Б|B, В|V, Г|G, Ґ|G, Ѓ|G, Ғ|G, Ӷ|G, y|Y, Д|D, Е|E, Ѐ|E, Ё|YO, Ӗ|E, Ҽ|E, Ҿ|E, Є|YE, Ж|ZH, Ӂ|DZH, Җ|ZH, Ӝ|DZH, З|Z, Ҙ|Z, Ӟ|DZ, Ӡ|DZ, Ѕ|DZ, И|I, Ѝ|I, Ӥ|I, Ӣ|I, І|I, Ї|JI, Ӏ|I, Й|Y, Ҋ|Y, Ј|J, К|K, Қ|Q, Ҟ|Q, Ҡ|K, Ӄ|Q, Ҝ|K, Л|L, Ӆ|L, Љ|L, М|M, Ӎ|M, Н|N, Ӊ|N, Ң|N, Ӈ|N, Ҥ|N, Њ|N, О|O, Ӧ|O, Ө|O, Ӫ|O, Ҩ|O, П|P, Ҧ|PF, Р|P, Ҏ|P, С|S, Ҫ|S, Т|T, Ҭ|TH, Ћ|T, Ќ|K, У|U, Ў|U, Ӳ|U, Ӱ|U, Ӯ|U, Ү|U, Ұ|U, Ф|F, Х|H, Ҳ|H, Һ|H, Ц|TS, Ҵ|TS, Ч|CH, Ӵ|CH, Ҷ|CH, Ӌ|CH, Ҹ|CH, Џ|DZ, Ш|SH, Щ|SHT, Ъ|A, Ы|Y, Ӹ|Y, Ь|Y, Ҍ|Y, Э|E, Ӭ|E, Ю|YU, Я|YA, а|a, ӑ|a, ӓ|a, ә|e, ӛ|e, ӕ|e, б|b, в|v, г|g, ґ|g, ѓ|g, ғ|g, ӷ|g, y|y, д|d, е|e, ѐ|e, ё|yo, ӗ|e, ҽ|e, ҿ|e, є|ye, ж|zh, ӂ|dzh, җ|zh, ӝ|dzh, з|z, ҙ|z, ӟ|dz, ӡ|dz, ѕ|dz, и|i, ѝ|i, ӥ|i, ӣ|i, і|i, ї|ji, Ӏ|i, й|y, ҋ|y, ј|j, к|k, қ|q, ҟ|q, ҡ|k, ӄ|q, ҝ|k, л|l, ӆ|l, љ|l, м|m, ӎ|m, н|n, ӊ|n, ң|n, ӈ|n, ҥ|n, њ|n, о|o, ӧ|o, ө|o, ӫ|o, ҩ|o, п|p, ҧ|pf, р|p, ҏ|p, с|s, ҫ|s, т|t, ҭ|th, ћ|t, ќ|k, у|u, ў|u, ӳ|u, ӱ|u, ӯ|u, ү|u, ұ|u, ф|f, х|h, ҳ|h, һ|h, ц|ts, ҵ|ts, ч|ch, ӵ|ch, ҷ|ch, ӌ|ch, ҹ|ch, џ|dz, ш|sh, щ|sht, ъ|a, ы|y, ӹ|y, ь|y, ҍ|y, э|e, ӭ|e, ю|yu, я|ya","k2Sef":"0","k2SefLabelCat":"content","k2SefLabelTag":"tag","k2SefLabelUser":"author","k2SefLabelSearch":"search","k2SefLabelDate":"date","k2SefLabelItem":"0","k2SefLabelItemCustomPrefix":"","k2SefInsertItemId":"1","k2SefItemIdTitleAliasSep":"dash","k2SefUseItemTitleAlias":"1","k2SefInsertCatId":"1","k2SefCatIdTitleAliasSep":"dash","k2SefUseCatTitleAlias":"1","sh404SefLabelCat":"","sh404SefLabelUser":"blog","sh404SefLabelItem":"2","sh404SefTitleAlias":"alias","sh404SefModK2ContentFeedAlias":"feed","sh404SefInsertItemId":"0","sh404SefInsertUniqueItemId":"0","cbIntegration":"0"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10014, 'plg_finder_k2', 'plugin', 'k2', 'finder', 0, 0, 1, 0, '{"name":"plg_finder_k2","type":"plugin","creationDate":"December 8th, 2014","author":"JoomlaWorks","copyright":"Copyright (c) 2006 - 2014 JoomlaWorks Ltd. All rights reserved.","authorEmail":"please-use-the-contact-form@joomlaworks.net","authorUrl":"www.joomlaworks.net","version":"2.6.9","description":"PLG_FINDER_K2_DESCRIPTION","group":"","filename":"k2"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10015, 'Search - K2', 'plugin', 'k2', 'search', 0, 1, 1, 0, '{"name":"Search - K2","type":"plugin","creationDate":"December 8th, 2014","author":"JoomlaWorks","copyright":"Copyright (c) 2006 - 2014 JoomlaWorks Ltd. All rights reserved.","authorEmail":"please-use-the-contact-form@joomlaworks.net","authorUrl":"www.joomlaworks.net","version":"2.6.9","description":"K2_THIS_PLUGIN_EXTENDS_THE_DEFAULT_JOOMLA_SEARCH_FUNCTIONALITY_TO_K2_CONTENT","group":"","filename":"k2"}', '{"search_limit":"50","search_tags":"0"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10016, 'System - K2', 'plugin', 'k2', 'system', 0, 1, 1, 0, '{"name":"System - K2","type":"plugin","creationDate":"December 8th, 2014","author":"JoomlaWorks","copyright":"Copyright (c) 2006 - 2014 JoomlaWorks Ltd. All rights reserved.","authorEmail":"please-use-the-contact-form@joomlaworks.net","authorUrl":"www.joomlaworks.net","version":"2.6.9","description":"K2_THE_K2_SYSTEM_PLUGIN_IS_USED_TO_ASSIST_THE_PROPER_FUNCTIONALITY_OF_THE_K2_COMPONENT_SITE_WIDE_MAKE_SURE_ITS_ALWAYS_PUBLISHED_WHEN_THE_K2_COMPONENT_IS_INSTALLED","group":"","filename":"k2"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10017, 'User - K2', 'plugin', 'k2', 'user', 0, 1, 1, 0, '{"name":"User - K2","type":"plugin","creationDate":"December 8th, 2014","author":"JoomlaWorks","copyright":"Copyright (c) 2006 - 2014 JoomlaWorks Ltd. All rights reserved.","authorEmail":"please-use-the-contact-form@joomlaworks.net","authorUrl":"www.joomlaworks.net","version":"2.6.9","description":"K2_A_USER_SYNCHRONIZATION_PLUGIN_FOR_K2","group":"","filename":"k2"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10018, 'Josetta - K2 Categories', 'plugin', 'k2category', 'josetta_ext', 0, 1, 1, 0, '{"name":"Josetta - K2 Categories","type":"plugin","creationDate":"December 8th, 2014","author":"JoomlaWorks","copyright":"Copyright (c) 2006 - 2014 JoomlaWorks Ltd. All rights reserved.","authorEmail":"please-use-the-contact-form@joomlaworks.net","authorUrl":"www.joomlaworks.net","version":"2.6.9","description":"","group":"","filename":"k2category"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10019, 'Josetta - K2 Items', 'plugin', 'k2item', 'josetta_ext', 0, 1, 1, 0, '{"name":"Josetta - K2 Items","type":"plugin","creationDate":"June 7th, 2012","author":"JoomlaWorks","copyright":"Copyright (c) 2006 - 2014 JoomlaWorks Ltd. All rights reserved.","authorEmail":"please-use-the-contact-form@joomlaworks.net","authorUrl":"www.joomlaworks.net","version":"2.6.9","description":"","group":"","filename":"k2item"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10020, 'K2 Comments', 'module', 'mod_k2_comments', '', 0, 1, 0, 0, '{"name":"K2 Comments","type":"module","creationDate":"December 8th, 2014","author":"JoomlaWorks","copyright":"Copyright (c) 2006 - 2014 JoomlaWorks Ltd. All rights reserved.","authorEmail":"please-use-the-contact-form@joomlaworks.net","authorUrl":"www.joomlaworks.net","version":"2.6.9","description":"MOD_K2_COMMENTS_DESCRIPTION","group":"","filename":"mod_k2_comments.j25"}', '{"moduleclass_sfx":"","module_usage":"","":"K2_TOP_COMMENTERS","catfilter":"0","category_id":"","comments_limit":"5","comments_word_limit":"10","commenterName":"1","commentAvatar":"1","commentAvatarWidthSelect":"custom","commentAvatarWidth":"50","commentDate":"1","commentDateFormat":"absolute","commentLink":"1","itemTitle":"1","itemCategory":"1","feed":"1","commenters_limit":"5","commenterNameOrUsername":"1","commenterAvatar":"1","commenterAvatarWidthSelect":"custom","commenterAvatarWidth":"50","commenterLink":"1","commenterCommentsCounter":"1","commenterLatestComment":"1","cache":"1","cache_time":"900"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10021, 'K2 Content', 'module', 'mod_k2_content', '', 0, 1, 0, 0, '{"name":"K2 Content","type":"module","creationDate":"December 8th, 2014","author":"JoomlaWorks","copyright":"Copyright (c) 2006 - 2014 JoomlaWorks Ltd. All rights reserved.","authorEmail":"please-use-the-contact-form@joomlaworks.net","authorUrl":"www.joomlaworks.net","version":"2.6.9","description":"K2_MOD_K2_CONTENT_DESCRIPTION","group":"","filename":"mod_k2_content.j25"}', '{"moduleclass_sfx":"","getTemplate":"Default","source":"filter","":"K2_OTHER_OPTIONS","catfilter":"0","category_id":"","getChildren":"0","itemCount":"5","itemsOrdering":"","FeaturedItems":"1","popularityRange":"","videosOnly":"0","item":"","items":"","itemTitle":"1","itemAuthor":"1","itemAuthorAvatar":"1","itemAuthorAvatarWidthSelect":"custom","itemAuthorAvatarWidth":"50","userDescription":"1","itemIntroText":"1","itemIntroTextWordLimit":"","itemImage":"1","itemImgSize":"Small","itemVideo":"1","itemVideoCaption":"1","itemVideoCredits":"1","itemAttachments":"1","itemTags":"1","itemCategory":"1","itemDateCreated":"1","itemHits":"1","itemReadMore":"1","itemExtraFields":"0","itemCommentsCounter":"1","feed":"1","itemPreText":"","itemCustomLink":"0","itemCustomLinkTitle":"","itemCustomLinkURL":"http:\\/\\/","itemCustomLinkMenuItem":"","K2Plugins":"1","JPlugins":"1","cache":"1","cache_time":"900"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10022, 'K2 Tools', 'module', 'mod_k2_tools', '', 0, 1, 0, 0, '{"name":"K2 Tools","type":"module","creationDate":"December 8th, 2014","author":"JoomlaWorks","copyright":"Copyright (c) 2006 - 2014 JoomlaWorks Ltd. All rights reserved.","authorEmail":"please-use-the-contact-form@joomlaworks.net","authorUrl":"www.joomlaworks.net","version":"2.6.9","description":"K2_TOOLS","group":"","filename":"mod_k2_tools.j25"}', '{"moduleclass_sfx":"","module_usage":"0","":"K2_CUSTOM_CODE_SETTINGS","archiveItemsCounter":"1","archiveCategory":"","authors_module_category":"","authorItemsCounter":"1","authorAvatar":"1","authorAvatarWidthSelect":"custom","authorAvatarWidth":"50","authorLatestItem":"1","calendarCategory":"","home":"","seperator":"","root_id":"","end_level":"","categoriesListOrdering":"","categoriesListItemsCounter":"1","root_id2":"","catfilter":"0","category_id":"","getChildren":"0","liveSearch":"","width":"20","text":"","button":"","imagebutton":"","button_text":"","min_size":"75","max_size":"300","cloud_limit":"30","cloud_category":"0","cloud_category_recursive":"0","customCode":"","parsePhp":"0","K2Plugins":"0","JPlugins":"0","cache":"1","cache_time":"900"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10023, 'K2 Users', 'module', 'mod_k2_users', '', 0, 1, 0, 0, '{"name":"K2 Users","type":"module","creationDate":"December 8th, 2014","author":"JoomlaWorks","copyright":"Copyright (c) 2006 - 2014 JoomlaWorks Ltd. All rights reserved.","authorEmail":"please-use-the-contact-form@joomlaworks.net","authorUrl":"www.joomlaworks.net","version":"2.6.9","description":"K2_MOD_K2_USERS_DESCRTIPTION","group":"","filename":"mod_k2_users.j25"}', '{"moduleclass_sfx":"","getTemplate":"Default","source":"0","":"K2_DISPLAY_OPTIONS","filter":"1","K2UserGroup":"","ordering":"1","limit":"4","userIDs":"","userName":"1","userAvatar":"1","userAvatarWidthSelect":"custom","userAvatarWidth":"50","userDescription":"1","userDescriptionWordLimit":"","userURL":"1","userEmail":"0","userFeed":"1","userItemCount":"1","cache":"1","cache_time":"900"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10024, 'K2 User', 'module', 'mod_k2_user', '', 0, 1, 0, 0, '{"name":"K2 User","type":"module","creationDate":"December 8th, 2014","author":"JoomlaWorks","copyright":"Copyright (c) 2006 - 2014 JoomlaWorks Ltd. All rights reserved.","authorEmail":"please-use-the-contact-form@joomlaworks.net","authorUrl":"www.joomlaworks.net","version":"2.6.9","description":"K2_MOD_K2_USER_DESCRIPTION","group":"","filename":"mod_k2_user.j25"}', '{"moduleclass_sfx":"","pretext":"","":"K2_LOGIN_LOGOUT_REDIRECTION","name":"1","userAvatar":"1","userAvatarWidthSelect":"custom","userAvatarWidth":"50","menu":"","login":"","logout":"","usesecure":"0","cache":"0","cache_time":"900"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10025, 'K2 Quick Icons (admin)', 'module', 'mod_k2_quickicons', '', 1, 1, 2, 0, '{"name":"K2 Quick Icons (admin)","type":"module","creationDate":"December 8th, 2014","author":"JoomlaWorks","copyright":"Copyright (c) 2006 - 2014 JoomlaWorks Ltd. All rights reserved.","authorEmail":"please-use-the-contact-form@joomlaworks.net","authorUrl":"www.joomlaworks.net","version":"2.6.9","description":"K2_QUICKICONS_FOR_USE_IN_THE_JOOMLA_CONTROL_PANEL_DASHBOARD_PAGE","group":"","filename":"mod_k2_quickicons.j25"}', '{"modCSSStyling":"1","modLogo":"1","cache":"0","cache_time":"900"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10026, 'K2 Stats (admin)', 'module', 'mod_k2_stats', '', 1, 1, 2, 0, '{"name":"K2 Stats (admin)","type":"module","creationDate":"December 8th, 2014","author":"JoomlaWorks","copyright":"Copyright (c) 2006 - 2014 JoomlaWorks Ltd. All rights reserved.","authorEmail":"please-use-the-contact-form@joomlaworks.net","authorUrl":"www.joomlaworks.net","version":"2.6.9","description":"K2_STATS_FOR_USE_IN_THE_K2_DASHBOARD_PAGE","group":"","filename":"mod_k2_stats.j25"}', '{"latestItems":"1","popularItems":"1","mostCommentedItems":"1","latestComments":"1","statistics":"1","cache":"0","cache_time":"900"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10061, 'F0F (NEW) DO NOT REMOVE', 'library', 'lib_f0f', '', 0, 1, 1, 0, '{"name":"F0F (NEW) DO NOT REMOVE","type":"library","creationDate":"2014-09-05 12:18:44","author":"Nicholas K. Dionysopoulos \\/ Akeeba Ltd","copyright":"(C)2011-2014 Nicholas K. Dionysopoulos","authorEmail":"nicholas@akeebabackup.com","authorUrl":"https:\\/\\/www.akeebabackup.com","version":"2.3.5","description":"Framework-on-Framework (FOF) newer version - DO NOT REMOVE - The rapid component development framework for Joomla!. This package is the newer version of FOF, not the one shipped with Joomla! as the official Joomla! RAD Layer. The Joomla! RAD Layer has ceased development in March 2014. DO NOT UNINSTALL THIS PACKAGE, IT IS *** N O T *** A DUPLICATE OF THE ''FOF'' PACKAGE. REMOVING EITHER FOF PACKAGE WILL BREAK YOUR SITE.","group":"","filename":"lib_f0f"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10157, 'Mijoshop Fallback Redirect plugin', 'plugin', 'mijo_redirect', 'system', 0, 0, 1, 0, '', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10159, 'Redshop Fallback Redirect plugin', 'plugin', 'reds_redirect', 'system', 0, 0, 1, 0, '', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10160, 'VirtueMart Fallback Redirect plugin', 'plugin', 'vm_redirect', 'system', 0, 0, 1, 0, '', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10179, 'FavEffects', 'module', 'mod_faveffects', '', 0, 1, 0, 0, '{"name":"FavEffects","type":"module","creationDate":"2013","author":"FavThemes","copyright":"Copyright (C) FavThemes. All rights reserved.","authorEmail":"","authorUrl":"http:\\/\\/www.favthemes.com","version":"1.5","description":"\\n\\n\\n<a href=\\"http:\\/\\/extensions.favthemes.com\\/faveffects\\" target=\\"_blank\\"><strong>FavEffects<\\/strong><\\/a> is a free responsive Joomla! module that uses any icon from Font Awesome and 8 preset icon hover CSS3 effects.\\n<br\\/>\\nFor more info, please see the <a href=\\"http:\\/\\/extensions.favthemes.com\\/faveffects\\" target=\\"_blank\\" style=\\"font-weight: bold;\\">demo and documentation<\\/a> for the FavEffects module.\\n<br\\/><br\\/>\\n\\n<a href=\\"http:\\/\\/extensions.favthemes.com\\/faveffects\\" target=\\"_blank\\">\\n  <img src=\\"..\\/modules\\/mod_faveffects\\/theme\\/img\\/faveffects.jpg\\" alt=\\"FavEffects Responsive Module\\">\\n<\\/a>\\n\\n<br\\/><br\\/>\\nCopyright &#169; 2012-2015 <a href=\\"http:\\/\\/www.favthemes.com\\" target=\\"_blank\\" style=\\"font-weight: bold;\\">FavThemes<\\/a>.\\n<br\\/><br\\/>\\n\\n<link rel=\\"stylesheet\\" href=\\"..\\/modules\\/mod_faveffects\\/theme\\/css\\/admin.css\\" type=\\"text\\/css\\" \\/>\\n<script src=\\"..\\/modules\\/mod_faveffects\\/theme\\/js\\/jscolor\\/jscolor.js\\" type=\\"text\\/javascript\\"><\\/script>\\n","group":"","filename":"mod_faveffects"}', '{"jquery_load":"1","layout_effect":"layout-effect1","icon_width":"96%","icon_border_radius":"50%","icon_border_type":"solid","icon_border_width":"2px","title_google_font":"Open Sans","title_font_size":"16px","show_icon1":"1","icon_effect1":"effect1","icon_name1":"fa-magic","icon_color1":"FFFFFF","icon_bg_color1":"7E57C2","show_icon_link1":"1","icon_link1":"http:\\/\\/www.favthemes.com","icon_link_target1":"blank","icon_font_size1":"3.3em","icon_border_color1":"B39DDB","title_text1":"Title Text","title_color1":"444444","show_icon2":"1","icon_effect2":"effect2","icon_name2":"fa-heart","icon_color2":"FFFFFF","icon_bg_color2":"EF5350","show_icon_link2":"1","icon_link2":"http:\\/\\/www.favthemes.com","icon_link_target2":"blank","icon_font_size2":"3.3em","icon_border_color2":"EF9A9A","title_text2":"Title Text","title_color2":"444444","show_icon3":"1","icon_effect3":"effect3","icon_name3":"fa-life-ring","icon_color3":"FFFFFF","icon_bg_color3":"42A5F5","show_icon_link3":"1","icon_link3":"http:\\/\\/www.favthemes.com","icon_link_target3":"blank","icon_font_size3":"3.3em","icon_border_color3":"90CAF9","title_text3":"Title Text","title_color3":"444444","show_icon4":"1","icon_effect4":"effect4","icon_name4":"fa-tree","icon_color4":"FFFFFF","icon_bg_color4":"66BB6A","show_icon_link4":"1","icon_link4":"http:\\/\\/www.favthemes.com","icon_link_target4":"blank","icon_font_size4":"3.3em","icon_border_color4":"A5D6A7","title_text4":"Title Text","title_color4":"444444","show_icon5":"1","icon_effect5":"effect5","icon_name5":"fa-spinner","icon_color5":"FFFFFF","icon_bg_color5":"FFA726","show_icon_link5":"1","icon_link5":"http:\\/\\/www.favthemes.com","icon_link_target5":"blank","icon_font_size5":"3.3em","icon_border_color5":"FFCC80","title_text5":"Title Text","title_color5":"444444","show_icon6":"1","icon_effect6":"effect6","icon_name6":"fa-paper-plane-o","icon_color6":"FFFFFF","icon_bg_color6":"26C6DA","show_icon_link6":"1","icon_link6":"http:\\/\\/www.favthemes.com","icon_link_target6":"blank","icon_font_size6":"3.3em","icon_border_color6":"80DEEA","title_text6":"Title Text","title_color6":"444444","cache":"1","cache_time":"900","cachemode":"static"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10180, 'FavGlyph', 'module', 'mod_favglyph', '', 0, 1, 0, 0, '{"name":"FavGlyph","type":"module","creationDate":"2014","author":"FavThemes","copyright":"Copyright (C) FavThemes. All rights reserved.","authorEmail":"","authorUrl":"http:\\/\\/www.favthemes.com","version":"1.4","description":"\\n\\n\\n<a href=\\"http:\\/\\/extensions.favthemes.com\\/favglyph\\" target=\\"_blank\\"><strong>FavGlyph<\\/strong><\\/a> is a free responsive Joomla! module that uses any icon from Font Awesome and assigns it a short description and a title.\\n<br\\/>\\nFor more info, please see the <a href=\\"http:\\/\\/extensions.favthemes.com\\/favglyph\\" target=\\"_blank\\" style=\\"font-weight: bold;\\">demo and documentation<\\/a> for the FavGlyph module.\\n<br\\/><br\\/>\\n\\n<a href=\\"http:\\/\\/extensions.favthemes.com\\/favglyph\\" target=\\"_blank\\">\\n  <img src=\\"..\\/modules\\/mod_favglyph\\/theme\\/img\\/favglyph.jpg\\" alt=\\"FavGlyph Responsive Module\\">\\n<\\/a>\\n\\n<br\\/><br\\/>\\nCopyright &#169; 2012-2015 <a href=\\"http:\\/\\/www.favthemes.com\\" target=\\"_blank\\" style=\\"font-weight: bold;\\">FavThemes<\\/a>.\\n<br\\/><br\\/>\\n\\n<link rel=\\"stylesheet\\" href=\\"..\\/modules\\/mod_favglyph\\/theme\\/css\\/admin.css\\" type=\\"text\\/css\\" \\/>\\n<script src=\\"..\\/modules\\/mod_favglyph\\/theme\\/js\\/jscolor\\/jscolor.js\\" type=\\"text\\/javascript\\"><\\/script>\\n","group":"","filename":"mod_favglyph"}', '{"jquery_load":"1","layout_effect":"layout-effect1","icon_width":"77%","icon_border_radius":"50%","icon_border_type":"solid","icon_border_width":"1px","title_google_font":"Open Sans","title_font_size":"21px","title_margin":"28px 0px 14px","description_description_font_size":"14px","show_icon1":"1","icon_layout1":"center","icon_name1":"fa-globe","icon_color1":"2196F3","icon_bg_color1":"FFFFFF","icon_border_color1":"CCCCCC","icon_link1":"http:\\/\\/www.favthemes.com","icon_link_target1":"blank","icon_font_size1":"6em","title_text1":"Title Text","title_color1":"444444","description_layout1":"0","description_text1":"Lorem ipsum dolor sit amet, dolore magna aliqua.","description_text_color1":"444444","show_icon2":"1","icon_layout2":"center","icon_name2":"fa-html5","icon_color2":"ff5252","icon_bg_color2":"FFFFFF","icon_border_color2":"CCCCCC","icon_link2":"http:\\/\\/www.favthemes.com","icon_link_target2":"blank","icon_font_size2":"6em","title_text2":"Title Text","title_color2":"444444","description_layout2":"0","description_text2":"Lorem ipsum dolor sit amet, dolore magna aliqua.","description_text_color2":"444444","show_icon3":"1","icon_layout3":"center","icon_name3":"fa-recycle","icon_color3":"8bc34a","icon_bg_color3":"FFFFFF","icon_border_color3":"CCCCCC","icon_link3":"http:\\/\\/www.favthemes.com","icon_link_target3":"blank","icon_font_size3":"6em","title_text3":"Title Text","title_color3":"444444","description_layout3":"0","description_text3":"Lorem ipsum dolor sit amet, dolore magna aliqua.","description_text_color3":"444444","show_icon4":"1","icon_layout4":"center","icon_name4":"fa-lightbulb-o","icon_color4":"ffc107","icon_bg_color4":"FFFFFF","icon_border_color4":"CCCCCC","icon_link4":"http:\\/\\/www.favthemes.com","icon_link_target4":"blank","icon_font_size4":"6em","title_text4":"Title Text","title_color4":"444444","description_layout4":"0","description_text4":"Lorem ipsum dolor sit amet, dolore magna aliqua.","description_text_color4":"444444","show_icon5":"0","icon_layout5":"center","icon_name5":"fa-tablet","icon_color5":"673ab7","icon_bg_color5":"FFFFFF","icon_border_color5":"CCCCCC","icon_link5":"http:\\/\\/www.favthemes.com","icon_link_target5":"blank","icon_font_size5":"6em","title_text5":"Title Text","title_color5":"444444","description_layout5":"0","description_text5":"Lorem ipsum dolor sit amet, dolore magna aliqua.","description_text_color5":"444444","show_icon6":"0","icon_layout6":"center","icon_name6":"fa-puzzle-piece","icon_color6":"00bcd4","icon_bg_color6":"FFFFFF","icon_border_color6":"CCCCCC","icon_link6":"http:\\/\\/www.favthemes.com","icon_link_target6":"blank","icon_font_size6":"6em","title_text6":"Title Text","title_color6":"444444","description_layout6":"0","description_text6":"Lorem ipsum dolor sit amet, dolore magna aliqua.","description_text_color6":"444444","cache":"1","cache_time":"900","cachemode":"static"}', '', '', 0, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `#__extensions` (`extension_id`, `name`, `type`, `element`, `folder`, `client_id`, `enabled`, `access`, `protected`, `manifest_cache`, `params`, `custom_data`, `system_data`, `checked_out`, `checked_out_time`, `ordering`, `state`) VALUES
(10182, 'FavPromote', 'module', 'mod_favpromote', '', 0, 1, 0, 0, '{"name":"FavPromote","type":"module","creationDate":"2013","author":"FavThemes","copyright":"Copyright (C) FavThemes. All rights reserved.","authorEmail":"","authorUrl":"http:\\/\\/www.favthemes.com","version":"1.7","description":"\\n\\n\\n<a href=\\"http:\\/\\/extensions.favthemes.com\\/favpromote\\" target=\\"_blank\\"><strong>FavPromote<\\/strong><\\/a> is a free responsive Joomla! module that lets you upload any image and assign it a short description and a title with a Font Awesome icon.\\n<br\\/>\\nFor more info, please see the <a href=\\"http:\\/\\/extensions.favthemes.com\\/favpromote\\" target=\\"_blank\\" style=\\"font-weight: bold;\\">demo and documentation<\\/a> for the FavPromote module.\\n<br\\/><br\\/>\\n\\n<a href=\\"http:\\/\\/extensions.favthemes.com\\/favpromote\\" target=\\"_blank\\">\\n  <img src=\\"..\\/modules\\/mod_favpromote\\/theme\\/img\\/favpromote.jpg\\" alt=\\"FavPromote Responsive Module\\">\\n<\\/a>\\n\\n<br\\/><br\\/>\\nCopyright &#169; 2012-2015 <a href=\\"http:\\/\\/www.favthemes.com\\" target=\\"_blank\\" style=\\"font-weight: bold;\\">FavThemes<\\/a>.\\n<br\\/><br\\/>\\n\\n<link rel=\\"stylesheet\\" href=\\"..\\/modules\\/mod_favpromote\\/theme\\/css\\/admin.css\\" type=\\"text\\/css\\" \\/>\\n<script src=\\"..\\/modules\\/mod_favpromote\\/theme\\/js\\/jscolor\\/jscolor.js\\" type=\\"text\\/javascript\\"><\\/script>\\n\\n\\t","group":"","filename":"mod_favpromote"}', '{"jquery_load":"1","layout_effect":"layout-effect1","title_google_font":"Open Sans","title_padding":"10px 20px","title_font_size":"18px","title_line_height":"1.4em","title_text_align":"left","title_icon_font_size":"18px","title_icon_vertical_align":"baseline","description_text_font_size":"14px","description_text_line_height":"1.5em","description_text_align":"justify","show_column1":"1","column_border_color1":"DDDDDD","column_border_radius1":"4px","show_image_link1":"1","image_link1":"http:\\/\\/www.favthemes.com","image_target1":"blank","image_alt1":"","title_text1":"Title Text","title_color1":"FFFFFF","title_bg_color1":"D63E52","show_title_link1":"1","title_link1":"http:\\/\\/www.favthemes.com","title_target1":"blank","title_icon1":"fa-coffee","description_text1":"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor.","description_text_color1":"444444","show_column2":"1","column_border_color2":"DDDDDD","column_border_radius2":"4px","show_image_link2":"1","image_link2":"http:\\/\\/www.favthemes.com","image_target2":"blank","image_alt2":"","title_text2":"Title Text","title_color2":"FFFFFF","title_bg_color2":"D63E52","show_title_link2":"1","title_link2":"http:\\/\\/www.favthemes.com","title_target2":"blank","title_icon2":"fa-puzzle-piece","description_text2":"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor.","description_text_color2":"444444","show_column3":"1","column_border_color3":"DDDDDD","column_border_radius3":"4px","show_image_link3":"1","image_link3":"http:\\/\\/www.favthemes.com","image_target3":"blank","image_alt3":"","title_text3":"Title Text","title_color3":"FFFFFF","title_bg_color3":"D63E52","show_title_link3":"1","title_link3":"http:\\/\\/www.favthemes.com","title_target3":"blank","title_icon3":"fa-bookmark","description_text3":"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor.","description_text_color3":"444444","show_column4":"1","column_border_color4":"DDDDDD","column_border_radius4":"4px","show_image_link4":"1","image_link4":"http:\\/\\/www.favthemes.com","image_target4":"blank","image_alt4":"","title_text4":"Title Text","title_color4":"FFFFFF","title_bg_color4":"D63E52","show_title_link4":"1","title_link4":"http:\\/\\/www.favthemes.com","title_target4":"blank","title_icon4":"fa-magic","description_text4":"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor.","description_text_color4":"444444","show_column5":"0","column_border_color5":"DDDDDD","column_border_radius5":"4px","show_image_link5":"1","image_link5":"http:\\/\\/www.favthemes.com","image_target5":"blank","image_alt5":"","title_text5":"Title Text","title_color5":"FFFFFF","title_bg_color5":"D63E52","show_title_link5":"1","title_link5":"http:\\/\\/www.favthemes.com","title_target5":"blank","title_icon5":"fa-quote-right","description_text5":"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor.","description_text_color5":"444444","show_column6":"0","column_border_color6":"DDDDDD","column_border_radius6":"4px","show_image_link6":"1","image_link6":"http:\\/\\/www.favthemes.com","image_target6":"blank","image_alt6":"","title_text6":"Title Text","title_color6":"FFFFFF","title_bg_color6":"D63E52","show_title_link6":"1","title_link6":"http:\\/\\/www.favthemes.com","title_target6":"blank","title_icon6":"fa-recycle","description_text6":"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor.","description_text_color6":"444444","cache":"1","cache_time":"900","cachemode":"static"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10183, 'FavSlider Responsive Slideshow', 'module', 'mod_favslider', '', 0, 1, 0, 0, '{"name":"FavSlider Responsive Slideshow","type":"module","creationDate":"2013","author":"FavThemes","copyright":"Copyright (C) FavThemes. All rights reserved.","authorEmail":"","authorUrl":"http:\\/\\/www.favthemes.com","version":"1.7","description":"\\n\\t\\n\\n<a href=\\"http:\\/\\/extensions.favthemes.com\\/favslider\\" target=\\"_blank\\"><strong>FavSlider<\\/strong><\\/a> is a free responsive Joomla! module that let''s you use images and video to create a slideshow with 4 default layouts and customize each slide with 100+ module parameters.\\n\\nFor more info, please see the <a href=\\"http:\\/\\/extensions.favthemes.com\\/favslider\\" target=\\"_blank\\" style=\\"font-weight: bold;\\">demo and documentation<\\/a> for the FavSlider module.\\n<br\\/><br\\/>\\n\\n<a href=\\"http:\\/\\/extensions.favthemes.com\\/favslider\\" target=\\"_blank\\">\\n\\t<img src=\\"..\\/modules\\/mod_favslider\\/theme\\/img\\/favslider.jpg\\" alt=\\"FavSlider Reponsive Slideshow\\">\\n<\\/a>\\n\\n<br\\/><br\\/>\\nCopyright &#169; 2012-2015 <a href=\\"http:\\/\\/www.favthemes.com\\" target=\\"_blank\\" style=\\"font-weight: bold;\\">FavThemes<\\/a>.\\n<br\\/><br\\/>\\n\\n<link rel=\\"stylesheet\\" href=\\"..\\/modules\\/mod_favslider\\/theme\\/css\\/admin.css\\" type=\\"text\\/css\\" \\/>\\n<script src=\\"..\\/modules\\/mod_favslider\\/theme\\/js\\/jscolor\\/jscolor.js\\" type=\\"text\\/javascript\\"><\\/script>\\n\\n\\t","group":"","filename":"mod_favslider"}', '{"sliderType":"slidernav","jqueryLoad":"1","animationEffect":"slide","slideHeight":"","thumbHeight":"","linkTarget":"self","arrowNav":"1","arrowNavStyle":"dark-arrows","controlNav":"1","slideshow":"0","randomize":"0","animationLoop":"1","pauseOnHover":"1","keyboardNav":"1","mousewheel":"0","slideshowSpeed":"7000","captionHide":"visible","layoutEffect":"layout-effect1","captionTextAlign":"favalign-left","captionStyle":"favstyle-default","captionBgStyle":"favstyle-bg-dark","captionWidth":"","captionHeight":"","captionTitleGoogleFont":"Open Sans","captionTitleFontSize":"","captionTitleTextTransform":"uppercase","captionTitlePadding":"","captionTitleMargin":"","captionDescriptionGoogleFont":"Open Sans","captionDescriptionFontSize":"","captionReadMoreColor":"","captionReadMoreBgColor":"","captionReadMoreGoogleFont":"Open Sans","captionReadMorePadding":"","captionReadMoreMargin":"","file1active":"1","file1type":"image","file1":"media\\/favslider\\/demo\\/slide1.jpg","file1link":"","file1favtitle":"","file1favdescription":"","file2active":"1","file2type":"image","file2":"media\\/favslider\\/demo\\/slide2.jpg","file3active":"1","file3type":"image","file3":"media\\/favslider\\/demo\\/slide3.jpg","file4active":"1","file4type":"image","file4":"media\\/favslider\\/demo\\/slide4.jpg","file5active":"1","file5type":"image","file5":"media\\/favslider\\/demo\\/slide5.jpg","file6active":"0","file6type":"image","file6":"","file7active":"0","file7type":"image","file7":"","file8active":"0","file8type":"image","file8":"","file9active":"0","file9type":"image","file9":"","file10active":"0","file10type":"image","file10":"","cache":"1","cache_time":"900","cachemode":"static"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10184, 'FavSocial', 'module', 'mod_favsocial', '', 0, 1, 0, 0, '{"name":"FavSocial","type":"module","creationDate":"2013","author":"FavThemes","copyright":"Copyright (C) FavThemes. All rights reserved.","authorEmail":"","authorUrl":"http:\\/\\/www.favthemes.com","version":"1.5","description":"\\n\\n\\n<a href=\\"http:\\/\\/extensions.favthemes.com\\/favsocial\\" target=\\"_blank\\"><strong>FavSocial<\\/strong><\\/a> is a free responsive Joomla! module that lets you choose up to 10 social media icons from Font Awesome and link each one to your social media accounts.\\n<br\\/>\\nFor more info, please see the <a href=\\"http:\\/\\/extensions.favthemes.com\\/favsocial\\" target=\\"_blank\\" style=\\"font-weight: bold;\\">demo and documentation<\\/a> for the FavSocial module.\\n<br\\/><br\\/>\\n\\n<a href=\\"http:\\/\\/extensions.favthemes.com\\/favsocial\\" target=\\"_blank\\">\\n  <img src=\\"..\\/modules\\/mod_favsocial\\/theme\\/img\\/favsocial.jpg\\" alt=\\"FavSocial Responsive Module\\">\\n<\\/a>\\n\\n<br\\/><br\\/>\\nCopyright &#169; 2012-2015 <a href=\\"http:\\/\\/www.favthemes.com\\" target=\\"_blank\\" style=\\"font-weight: bold;\\">FavThemes<\\/a>.\\n<br\\/><br\\/>\\n\\n<link rel=\\"stylesheet\\" href=\\"..\\/modules\\/mod_favsocial\\/theme\\/css\\/admin.css\\" type=\\"text\\/css\\" \\/>\\n<script src=\\"..\\/modules\\/mod_favsocial\\/theme\\/js\\/jscolor\\/jscolor.js\\" type=\\"text\\/javascript\\"><\\/script>\\n","group":"","filename":"mod_favsocial"}', '{"module_align":"favsocial-left","show_icon1":"1","icon_name1":"fa-twitter","icon_color1":"FFFFFF","icon_bg_color1":"00ACED","icon_font_size1":"21px","icon_link1":"https:\\/\\/twitter.com\\/","icon_link_target1":"blank","icon_padding1":"0.5em","icon_border_radius1":"50%","show_icon2":"1","icon_name2":"fa-facebook","icon_color2":"FFFFFF","icon_bg_color2":"3B5998","icon_font_size2":"21px","icon_link2":"https:\\/\\/www.facebook.com\\/","icon_link_target2":"blank","icon_padding2":"0.5em","icon_border_radius2":"50%","show_icon3":"1","icon_name3":"fa-dribbble","icon_color3":"FFFFFF","icon_bg_color3":"EA4C89","icon_font_size3":"21px","icon_link3":"https:\\/\\/dribbble.com\\/","icon_link_target3":"blank","icon_padding3":"0.5em","icon_border_radius3":"50%","show_icon4":"0","icon_name4":"","icon_color4":"","icon_bg_color4":"","icon_font_size4":"","icon_link4":"","icon_link_target4":"blank","icon_padding4":"","icon_border_radius4":"","show_icon5":"0","icon_name5":"","icon_color5":"","icon_bg_color5":"","icon_font_size5":"","icon_link5":"","icon_link_target5":"blank","icon_padding5":"","icon_border_radius5":"","show_icon6":"0","icon_name6":"","icon_color6":"","icon_bg_color6":"","icon_font_size6":"","icon_link6":"","icon_link_target6":"blank","icon_padding6":"","icon_border_radius6":"","show_icon7":"0","icon_name7":"","icon_color7":"","icon_bg_color7":"","icon_font_size7":"","icon_link7":"","icon_link_target7":"blank","icon_padding7":"","icon_border_radius7":"","show_icon8":"0","icon_name8":"","icon_color8":"","icon_bg_color8":"","icon_font_size8":"","icon_link8":"","icon_link_target8":"blank","icon_padding8":"","icon_border_radius8":"","show_icon9":"0","icon_name9":"","icon_color9":"","icon_bg_color9":"","icon_font_size9":"","icon_link9":"","icon_link_target9":"blank","icon_padding9":"","icon_border_radius9":"","show_icon10":"0","icon_name10":"","icon_color10":"","icon_bg_color10":"","icon_font_size10":"","icon_link10":"","icon_link_target10":"blank","icon_padding10":"","icon_border_radius10":"","cache":"1","cache_time":"900","cachemode":"static"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10186, 'Favourite', 'template', 'favourite', '', 0, 1, 1, 0, '{"name":"Favourite","type":"template","creationDate":"September 2012","author":"FavThemes","copyright":"Copyright (C) 2012-2015 FavThemes. All rights reserved.","authorEmail":"hello@favthemes.com","authorUrl":"http:\\/\\/www.favthemes.com","version":"3.3","description":"\\n\\n<p style=\\"max-width: 900px; margin-bottom: 21px; text-align: justify;\\"><strong><a href=\\"http:\\/\\/demo.favthemes.com\\/favourite\\/\\" target=\\"_blank\\">Favourite<\\/a><\\/strong> is a Premium responsive Joomla! template with over 200 parameters for an easy and fast customization to suit your clients\\u2019 needs as quickly as possible. Design amazing responsive websites with this smart template that adapts and resizes itself to desktop and mobile devices, making your website looking great for everyone!\\n\\n<\\/br><\\/br>\\n\\n<strong><a href=\\"http:\\/\\/demo.favthemes.com\\/favourite\\/\\" target=\\"_blank\\">Favourite<\\/a><\\/strong> is built using Bootstrap, HTML5, CSS3, Google Fonts and Font Awesome. We also make all our templates compatible with K2, the powerful content extension for Joomla!.<\\/p>\\n\\n<a href=\\"http:\\/\\/demo.favthemes.com\\/favourite\\/\\" class=\\"btn btn-success\\" target=\\"_blank\\">Demo<\\/a>\\n\\n<a href=\\"http:\\/\\/www.favthemes.com\\/docs.html\\" class=\\"btn btn-info\\" target=\\"_blank\\">Documentation<\\/a>\\n\\n<\\/br><\\/br>\\n\\n<a href=\\"http:\\/\\/demo.favthemes.com\\/favourite\\/\\" target=\\"_blank\\">\\n  <img class=\\"fav-admin-img\\" style=\\"padding: 4px; border: 1px solid #DDD; border-radius: 4px;\\" src=\\"..\\/templates\\/favourite\\/template_preview.png\\" alt=\\"Favourite Responsive Template\\">\\n<\\/a>\\n\\n<\\/br><\\/br>\\nCopyright &#169; 2012-2015 <a href=\\"http:\\/\\/www.favthemes.com\\" target=\\"_blank\\" style=\\"font-weight: bold;\\">FavThemes<\\/a>.\\n<\\/br><\\/br>\\n\\n<script src=\\"..\\/templates\\/favourite\\/admin\\/jscolor\\/jscolor.js\\" type=\\"text\\/javascript\\"><\\/script>\\n<link rel=\\"stylesheet\\" href=\\"..\\/templates\\/favourite\\/admin\\/admin.css\\" type=\\"text\\/css\\" \\/>\\n\\n","group":"","filename":"templateDetails"}', '{"template_styles":"style1","max_width":"","show_copyright":"1","copyright_text":"FavThemes","copyright_text_link":"www.favthemes.com","header_style":"fav-dark","header_position":"fav-fixed","header_fixed_height":"","header_fixed_height_tablet":"","nav_style":"navstyle-1","nav_style_icon":"navstyle-icon-horizontal","nav_style_color":"","nav_google_font":"","nav_google_font_weight":"400","nav_google_font_style":"normal","nav_text_transform":"uppercase","nav_font_size":"","nav_link_padding":"","nav_icons_color":"","nav_icons_font_size":"","product_img_padding":"0","link_color":"","link_color_hover":"","titles_google_font":"","titles_google_font_weight":"400","titles_google_font_style":"normal","titles_text_align":"left","titles_text_transform":"none","article_title_color":"","article_title_link_color":"","article_title_link_hover_color":"","article_title_font_size":"","article_title_line_height":"","module_title_color":"","module_title_font_size":"","module_title_line_height":"","module_title_icon_color":"","module_title_icon_font_size":"","module_title_icon_padding":"","module_title_icon_border_color":"","module_title_icon_border_radius":"","variation_style_color":"","variation_style_icon_font_size":"","variation_style_icon_padding":"","button_color":"","button_bg_color":"","button_hover_color":"","button_bg_hover_color":"","button_google_font":"","button_google_font_weight":"400","button_google_font_style":"normal","button_text_transform":"","vertical_menus_style_color":"","vertical_menus_text_transform":"none","horizontal_menus_style_color":"","horizontal_menus_google_font":"","horizontal_menus_google_font_weight":"400","horizontal_menus_google_font_style":"normal","horizontal_menus_text_transform":"none","list_style_color":"","show_back_to_top":"1","error_page_article_id":"3","offline_page_style":"offline-dark","offline_page_bg_image_style":"no-repeat; background-attachment: fixed; -webkit-background-size: cover; -moz-background-size: cover; -o-background-size: cover; background-size: cover;","responsive_k2_image":"100%!important","body_bg_image_style":"repeat; background-attachment: initial; -webkit-background-size: auto; -moz-background-size: auto; -o-background-size: auto; background-size: auto;","body_bg_image_overlay":"fav-transparent","body_bg_color":"","body_color":"","body_title_color":"","body_link_color":"","body_link_hover_color":"","show_advert_button":"1","advert_bg_color":"","advert_color":"","advert_title_color":"","advert_link_color":"","advert_link_hover_color":"","topbar_bg_image_style":"repeat; background-attachment: initial; -webkit-background-size: auto; -moz-background-size: auto; -o-background-size: auto; background-size: auto;","topbar_bg_image_overlay":"fav-transparent","topbar_bg_color":"","topbar_color":"","topbar_title_color":"","topbar_link_color":"","topbar_link_hover_color":"","slide_width":"","slide_bg_image_style":"repeat; background-attachment: initial; -webkit-background-size: auto; -moz-background-size: auto; -o-background-size: auto; background-size: auto;","slide_bg_image_overlay":"fav-transparent","slide_bg_color":"","slide_color":"","slide_title_color":"","slide_link_color":"","slide_link_hover_color":"","intro_bg_image_style":"no-repeat; background-attachment: fixed; -webkit-background-size: cover; -moz-background-size: cover; -o-background-size: cover; background-size: cover;","intro_bg_image_overlay":"fav-overlay","intro_bg_color":"","intro_color":"","intro_title_color":"","intro_link_color":"","intro_link_hover_color":"","lead_bg_image_style":"repeat; background-attachment: initial; -webkit-background-size: auto; -moz-background-size: auto; -o-background-size: auto; background-size: auto;","lead_bg_image_overlay":"fav-transparent","lead_bg_color":"","lead_color":"","lead_title_color":"","lead_link_color":"","lead_link_hover_color":"","promo_bg_image_style":"repeat; background-attachment: initial; -webkit-background-size: auto; -moz-background-size: auto; -o-background-size: auto; background-size: auto;","promo_bg_image_overlay":"fav-transparent","promo_bg_color":"","promo_color":"","promo_title_color":"","promo_link_color":"","promo_link_hover_color":"","prime_bg_image_style":"no-repeat; background-attachment: fixed; -webkit-background-size: cover; -moz-background-size: cover; -o-background-size: cover; background-size: cover;","prime_bg_image_overlay":"fav-overlay","prime_bg_color":"","prime_color":"","prime_title_color":"","prime_link_color":"","prime_link_hover_color":"","showcase_bg_image_style":"repeat; background-attachment: initial; -webkit-background-size: auto; -moz-background-size: auto; -o-background-size: auto; background-size: auto;","showcase_bg_image_overlay":"fav-transparent","showcase_bg_color":"","showcase_color":"","showcase_title_color":"","showcase_link_color":"","showcase_link_hover_color":"","feature_bg_image_style":"repeat; background-attachment: initial; -webkit-background-size: auto; -moz-background-size: auto; -o-background-size: auto; background-size: auto;","feature_bg_image_overlay":"fav-transparent","feature_bg_color":"","feature_color":"","feature_title_color":"","feature_link_color":"","feature_link_hover_color":"","focus_bg_image_style":"repeat; background-attachment: initial; -webkit-background-size: auto; -moz-background-size: auto; -o-background-size: auto; background-size: auto;","focus_bg_image_overlay":"fav-transparent","focus_bg_color":"","focus_color":"","focus_title_color":"","focus_link_color":"","focus_link_hover_color":"","portfolio_bg_image_style":"repeat; background-attachment: initial; -webkit-background-size: auto; -moz-background-size: auto; -o-background-size: auto; background-size: auto;","portfolio_bg_image_overlay":"fav-transparent","portfolio_bg_color":"","portfolio_color":"","portfolio_title_color":"","portfolio_link_color":"","portfolio_link_hover_color":"","screen_bg_image_style":"no-repeat; background-attachment: fixed; -webkit-background-size: cover; -moz-background-size: cover; -o-background-size: cover; background-size: cover;","screen_bg_image_overlay":"fav-overlay","screen_bg_color":"","screen_color":"","screen_title_color":"","screen_link_color":"","screen_link_hover_color":"","top_bg_image_style":"repeat; background-attachment: initial; -webkit-background-size: auto; -moz-background-size: auto; -o-background-size: auto; background-size: auto;","top_bg_image_overlay":"fav-transparent","top_bg_color":"","top_color":"","top_title_color":"","top_link_color":"","top_link_hover_color":"","maintop_bg_image_style":"repeat; background-attachment: initial; -webkit-background-size: auto; -moz-background-size: auto; -o-background-size: auto; background-size: auto;","maintop_bg_image_overlay":"fav-transparent","maintop_bg_color":"","maintop_color":"","maintop_title_color":"","maintop_link_color":"","maintop_link_hover_color":"","mainbottom_bg_image_style":"repeat; background-attachment: initial; -webkit-background-size: auto; -moz-background-size: auto; -o-background-size: auto; background-size: auto;","mainbottom_bg_image_overlay":"fav-transparent","mainbottom_bg_color":"","mainbottom_color":"","mainbottom_title_color":"","mainbottom_link_color":"","mainbottom_link_hover_color":"","bottom_bg_image_style":"repeat; background-attachment: initial; -webkit-background-size: auto; -moz-background-size: auto; -o-background-size: auto; background-size: auto;","bottom_bg_image_overlay":"fav-transparent","bottom_bg_color":"","bottom_color":"","bottom_title_color":"","bottom_link_color":"","bottom_link_hover_color":"","note_bg_image_style":"repeat; background-attachment: initial; -webkit-background-size: auto; -moz-background-size: auto; -o-background-size: auto; background-size: auto;","note_bg_image_overlay":"fav-transparent","note_bg_color":"","note_color":"","note_title_color":"","note_link_color":"","note_link_hover_color":"","base_bg_image_style":"no-repeat; background-attachment: fixed; -webkit-background-size: cover; -moz-background-size: cover; -o-background-size: cover; background-size: cover;","base_bg_image_overlay":"fav-overlay","base_bg_color":"","base_color":"","base_title_color":"","base_link_color":"","base_link_hover_color":"","block_bg_image_style":"repeat; background-attachment: initial; -webkit-background-size: auto; -moz-background-size: auto; -o-background-size: auto; background-size: auto;","block_bg_image_overlay":"fav-transparent","block_bg_color":"","block_color":"","block_title_color":"","block_link_color":"","block_link_hover_color":"","user_bg_image_style":"repeat; background-attachment: initial; -webkit-background-size: auto; -moz-background-size: auto; -o-background-size: auto; background-size: auto;","user_bg_image_overlay":"fav-transparent","user_bg_color":"","user_color":"","user_title_color":"","user_link_color":"","user_link_hover_color":"","footer_bg_image_style":"repeat; background-attachment: initial; -webkit-background-size: auto; -moz-background-size: auto; -o-background-size: auto; background-size: auto;","footer_bg_image_overlay":"fav-transparent","footer_bg_color":"","footer_color":"","footer_title_color":"","footer_link_color":"","footer_link_hover_color":"","show_default_logo":"1","default_logo":"logo.png","default_logo_img_alt":"Favourite template","default_logo_padding":"","default_logo_margin":"","show_media_logo":"0","media_logo_img_alt":"Favourite template","media_logo_padding":"","media_logo_margin":"","show_text_logo":"0","text_logo":"Favourite","text_logo_color":"","text_logo_font_size":"","text_logo_google_font":"","text_logo_google_font_weight":"400","text_logo_google_font_style":"normal","text_logo_line_height":"","text_logo_padding":"","text_logo_margin":"","show_slogan":"0","slogan":"slogan text here","slogan_color":"","slogan_font_size":"","slogan_line_height":"","slogan_padding":"","slogan_margin":"","show_retina_logo":"0","retina_logo_height":"52px","retina_logo_width":"188px","retina_logo_img_alt":"Favourite template","retina_logo_padding":"0px","retina_logo_margin":"0px","mobile_nav_color":"navbar","show_mobile_submenu":"none","show_mobile_menu_text":"1","mobile_menu_text":"Menu","mobile_show_images":"inline-base","paragraph_mobile_font_size":"","article_mobile_title_font_size":"","module_mobile_title_font_size":""}', '', '', 0, '0000-00-00 00:00:00', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `#__finder_filters`
--

DROP TABLE IF EXISTS `#__finder_filters`;
CREATE TABLE IF NOT EXISTS `#__finder_filters` (
  `filter_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `alias` varchar(255) NOT NULL,
  `state` tinyint(1) NOT NULL DEFAULT '1',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(10) unsigned NOT NULL,
  `created_by_alias` varchar(255) NOT NULL,
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(10) unsigned NOT NULL DEFAULT '0',
  `checked_out` int(10) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `map_count` int(10) unsigned NOT NULL DEFAULT '0',
  `data` text NOT NULL,
  `params` mediumtext,
  PRIMARY KEY (`filter_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `#__finder_links`
--

DROP TABLE IF EXISTS `#__finder_links`;
CREATE TABLE IF NOT EXISTS `#__finder_links` (
  `link_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `url` varchar(255) NOT NULL,
  `route` varchar(255) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `indexdate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `md5sum` varchar(32) DEFAULT NULL,
  `published` tinyint(1) NOT NULL DEFAULT '1',
  `state` int(5) DEFAULT '1',
  `access` int(5) DEFAULT '0',
  `language` varchar(8) NOT NULL,
  `publish_start_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_end_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `start_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `end_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `list_price` double unsigned NOT NULL DEFAULT '0',
  `sale_price` double unsigned NOT NULL DEFAULT '0',
  `type_id` int(11) NOT NULL,
  `object` mediumblob NOT NULL,
  PRIMARY KEY (`link_id`),
  KEY `idx_type` (`type_id`),
  KEY `idx_title` (`title`),
  KEY `idx_md5` (`md5sum`),
  KEY `idx_url` (`url`(75)),
  KEY `idx_published_list` (`published`,`state`,`access`,`publish_start_date`,`publish_end_date`,`list_price`),
  KEY `idx_published_sale` (`published`,`state`,`access`,`publish_start_date`,`publish_end_date`,`sale_price`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

-- --------------------------------------------------------

--
-- Table structure for table `#__finder_links_terms0`
--

DROP TABLE IF EXISTS `#__finder_links_terms0`;
CREATE TABLE IF NOT EXISTS `#__finder_links_terms0` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `#__finder_links_terms1`
--

DROP TABLE IF EXISTS `#__finder_links_terms1`;
CREATE TABLE IF NOT EXISTS `#__finder_links_terms1` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `#__finder_links_terms2`
--

DROP TABLE IF EXISTS `#__finder_links_terms2`;
CREATE TABLE IF NOT EXISTS `#__finder_links_terms2` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `#__finder_links_terms3`
--

DROP TABLE IF EXISTS `#__finder_links_terms3`;
CREATE TABLE IF NOT EXISTS `#__finder_links_terms3` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `#__finder_links_terms4`
--

DROP TABLE IF EXISTS `#__finder_links_terms4`;
CREATE TABLE IF NOT EXISTS `#__finder_links_terms4` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `#__finder_links_terms5`
--

DROP TABLE IF EXISTS `#__finder_links_terms5`;
CREATE TABLE IF NOT EXISTS `#__finder_links_terms5` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `#__finder_links_terms6`
--

DROP TABLE IF EXISTS `#__finder_links_terms6`;
CREATE TABLE IF NOT EXISTS `#__finder_links_terms6` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `#__finder_links_terms7`
--

DROP TABLE IF EXISTS `#__finder_links_terms7`;
CREATE TABLE IF NOT EXISTS `#__finder_links_terms7` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `#__finder_links_terms8`
--

DROP TABLE IF EXISTS `#__finder_links_terms8`;
CREATE TABLE IF NOT EXISTS `#__finder_links_terms8` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `#__finder_links_terms9`
--

DROP TABLE IF EXISTS `#__finder_links_terms9`;
CREATE TABLE IF NOT EXISTS `#__finder_links_terms9` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `#__finder_links_termsa`
--

DROP TABLE IF EXISTS `#__finder_links_termsa`;
CREATE TABLE IF NOT EXISTS `#__finder_links_termsa` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `#__finder_links_termsb`
--

DROP TABLE IF EXISTS `#__finder_links_termsb`;
CREATE TABLE IF NOT EXISTS `#__finder_links_termsb` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `#__finder_links_termsc`
--

DROP TABLE IF EXISTS `#__finder_links_termsc`;
CREATE TABLE IF NOT EXISTS `#__finder_links_termsc` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `#__finder_links_termsd`
--

DROP TABLE IF EXISTS `#__finder_links_termsd`;
CREATE TABLE IF NOT EXISTS `#__finder_links_termsd` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `#__finder_links_termse`
--

DROP TABLE IF EXISTS `#__finder_links_termse`;
CREATE TABLE IF NOT EXISTS `#__finder_links_termse` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `#__finder_links_termsf`
--

DROP TABLE IF EXISTS `#__finder_links_termsf`;
CREATE TABLE IF NOT EXISTS `#__finder_links_termsf` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `#__finder_taxonomy`
--

DROP TABLE IF EXISTS `#__finder_taxonomy`;
CREATE TABLE IF NOT EXISTS `#__finder_taxonomy` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) unsigned NOT NULL DEFAULT '0',
  `title` varchar(255) NOT NULL,
  `state` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `access` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `ordering` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `parent_id` (`parent_id`),
  KEY `state` (`state`),
  KEY `ordering` (`ordering`),
  KEY `access` (`access`),
  KEY `idx_parent_published` (`parent_id`,`state`,`access`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=23 ;

-- --------------------------------------------------------

--
-- Table structure for table `#__finder_taxonomy_map`
--

DROP TABLE IF EXISTS `#__finder_taxonomy_map`;
CREATE TABLE IF NOT EXISTS `#__finder_taxonomy_map` (
  `link_id` int(10) unsigned NOT NULL,
  `node_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`node_id`),
  KEY `link_id` (`link_id`),
  KEY `node_id` (`node_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `#__finder_terms`
--

DROP TABLE IF EXISTS `#__finder_terms`;
CREATE TABLE IF NOT EXISTS `#__finder_terms` (
  `term_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `term` varchar(75) NOT NULL,
  `stem` varchar(75) NOT NULL,
  `common` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `phrase` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `weight` float unsigned NOT NULL DEFAULT '0',
  `soundex` varchar(75) NOT NULL,
  `links` int(10) NOT NULL DEFAULT '0',
  `language` char(3) NOT NULL DEFAULT '',
  PRIMARY KEY (`term_id`),
  UNIQUE KEY `idx_term` (`term`),
  KEY `idx_term_phrase` (`term`,`phrase`),
  KEY `idx_stem_phrase` (`stem`,`phrase`),
  KEY `idx_soundex_phrase` (`soundex`,`phrase`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1876 ;

-- --------------------------------------------------------

--
-- Table structure for table `#__finder_terms_common`
--

DROP TABLE IF EXISTS `#__finder_terms_common`;
CREATE TABLE IF NOT EXISTS `#__finder_terms_common` (
  `term` varchar(75) NOT NULL,
  `language` varchar(3) NOT NULL,
  KEY `idx_word_lang` (`term`,`language`),
  KEY `idx_lang` (`language`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `#__finder_tokens`
--

DROP TABLE IF EXISTS `#__finder_tokens`;
CREATE TABLE IF NOT EXISTS `#__finder_tokens` (
  `term` varchar(75) NOT NULL,
  `stem` varchar(75) NOT NULL,
  `common` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `phrase` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `weight` float unsigned NOT NULL DEFAULT '1',
  `context` tinyint(1) unsigned NOT NULL DEFAULT '2',
  `language` char(3) NOT NULL DEFAULT '',
  KEY `idx_word` (`term`),
  KEY `idx_context` (`context`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `#__finder_tokens_aggregate`
--

DROP TABLE IF EXISTS `#__finder_tokens_aggregate`;
CREATE TABLE IF NOT EXISTS `#__finder_tokens_aggregate` (
  `term_id` int(10) unsigned NOT NULL,
  `map_suffix` char(1) NOT NULL,
  `term` varchar(75) NOT NULL,
  `stem` varchar(75) NOT NULL,
  `common` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `phrase` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `term_weight` float unsigned NOT NULL,
  `context` tinyint(1) unsigned NOT NULL DEFAULT '2',
  `context_weight` float unsigned NOT NULL,
  `total_weight` float unsigned NOT NULL,
  `language` char(3) NOT NULL DEFAULT '',
  KEY `token` (`term`),
  KEY `keyword_id` (`term_id`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `#__finder_types`
--

DROP TABLE IF EXISTS `#__finder_types`;
CREATE TABLE IF NOT EXISTS `#__finder_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `mime` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `title` (`title`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

-- --------------------------------------------------------

--
-- Table structure for table `#__k2_attachments`
--

DROP TABLE IF EXISTS `#__k2_attachments`;
CREATE TABLE IF NOT EXISTS `#__k2_attachments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `itemID` int(11) NOT NULL,
  `filename` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `titleAttribute` text NOT NULL,
  `hits` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `itemID` (`itemID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `#__k2_categories`
--

DROP TABLE IF EXISTS `#__k2_categories`;
CREATE TABLE IF NOT EXISTS `#__k2_categories` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `alias` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `parent` int(11) DEFAULT '0',
  `extraFieldsGroup` int(11) NOT NULL,
  `published` smallint(6) NOT NULL DEFAULT '0',
  `access` int(11) NOT NULL DEFAULT '0',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `image` varchar(255) NOT NULL,
  `params` text NOT NULL,
  `trash` smallint(6) NOT NULL DEFAULT '0',
  `plugins` text NOT NULL,
  `language` char(7) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `category` (`published`,`access`,`trash`),
  KEY `parent` (`parent`),
  KEY `ordering` (`ordering`),
  KEY `published` (`published`),
  KEY `access` (`access`),
  KEY `trash` (`trash`),
  KEY `language` (`language`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `#__k2_categories`
--

INSERT INTO `#__k2_categories` (`id`, `name`, `alias`, `description`, `parent`, `extraFieldsGroup`, `published`, `access`, `ordering`, `image`, `params`, `trash`, `plugins`, `language`) VALUES
(1, 'Demo', 'demo', '', 0, 0, 1, 1, 1, '', '{"inheritFrom":"0","theme":"","num_leading_items":"1","num_leading_columns":"1","leadingImgSize":"XLarge","num_primary_items":"2","num_primary_columns":"2","primaryImgSize":"Medium","num_secondary_items":"0","num_secondary_columns":"0","secondaryImgSize":"Small","num_links":"1","num_links_columns":"1","linksImgSize":"none","catCatalogMode":"0","catFeaturedItems":"1","catOrdering":"order","catPagination":"2","catPaginationResults":"1","catTitle":"1","catTitleItemCounter":"1","catDescription":"1","catImage":"1","catFeedLink":"0","catFeedIcon":"0","subCategories":"1","subCatColumns":"2","subCatOrdering":"","subCatTitle":"1","subCatTitleItemCounter":"1","subCatDescription":"1","subCatImage":"1","itemImageXS":"","itemImageS":"","itemImageM":"","itemImageL":"","itemImageXL":"800","catItemTitle":"1","catItemTitleLinked":"1","catItemFeaturedNotice":"1","catItemAuthor":"1","catItemDateCreated":"1","catItemRating":"0","catItemImage":"1","catItemIntroText":"1","catItemIntroTextWordLimit":"","catItemExtraFields":"1","catItemHits":"1","catItemCategory":"1","catItemTags":"1","catItemAttachments":"1","catItemAttachmentsCounter":"1","catItemVideo":"1","catItemVideoWidth":"","catItemVideoHeight":"","catItemAudioWidth":"","catItemAudioHeight":"","catItemVideoAutoPlay":"0","catItemImageGallery":"0","catItemDateModified":"1","catItemReadMore":"1","catItemCommentsAnchor":"1","catItemK2Plugins":"1","itemDateCreated":"1","itemTitle":"1","itemFeaturedNotice":"1","itemAuthor":"1","itemFontResizer":"1","itemPrintButton":"1","itemEmailButton":"1","itemSocialButton":"1","itemVideoAnchor":"1","itemImageGalleryAnchor":"1","itemCommentsAnchor":"1","itemRating":"0","itemImage":"1","itemImgSize":"XLarge","itemImageMainCaption":"1","itemImageMainCredits":"1","itemIntroText":"1","itemFullText":"1","itemExtraFields":"1","itemDateModified":"1","itemHits":"1","itemCategory":"1","itemTags":"1","itemAttachments":"1","itemAttachmentsCounter":"1","itemVideo":"1","itemVideoWidth":"","itemVideoHeight":"","itemAudioWidth":"","itemAudioHeight":"","itemVideoAutoPlay":"0","itemVideoCaption":"1","itemVideoCredits":"1","itemImageGallery":"1","itemNavigation":"1","itemComments":"1","itemTwitterButton":"1","itemFacebookButton":"1","itemGooglePlusOneButton":"1","itemAuthorBlock":"0","itemAuthorImage":"0","itemAuthorDescription":"0","itemAuthorURL":"0","itemAuthorEmail":"0","itemAuthorLatest":"0","itemAuthorLatestLimit":"5","itemRelated":"1","itemRelatedLimit":"5","itemRelatedTitle":"1","itemRelatedCategory":"1","itemRelatedImageSize":"0","itemRelatedIntrotext":"1","itemRelatedFulltext":"1","itemRelatedAuthor":"1","itemRelatedMedia":"1","itemRelatedImageGallery":"1","itemK2Plugins":"1","catMetaDesc":"","catMetaKey":"","catMetaRobots":"","catMetaAuthor":""}', 0, '', '*');

-- --------------------------------------------------------

--
-- Table structure for table `#__k2_comments`
--

DROP TABLE IF EXISTS `#__k2_comments`;
CREATE TABLE IF NOT EXISTS `#__k2_comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `itemID` int(11) NOT NULL,
  `userID` int(11) NOT NULL,
  `userName` varchar(255) NOT NULL,
  `commentDate` datetime NOT NULL,
  `commentText` text NOT NULL,
  `commentEmail` varchar(255) NOT NULL,
  `commentURL` varchar(255) NOT NULL,
  `published` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `itemID` (`itemID`),
  KEY `userID` (`userID`),
  KEY `published` (`published`),
  KEY `latestComments` (`published`,`commentDate`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `#__k2_comments`
--

INSERT INTO `#__k2_comments` (`id`, `itemID`, `userID`, `userName`, `commentDate`, `commentText`, `commentEmail`, `commentURL`, `published`) VALUES
(1, 1, 0, 'FavThemes', '2015-04-12 20:33:27', 'Hi! I''m a demo comment about this awesome theme!', 'demo@favthemes.com', 'http://www.favthemes.com', 1);

-- --------------------------------------------------------

--
-- Table structure for table `#__k2_extra_fields`
--

DROP TABLE IF EXISTS `#__k2_extra_fields`;
CREATE TABLE IF NOT EXISTS `#__k2_extra_fields` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `value` text NOT NULL,
  `type` varchar(255) NOT NULL,
  `group` int(11) NOT NULL,
  `published` tinyint(4) NOT NULL,
  `ordering` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `group` (`group`),
  KEY `published` (`published`),
  KEY `ordering` (`ordering`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `#__k2_extra_fields_groups`
--

DROP TABLE IF EXISTS `#__k2_extra_fields_groups`;
CREATE TABLE IF NOT EXISTS `#__k2_extra_fields_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `#__k2_items`
--

DROP TABLE IF EXISTS `#__k2_items`;
CREATE TABLE IF NOT EXISTS `#__k2_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `alias` varchar(255) DEFAULT NULL,
  `catid` int(11) NOT NULL,
  `published` smallint(6) NOT NULL DEFAULT '0',
  `introtext` mediumtext NOT NULL,
  `fulltext` mediumtext NOT NULL,
  `video` text,
  `gallery` varchar(255) DEFAULT NULL,
  `extra_fields` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `extra_fields_search` text NOT NULL,
  `created` datetime NOT NULL,
  `created_by` int(11) NOT NULL DEFAULT '0',
  `created_by_alias` varchar(255) NOT NULL,
  `checked_out` int(10) unsigned NOT NULL,
  `checked_out_time` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `modified_by` int(11) NOT NULL DEFAULT '0',
  `publish_up` datetime NOT NULL,
  `publish_down` datetime NOT NULL,
  `trash` smallint(6) NOT NULL DEFAULT '0',
  `access` int(11) NOT NULL DEFAULT '0',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `featured` smallint(6) NOT NULL DEFAULT '0',
  `featured_ordering` int(11) NOT NULL DEFAULT '0',
  `image_caption` text NOT NULL,
  `image_credits` varchar(255) NOT NULL,
  `video_caption` text NOT NULL,
  `video_credits` varchar(255) NOT NULL,
  `hits` int(10) unsigned NOT NULL,
  `params` text NOT NULL,
  `metadesc` text NOT NULL,
  `metadata` text NOT NULL,
  `metakey` text NOT NULL,
  `plugins` text NOT NULL,
  `language` char(7) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `item` (`published`,`publish_up`,`publish_down`,`trash`,`access`),
  KEY `catid` (`catid`),
  KEY `created_by` (`created_by`),
  KEY `ordering` (`ordering`),
  KEY `featured` (`featured`),
  KEY `featured_ordering` (`featured_ordering`),
  KEY `hits` (`hits`),
  KEY `created` (`created`),
  KEY `language` (`language`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `#__k2_items`
--

INSERT INTO `#__k2_items` (`id`, `title`, `alias`, `catid`, `published`, `introtext`, `fulltext`, `video`, `gallery`, `extra_fields`, `extra_fields_search`, `created`, `created_by`, `created_by_alias`, `checked_out`, `checked_out_time`, `modified`, `modified_by`, `publish_up`, `publish_down`, `trash`, `access`, `ordering`, `featured`, `featured_ordering`, `image_caption`, `image_credits`, `video_caption`, `video_credits`, `hits`, `params`, `metadesc`, `metadata`, `metakey`, `plugins`, `language`) VALUES
(1, 'Say hello to Favourite!', 'say-hello-to-favourite', 1, 1, '<p>We have created this awesome responsive template with over 200 settings, so that you can customize it quickly and easily for any project! Being responsive, this is a smart template that adapts and resizes itself to desktop and mobile devices.</p>\r\n<p>Our themes are built using Bootstrap, HTML5, CSS3, Google Fonts and Font Awesome, while adding support for K2. Take a look at the <a href="index.php/documentation" target="_self">docs page</a> and explore the demo to discover the amazing features of this template!</p>\r\n<p>This free template can be used for unlimited personal or commercial websites, just don''t resell it.</p>\r\n', '\r\n<p>This Premium product also has extensive <a href="http://favthemes.com/tutorials.html" target="_blank">tutorials</a> for downloading, setting up and using the parameters of the template.</p>', NULL, NULL, '[]', '', '2015-04-12 16:03:22', 851, '', 0, '0000-00-00 00:00:00', '2015-11-27 15:39:08', 851, '2015-04-12 16:03:22', '0000-00-00 00:00:00', 0, 1, 1, 1, 0, '', '', '', '', 70, '{"catItemTitle":"","catItemTitleLinked":"","catItemFeaturedNotice":"","catItemAuthor":"","catItemDateCreated":"","catItemRating":"","catItemImage":"","catItemIntroText":"","catItemExtraFields":"","catItemHits":"","catItemCategory":"","catItemTags":"","catItemAttachments":"","catItemAttachmentsCounter":"","catItemVideo":"","catItemVideoWidth":"","catItemVideoHeight":"","catItemAudioWidth":"","catItemAudioHeight":"","catItemVideoAutoPlay":"","catItemImageGallery":"","catItemDateModified":"","catItemReadMore":"","catItemCommentsAnchor":"","catItemK2Plugins":"","itemDateCreated":"","itemTitle":"","itemFeaturedNotice":"","itemAuthor":"","itemFontResizer":"","itemPrintButton":"","itemEmailButton":"","itemSocialButton":"","itemVideoAnchor":"","itemImageGalleryAnchor":"","itemCommentsAnchor":"","itemRating":"","itemImage":"","itemImgSize":"","itemImageMainCaption":"","itemImageMainCredits":"","itemIntroText":"","itemFullText":"","itemExtraFields":"","itemDateModified":"","itemHits":"","itemCategory":"","itemTags":"","itemAttachments":"","itemAttachmentsCounter":"","itemVideo":"","itemVideoWidth":"","itemVideoHeight":"","itemAudioWidth":"","itemAudioHeight":"","itemVideoAutoPlay":"","itemVideoCaption":"","itemVideoCredits":"","itemImageGallery":"","itemNavigation":"","itemComments":"","itemTwitterButton":"","itemFacebookButton":"","itemGooglePlusOneButton":"","itemAuthorBlock":"","itemAuthorImage":"","itemAuthorDescription":"","itemAuthorURL":"","itemAuthorEmail":"","itemAuthorLatest":"","itemAuthorLatestLimit":"","itemRelated":"","itemRelatedLimit":"","itemRelatedTitle":"","itemRelatedCategory":"","itemRelatedImageSize":"","itemRelatedIntrotext":"","itemRelatedFulltext":"","itemRelatedAuthor":"","itemRelatedMedia":"","itemRelatedImageGallery":"","itemK2Plugins":""}', '', 'robots=\nauthor=', '', '', '*'),
(2, 'Customize everything', 'customize-everything', 1, 1, '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Elit mus aliquet quisquam aute numquam assumenda unde libero dolores nostrud reprehenderit odit?</p>\r\n<p>Vivamus sit amet libero turpis, non venenatis urna. In blandit, odio convallis suscipit venenatis, ante ipsum cursus augue.</p>\r\n', '\r\n<p>Elit mus aliquet quisquam aute numquam assumenda unde libero dolores nostrud reprehenderit odit? Proin facilisi molestiae dolorem duis, consequuntur lectus, vestibulum risus eleifend provident, ultricies perspiciatis hac molestie conubia eius, aute, consequatur, pellentesque.</p>', NULL, NULL, '[]', '', '2015-04-12 16:37:32', 851, '', 0, '0000-00-00 00:00:00', '2015-11-27 09:52:34', 851, '2015-04-12 16:37:32', '0000-00-00 00:00:00', 0, 1, 2, 0, 0, '', '', '', '', 2, '{"catItemTitle":"","catItemTitleLinked":"","catItemFeaturedNotice":"","catItemAuthor":"","catItemDateCreated":"","catItemRating":"","catItemImage":"","catItemIntroText":"","catItemExtraFields":"","catItemHits":"","catItemCategory":"","catItemTags":"","catItemAttachments":"","catItemAttachmentsCounter":"","catItemVideo":"","catItemVideoWidth":"","catItemVideoHeight":"","catItemAudioWidth":"","catItemAudioHeight":"","catItemVideoAutoPlay":"","catItemImageGallery":"","catItemDateModified":"","catItemReadMore":"","catItemCommentsAnchor":"","catItemK2Plugins":"","itemDateCreated":"","itemTitle":"","itemFeaturedNotice":"","itemAuthor":"","itemFontResizer":"","itemPrintButton":"","itemEmailButton":"","itemSocialButton":"","itemVideoAnchor":"","itemImageGalleryAnchor":"","itemCommentsAnchor":"","itemRating":"","itemImage":"","itemImgSize":"","itemImageMainCaption":"","itemImageMainCredits":"","itemIntroText":"","itemFullText":"","itemExtraFields":"","itemDateModified":"","itemHits":"","itemCategory":"","itemTags":"","itemAttachments":"","itemAttachmentsCounter":"","itemVideo":"","itemVideoWidth":"","itemVideoHeight":"","itemAudioWidth":"","itemAudioHeight":"","itemVideoAutoPlay":"","itemVideoCaption":"","itemVideoCredits":"","itemImageGallery":"","itemNavigation":"","itemComments":"","itemTwitterButton":"","itemFacebookButton":"","itemGooglePlusOneButton":"","itemAuthorBlock":"","itemAuthorImage":"","itemAuthorDescription":"","itemAuthorURL":"","itemAuthorEmail":"","itemAuthorLatest":"","itemAuthorLatestLimit":"","itemRelated":"","itemRelatedLimit":"","itemRelatedTitle":"","itemRelatedCategory":"","itemRelatedImageSize":"","itemRelatedIntrotext":"","itemRelatedFulltext":"","itemRelatedAuthor":"","itemRelatedMedia":"","itemRelatedImageGallery":"","itemK2Plugins":""}', '', 'robots=\nauthor=', '', '', '*'),
(3, 'Responsive theme', 'responsive-theme', 1, 1, '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Elit mus aliquet quisquam aute numquam assumenda unde libero dolores nostrud reprehenderit odit?</p>\r\n<p>Vivamus sit amet libero turpis, non venenatis urna. In blandit, odio convallis suscipit venenatis, ante ipsum cursus augue.</p>\r\n', '\r\n<p>Elit mus aliquet quisquam aute numquam assumenda unde libero dolores nostrud reprehenderit odit? Proin facilisi molestiae dolorem duis, consequuntur lectus, vestibulum risus eleifend provident, ultricies perspiciatis hac molestie conubia eius, aute, consequatur, pellentesque.</p>', NULL, NULL, '[]', '', '2015-04-12 16:38:53', 851, '', 0, '0000-00-00 00:00:00', '2015-11-27 09:52:48', 851, '2015-04-12 16:38:53', '0000-00-00 00:00:00', 0, 1, 3, 0, 0, 'Item image caption here', 'Item image credits here', '', '', 0, '{"catItemTitle":"","catItemTitleLinked":"","catItemFeaturedNotice":"","catItemAuthor":"","catItemDateCreated":"","catItemRating":"","catItemImage":"","catItemIntroText":"","catItemExtraFields":"","catItemHits":"","catItemCategory":"","catItemTags":"","catItemAttachments":"","catItemAttachmentsCounter":"","catItemVideo":"","catItemVideoWidth":"","catItemVideoHeight":"","catItemAudioWidth":"","catItemAudioHeight":"","catItemVideoAutoPlay":"","catItemImageGallery":"","catItemDateModified":"","catItemReadMore":"","catItemCommentsAnchor":"","catItemK2Plugins":"","itemDateCreated":"","itemTitle":"","itemFeaturedNotice":"","itemAuthor":"","itemFontResizer":"","itemPrintButton":"","itemEmailButton":"","itemSocialButton":"","itemVideoAnchor":"","itemImageGalleryAnchor":"","itemCommentsAnchor":"","itemRating":"","itemImage":"","itemImgSize":"","itemImageMainCaption":"","itemImageMainCredits":"","itemIntroText":"","itemFullText":"","itemExtraFields":"","itemDateModified":"","itemHits":"","itemCategory":"","itemTags":"","itemAttachments":"","itemAttachmentsCounter":"","itemVideo":"","itemVideoWidth":"","itemVideoHeight":"","itemAudioWidth":"","itemAudioHeight":"","itemVideoAutoPlay":"","itemVideoCaption":"","itemVideoCredits":"","itemImageGallery":"","itemNavigation":"","itemComments":"","itemTwitterButton":"","itemFacebookButton":"","itemGooglePlusOneButton":"","itemAuthorBlock":"","itemAuthorImage":"","itemAuthorDescription":"","itemAuthorURL":"","itemAuthorEmail":"","itemAuthorLatest":"","itemAuthorLatestLimit":"","itemRelated":"","itemRelatedLimit":"","itemRelatedTitle":"","itemRelatedCategory":"","itemRelatedImageSize":"","itemRelatedIntrotext":"","itemRelatedFulltext":"","itemRelatedAuthor":"","itemRelatedMedia":"","itemRelatedImageGallery":"","itemK2Plugins":""}', '', 'robots=\nauthor=', '', '', '*'),
(4, 'Ready for mobile devices', 'ready-for-mobile-devices', 1, 1, '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Elit mus aliquet quisquam aute numquam assumenda unde libero dolores nostrud reprehenderit odit?</p>\r\n<p>Vivamus sit amet libero turpis, non venenatis urna. In blandit, odio convallis suscipit venenatis, ante ipsum cursus augue.</p>\r\n', '\r\n<p>Elit mus aliquet quisquam aute numquam assumenda unde libero dolores nostrud reprehenderit odit? Proin facilisi molestiae dolorem duis, consequuntur lectus, vestibulum risus eleifend provident, ultricies perspiciatis hac molestie conubia eius, aute, consequatur, pellentesque.</p>', NULL, NULL, '[]', '', '2015-04-12 16:42:27', 851, '', 0, '0000-00-00 00:00:00', '2015-11-27 09:53:14', 851, '2015-04-12 16:42:27', '0000-00-00 00:00:00', 0, 1, 4, 0, 0, '', '', '', '', 2, '{"catItemTitle":"","catItemTitleLinked":"","catItemFeaturedNotice":"","catItemAuthor":"","catItemDateCreated":"","catItemRating":"","catItemImage":"","catItemIntroText":"","catItemExtraFields":"","catItemHits":"","catItemCategory":"","catItemTags":"","catItemAttachments":"","catItemAttachmentsCounter":"","catItemVideo":"","catItemVideoWidth":"","catItemVideoHeight":"","catItemAudioWidth":"","catItemAudioHeight":"","catItemVideoAutoPlay":"","catItemImageGallery":"","catItemDateModified":"","catItemReadMore":"","catItemCommentsAnchor":"","catItemK2Plugins":"","itemDateCreated":"","itemTitle":"","itemFeaturedNotice":"","itemAuthor":"","itemFontResizer":"","itemPrintButton":"","itemEmailButton":"","itemSocialButton":"","itemVideoAnchor":"","itemImageGalleryAnchor":"","itemCommentsAnchor":"","itemRating":"","itemImage":"","itemImgSize":"","itemImageMainCaption":"","itemImageMainCredits":"","itemIntroText":"","itemFullText":"","itemExtraFields":"","itemDateModified":"","itemHits":"","itemCategory":"","itemTags":"","itemAttachments":"","itemAttachmentsCounter":"","itemVideo":"","itemVideoWidth":"","itemVideoHeight":"","itemAudioWidth":"","itemAudioHeight":"","itemVideoAutoPlay":"","itemVideoCaption":"","itemVideoCredits":"","itemImageGallery":"","itemNavigation":"","itemComments":"","itemTwitterButton":"","itemFacebookButton":"","itemGooglePlusOneButton":"","itemAuthorBlock":"","itemAuthorImage":"","itemAuthorDescription":"","itemAuthorURL":"","itemAuthorEmail":"","itemAuthorLatest":"","itemAuthorLatestLimit":"","itemRelated":"","itemRelatedLimit":"","itemRelatedTitle":"","itemRelatedCategory":"","itemRelatedImageSize":"","itemRelatedIntrotext":"","itemRelatedFulltext":"","itemRelatedAuthor":"","itemRelatedMedia":"","itemRelatedImageGallery":"","itemK2Plugins":""}', '', 'robots=\nauthor=', '', '', '*'),
(5, 'A brand new store', 'a-brand-new-store', 1, 1, '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Elit mus aliquet quisquam aute numquam assumenda unde libero dolores nostrud reprehenderit odit?</p>\r\n<p>Vivamus sit amet libero turpis, non venenatis urna. In blandit, odio convallis suscipit venenatis, ante ipsum cursus augue.</p>\r\n', '\r\n<p>Elit mus aliquet quisquam aute numquam assumenda unde libero dolores nostrud reprehenderit odit? Proin facilisi molestiae dolorem duis, consequuntur lectus, vestibulum risus eleifend provident, ultricies perspiciatis hac molestie conubia eius, aute, consequatur, pellentesque.</p>', NULL, NULL, '[]', '', '2015-04-12 16:44:00', 851, '', 0, '0000-00-00 00:00:00', '2015-11-27 09:53:36', 851, '2015-04-12 16:44:00', '0000-00-00 00:00:00', 0, 1, 5, 0, 0, '', '', '', '', 0, '{"catItemTitle":"","catItemTitleLinked":"","catItemFeaturedNotice":"","catItemAuthor":"","catItemDateCreated":"","catItemRating":"","catItemImage":"","catItemIntroText":"","catItemExtraFields":"","catItemHits":"","catItemCategory":"","catItemTags":"","catItemAttachments":"","catItemAttachmentsCounter":"","catItemVideo":"","catItemVideoWidth":"","catItemVideoHeight":"","catItemAudioWidth":"","catItemAudioHeight":"","catItemVideoAutoPlay":"","catItemImageGallery":"","catItemDateModified":"","catItemReadMore":"","catItemCommentsAnchor":"","catItemK2Plugins":"","itemDateCreated":"","itemTitle":"","itemFeaturedNotice":"","itemAuthor":"","itemFontResizer":"","itemPrintButton":"","itemEmailButton":"","itemSocialButton":"","itemVideoAnchor":"","itemImageGalleryAnchor":"","itemCommentsAnchor":"","itemRating":"","itemImage":"","itemImgSize":"","itemImageMainCaption":"","itemImageMainCredits":"","itemIntroText":"","itemFullText":"","itemExtraFields":"","itemDateModified":"","itemHits":"","itemCategory":"","itemTags":"","itemAttachments":"","itemAttachmentsCounter":"","itemVideo":"","itemVideoWidth":"","itemVideoHeight":"","itemAudioWidth":"","itemAudioHeight":"","itemVideoAutoPlay":"","itemVideoCaption":"","itemVideoCredits":"","itemImageGallery":"","itemNavigation":"","itemComments":"","itemTwitterButton":"","itemFacebookButton":"","itemGooglePlusOneButton":"","itemAuthorBlock":"","itemAuthorImage":"","itemAuthorDescription":"","itemAuthorURL":"","itemAuthorEmail":"","itemAuthorLatest":"","itemAuthorLatestLimit":"","itemRelated":"","itemRelatedLimit":"","itemRelatedTitle":"","itemRelatedCategory":"","itemRelatedImageSize":"","itemRelatedIntrotext":"","itemRelatedFulltext":"","itemRelatedAuthor":"","itemRelatedMedia":"","itemRelatedImageGallery":"","itemK2Plugins":""}', '', 'robots=\nauthor=', '', '', '*');

-- --------------------------------------------------------

--
-- Table structure for table `#__k2_rating`
--

DROP TABLE IF EXISTS `#__k2_rating`;
CREATE TABLE IF NOT EXISTS `#__k2_rating` (
  `itemID` int(11) NOT NULL DEFAULT '0',
  `rating_sum` int(11) unsigned NOT NULL DEFAULT '0',
  `rating_count` int(11) unsigned NOT NULL DEFAULT '0',
  `lastip` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`itemID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `#__k2_tags`
--

DROP TABLE IF EXISTS `#__k2_tags`;
CREATE TABLE IF NOT EXISTS `#__k2_tags` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `published` smallint(6) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `published` (`published`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `#__k2_tags`
--

INSERT INTO `#__k2_tags` (`id`, `name`, `published`) VALUES
(1, 'joomla', 1),
(2, 'premium', 1);

-- --------------------------------------------------------

--
-- Table structure for table `#__k2_tags_xref`
--

DROP TABLE IF EXISTS `#__k2_tags_xref`;
CREATE TABLE IF NOT EXISTS `#__k2_tags_xref` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tagID` int(11) NOT NULL,
  `itemID` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `tagID` (`tagID`),
  KEY `itemID` (`itemID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `#__k2_tags_xref`
--

INSERT INTO `#__k2_tags_xref` (`id`, `tagID`, `itemID`) VALUES
(6, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `#__k2_users`
--

DROP TABLE IF EXISTS `#__k2_users`;
CREATE TABLE IF NOT EXISTS `#__k2_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userID` int(11) NOT NULL,
  `userName` varchar(255) DEFAULT NULL,
  `gender` enum('m','f') NOT NULL DEFAULT 'm',
  `description` text NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `group` int(11) NOT NULL DEFAULT '0',
  `plugins` text NOT NULL,
  `ip` varchar(15) NOT NULL,
  `hostname` varchar(255) NOT NULL,
  `notes` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `userID` (`userID`),
  KEY `group` (`group`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `#__k2_users`
--

INSERT INTO `#__k2_users` (`id`, `userID`, `userName`, `gender`, `description`, `image`, `url`, `group`, `plugins`, `ip`, `hostname`, `notes`) VALUES
(1, 851, 'FavThemes Demo User', 'm', '<p>We craft themes that are responsive, elegant and highly customizable.</p>', NULL, 'http://www.favthemes.com', 3, '', '', '', 'Demo notes here');

-- --------------------------------------------------------

--
-- Table structure for table `#__k2_user_groups`
--

DROP TABLE IF EXISTS `#__k2_user_groups`;
CREATE TABLE IF NOT EXISTS `#__k2_user_groups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `permissions` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `#__k2_user_groups`
--

INSERT INTO `#__k2_user_groups` (`id`, `name`, `permissions`) VALUES
(3, 'Demo User Grup', '{"comment":"1","frontEdit":"1","add":"1","editOwn":"1","editAll":"1","publish":"1","editPublished":"1","inheritance":"0","categories":"all"}');

-- --------------------------------------------------------

--
-- Table structure for table `#__languages`
--

DROP TABLE IF EXISTS `#__languages`;
CREATE TABLE IF NOT EXISTS `#__languages` (
  `lang_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `lang_code` char(7) NOT NULL,
  `title` varchar(50) NOT NULL,
  `title_native` varchar(50) NOT NULL,
  `sef` varchar(50) NOT NULL,
  `image` varchar(50) NOT NULL,
  `description` varchar(512) NOT NULL,
  `metakey` text NOT NULL,
  `metadesc` text NOT NULL,
  `sitename` varchar(1024) NOT NULL DEFAULT '',
  `published` int(11) NOT NULL DEFAULT '0',
  `access` int(10) unsigned NOT NULL DEFAULT '0',
  `ordering` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`lang_id`),
  UNIQUE KEY `idx_sef` (`sef`),
  UNIQUE KEY `idx_image` (`image`),
  UNIQUE KEY `idx_langcode` (`lang_code`),
  KEY `idx_access` (`access`),
  KEY `idx_ordering` (`ordering`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `#__languages`
--

INSERT INTO `#__languages` (`lang_id`, `lang_code`, `title`, `title_native`, `sef`, `image`, `description`, `metakey`, `metadesc`, `sitename`, `published`, `access`, `ordering`) VALUES
(1, 'en-GB', 'English (UK)', 'English (UK)', 'en', 'en', '', '', '', '', 1, 1, -1);

-- --------------------------------------------------------

--
-- Table structure for table `#__menu`
--

DROP TABLE IF EXISTS `#__menu`;
CREATE TABLE IF NOT EXISTS `#__menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `menutype` varchar(24) NOT NULL COMMENT 'The type of menu this item belongs to. FK to #__menu_types.menutype',
  `title` varchar(255) NOT NULL COMMENT 'The display title of the menu item.',
  `alias` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'The SEF alias of the menu item.',
  `note` varchar(255) NOT NULL DEFAULT '',
  `path` varchar(1024) NOT NULL COMMENT 'The computed path of the menu item based on the alias field.',
  `link` varchar(1024) NOT NULL COMMENT 'The actually link the menu item refers to.',
  `type` varchar(16) NOT NULL COMMENT 'The type of link: Component, URL, Alias, Separator',
  `published` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'The published state of the menu link.',
  `parent_id` int(10) unsigned NOT NULL DEFAULT '1' COMMENT 'The parent menu item in the menu tree.',
  `level` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'The relative level in the tree.',
  `component_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'FK to #__extensions.id',
  `checked_out` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'FK to #__users.id',
  `checked_out_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'The time the menu item was checked out.',
  `browserNav` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'The click behaviour of the link.',
  `access` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'The access level required to view the menu item.',
  `img` varchar(255) NOT NULL COMMENT 'The image of the menu item.',
  `template_style_id` int(10) unsigned NOT NULL DEFAULT '0',
  `params` text NOT NULL COMMENT 'JSON encoded data for the menu item.',
  `lft` int(11) NOT NULL DEFAULT '0' COMMENT 'Nested set lft.',
  `rgt` int(11) NOT NULL DEFAULT '0' COMMENT 'Nested set rgt.',
  `home` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT 'Indicates if this menu item is the home or default page.',
  `language` char(7) NOT NULL DEFAULT '',
  `client_id` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_client_id_parent_id_alias_language` (`client_id`,`parent_id`,`alias`,`language`),
  KEY `idx_componentid` (`component_id`,`menutype`,`published`,`access`),
  KEY `idx_menutype` (`menutype`),
  KEY `idx_left_right` (`lft`,`rgt`),
  KEY `idx_alias` (`alias`),
  KEY `idx_path` (`path`(255)),
  KEY `idx_language` (`language`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=229 ;

--
-- Dumping data for table `#__menu`
--

INSERT INTO `#__menu` (`id`, `menutype`, `title`, `alias`, `note`, `path`, `link`, `type`, `published`, `parent_id`, `level`, `component_id`, `checked_out`, `checked_out_time`, `browserNav`, `access`, `img`, `template_style_id`, `params`, `lft`, `rgt`, `home`, `language`, `client_id`) VALUES
(1, '', 'Menu_Item_Root', 'root', '', '', '', '', 1, 0, 0, 0, 0, '0000-00-00 00:00:00', 0, 0, '', 0, '', 0, 205, 0, '*', 0),
(2, 'menu', 'com_banners', 'Banners', '', 'Banners', 'index.php?option=com_banners', 'component', 0, 1, 1, 4, 0, '0000-00-00 00:00:00', 0, 0, 'class:banners', 0, '', 1, 10, 0, '*', 1),
(3, 'menu', 'com_banners', 'Banners', '', 'Banners/Banners', 'index.php?option=com_banners', 'component', 0, 2, 2, 4, 0, '0000-00-00 00:00:00', 0, 0, 'class:banners', 0, '', 2, 3, 0, '*', 1),
(4, 'menu', 'com_banners_categories', 'Categories', '', 'Banners/Categories', 'index.php?option=com_categories&extension=com_banners', 'component', 0, 2, 2, 6, 0, '0000-00-00 00:00:00', 0, 0, 'class:banners-cat', 0, '', 4, 5, 0, '*', 1),
(5, 'menu', 'com_banners_clients', 'Clients', '', 'Banners/Clients', 'index.php?option=com_banners&view=clients', 'component', 0, 2, 2, 4, 0, '0000-00-00 00:00:00', 0, 0, 'class:banners-clients', 0, '', 6, 7, 0, '*', 1),
(6, 'menu', 'com_banners_tracks', 'Tracks', '', 'Banners/Tracks', 'index.php?option=com_banners&view=tracks', 'component', 0, 2, 2, 4, 0, '0000-00-00 00:00:00', 0, 0, 'class:banners-tracks', 0, '', 8, 9, 0, '*', 1),
(7, 'menu', 'com_contact', 'Contacts', '', 'Contacts', 'index.php?option=com_contact', 'component', 0, 1, 1, 8, 0, '0000-00-00 00:00:00', 0, 0, 'class:contact', 0, '', 153, 158, 0, '*', 1),
(8, 'menu', 'com_contact', 'Contacts', '', 'Contacts/Contacts', 'index.php?option=com_contact', 'component', 0, 7, 2, 8, 0, '0000-00-00 00:00:00', 0, 0, 'class:contact', 0, '', 154, 155, 0, '*', 1),
(9, 'menu', 'com_contact_categories', 'Categories', '', 'Contacts/Categories', 'index.php?option=com_categories&extension=com_contact', 'component', 0, 7, 2, 6, 0, '0000-00-00 00:00:00', 0, 0, 'class:contact-cat', 0, '', 156, 157, 0, '*', 1),
(10, 'menu', 'com_messages', 'Messaging', '', 'Messaging', 'index.php?option=com_messages', 'component', 0, 1, 1, 15, 0, '0000-00-00 00:00:00', 0, 0, 'class:messages', 0, '', 159, 164, 0, '*', 1),
(11, 'menu', 'com_messages_add', 'New Private Message', '', 'Messaging/New Private Message', 'index.php?option=com_messages&task=message.add', 'component', 0, 10, 2, 15, 0, '0000-00-00 00:00:00', 0, 0, 'class:messages-add', 0, '', 160, 161, 0, '*', 1),
(12, 'menu', 'com_messages_read', 'Read Private Message', '', 'Messaging/Read Private Message', 'index.php?option=com_messages', 'component', 0, 10, 2, 15, 0, '0000-00-00 00:00:00', 0, 0, 'class:messages-read', 0, '', 162, 163, 0, '*', 1),
(13, 'menu', 'com_newsfeeds', 'News Feeds', '', 'News Feeds', 'index.php?option=com_newsfeeds', 'component', 0, 1, 1, 17, 0, '0000-00-00 00:00:00', 0, 0, 'class:newsfeeds', 0, '', 165, 170, 0, '*', 1),
(14, 'menu', 'com_newsfeeds_feeds', 'Feeds', '', 'News Feeds/Feeds', 'index.php?option=com_newsfeeds', 'component', 0, 13, 2, 17, 0, '0000-00-00 00:00:00', 0, 0, 'class:newsfeeds', 0, '', 166, 167, 0, '*', 1),
(15, 'menu', 'com_newsfeeds_categories', 'Categories', '', 'News Feeds/Categories', 'index.php?option=com_categories&extension=com_newsfeeds', 'component', 0, 13, 2, 6, 0, '0000-00-00 00:00:00', 0, 0, 'class:newsfeeds-cat', 0, '', 168, 169, 0, '*', 1),
(16, 'menu', 'com_redirect', 'Redirect', '', 'Redirect', 'index.php?option=com_redirect', 'component', 0, 1, 1, 24, 0, '0000-00-00 00:00:00', 0, 0, 'class:redirect', 0, '', 171, 172, 0, '*', 1),
(17, 'menu', 'com_search', 'Basic Search', '', 'Basic Search', 'index.php?option=com_search', 'component', 0, 1, 1, 19, 0, '0000-00-00 00:00:00', 0, 0, 'class:search', 0, '', 173, 174, 0, '*', 1),
(18, 'menu', 'com_finder', 'Smart Search', '', 'Smart Search', 'index.php?option=com_finder', 'component', 0, 1, 1, 27, 0, '0000-00-00 00:00:00', 0, 0, 'class:finder', 0, '', 175, 176, 0, '*', 1),
(19, 'menu', 'com_joomlaupdate', 'Joomla! Update', '', 'Joomla! Update', 'index.php?option=com_joomlaupdate', 'component', 1, 1, 1, 28, 0, '0000-00-00 00:00:00', 0, 0, 'class:joomlaupdate', 0, '', 177, 178, 0, '*', 1),
(20, 'main', 'com_tags', 'Tags', '', 'Tags', 'index.php?option=com_tags', 'component', 0, 1, 1, 29, 0, '0000-00-00 00:00:00', 0, 1, 'class:tags', 0, '', 179, 180, 0, '', 1),
(21, 'main', 'com_postinstall', 'Post-installation messages', '', 'Post-installation messages', 'index.php?option=com_postinstall', 'component', 0, 1, 1, 32, 0, '0000-00-00 00:00:00', 0, 1, 'class:postinstall', 0, '', 181, 182, 0, '*', 1),
(101, 'mainmenu', 'Home', 'homepage', '', 'homepage', 'index.php?option=com_content&view=featured', 'component', 1, 1, 1, 22, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"featured_categories":[""],"layout_type":"blog","num_leading_articles":"","num_intro_articles":"","num_columns":"","num_links":"","multi_column_order":"","orderby_pri":"","orderby_sec":"","order_date":"","show_pagination":"","show_pagination_results":"","show_title":"1","link_titles":"","show_intro":"","info_block_position":"0","show_category":"0","link_category":"0","show_parent_category":"0","link_parent_category":"0","show_author":"0","link_author":"0","show_create_date":"0","show_modify_date":"0","show_publish_date":"0","show_item_navigation":"0","show_vote":"","show_readmore":"","show_readmore_title":"","show_icons":"0","show_print_icon":"0","show_email_icon":"0","show_hits":"0","show_tags":"","show_noauth":"","show_feed_link":"","feed_summary":"","menu-anchor_title":"","menu-anchor_css":"fa-home","menu_image":"","menu_text":1,"page_title":"","show_page_heading":"","page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0}', 11, 12, 1, '*', 0),
(102, 'usermenu', 'Your Profile', 'your-profile', '', 'your-profile', 'index.php?option=com_users&view=profile&layout=edit', 'component', 1, 1, 1, 25, 0, '0000-00-00 00:00:00', 0, 2, '', 0, '{"menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"","show_page_heading":0,"page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0}', 143, 144, 0, '*', 0),
(103, 'usermenu', 'Site Administrator', '2013-11-16-23-26-41', '', '2013-11-16-23-26-41', 'administrator', 'url', 1, 1, 1, 0, 0, '0000-00-00 00:00:00', 0, 6, '', 0, '{"menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1}', 147, 148, 0, '*', 0),
(104, 'usermenu', 'Submit an Article', 'submit-an-article', '', 'submit-an-article', 'index.php?option=com_content&view=form&layout=edit', 'component', 1, 1, 1, 22, 0, '0000-00-00 00:00:00', 0, 3, '', 0, '{"enable_category":"0","catid":"2","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"","show_page_heading":0,"page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0}', 145, 146, 0, '*', 0),
(106, 'usermenu', 'Template Settings', 'template-settings', '', 'template-settings', 'index.php?option=com_config&view=templates&controller=config.display.templates', 'component', 1, 1, 1, 23, 0, '0000-00-00 00:00:00', 0, 6, '', 0, '{"menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"","show_page_heading":0,"page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0}', 149, 150, 0, '*', 0),
(107, 'usermenu', 'Site Settings', 'site-settings', '', 'site-settings', 'index.php?option=com_config&view=config&controller=config.display.config', 'component', 1, 1, 1, 23, 0, '0000-00-00 00:00:00', 0, 6, '', 0, '{"menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"","show_page_heading":0,"page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0}', 151, 152, 0, '*', 0),
(112, 'mainmenu', 'Features', 'features', '', 'features', '', 'heading', 1, 1, 1, 0, 0, '0000-00-00 00:00:00', 0, 1, ' ', 0, '{"menu-anchor_title":"","menu-anchor_css":"fa-cogs","menu_image":"","menu_text":1}', 13, 30, 0, '*', 0),
(113, 'mainmenu', 'Module Positions', 'module-positions', '', 'features/module-positions', 'index.php?option=com_content&view=category&layout=blog&id=15', 'component', 1, 112, 2, 22, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"layout_type":"blog","show_category_heading_title_text":"","show_category_title":"","show_description":"","show_description_image":"","maxLevel":"","show_empty_categories":"","show_no_articles":"","show_subcat_desc":"","show_cat_num_articles":"","show_cat_tags":"","page_subheading":"","num_leading_articles":"","num_intro_articles":"","num_columns":"","num_links":"","multi_column_order":"","show_subcategory_content":"","orderby_pri":"","orderby_sec":"","order_date":"","show_pagination":"","show_pagination_results":"","show_featured":"","show_title":"","link_titles":"0","show_intro":"","info_block_position":"","show_category":"0","link_category":"0","show_parent_category":"0","link_parent_category":"0","show_author":"0","link_author":"0","show_create_date":"0","show_modify_date":"0","show_publish_date":"0","show_item_navigation":"","show_vote":"0","show_readmore":"","show_readmore_title":"","show_icons":"0","show_print_icon":"0","show_email_icon":"0","show_hits":"0","show_tags":"","show_noauth":"","show_feed_link":"","feed_summary":"","menu-anchor_title":"","menu-anchor_css":"fa-th","menu_image":"","menu_text":1,"page_title":"","show_page_heading":"","page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0}', 14, 15, 0, '*', 0),
(114, 'mainmenu', 'Module Variations', 'module-variations', '', 'features/module-variations', 'index.php?option=com_content&view=category&layout=blog&id=16', 'component', 1, 112, 2, 22, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"layout_type":"blog","show_category_heading_title_text":"","show_category_title":"","show_description":"","show_description_image":"","maxLevel":"","show_empty_categories":"","show_no_articles":"","show_subcat_desc":"","show_cat_num_articles":"","show_cat_tags":"","page_subheading":"","num_leading_articles":"","num_intro_articles":"","num_columns":"","num_links":"","multi_column_order":"","show_subcategory_content":"","orderby_pri":"","orderby_sec":"","order_date":"","show_pagination":"","show_pagination_results":"","show_featured":"","show_title":"","link_titles":"","show_intro":"","info_block_position":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_vote":"","show_readmore":"","show_readmore_title":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_hits":"","show_tags":"","show_noauth":"","show_feed_link":"","feed_summary":"","menu-anchor_title":"","menu-anchor_css":"fa-magic","menu_image":"","menu_text":1,"page_title":"","show_page_heading":"","page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0}', 16, 17, 0, '*', 0),
(115, 'mainmenu', 'Navigation Styles', 'navigation-styles', '', 'features/navigation-styles', '', 'heading', 1, 112, 2, 0, 0, '0000-00-00 00:00:00', 0, 1, ' ', 0, '{"menu-anchor_title":"","menu-anchor_css":"fa-ellipsis-h","menu_image":"","menu_text":1}', 18, 25, 0, '*', 0),
(116, 'mainmenu', 'Typography', 'typography', '', 'typography', '', 'heading', 1, 1, 1, 0, 0, '0000-00-00 00:00:00', 0, 1, ' ', 0, '{"menu-anchor_title":"","menu-anchor_css":"fa-font","menu_image":"","menu_text":1}', 31, 42, 0, '*', 0),
(117, 'mainmenu', 'Icon Styles', 'icon-styles', '', 'typography/icon-styles', 'index.php?option=com_content&view=category&layout=blog&id=18', 'component', 1, 116, 2, 22, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"layout_type":"blog","show_category_heading_title_text":"","show_category_title":"","show_description":"","show_description_image":"","maxLevel":"","show_empty_categories":"","show_no_articles":"","show_subcat_desc":"","show_cat_num_articles":"","show_cat_tags":"","page_subheading":"","num_leading_articles":"","num_intro_articles":"","num_columns":"","num_links":"","multi_column_order":"","show_subcategory_content":"","orderby_pri":"","orderby_sec":"","order_date":"","show_pagination":"","show_pagination_results":"","show_featured":"","show_title":"","link_titles":"","show_intro":"","info_block_position":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_vote":"","show_readmore":"","show_readmore_title":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_hits":"","show_tags":"","show_noauth":"","show_feed_link":"","feed_summary":"","menu-anchor_title":"","menu-anchor_css":"fa-flag","menu_image":"","menu_text":1,"page_title":"","show_page_heading":"","page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0}', 32, 33, 0, '*', 0),
(118, 'mainmenu', 'Image Styles', 'image-styles', '', 'typography/image-styles', 'index.php?option=com_content&view=category&layout=blog&id=18', 'component', 1, 116, 2, 22, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"layout_type":"blog","show_category_heading_title_text":"","show_category_title":"","show_description":"","show_description_image":"","maxLevel":"","show_empty_categories":"","show_no_articles":"","show_subcat_desc":"","show_cat_num_articles":"","show_cat_tags":"","page_subheading":"","num_leading_articles":"","num_intro_articles":"","num_columns":"","num_links":"","multi_column_order":"","show_subcategory_content":"","orderby_pri":"","orderby_sec":"","order_date":"","show_pagination":"","show_pagination_results":"","show_featured":"","show_title":"","link_titles":"","show_intro":"","info_block_position":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_vote":"","show_readmore":"","show_readmore_title":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_hits":"","show_tags":"","show_noauth":"","show_feed_link":"","feed_summary":"","menu-anchor_title":"","menu-anchor_css":"fa-picture-o","menu_image":"","menu_text":1,"page_title":"","show_page_heading":"","page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0}', 34, 35, 0, '*', 0),
(119, 'mainmenu', 'Button Styles', 'button-styles', '', 'typography/button-styles', 'index.php?option=com_content&view=category&layout=blog&id=18', 'component', 1, 116, 2, 22, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"layout_type":"blog","show_category_heading_title_text":"","show_category_title":"","show_description":"","show_description_image":"","maxLevel":"","show_empty_categories":"","show_no_articles":"","show_subcat_desc":"","show_cat_num_articles":"","show_cat_tags":"","page_subheading":"","num_leading_articles":"","num_intro_articles":"","num_columns":"","num_links":"","multi_column_order":"","show_subcategory_content":"","orderby_pri":"","orderby_sec":"","order_date":"","show_pagination":"","show_pagination_results":"","show_featured":"","show_title":"","link_titles":"","show_intro":"","info_block_position":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_vote":"","show_readmore":"","show_readmore_title":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_hits":"","show_tags":"","show_noauth":"","show_feed_link":"","feed_summary":"","menu-anchor_title":"","menu-anchor_css":"fa-toggle-on ","menu_image":"","menu_text":1,"page_title":"","show_page_heading":"","page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0}', 36, 37, 0, '*', 0),
(120, 'mainmenu', 'Bootstrap Elements', 'bootstrap-elements', '', 'typography/bootstrap-elements', 'index.php?option=com_content&view=category&layout=blog&id=18', 'component', 1, 116, 2, 22, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"layout_type":"blog","show_category_heading_title_text":"","show_category_title":"","show_description":"","show_description_image":"","maxLevel":"","show_empty_categories":"","show_no_articles":"","show_subcat_desc":"","show_cat_num_articles":"","show_cat_tags":"","page_subheading":"","num_leading_articles":"","num_intro_articles":"","num_columns":"","num_links":"","multi_column_order":"","show_subcategory_content":"","orderby_pri":"","orderby_sec":"","order_date":"","show_pagination":"","show_pagination_results":"","show_featured":"","show_title":"","link_titles":"","show_intro":"","info_block_position":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_vote":"","show_readmore":"","show_readmore_title":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_hits":"","show_tags":"","show_noauth":"","show_feed_link":"","feed_summary":"","menu-anchor_title":"","menu-anchor_css":"fa-laptop","menu_image":"","menu_text":1,"page_title":"","show_page_heading":"","page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0}', 38, 39, 0, '*', 0),
(121, 'mainmenu', 'Joomla', 'joomla', '', 'joomla', 'index.php?option=com_content&view=category&layout=blog&id=19', 'component', 1, 1, 1, 22, 0, '0000-00-00 00:00:00', 0, 1, ' ', 0, '{"layout_type":"blog","show_category_heading_title_text":"","show_category_title":"","show_description":"","show_description_image":"","maxLevel":"","show_empty_categories":"","show_no_articles":"","show_subcat_desc":"","show_cat_num_articles":"","show_cat_tags":"","page_subheading":"","num_leading_articles":"","num_intro_articles":"","num_columns":"","num_links":"","multi_column_order":"","show_subcategory_content":"","orderby_pri":"","orderby_sec":"","order_date":"","show_pagination":"","show_pagination_results":"","show_featured":"","show_title":"","link_titles":"","show_intro":"","info_block_position":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_vote":"","show_readmore":"","show_readmore_title":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_hits":"","show_tags":"","show_noauth":"","show_feed_link":"","feed_summary":"","menu-anchor_title":"","menu-anchor_css":"fa-joomla","menu_image":"","menu_text":1,"page_title":"","show_page_heading":"","page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0}', 43, 100, 0, '*', 0),
(122, 'mainmenu', 'Joomla Content', 'joomla-content', '', 'joomla/joomla-content', 'index.php?option=com_content&view=category&layout=blog&id=19', 'component', 1, 121, 2, 22, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"layout_type":"blog","show_category_heading_title_text":"","show_category_title":"","show_description":"","show_description_image":"","maxLevel":"","show_empty_categories":"","show_no_articles":"","show_subcat_desc":"","show_cat_num_articles":"","show_cat_tags":"","page_subheading":"","num_leading_articles":"","num_intro_articles":"","num_columns":"","num_links":"","multi_column_order":"","show_subcategory_content":"","orderby_pri":"","orderby_sec":"","order_date":"","show_pagination":"","show_pagination_results":"","show_featured":"","show_title":"","link_titles":"","show_intro":"","info_block_position":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_vote":"","show_readmore":"","show_readmore_title":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_hits":"","show_tags":"","show_noauth":"","show_feed_link":"","feed_summary":"","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"","show_page_heading":"","page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0}', 44, 97, 0, '*', 0),
(123, 'mainmenu', 'Category Blog', 'category-blog', '', 'joomla/joomla-content/category-blog', 'index.php?option=com_content&view=category&layout=blog&id=19', 'component', 1, 122, 3, 22, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"layout_type":"blog","show_category_heading_title_text":"","show_category_title":"","show_description":"","show_description_image":"","maxLevel":"","show_empty_categories":"","show_no_articles":"","show_subcat_desc":"","show_cat_num_articles":"","show_cat_tags":"","page_subheading":"","num_leading_articles":"","num_intro_articles":"","num_columns":"","num_links":"","multi_column_order":"","show_subcategory_content":"","orderby_pri":"","orderby_sec":"","order_date":"","show_pagination":"","show_pagination_results":"","show_featured":"","show_title":"","link_titles":"","show_intro":"","info_block_position":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_vote":"","show_readmore":"","show_readmore_title":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_hits":"","show_tags":"","show_noauth":"","show_feed_link":"","feed_summary":"","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"","show_page_heading":"","page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0}', 45, 46, 0, '*', 0),
(124, 'mainmenu', 'Single Article', 'single-article', '', 'joomla/joomla-content/single-article', 'index.php?option=com_content&view=article&id=2', 'component', 1, 122, 3, 22, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"show_title":"","link_titles":"","show_intro":"","info_block_position":"0","show_category":"1","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"1","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_vote":"","show_icons":"1","show_print_icon":"1","show_email_icon":"1","show_hits":"1","show_tags":"1","show_noauth":"","urls_position":"","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"","show_page_heading":"","page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0}', 47, 48, 0, '*', 0),
(125, 'mainmenu', 'Featured Articles', 'featured-articles', '', 'joomla/joomla-content/featured-articles', 'index.php?option=com_content&view=featured', 'component', 1, 122, 3, 22, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"featured_categories":[""],"layout_type":"blog","num_leading_articles":"","num_intro_articles":"","num_columns":"","num_links":"","multi_column_order":"","orderby_pri":"","orderby_sec":"","order_date":"","show_pagination":"","show_pagination_results":"","show_title":"","link_titles":"","show_intro":"","info_block_position":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_vote":"","show_readmore":"","show_readmore_title":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_hits":"","show_tags":"","show_noauth":"","show_feed_link":"","feed_summary":"","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"","show_page_heading":"","page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0}', 49, 50, 0, '*', 0),
(126, 'mainmenu', 'Archived Article', 'archived-article', '', 'joomla/joomla-content/archived-article', 'index.php?option=com_content&view=archive', 'component', 1, 122, 3, 22, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"orderby_sec":"alpha","order_date":"created","display_num":"5","filter_field":"","introtext_limit":"500","show_intro":"","info_block_position":"","show_category":"0","link_category":"0","show_parent_category":"0","link_parent_category":"0","link_titles":"0","show_author":"0","link_author":"0","show_create_date":"0","show_modify_date":"0","show_publish_date":"0","show_item_navigation":"0","show_hits":"0","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"","show_page_heading":"","page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0}', 51, 52, 0, '*', 0),
(127, 'mainmenu', 'List All Categories', 'list-all-categories', '', 'joomla/joomla-content/list-all-categories', 'index.php?option=com_content&view=categories&id=0', 'component', 1, 122, 3, 22, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"show_base_description":"","categories_description":"","maxLevelcat":"","show_empty_categories_cat":"","show_subcat_desc_cat":"","show_cat_num_articles_cat":"","show_category_title":"","show_description":"","show_description_image":"","maxLevel":"","show_empty_categories":"","show_no_articles":"","show_subcat_desc":"","show_cat_num_articles":"","num_leading_articles":"1","num_intro_articles":"4","num_columns":"2","num_links":"4","multi_column_order":"","show_subcategory_content":"","orderby_pri":"","orderby_sec":"","order_date":"","show_pagination":"","show_pagination_results":"","show_pagination_limit":"","filter_field":"","show_headings":"","list_show_date":"","date_format":"","list_show_hits":"","list_show_author":"","display_num":"10","show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_vote":"","show_readmore":"","show_readmore_title":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_hits":"","show_noauth":"","show_feed_link":"","feed_summary":"","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"","show_page_heading":"","page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0}', 53, 54, 0, '*', 0),
(128, 'mainmenu', 'Category List', 'category-list', '', 'joomla/joomla-content/category-list', 'index.php?option=com_content&view=category&id=2', 'component', 0, 122, 3, 22, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"show_category_title":"","show_description":"","show_description_image":"","maxLevel":"","show_empty_categories":"","show_no_articles":"","show_category_heading_title":"","show_subcat_desc":"","show_cat_num_articles":"","show_cat_tags":"","page_subheading":"","show_pagination_limit":"","filter_field":"","show_headings":"","list_show_date":"","date_format":"","list_show_hits":"","list_show_author":"","orderby_pri":"","orderby_sec":"","order_date":"","show_pagination":"","show_pagination_results":"","display_num":"10","show_featured":"","show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_vote":"","show_readmore":"","show_readmore_title":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_hits":"","show_noauth":"","show_feed_link":"","feed_summary":"","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"","show_page_heading":"","page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0}', 55, 56, 0, '*', 0),
(129, 'mainmenu', 'Tagged Items', 'tagged-items', '', 'joomla/joomla-content/tagged-items', 'index.php?option=com_tags&view=tag&id[0]=2', 'component', 0, 122, 3, 29, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"show_tag_title":"","tag_list_show_tag_image":"","tag_list_show_tag_description":"","tag_list_image":"","tag_list_description":"","show_tag_num_items":"","tag_list_orderby":"","tag_list_orderby_direction":"","tag_list_show_item_image":"","tag_list_show_item_description":"","tag_list_item_maximum_characters":0,"filter_field":"","show_pagination_limit":"","show_pagination":"","show_pagination_results":"","return_any_or_all":"","include_children":"","maximum":200,"show_feed_link":"","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"","show_page_heading":"","page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0}', 57, 58, 0, '*', 0),
(130, 'mainmenu', 'List of Tagged Items', 'list-of-tagged-items', '', 'joomla/joomla-content/list-of-tagged-items', 'index.php?option=com_tags&view=tag&layout=list&id[0]=2', 'component', 0, 122, 3, 29, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"show_tag_title":"","tag_list_show_tag_image":"","tag_list_show_tag_description":"","tag_list_image":"","tag_list_description":"","show_tag_num_items":"","tag_list_orderby":"","tag_list_orderby_direction":"","tag_list_show_item_image":"","tag_list_show_item_description":"","tag_list_item_maximum_characters":0,"filter_field":"","show_pagination_limit":"","show_pagination":"","show_pagination_results":"","tag_list_show_date":"","date_format":"","return_any_or_all":"","include_children":"","maximum":200,"show_feed_link":"","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"","show_page_heading":"","page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0}', 59, 60, 0, '*', 0),
(131, 'mainmenu', 'List of All Tags', 'list-of-all-tags', '', 'joomla/joomla-content/list-of-all-tags', 'index.php?option=com_tags&view=tags', 'component', 0, 122, 3, 29, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"tag_columns":4,"all_tags_description":"","all_tags_show_description_image":"","all_tags_description_image":"","all_tags_orderby":"","all_tags_orderby_direction":"","all_tags_show_tag_image":"","all_tags_show_tag_description":"","all_tags_tag_maximum_characters":0,"all_tags_show_tag_hits":"","maximum":200,"filter_field":"","show_pagination_limit":"","show_pagination":"","show_pagination_results":"","show_feed_link":"","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"","show_page_heading":"","page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0}', 61, 62, 0, '*', 0),
(132, 'mainmenu', 'Iframe Wrapper', 'iframe-wrapper', '', 'joomla/joomla-content/iframe-wrapper', 'index.php?option=com_wrapper&view=wrapper', 'component', 1, 122, 3, 2, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"url":"http:\\/\\/www.favthemes.com\\/","scrolling":"auto","width":"100%","height":"500","height_auto":"0","add_scheme":"1","frameborder":"1","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"","show_page_heading":"","page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0}', 63, 64, 0, '*', 0),
(133, 'mainmenu', 'Search', 'search', '', 'joomla/joomla-content/search', 'index.php?option=com_search&view=search', 'component', 1, 122, 3, 19, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"search_areas":"","show_date":"","searchphrase":"0","ordering":"newest","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"","show_page_heading":"","page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0}', 65, 66, 0, '*', 0),
(134, 'mainmenu', 'Smart Search', 'smart-search', '', 'joomla/joomla-content/smart-search', 'index.php?option=com_finder&view=search', 'component', 1, 122, 3, 27, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"show_date_filters":"","show_advanced":"","expand_advanced":"","show_description":"","description_length":255,"show_url":"","show_pagination_limit":"","show_pagination":"","show_pagination_results":"","allow_empty_query":"0","show_suggested_query":"1","show_explained_query":"1","sort_order":"","sort_direction":"","show_feed":"0","show_feed_text":"0","show_feed_link":"","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"","show_page_heading":"","page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0}', 67, 68, 0, '*', 0),
(135, 'mainmenu', 'Single Contact', 'single-contact', '', 'joomla/joomla-content/single-contact', 'index.php?option=com_contact&view=contact&id=1', 'component', 0, 122, 3, 8, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"presentation_style":"","show_contact_category":"","show_contact_list":"","show_tags":"","show_name":"","show_position":"","show_email":"","show_street_address":"","show_suburb":"","show_state":"","show_postcode":"","show_country":"","show_telephone":"","show_mobile":"","show_fax":"","show_webpage":"","show_misc":"","show_image":"","allow_vcard":"","show_articles":"","show_links":"","linka_name":"","linkb_name":"","linkc_name":"","linkd_name":"","linke_name":"","show_email_form":"","show_email_copy":"","banned_email":"","banned_subject":"","banned_text":"","validate_session":"","custom_reply":"","redirect":"","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"","show_page_heading":"","page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0}', 69, 70, 0, '*', 0),
(136, 'mainmenu', 'List Contacts', 'list-contacts', '', 'joomla/joomla-content/list-contacts', 'index.php?option=com_contact&view=category&id=4', 'component', 0, 122, 3, 8, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"show_category_title":"","show_description":"","show_description_image":"","maxLevel":"","show_empty_categories":"","show_subcat_desc":"","show_cat_items":"","filter_field":"","show_pagination_limit":"","show_headings":"","show_position_headings":"","show_email_headings":"","show_telephone_headings":"","show_mobile_headings":"","show_fax_headings":"","show_suburb_headings":"","show_state_headings":"","show_country_headings":"","show_pagination":"","show_pagination_results":"","initial_sort":"","presentation_style":"","show_contact_category":"","show_contact_list":"","show_name":"","show_position":"","show_email":"","show_street_address":"","show_suburb":"","show_state":"","show_postcode":"","show_country":"","show_telephone":"","show_mobile":"","show_fax":"","show_webpage":"","show_misc":"","show_image":"","allow_vcard":"","show_articles":"","show_links":"","linka_name":"","linkb_name":"","linkc_name":"","linkd_name":"","linke_name":"","show_email_form":"","show_email_copy":"","banned_email":"","banned_subject":"","banned_text":"","validate_session":"","custom_reply":"","redirect":"","show_feed_link":"","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"","show_page_heading":"","page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0}', 71, 72, 0, '*', 0),
(137, 'mainmenu', 'Featured Contacts', 'featured-contacts', '', 'joomla/joomla-content/featured-contacts', 'index.php?option=com_contact&view=featured', 'component', 0, 122, 3, 8, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"show_pagination_limit":"","show_headings":"","show_position_headings":"","show_email_headings":"","show_telephone_headings":"","show_mobile_headings":"","show_fax_headings":"","show_suburb_headings":"","show_state_headings":"","show_country_headings":"","show_pagination":"","show_pagination_results":"","presentation_style":"","show_name":"","show_position":"","show_email":"","show_street_address":"","show_suburb":"","show_state":"","show_postcode":"","show_country":"","show_telephone":"","show_mobile":"","show_fax":"","show_webpage":"","show_misc":"","show_image":"","allow_vcard":"","show_articles":"","show_links":"","linka_name":"","linkb_name":"","linkc_name":"","linkd_name":"","linke_name":"","show_email_form":"","show_email_copy":"","banned_email":"","banned_subject":"","banned_text":"","validate_session":"","custom_reply":"","redirect":"","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"","show_page_heading":"","page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0}', 73, 74, 0, '*', 0),
(138, 'mainmenu', 'Single News Feed', 'single-news-feed', '', 'joomla/joomla-content/single-news-feed', 'index.php?option=com_newsfeeds&view=newsfeed&id=1', 'component', 0, 122, 3, 17, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"show_feed_image":"","show_feed_description":"","show_item_description":"","show_tags":"","feed_character_count":"0","feed_display_order":"","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"","show_page_heading":"","page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0}', 75, 76, 0, '*', 0),
(139, 'mainmenu', 'List News Feeds', 'list-news-feeds', '', 'joomla/joomla-content/list-news-feeds', 'index.php?option=com_newsfeeds&view=category&id=5', 'component', 0, 122, 3, 17, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"show_category_title":"","show_description":"","show_description_image":"","maxLevel":"","show_empty_categories":"","show_subcat_desc":"","show_cat_items":"","filter_field":"","show_pagination_limit":"","show_headings":"","show_articles":"","show_link":"","show_pagination":"","show_pagination_results":"","show_feed_image":"","show_feed_description":"","show_item_description":"","feed_character_count":"0","feed_display_order":"","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"","show_page_heading":"","page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0}', 77, 78, 0, '*', 0),
(140, 'mainmenu', 'Login Form', 'login-form', '', 'joomla/joomla-content/login-form', 'index.php?option=com_users&view=login', 'component', 0, 122, 3, 25, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"login_redirect_url":"","logindescription_show":"1","login_description":"","login_image":"","logout_redirect_url":"","logoutdescription_show":"1","logout_description":"","logout_image":"","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"","show_page_heading":"","page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0}', 79, 80, 0, '*', 0),
(141, 'mainmenu', 'Username Reminder', 'username-reminder', '', 'joomla/joomla-content/username-reminder', 'index.php?option=com_users&view=remind', 'component', 0, 122, 3, 25, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"","show_page_heading":"","page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0}', 81, 82, 0, '*', 0),
(142, 'mainmenu', 'Password Reset', 'password-reset', '', 'joomla/joomla-content/password-reset', 'index.php?option=com_users&view=reset', 'component', 0, 122, 3, 25, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"","show_page_heading":"","page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0}', 83, 84, 0, '*', 0),
(143, 'mainmenu', 'Site Configuration', 'site-configuration', '', 'joomla/joomla-content/site-configuration', 'index.php?option=com_config&view=config&controller=config.display.config', 'component', 0, 122, 3, 23, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"","show_page_heading":"","page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0}', 85, 86, 0, '*', 0),
(144, 'mainmenu', 'Template Options', 'template-options', '', 'joomla/joomla-content/template-options', 'index.php?option=com_config&view=templates&controller=config.display.templates', 'component', 0, 122, 3, 23, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"","show_page_heading":"","page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0}', 87, 88, 0, '*', 0),
(145, 'mainmenu', 'Create Article', 'create-article', '', 'joomla/joomla-content/create-article', 'index.php?option=com_content&view=form&layout=edit', 'component', 0, 122, 3, 22, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"enable_category":"0","catid":"2","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"","show_page_heading":"","page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0}', 89, 90, 0, '*', 0),
(146, 'mainmenu', 'User Profile', 'user-profile', '', 'joomla/joomla-content/user-profile', 'index.php?option=com_users&view=profile', 'component', 0, 122, 3, 25, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"","show_page_heading":"","page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0}', 91, 92, 0, '*', 0),
(147, 'mainmenu', 'Edit User Profile', 'edit-user-profile', '', 'joomla/joomla-content/edit-user-profile', 'index.php?option=com_users&view=profile&layout=edit', 'component', 0, 122, 3, 25, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"","show_page_heading":"","page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0}', 93, 94, 0, '*', 0),
(148, 'mainmenu', 'Registration Form', 'registration-form', '', 'joomla/joomla-content/registration-form', 'index.php?option=com_users&view=registration', 'component', 0, 122, 3, 25, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"","show_page_heading":"","page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0}', 95, 96, 0, '*', 0),
(149, 'mainmenu', 'Joomla Modules', 'joomla-modules', '', 'joomla/joomla-modules', 'index.php?option=com_content&view=category&layout=blog&id=20', 'component', 1, 121, 2, 22, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"layout_type":"blog","show_category_heading_title_text":"","show_category_title":"","show_description":"","show_description_image":"","maxLevel":"","show_empty_categories":"","show_no_articles":"","show_subcat_desc":"","show_cat_num_articles":"","show_cat_tags":"","page_subheading":"","num_leading_articles":"","num_intro_articles":"","num_columns":"","num_links":"","multi_column_order":"","show_subcategory_content":"","orderby_pri":"","orderby_sec":"","order_date":"","show_pagination":"","show_pagination_results":"","show_featured":"","show_title":"","link_titles":"","show_intro":"","info_block_position":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_vote":"","show_readmore":"","show_readmore_title":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_hits":"","show_tags":"","show_noauth":"","show_feed_link":"","feed_summary":"","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"","show_page_heading":"","page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0}', 98, 99, 0, '*', 0),
(150, 'mainmenu', 'Error 404 Page', '404', '', 'features/404', 'index.php?option=com_content&view=article&id=3', 'component', 1, 112, 2, 22, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"show_title":"0","link_titles":"0","show_intro":"0","info_block_position":"","show_category":"0","link_category":"0","show_parent_category":"0","link_parent_category":"0","show_author":"0","link_author":"0","show_create_date":"0","show_modify_date":"0","show_publish_date":"0","show_item_navigation":"0","show_vote":"0","show_icons":"0","show_print_icon":"0","show_email_icon":"0","show_hits":"0","show_tags":"0","show_noauth":"0","urls_position":"","menu-anchor_title":"","menu-anchor_css":" fa-flask","menu_image":"","menu_text":1,"page_title":"","show_page_heading":"","page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0}', 26, 27, 0, '*', 0),
(151, 'mainmenu', 'Extensions', 'extensions', '', 'extensions', 'index.php?option=com_content&view=category&layout=blog&id=13', 'component', 1, 1, 1, 22, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"layout_type":"blog","show_category_heading_title_text":"","show_category_title":"","show_description":"","show_description_image":"","maxLevel":"","show_empty_categories":"","show_no_articles":"","show_subcat_desc":"","show_cat_num_articles":"","show_cat_tags":"","page_subheading":"","num_leading_articles":"","num_intro_articles":"","num_columns":"","num_links":"","multi_column_order":"","show_subcategory_content":"","orderby_pri":"","orderby_sec":"","order_date":"","show_pagination":"","show_pagination_results":"","show_featured":"","show_title":"","link_titles":"","show_intro":"","info_block_position":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_vote":"","show_readmore":"","show_readmore_title":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_hits":"","show_tags":"","show_noauth":"","show_feed_link":"","feed_summary":"","menu-anchor_title":"","menu-anchor_css":"fa-puzzle-piece","menu_image":"","menu_text":1,"page_title":"","show_page_heading":"","page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0}', 117, 118, 0, '*', 0),
(152, 'mainmenu', 'Docs', 'documentation', '', 'documentation', 'index.php?option=com_content&view=category&layout=blog&id=14', 'component', 1, 1, 1, 22, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"layout_type":"blog","show_category_heading_title_text":"","show_category_title":"","show_description":"","show_description_image":"","maxLevel":"","show_empty_categories":"","show_no_articles":"","show_subcat_desc":"","show_cat_num_articles":"","show_cat_tags":"","page_subheading":"","num_leading_articles":"","num_intro_articles":"","num_columns":"","num_links":"","multi_column_order":"","show_subcategory_content":"","orderby_pri":"","orderby_sec":"","order_date":"","show_pagination":"","show_pagination_results":"","show_featured":"","show_title":"","link_titles":"","show_intro":"","info_block_position":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_vote":"","show_readmore":"","show_readmore_title":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_hits":"","show_tags":"","show_noauth":"","show_feed_link":"","feed_summary":"","menu-anchor_title":"","menu-anchor_css":"fa-graduation-cap ","menu_image":"","menu_text":1,"page_title":"","show_page_heading":"","page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0}', 119, 120, 0, '*', 0),
(164, 'main', 'COM_K2', 'com-k2', '', 'com-k2', 'index.php?option=com_k2', 'component', 0, 1, 1, 10013, 0, '0000-00-00 00:00:00', 0, 1, '../media/k2/assets/images/system/k2_16x16.png', 0, '', 183, 204, 0, '', 1),
(165, 'main', 'K2_ITEMS', 'k2-items', '', 'com-k2/k2-items', 'index.php?option=com_k2&view=items', 'component', 0, 164, 2, 10013, 0, '0000-00-00 00:00:00', 0, 1, 'class:component', 0, '', 184, 185, 0, '', 1),
(166, 'main', 'K2_CATEGORIES', 'k2-categories', '', 'com-k2/k2-categories', 'index.php?option=com_k2&view=categories', 'component', 0, 164, 2, 10013, 0, '0000-00-00 00:00:00', 0, 1, 'class:component', 0, '', 186, 187, 0, '', 1),
(167, 'main', 'K2_TAGS', 'k2-tags', '', 'com-k2/k2-tags', 'index.php?option=com_k2&view=tags', 'component', 0, 164, 2, 10013, 0, '0000-00-00 00:00:00', 0, 1, 'class:component', 0, '', 188, 189, 0, '', 1),
(168, 'main', 'K2_COMMENTS', 'k2-comments', '', 'com-k2/k2-comments', 'index.php?option=com_k2&view=comments', 'component', 0, 164, 2, 10013, 0, '0000-00-00 00:00:00', 0, 1, 'class:component', 0, '', 190, 191, 0, '', 1),
(169, 'main', 'K2_USERS', 'k2-users', '', 'com-k2/k2-users', 'index.php?option=com_k2&view=users', 'component', 0, 164, 2, 10013, 0, '0000-00-00 00:00:00', 0, 1, 'class:component', 0, '', 192, 193, 0, '', 1),
(170, 'main', 'K2_USER_GROUPS', 'k2-user-groups', '', 'com-k2/k2-user-groups', 'index.php?option=com_k2&view=usergroups', 'component', 0, 164, 2, 10013, 0, '0000-00-00 00:00:00', 0, 1, 'class:component', 0, '', 194, 195, 0, '', 1);
INSERT INTO `#__menu` (`id`, `menutype`, `title`, `alias`, `note`, `path`, `link`, `type`, `published`, `parent_id`, `level`, `component_id`, `checked_out`, `checked_out_time`, `browserNav`, `access`, `img`, `template_style_id`, `params`, `lft`, `rgt`, `home`, `language`, `client_id`) VALUES
(171, 'main', 'K2_EXTRA_FIELDS', 'k2-extra-fields', '', 'com-k2/k2-extra-fields', 'index.php?option=com_k2&view=extrafields', 'component', 0, 164, 2, 10013, 0, '0000-00-00 00:00:00', 0, 1, 'class:component', 0, '', 196, 197, 0, '', 1),
(172, 'main', 'K2_EXTRA_FIELD_GROUPS', 'k2-extra-field-groups', '', 'com-k2/k2-extra-field-groups', 'index.php?option=com_k2&view=extrafieldsgroups', 'component', 0, 164, 2, 10013, 0, '0000-00-00 00:00:00', 0, 1, 'class:component', 0, '', 198, 199, 0, '', 1),
(173, 'main', 'K2_MEDIA_MANAGER', 'k2-media-manager', '', 'com-k2/k2-media-manager', 'index.php?option=com_k2&view=media', 'component', 0, 164, 2, 10013, 0, '0000-00-00 00:00:00', 0, 1, 'class:component', 0, '', 200, 201, 0, '', 1),
(174, 'main', 'K2_INFORMATION', 'k2-information', '', 'com-k2/k2-information', 'index.php?option=com_k2&view=info', 'component', 0, 164, 2, 10013, 0, '0000-00-00 00:00:00', 0, 1, 'class:component', 0, '', 202, 203, 0, '', 1),
(175, 'mainmenu', 'K2', 'k2', '', 'k2', 'index.php?option=com_k2&view=itemlist&layout=category&task=category&id=1', 'component', 1, 1, 1, 10013, 0, '0000-00-00 00:00:00', 0, 1, ' ', 0, '{"categories":["1"],"singleCatOrdering":"","menu-anchor_title":"","menu-anchor_css":"fa-globe","menu_image":"","menu_text":1,"page_title":"","show_page_heading":"","page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0}', 101, 116, 0, '*', 0),
(176, 'mainmenu', 'K2 Content', 'k2-content', '', 'k2/k2-content', 'index.php?option=com_k2&view=itemlist&layout=category&task=category&id=1', 'component', 1, 175, 2, 10013, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"categories":["1"],"singleCatOrdering":"","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"","show_page_heading":"","page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0}', 102, 113, 0, '*', 0),
(177, 'mainmenu', 'K2 Categories', 'k2-categories', '', 'k2/k2-content/k2-categories', 'index.php?option=com_k2&view=itemlist&layout=category', 'component', 1, 176, 3, 10013, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"catCatalogMode":"0","theme":"default","num_leading_items":"1","num_leading_columns":"1","leadingImgSize":"XLarge","num_primary_items":"2","num_primary_columns":"2","primaryImgSize":"Medium","num_secondary_items":"0","num_secondary_columns":"0","secondaryImgSize":"Small","num_links":"1","num_links_columns":"1","linksImgSize":"none","catFeaturedItems":"1","catOrdering":"order","catPagination":"2","catPaginationResults":"1","catFeedLink":"0","catFeedIcon":"0","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"","show_page_heading":"","page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0}', 103, 104, 0, '*', 0),
(178, 'mainmenu', 'K2 Item', 'k2-item', '', 'k2/k2-content/k2-item', 'index.php?option=com_k2&view=item&layout=item&id=1', 'component', 1, 176, 3, 10013, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"","show_page_heading":"","page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0}', 105, 106, 0, '*', 0),
(179, 'mainmenu', 'K2 Tags', 'k2-tags', '', 'k2/k2-content/k2-tags', 'index.php?option=com_k2&view=itemlist&layout=tag&tag=joomla&task=tag', 'component', 1, 176, 3, 10013, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"theme":"default","tagOrdering":"","tagFeedLink":"1","tagFeedIcon":"1","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"","show_page_heading":"","page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0}', 107, 108, 0, '*', 0),
(180, 'mainmenu', 'K2 Latest Items', 'k2-latest-items', '', 'k2/k2-content/k2-latest-items', 'index.php?option=com_k2&view=latest&layout=latest', 'component', 1, 176, 3, 10013, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"theme":"default","source":"1","latestItemsCols":"1","latestItemsLimit":"3","latestItemsDisplayEffect":"all","userName":"1","userImage":"1","userDescription":"1","userURL":"1","userEmail":"0","userFeed":"1","categoryIDs":["1"],"categoryTitle":"1","categoryDescription":"1","categoryImage":"1","categoryFeed":"0","latestItemTitle":"1","latestItemTitleLinked":"1","latestItemDateCreated":"1","latestItemImage":"1","latestItemImageSize":"XLarge","latestItemVideo":"1","latestItemVideoWidth":"","latestItemVideoHeight":"","latestItemAudioWidth":"","latestItemAudioHeight":"","latestItemVideoAutoPlay":"0","latestItemIntroText":"1","latestItemCategory":"1","latestItemTags":"1","latestItemReadMore":"1","latestItemCommentsAnchor":"0","feedLink":"1","latestItemK2Plugins":"0","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"","show_page_heading":"","page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0}', 109, 110, 0, '*', 0),
(181, 'mainmenu', 'K2 Item Edit Form', 'k2-item-edit-form', '', 'k2/k2-content/k2-item-edit-form', 'index.php?option=com_k2&view=item&layout=itemform&task=add', 'component', 0, 176, 3, 10013, 0, '0000-00-00 00:00:00', 2, 1, '', 0, '{"theme":"default","category":"0","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"","show_page_heading":"","page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0}', 111, 112, 0, '*', 0),
(182, 'mainmenu', 'K2 Modules', 'k2-modules', '', 'k2/k2-modules', 'index.php?option=com_content&view=category&layout=blog&id=23', 'component', 1, 175, 2, 22, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"layout_type":"blog","show_category_heading_title_text":"","show_category_title":"","show_description":"","show_description_image":"","maxLevel":"","show_empty_categories":"","show_no_articles":"","show_subcat_desc":"","show_cat_num_articles":"","show_cat_tags":"","page_subheading":"","num_leading_articles":"","num_intro_articles":"","num_columns":"","num_links":"","multi_column_order":"","show_subcategory_content":"","orderby_pri":"","orderby_sec":"","order_date":"","show_pagination":"","show_pagination_results":"","show_featured":"","show_title":"","link_titles":"","show_intro":"","info_block_position":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_vote":"","show_readmore":"","show_readmore_title":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_hits":"","show_tags":"","show_noauth":"","show_feed_link":"","feed_summary":"","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"","show_page_heading":"","page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0}', 114, 115, 0, '*', 0),
(186, 'mainmenu', 'List Styles', 'list-styles', '', 'typography/list-styles', 'index.php?option=com_content&view=category&layout=blog&id=18', 'component', 1, 116, 2, 22, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"layout_type":"blog","show_category_heading_title_text":"","show_category_title":"","show_description":"","show_description_image":"","maxLevel":"","show_empty_categories":"","show_no_articles":"","show_subcat_desc":"","show_cat_num_articles":"","show_cat_tags":"","page_subheading":"","num_leading_articles":"","num_intro_articles":"","num_columns":"","num_links":"","multi_column_order":"","show_subcategory_content":"","orderby_pri":"","orderby_sec":"","order_date":"","show_pagination":"","show_pagination_results":"","show_featured":"","show_title":"","link_titles":"","show_intro":"","info_block_position":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_vote":"","show_readmore":"","show_readmore_title":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_hits":"","show_tags":"","show_noauth":"","show_feed_link":"","feed_summary":"","menu-anchor_title":"","menu-anchor_css":"fa-list ","menu_image":"","menu_text":1,"page_title":"","show_page_heading":"","page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0}', 40, 41, 0, '*', 0),
(187, 'mainmenu', 'Main Navigation', 'main-navigation-styles', '', 'features/navigation-styles/main-navigation-styles', 'index.php?option=com_content&view=category&layout=blog&id=17', 'component', 1, 115, 3, 22, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"layout_type":"blog","show_category_heading_title_text":"","show_category_title":"","show_description":"","show_description_image":"","maxLevel":"","show_empty_categories":"","show_no_articles":"","show_subcat_desc":"","show_cat_num_articles":"","show_cat_tags":"","page_subheading":"","num_leading_articles":"","num_intro_articles":"","num_columns":"","num_links":"","multi_column_order":"","show_subcategory_content":"","orderby_pri":"","orderby_sec":"","order_date":"","show_pagination":"","show_pagination_results":"","show_featured":"","show_title":"","link_titles":"","show_intro":"","info_block_position":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_vote":"","show_readmore":"","show_readmore_title":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_hits":"","show_tags":"","show_noauth":"","show_feed_link":"","feed_summary":"","menu-anchor_title":"","menu-anchor_css":"fa-minus ","menu_image":"","menu_text":1,"page_title":"","show_page_heading":"","page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0}', 19, 20, 0, '*', 0),
(188, 'mainmenu', 'Vertical Menus', 'vertical-menu-styles', '', 'features/navigation-styles/vertical-menu-styles', 'index.php?option=com_content&view=category&layout=blog&id=17', 'component', 1, 115, 3, 22, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"layout_type":"blog","show_category_heading_title_text":"","show_category_title":"","show_description":"","show_description_image":"","maxLevel":"","show_empty_categories":"","show_no_articles":"","show_subcat_desc":"","show_cat_num_articles":"","show_cat_tags":"","page_subheading":"","num_leading_articles":"","num_intro_articles":"","num_columns":"","num_links":"","multi_column_order":"","show_subcategory_content":"","orderby_pri":"","orderby_sec":"","order_date":"","show_pagination":"","show_pagination_results":"","show_featured":"","show_title":"","link_titles":"","show_intro":"","info_block_position":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_vote":"","show_readmore":"","show_readmore_title":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_hits":"","show_tags":"","show_noauth":"","show_feed_link":"","feed_summary":"","menu-anchor_title":"","menu-anchor_css":"fa-bars","menu_image":"","menu_text":1,"page_title":"","show_page_heading":"","page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0}', 21, 22, 0, '*', 0),
(189, 'mainmenu', 'Horizontal Menus', 'horizontal-menu-styles', '', 'features/navigation-styles/horizontal-menu-styles', 'index.php?option=com_content&view=category&layout=blog&id=17', 'component', 1, 115, 3, 22, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"layout_type":"blog","show_category_heading_title_text":"","show_category_title":"","show_description":"","show_description_image":"","maxLevel":"","show_empty_categories":"","show_no_articles":"","show_subcat_desc":"","show_cat_num_articles":"","show_cat_tags":"","page_subheading":"","num_leading_articles":"","num_intro_articles":"","num_columns":"","num_links":"","multi_column_order":"","show_subcategory_content":"","orderby_pri":"","orderby_sec":"","order_date":"","show_pagination":"","show_pagination_results":"","show_featured":"","show_title":"","link_titles":"","show_intro":"","info_block_position":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_vote":"","show_readmore":"","show_readmore_title":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_hits":"","show_tags":"","show_noauth":"","show_feed_link":"","feed_summary":"","menu-anchor_title":"","menu-anchor_css":"fa-ellipsis-h ","menu_image":"","menu_text":1,"page_title":"","show_page_heading":"","page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0}', 23, 24, 0, '*', 0),
(217, 'mainmenu', 'Offline Page', 'offline-page', '', 'features/offline-page', 'index.php?option=com_content&view=category&layout=blog&id=24', 'component', 1, 112, 2, 22, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"layout_type":"blog","show_category_heading_title_text":"","show_category_title":"","show_description":"","show_description_image":"","maxLevel":"","show_empty_categories":"","show_no_articles":"","show_subcat_desc":"","show_cat_num_articles":"","show_cat_tags":"","page_subheading":"","num_leading_articles":"","num_intro_articles":"","num_columns":"","num_links":"","multi_column_order":"","show_subcategory_content":"","orderby_pri":"","orderby_sec":"","order_date":"","show_pagination":"","show_pagination_results":"","show_featured":"","show_title":"","link_titles":"","show_intro":"","info_block_position":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_vote":"","show_readmore":"","show_readmore_title":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_hits":"","show_tags":"","show_noauth":"","show_feed_link":"","feed_summary":"","menu-anchor_title":"","menu-anchor_css":"fa-paper-plane-o","menu_image":"","menu_text":1,"page_title":"","show_page_heading":"","page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0}', 28, 29, 0, '*', 0),
(218, 'mainmenu', 'Sample Menu', '2015-08-07-13-14-23', '', '2015-08-07-13-14-23', '#', 'url', 0, 1, 1, 22, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1}', 121, 142, 0, '*', 0),
(219, 'mainmenu', 'Sample Menu', '2015-08-07-13-14-03', '', '2015-08-07-13-14-23/2015-08-07-13-14-03', '#', 'url', 0, 218, 2, 0, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1}', 122, 123, 0, '*', 0),
(220, 'mainmenu', 'Sample Menu', '2015-08-07-13-14-58', '', '2015-08-07-13-14-23/2015-08-07-13-14-58', '#', 'url', 0, 218, 2, 0, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1}', 124, 141, 0, '*', 0),
(221, 'mainmenu', 'Sample Menu', '2015-08-07-13-15-29', '', '2015-08-07-13-14-23/2015-08-07-13-14-58/2015-08-07-13-15-29', '#', 'url', 0, 220, 3, 0, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1}', 125, 126, 0, '*', 0),
(222, 'mainmenu', 'Sample Menu', '2015-08-07-13-15-54', '', '2015-08-07-13-14-23/2015-08-07-13-14-58/2015-08-07-13-15-54', '#', 'url', 0, 220, 3, 0, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1}', 127, 140, 0, '*', 0),
(223, 'mainmenu', 'Sample Menu', '2015-08-07-13-16-19', '', '2015-08-07-13-14-23/2015-08-07-13-14-58/2015-08-07-13-15-54/2015-08-07-13-16-19', '#', 'url', 0, 222, 4, 0, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1}', 128, 129, 0, '*', 0),
(224, 'mainmenu', 'Sample Menu', '2015-08-07-13-16-47', '', '2015-08-07-13-14-23/2015-08-07-13-14-58/2015-08-07-13-15-54/2015-08-07-13-16-47', '#', 'url', 0, 222, 4, 0, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1}', 130, 139, 0, '*', 0),
(225, 'mainmenu', 'Sample Menu', '2015-08-07-13-15-29', '', '2015-08-07-13-14-23/2015-08-07-13-14-58/2015-08-07-13-15-54/2015-08-07-13-16-47/2015-08-07-13-15-29', '#', 'url', 0, 224, 5, 0, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1}', 131, 132, 0, '*', 0),
(226, 'mainmenu', 'Sample Menu', '2015-08-07-13-15-54', '', '2015-08-07-13-14-23/2015-08-07-13-14-58/2015-08-07-13-15-54/2015-08-07-13-16-47/2015-08-07-13-15-54', '#', 'url', 0, 224, 5, 0, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1}', 133, 138, 0, '*', 0),
(227, 'mainmenu', 'Sample Menu', '2015-08-07-13-16-19', '', '2015-08-07-13-14-23/2015-08-07-13-14-58/2015-08-07-13-15-54/2015-08-07-13-16-47/2015-08-07-13-15-54/2015-08-07-13-16-19', '#', 'url', 0, 226, 6, 0, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1}', 134, 135, 0, '*', 0),
(228, 'mainmenu', 'Sample Menu', '2015-08-07-13-16-47', '', '2015-08-07-13-14-23/2015-08-07-13-14-58/2015-08-07-13-15-54/2015-08-07-13-16-47/2015-08-07-13-15-54/2015-08-07-13-16-47', '#', 'url', 0, 226, 6, 0, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1}', 136, 137, 0, '*', 0);

-- --------------------------------------------------------

--
-- Table structure for table `#__menu_types`
--

DROP TABLE IF EXISTS `#__menu_types`;
CREATE TABLE IF NOT EXISTS `#__menu_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `menutype` varchar(24) NOT NULL,
  `title` varchar(48) NOT NULL,
  `description` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_menutype` (`menutype`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `#__menu_types`
--

INSERT INTO `#__menu_types` (`id`, `menutype`, `title`, `description`) VALUES
(1, 'mainmenu', 'Main Menu', 'The main menu for the site'),
(2, 'usermenu', 'User Menu', 'A Menu for logged-in Users');

-- --------------------------------------------------------

--
-- Table structure for table `#__messages`
--

DROP TABLE IF EXISTS `#__messages`;
CREATE TABLE IF NOT EXISTS `#__messages` (
  `message_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id_from` int(10) unsigned NOT NULL DEFAULT '0',
  `user_id_to` int(10) unsigned NOT NULL DEFAULT '0',
  `folder_id` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `date_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `state` tinyint(1) NOT NULL DEFAULT '0',
  `priority` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `subject` varchar(255) NOT NULL DEFAULT '',
  `message` text NOT NULL,
  PRIMARY KEY (`message_id`),
  KEY `useridto_state` (`user_id_to`,`state`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `#__messages_cfg`
--

DROP TABLE IF EXISTS `#__messages_cfg`;
CREATE TABLE IF NOT EXISTS `#__messages_cfg` (
  `user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `cfg_name` varchar(100) NOT NULL DEFAULT '',
  `cfg_value` varchar(255) NOT NULL DEFAULT '',
  UNIQUE KEY `idx_user_var_name` (`user_id`,`cfg_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `#__modules`
--

DROP TABLE IF EXISTS `#__modules`;
CREATE TABLE IF NOT EXISTS `#__modules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `asset_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'FK to the #__assets table.',
  `title` varchar(100) NOT NULL DEFAULT '',
  `note` varchar(255) NOT NULL DEFAULT '',
  `content` text NOT NULL,
  `ordering` int(11) NOT NULL DEFAULT '0',
  `position` varchar(50) NOT NULL DEFAULT '',
  `checked_out` int(10) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `module` varchar(50) DEFAULT NULL,
  `access` int(10) unsigned NOT NULL DEFAULT '0',
  `showtitle` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `params` text NOT NULL,
  `client_id` tinyint(4) NOT NULL DEFAULT '0',
  `language` char(7) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `published` (`published`,`access`),
  KEY `newsfeeds` (`module`,`published`),
  KEY `idx_language` (`language`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=491 ;

-- --------------------------------------------------------

--
-- Table structure for table `#__modules_menu`
--

DROP TABLE IF EXISTS `#__modules_menu`;
CREATE TABLE IF NOT EXISTS `#__modules_menu` (
  `moduleid` int(11) NOT NULL DEFAULT '0',
  `menuid` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`moduleid`,`menuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `#__newsfeeds`
--

DROP TABLE IF EXISTS `#__newsfeeds`;
CREATE TABLE IF NOT EXISTS `#__newsfeeds` (
  `catid` int(11) NOT NULL DEFAULT '0',
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL DEFAULT '',
  `alias` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `link` varchar(200) NOT NULL DEFAULT '',
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `numarticles` int(10) unsigned NOT NULL DEFAULT '1',
  `cache_time` int(10) unsigned NOT NULL DEFAULT '3600',
  `checked_out` int(10) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `rtl` tinyint(4) NOT NULL DEFAULT '0',
  `access` int(10) unsigned NOT NULL DEFAULT '0',
  `language` char(7) NOT NULL DEFAULT '',
  `params` text NOT NULL,
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(10) unsigned NOT NULL DEFAULT '0',
  `created_by_alias` varchar(255) NOT NULL DEFAULT '',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(10) unsigned NOT NULL DEFAULT '0',
  `metakey` text NOT NULL,
  `metadesc` text NOT NULL,
  `metadata` text NOT NULL,
  `xreference` varchar(50) NOT NULL COMMENT 'A reference to enable linkages to external data sets.',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `description` text NOT NULL,
  `version` int(10) unsigned NOT NULL DEFAULT '1',
  `hits` int(10) unsigned NOT NULL DEFAULT '0',
  `images` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_access` (`access`),
  KEY `idx_checkout` (`checked_out`),
  KEY `idx_state` (`published`),
  KEY `idx_catid` (`catid`),
  KEY `idx_createdby` (`created_by`),
  KEY `idx_language` (`language`),
  KEY `idx_xreference` (`xreference`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `#__newsfeeds`
--

INSERT INTO `#__newsfeeds` (`catid`, `id`, `name`, `alias`, `link`, `published`, `numarticles`, `cache_time`, `checked_out`, `checked_out_time`, `ordering`, `rtl`, `access`, `language`, `params`, `created`, `created_by`, `created_by_alias`, `modified`, `modified_by`, `metakey`, `metadesc`, `metadata`, `xreference`, `publish_up`, `publish_down`, `description`, `version`, `hits`, `images`) VALUES
(5, 1, 'Demo News Feed', 'demo-news-feed', 'http://feeds.joomla.org/JoomlaAnnouncements', 1, 5, 3600, 0, '0000-00-00 00:00:00', 1, 0, 1, '*', '{"show_feed_image":"","show_feed_description":"","show_item_description":"","feed_character_count":"0","newsfeed_layout":"","feed_display_order":""}', '2015-04-12 14:46:03', 851, '', '2015-04-12 14:46:03', 0, '', '', '{"robots":"","rights":""}', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', 1, 14, '{"image_first":"","float_first":"","image_first_alt":"","image_first_caption":"","image_second":"","float_second":"","image_second_alt":"","image_second_caption":""}');

-- --------------------------------------------------------

--
-- Table structure for table `#__overrider`
--

DROP TABLE IF EXISTS `#__overrider`;
CREATE TABLE IF NOT EXISTS `#__overrider` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `constant` varchar(255) NOT NULL,
  `string` text NOT NULL,
  `file` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `#__postinstall_messages`
--

DROP TABLE IF EXISTS `#__postinstall_messages`;
CREATE TABLE IF NOT EXISTS `#__postinstall_messages` (
  `postinstall_message_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `extension_id` bigint(20) NOT NULL DEFAULT '700' COMMENT 'FK to #__extensions',
  `title_key` varchar(255) NOT NULL DEFAULT '' COMMENT 'Lang key for the title',
  `description_key` varchar(255) NOT NULL DEFAULT '' COMMENT 'Lang key for description',
  `action_key` varchar(255) NOT NULL DEFAULT '',
  `language_extension` varchar(255) NOT NULL DEFAULT 'com_postinstall' COMMENT 'Extension holding lang keys',
  `language_client_id` tinyint(3) NOT NULL DEFAULT '1',
  `type` varchar(10) NOT NULL DEFAULT 'link' COMMENT 'Message type - message, link, action',
  `action_file` varchar(255) DEFAULT '' COMMENT 'RAD URI to the PHP file containing action method',
  `action` varchar(255) DEFAULT '' COMMENT 'Action method name or URL',
  `condition_file` varchar(255) DEFAULT NULL COMMENT 'RAD URI to file holding display condition method',
  `condition_method` varchar(255) DEFAULT NULL COMMENT 'Display condition method, must return boolean',
  `version_introduced` varchar(50) NOT NULL DEFAULT '3.2.0' COMMENT 'Version when this message was introduced',
  `enabled` tinyint(3) NOT NULL DEFAULT '1',
  PRIMARY KEY (`postinstall_message_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `#__postinstall_messages`
--

INSERT INTO `#__postinstall_messages` (`postinstall_message_id`, `extension_id`, `title_key`, `description_key`, `action_key`, `language_extension`, `language_client_id`, `type`, `action_file`, `action`, `condition_file`, `condition_method`, `version_introduced`, `enabled`) VALUES
(1, 700, 'PLG_TWOFACTORAUTH_TOTP_POSTINSTALL_TITLE', 'PLG_TWOFACTORAUTH_TOTP_POSTINSTALL_BODY', 'PLG_TWOFACTORAUTH_TOTP_POSTINSTALL_ACTION', 'plg_twofactorauth_totp', 1, 'action', 'site://plugins/twofactorauth/totp/postinstall/actions.php', 'twofactorauth_postinstall_action', 'site://plugins/twofactorauth/totp/postinstall/actions.php', 'twofactorauth_postinstall_condition', '3.2.0', 1),
(2, 700, 'COM_CPANEL_WELCOME_BEGINNERS_TITLE', 'COM_CPANEL_WELCOME_BEGINNERS_MESSAGE', '', 'com_cpanel', 1, 'message', '', '', '', '', '3.2.0', 1);

-- --------------------------------------------------------

--
-- Table structure for table `#__redirect_links`
--

DROP TABLE IF EXISTS `#__redirect_links`;
CREATE TABLE IF NOT EXISTS `#__redirect_links` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `old_url` varchar(255) NOT NULL,
  `new_url` varchar(255) DEFAULT NULL,
  `referer` varchar(150) NOT NULL,
  `comment` varchar(255) NOT NULL,
  `hits` int(10) unsigned NOT NULL DEFAULT '0',
  `published` tinyint(4) NOT NULL,
  `created_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `header` smallint(3) NOT NULL DEFAULT '301',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_link_old` (`old_url`),
  KEY `idx_link_modifed` (`modified_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `#__schemas`
--

DROP TABLE IF EXISTS `#__schemas`;
CREATE TABLE IF NOT EXISTS `#__schemas` (
  `extension_id` int(11) NOT NULL,
  `version_id` varchar(20) NOT NULL,
  PRIMARY KEY (`extension_id`,`version_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `#__schemas`
--

INSERT INTO `#__schemas` (`extension_id`, `version_id`) VALUES
(700, '3.4.0-2015-02-26');

-- --------------------------------------------------------

--
-- Table structure for table `#__session`
--

DROP TABLE IF EXISTS `#__session`;
CREATE TABLE IF NOT EXISTS `#__session` (
  `session_id` varchar(200) NOT NULL DEFAULT '',
  `client_id` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `guest` tinyint(4) unsigned DEFAULT '1',
  `time` varchar(14) DEFAULT '',
  `data` mediumtext,
  `userid` int(11) DEFAULT '0',
  `username` varchar(150) DEFAULT '',
  PRIMARY KEY (`session_id`),
  KEY `userid` (`userid`),
  KEY `time` (`time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `#__tags`
--

DROP TABLE IF EXISTS `#__tags`;
CREATE TABLE IF NOT EXISTS `#__tags` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) unsigned NOT NULL DEFAULT '0',
  `lft` int(11) NOT NULL DEFAULT '0',
  `rgt` int(11) NOT NULL DEFAULT '0',
  `level` int(10) unsigned NOT NULL DEFAULT '0',
  `path` varchar(255) NOT NULL DEFAULT '',
  `title` varchar(255) NOT NULL,
  `alias` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `note` varchar(255) NOT NULL DEFAULT '',
  `description` mediumtext NOT NULL,
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `checked_out` int(11) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `access` int(10) unsigned NOT NULL DEFAULT '0',
  `params` text NOT NULL,
  `metadesc` varchar(1024) NOT NULL COMMENT 'The meta description for the page.',
  `metakey` varchar(1024) NOT NULL COMMENT 'The meta keywords for the page.',
  `metadata` varchar(2048) NOT NULL COMMENT 'JSON encoded metadata properties.',
  `created_user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `created_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by_alias` varchar(255) NOT NULL DEFAULT '',
  `modified_user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `modified_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `images` text NOT NULL,
  `urls` text NOT NULL,
  `hits` int(10) unsigned NOT NULL DEFAULT '0',
  `language` char(7) NOT NULL,
  `version` int(10) unsigned NOT NULL DEFAULT '1',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `tag_idx` (`published`,`access`),
  KEY `idx_access` (`access`),
  KEY `idx_checkout` (`checked_out`),
  KEY `idx_path` (`path`),
  KEY `idx_left_right` (`lft`,`rgt`),
  KEY `idx_alias` (`alias`),
  KEY `idx_language` (`language`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `#__tags`
--

INSERT INTO `#__tags` (`id`, `parent_id`, `lft`, `rgt`, `level`, `path`, `title`, `alias`, `note`, `description`, `published`, `checked_out`, `checked_out_time`, `access`, `params`, `metadesc`, `metakey`, `metadata`, `created_user_id`, `created_time`, `created_by_alias`, `modified_user_id`, `modified_time`, `images`, `urls`, `hits`, `language`, `version`, `publish_up`, `publish_down`) VALUES
(1, 0, 0, 7, 0, '', 'ROOT', 'root', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{}', '', '', '', 851, '2011-01-01 00:00:01', '', 0, '0000-00-00 00:00:00', '', '', 0, '*', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 1, 1, 2, 1, 'joomla', 'joomla', 'joomla', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{"tag_layout":"","tag_link_class":"label label-info"}', '', '', '{"author":"","robots":""}', 851, '2013-11-16 00:00:00', '', 851, '2015-04-12 14:21:38', '{"image_intro":"","float_intro":"","image_intro_alt":"","image_intro_caption":"","image_fulltext":"","float_fulltext":"","image_fulltext_alt":"","image_fulltext_caption":""}', '{"0":"{\\"urla\\":\\"\\"}"}', 30, '*', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 1, 3, 4, 1, 'responsive', 'responsive', 'responsive', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{"tag_layout":"","tag_link_class":"label label-info"}', '', '', '{"author":"","robots":""}', 851, '2015-04-12 14:21:54', '', 0, '2015-04-12 14:21:54', '{"image_intro":"","float_intro":"","image_intro_alt":"","image_intro_caption":"","image_fulltext":"","float_fulltext":"","image_fulltext_alt":"","image_fulltext_caption":""}', '{"0":"{\\"urla\\":\\"\\"}"}', 0, '*', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 1, 5, 6, 1, 'premium', 'premium', 'premium', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{"tag_layout":"","tag_link_class":"label label-info"}', '', '', '{"author":"","robots":""}', 851, '2015-04-12 14:22:06', '', 0, '2015-04-12 14:22:06', '{"image_intro":"","float_intro":"","image_intro_alt":"","image_intro_caption":"","image_fulltext":"","float_fulltext":"","image_fulltext_alt":"","image_fulltext_caption":""}', '{"0":"{\\"urla\\":\\"\\"}"}', 0, '*', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `#__template_styles`
--

DROP TABLE IF EXISTS `#__template_styles`;
CREATE TABLE IF NOT EXISTS `#__template_styles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `template` varchar(50) NOT NULL DEFAULT '',
  `client_id` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `home` char(7) NOT NULL DEFAULT '0',
  `title` varchar(255) NOT NULL DEFAULT '',
  `params` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_template` (`template`),
  KEY `idx_home` (`home`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=25 ;

--
-- Dumping data for table `#__template_styles`
--

INSERT INTO `#__template_styles` (`id`, `template`, `client_id`, `home`, `title`, `params`) VALUES
(4, 'beez3', 0, '0', 'Beez3 - Default', '{"wrapperSmall":"53","wrapperLarge":"72","logo":"images\\/joomla_black.gif","sitetitle":"Joomla!","sitedescription":"Open Source Content Management","navposition":"left","templatecolor":"personal","html5":"0"}'),
(5, 'hathor', 1, '0', 'Hathor - Default', '{"showSiteName":"0","colourChoice":"","boldText":"0"}'),
(7, 'protostar', 0, '0', 'protostar - Default', '{"templateColor":"","logoFile":"","googleFont":"1","googleFontName":"Open+Sans","fluidContainer":"0"}'),
(8, 'isis', 1, '1', 'isis - Default', '{"templateColor":"","logoFile":""}'),
(24, 'favourite', 0, '1', 'Favourite - Default', '{"template_styles":"style1","max_width":"","show_copyright":"1","copyright_text":"FavThemes","copyright_text_link":"www.favthemes.com","nav_style_color":"","nav_google_font":"","nav_google_font_weight":"400","nav_google_font_style":"normal","nav_text_transform":"none","nav_font_size":"","nav_link_padding":"","nav_icons_color":"","nav_icons_font_size":"","link_color":"","link_color_hover":"","titles_google_font":"","titles_google_font_weight":"400","titles_google_font_style":"normal","titles_text_align":"left","titles_text_transform":"none","article_title_color":"","article_title_link_color":"","article_title_link_hover_color":"","article_title_font_size":"","article_title_line_height":"","module_title_color":"","module_title_font_size":"","module_title_line_height":"","module_title_icon_color":"","module_title_icon_font_size":"","module_title_icon_padding":"","module_title_icon_border_color":"","module_title_icon_border_radius":"","variation_style_color":"","variation_style_icon_font_size":"","variation_style_icon_padding":"","button_color":"","button_bg_color":"","button_hover_color":"","button_bg_hover_color":"","button_google_font":"","button_google_font_weight":"400","button_google_font_style":"normal","button_text_transform":"none","vertical_menus_style_color":"","vertical_menus_text_transform":"none","horizontal_menus_style_color":"","horizontal_menus_google_font":"","horizontal_menus_google_font_weight":"400","horizontal_menus_google_font_style":"normal","horizontal_menus_text_transform":"none","list_style_color":"","show_back_to_top":"1","error_page_article_id":"3","offline_page_style":"offline-light","offline_page_bg_image":"","offline_page_bg_image_style":"no-repeat; background-attachment: fixed; -webkit-background-size: cover; -moz-background-size: cover; -o-background-size: cover; background-size: cover;","responsive_k2_image":"100%!important","body_bg_image":"","body_bg_image_style":"repeat; background-attachment: initial; -webkit-background-size: auto; -moz-background-size: auto; -o-background-size: auto; background-size: auto;","body_bg_image_overlay":"fav-transparent","body_bg_color":"","body_color":"","body_title_color":"","body_link_color":"","body_link_hover_color":"","show_advert_button":"1","advert_bg_color":"","advert_color":"","advert_title_color":"","advert_link_color":"","advert_link_hover_color":"","topbar_bg_image":"","topbar_bg_image_style":"repeat; background-attachment: initial; -webkit-background-size: auto; -moz-background-size: auto; -o-background-size: auto; background-size: auto;","topbar_bg_image_overlay":"fav-transparent","topbar_bg_color":"","topbar_color":"","topbar_title_color":"","topbar_link_color":"","topbar_link_hover_color":"","slide_width":"","slide_bg_image":"","slide_bg_image_style":"repeat; background-attachment: initial; -webkit-background-size: auto; -moz-background-size: auto; -o-background-size: auto; background-size: auto;","slide_bg_image_overlay":"fav-transparent","slide_bg_color":"","slide_color":"","slide_title_color":"","slide_link_color":"","slide_link_hover_color":"","intro_bg_image":"","intro_bg_image_style":"no-repeat; background-attachment: fixed; -webkit-background-size: cover; -moz-background-size: cover; -o-background-size: cover; background-size: cover;","intro_bg_image_overlay":"fav-overlay","intro_bg_color":"","intro_color":"","intro_title_color":"","intro_link_color":"","intro_link_hover_color":"","lead_bg_image":"","lead_bg_image_style":"repeat; background-attachment: initial; -webkit-background-size: auto; -moz-background-size: auto; -o-background-size: auto; background-size: auto;","lead_bg_image_overlay":"fav-transparent","lead_bg_color":"","lead_color":"","lead_title_color":"","lead_link_color":"","lead_link_hover_color":"","promo_bg_image":"","promo_bg_image_style":"repeat; background-attachment: initial; -webkit-background-size: auto; -moz-background-size: auto; -o-background-size: auto; background-size: auto;","promo_bg_image_overlay":"fav-transparent","promo_bg_color":"","promo_color":"","promo_title_color":"","promo_link_color":"","promo_link_hover_color":"","prime_bg_image":"","prime_bg_image_style":"no-repeat; background-attachment: fixed; -webkit-background-size: cover; -moz-background-size: cover; -o-background-size: cover; background-size: cover;","prime_bg_image_overlay":"fav-overlay","prime_bg_color":"","prime_color":"","prime_title_color":"","prime_link_color":"","prime_link_hover_color":"","showcase_bg_image":"","showcase_bg_image_style":"repeat; background-attachment: initial; -webkit-background-size: auto; -moz-background-size: auto; -o-background-size: auto; background-size: auto;","showcase_bg_image_overlay":"fav-transparent","showcase_bg_color":"","showcase_color":"","showcase_title_color":"","showcase_link_color":"","showcase_link_hover_color":"","feature_bg_image":"","feature_bg_image_style":"repeat; background-attachment: initial; -webkit-background-size: auto; -moz-background-size: auto; -o-background-size: auto; background-size: auto;","feature_bg_image_overlay":"fav-transparent","feature_bg_color":"","feature_color":"","feature_title_color":"","feature_link_color":"","feature_link_hover_color":"","focus_bg_image":"","focus_bg_image_style":"repeat; background-attachment: initial; -webkit-background-size: auto; -moz-background-size: auto; -o-background-size: auto; background-size: auto;","focus_bg_image_overlay":"fav-transparent","focus_bg_color":"","focus_color":"","focus_title_color":"","focus_link_color":"","focus_link_hover_color":"","portfolio_bg_image":"","portfolio_bg_image_style":"repeat; background-attachment: initial; -webkit-background-size: auto; -moz-background-size: auto; -o-background-size: auto; background-size: auto;","portfolio_bg_image_overlay":"fav-transparent","portfolio_bg_color":"","portfolio_color":"","portfolio_title_color":"","portfolio_link_color":"","portfolio_link_hover_color":"","screen_bg_image":"","screen_bg_image_style":"no-repeat; background-attachment: fixed; -webkit-background-size: cover; -moz-background-size: cover; -o-background-size: cover; background-size: cover;","screen_bg_image_overlay":"fav-overlay","screen_bg_color":"","screen_color":"","screen_title_color":"","screen_link_color":"","screen_link_hover_color":"","top_bg_image":"","top_bg_image_style":"repeat; background-attachment: initial; -webkit-background-size: auto; -moz-background-size: auto; -o-background-size: auto; background-size: auto;","top_bg_image_overlay":"fav-transparent","top_bg_color":"","top_color":"","top_title_color":"","top_link_color":"","top_link_hover_color":"","maintop_bg_image":"","maintop_bg_image_style":"repeat; background-attachment: initial; -webkit-background-size: auto; -moz-background-size: auto; -o-background-size: auto; background-size: auto;","maintop_bg_image_overlay":"fav-transparent","maintop_bg_color":"","maintop_color":"","maintop_title_color":"","maintop_link_color":"","maintop_link_hover_color":"","mainbottom_bg_image":"","mainbottom_bg_image_style":"repeat; background-attachment: initial; -webkit-background-size: auto; -moz-background-size: auto; -o-background-size: auto; background-size: auto;","mainbottom_bg_image_overlay":"fav-transparent","mainbottom_bg_color":"","mainbottom_color":"","mainbottom_title_color":"","mainbottom_link_color":"","mainbottom_link_hover_color":"","bottom_bg_image":"","bottom_bg_image_style":"repeat; background-attachment: initial; -webkit-background-size: auto; -moz-background-size: auto; -o-background-size: auto; background-size: auto;","bottom_bg_image_overlay":"fav-transparent","bottom_bg_color":"","bottom_color":"","bottom_title_color":"","bottom_link_color":"","bottom_link_hover_color":"","note_bg_image":"","note_bg_image_style":"repeat; background-attachment: initial; -webkit-background-size: auto; -moz-background-size: auto; -o-background-size: auto; background-size: auto;","note_bg_image_overlay":"fav-transparent","note_bg_color":"","note_color":"","note_title_color":"","note_link_color":"","note_link_hover_color":"","base_bg_image":"","base_bg_image_style":"no-repeat; background-attachment: fixed; -webkit-background-size: cover; -moz-background-size: cover; -o-background-size: cover; background-size: cover;","base_bg_image_overlay":"fav-overlay","base_bg_color":"","base_color":"","base_title_color":"","base_link_color":"","base_link_hover_color":"","block_bg_image":"","block_bg_image_style":"repeat; background-attachment: initial; -webkit-background-size: auto; -moz-background-size: auto; -o-background-size: auto; background-size: auto;","block_bg_image_overlay":"fav-transparent","block_bg_color":"","block_color":"","block_title_color":"","block_link_color":"","block_link_hover_color":"","user_bg_image":"","user_bg_image_style":"repeat; background-attachment: initial; -webkit-background-size: auto; -moz-background-size: auto; -o-background-size: auto; background-size: auto;","user_bg_image_overlay":"fav-transparent","user_bg_color":"","user_color":"","user_title_color":"","user_link_color":"","user_link_hover_color":"","footer_bg_image":"","footer_bg_image_style":"repeat; background-attachment: initial; -webkit-background-size: auto; -moz-background-size: auto; -o-background-size: auto; background-size: auto;","footer_bg_image_overlay":"fav-transparent","footer_bg_color":"","footer_color":"","footer_title_color":"","footer_link_color":"","footer_link_hover_color":"","show_default_logo":"1","default_logo":"logo.png","default_logo_img_alt":"Favourite template","default_logo_padding":"","default_logo_margin":"","show_media_logo":"0","media_logo":"","media_logo_img_alt":"Favourite template","media_logo_padding":"","media_logo_margin":"","show_text_logo":"0","text_logo":"Favourite","text_logo_color":"","text_logo_font_size":"","text_logo_google_font":"","text_logo_google_font_weight":"400","text_logo_google_font_style":"normal","text_logo_line_height":"","text_logo_padding":"","text_logo_margin":"","show_slogan":"0","slogan":"slogan text here","slogan_color":"","slogan_font_size":"","slogan_line_height":"","slogan_padding":"","slogan_margin":"","show_retina_logo":"0","retina_logo":"","retina_logo_height":"52px","retina_logo_width":"188px","retina_logo_img_alt":"Favourite template","retina_logo_padding":"0px","retina_logo_margin":"0px","mobile_nav_color":"navbar","show_mobile_submenu":"none","show_mobile_menu_text":"1","mobile_menu_text":"Menu","mobile_show_images":"inline-base","paragraph_mobile_font_size":"","article_mobile_title_font_size":"","module_mobile_title_font_size":"","analytics_code":""}');

-- --------------------------------------------------------

--
-- Table structure for table `#__ucm_base`
--

DROP TABLE IF EXISTS `#__ucm_base`;
CREATE TABLE IF NOT EXISTS `#__ucm_base` (
  `ucm_id` int(10) unsigned NOT NULL,
  `ucm_item_id` int(10) NOT NULL,
  `ucm_type_id` int(11) NOT NULL,
  `ucm_language_id` int(11) NOT NULL,
  PRIMARY KEY (`ucm_id`),
  KEY `idx_ucm_item_id` (`ucm_item_id`),
  KEY `idx_ucm_type_id` (`ucm_type_id`),
  KEY `idx_ucm_language_id` (`ucm_language_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `#__ucm_base`
--

INSERT INTO `#__ucm_base` (`ucm_id`, `ucm_item_id`, `ucm_type_id`, `ucm_language_id`) VALUES
(2, 5, 1, 0),
(3, 4, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `#__ucm_content`
--

DROP TABLE IF EXISTS `#__ucm_content`;
CREATE TABLE IF NOT EXISTS `#__ucm_content` (
  `core_content_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `core_type_alias` varchar(255) NOT NULL DEFAULT '' COMMENT 'FK to the content types table',
  `core_title` varchar(255) NOT NULL,
  `core_alias` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `core_body` mediumtext NOT NULL,
  `core_state` tinyint(1) NOT NULL DEFAULT '0',
  `core_checked_out_time` varchar(255) NOT NULL DEFAULT '',
  `core_checked_out_user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `core_access` int(10) unsigned NOT NULL DEFAULT '0',
  `core_params` text NOT NULL,
  `core_featured` tinyint(4) unsigned NOT NULL DEFAULT '0',
  `core_metadata` varchar(2048) NOT NULL COMMENT 'JSON encoded metadata properties.',
  `core_created_user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `core_created_by_alias` varchar(255) NOT NULL DEFAULT '',
  `core_created_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `core_modified_user_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Most recent user that modified',
  `core_modified_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `core_language` char(7) NOT NULL,
  `core_publish_up` datetime NOT NULL,
  `core_publish_down` datetime NOT NULL,
  `core_content_item_id` int(10) unsigned DEFAULT NULL COMMENT 'ID from the individual type table',
  `asset_id` int(10) unsigned DEFAULT NULL COMMENT 'FK to the #__assets table.',
  `core_images` text NOT NULL,
  `core_urls` text NOT NULL,
  `core_hits` int(10) unsigned NOT NULL DEFAULT '0',
  `core_version` int(10) unsigned NOT NULL DEFAULT '1',
  `core_ordering` int(11) NOT NULL DEFAULT '0',
  `core_metakey` text NOT NULL,
  `core_metadesc` text NOT NULL,
  `core_catid` int(10) unsigned NOT NULL DEFAULT '0',
  `core_xreference` varchar(50) NOT NULL COMMENT 'A reference to enable linkages to external data sets.',
  `core_type_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`core_content_id`),
  KEY `tag_idx` (`core_state`,`core_access`),
  KEY `idx_access` (`core_access`),
  KEY `idx_alias` (`core_alias`),
  KEY `idx_language` (`core_language`),
  KEY `idx_title` (`core_title`),
  KEY `idx_modified_time` (`core_modified_time`),
  KEY `idx_created_time` (`core_created_time`),
  KEY `idx_content_type` (`core_type_alias`),
  KEY `idx_core_modified_user_id` (`core_modified_user_id`),
  KEY `idx_core_checked_out_user_id` (`core_checked_out_user_id`),
  KEY `idx_core_created_user_id` (`core_created_user_id`),
  KEY `idx_core_type_id` (`core_type_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Contains core content data in name spaced fields' AUTO_INCREMENT=4 ;

--
-- Dumping data for table `#__ucm_content`
--

INSERT INTO `#__ucm_content` (`core_content_id`, `core_type_alias`, `core_title`, `core_alias`, `core_body`, `core_state`, `core_checked_out_time`, `core_checked_out_user_id`, `core_access`, `core_params`, `core_featured`, `core_metadata`, `core_created_user_id`, `core_created_by_alias`, `core_created_time`, `core_modified_user_id`, `core_modified_time`, `core_language`, `core_publish_up`, `core_publish_down`, `core_content_item_id`, `asset_id`, `core_images`, `core_urls`, `core_hits`, `core_version`, `core_ordering`, `core_metakey`, `core_metadesc`, `core_catid`, `core_xreference`, `core_type_id`) VALUES
(2, 'com_content.article', 'Responsive theme', 'responsive-theme', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Elit mus aliquet quisquam aute numquam assumenda unde libero dolores nostrud reprehenderit odit?</p>\r\n<p>Vivamus sit amet libero turpis, non venenatis urna. In blandit, odio convallis suscipit venenatis, ante ipsum cursus augue.</p>\r\n', 1, '', 0, 1, '{"show_title":"","link_titles":"","show_tags":"","show_intro":"","info_block_position":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_vote":"","show_hits":"","show_noauth":"","urls_position":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 0, '{"robots":"","author":"","rights":"","xreference":""}', 851, '', '2015-04-12 16:32:09', 851, '2015-06-06 12:42:08', '*', '2015-04-12 16:32:09', '0000-00-00 00:00:00', 5, 98, '{"image_intro":"images\\/demo\\/img\\/demo-img-5.jpg","float_intro":"","image_intro_alt":"","image_intro_caption":"","image_fulltext":"images\\/demo\\/img\\/demo-img-5.jpg","float_fulltext":"","image_fulltext_alt":"","image_fulltext_caption":""}', '{"urla":false,"urlatext":"","targeta":"","urlb":false,"urlbtext":"","targetb":"","urlc":false,"urlctext":"","targetc":""}', 1, 4, 2, '', '', 19, '', 1),
(3, 'com_content.article', 'Customize everything', 'customize-everything', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Elit mus aliquet quisquam aute numquam assumenda unde libero dolores nostrud reprehenderit odit?</p>\r\n<p>Vivamus sit amet libero turpis, non venenatis urna. In blandit, odio convallis suscipit venenatis, ante ipsum cursus augue.</p>\r\n', 1, '', 0, 1, '{"show_title":"","link_titles":"","show_tags":"","show_intro":"","info_block_position":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_vote":"","show_hits":"","show_noauth":"","urls_position":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 0, '{"robots":"","author":"","rights":"","xreference":""}', 851, '', '2015-04-12 16:30:42', 851, '2015-06-06 12:42:28', '*', '2015-04-12 16:30:42', '0000-00-00 00:00:00', 4, 99, '{"image_intro":"images\\/demo\\/img\\/demo-img-3.jpg","float_intro":"","image_intro_alt":"","image_intro_caption":"","image_fulltext":"images\\/demo\\/img\\/demo-img-3.jpg","float_fulltext":"","image_fulltext_alt":"","image_fulltext_caption":""}', '{"urla":false,"urlatext":"","targeta":"","urlb":false,"urlbtext":"","targetb":"","urlc":false,"urlctext":"","targetc":""}', 0, 4, 3, '', '', 19, '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `#__ucm_history`
--

DROP TABLE IF EXISTS `#__ucm_history`;
CREATE TABLE IF NOT EXISTS `#__ucm_history` (
  `version_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ucm_item_id` int(10) unsigned NOT NULL,
  `ucm_type_id` int(10) unsigned NOT NULL,
  `version_note` varchar(255) NOT NULL DEFAULT '' COMMENT 'Optional version name',
  `save_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `editor_user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `character_count` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Number of characters in this version.',
  `sha1_hash` varchar(50) NOT NULL DEFAULT '' COMMENT 'SHA1 hash of the version_data column.',
  `version_data` mediumtext NOT NULL COMMENT 'json-encoded string of version data',
  `keep_forever` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=auto delete; 1=keep',
  PRIMARY KEY (`version_id`),
  KEY `idx_ucm_item_id` (`ucm_type_id`,`ucm_item_id`),
  KEY `idx_save_date` (`save_date`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=211 ;

-- --------------------------------------------------------

--
-- Table structure for table `#__updates`
--

DROP TABLE IF EXISTS `#__updates`;
CREATE TABLE IF NOT EXISTS `#__updates` (
  `update_id` int(11) NOT NULL AUTO_INCREMENT,
  `update_site_id` int(11) DEFAULT '0',
  `extension_id` int(11) DEFAULT '0',
  `name` varchar(100) DEFAULT '',
  `description` text NOT NULL,
  `element` varchar(100) DEFAULT '',
  `type` varchar(20) DEFAULT '',
  `folder` varchar(20) DEFAULT '',
  `client_id` tinyint(3) DEFAULT '0',
  `version` varchar(32) DEFAULT '',
  `data` text NOT NULL,
  `detailsurl` text NOT NULL,
  `infourl` text NOT NULL,
  `extra_query` varchar(1000) DEFAULT '',
  PRIMARY KEY (`update_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Available Updates' AUTO_INCREMENT=36 ;

--
-- Dumping data for table `#__updates`
--

INSERT INTO `#__updates` (`update_id`, `update_site_id`, `extension_id`, `name`, `description`, `element`, `type`, `folder`, `client_id`, `version`, `data`, `detailsurl`, `infourl`, `extra_query`) VALUES
(1, 3, 0, 'Latvian', '', 'pkg_lv-LV', 'package', '', 0, '3.4.3.1', '', 'http://update.joomla.org/language/details3/lv-LV_details.xml', '', ''),
(2, 3, 0, 'Macedonian', '', 'pkg_mk-MK', 'package', '', 0, '3.4.5.1', '', 'http://update.joomla.org/language/details3/mk-MK_details.xml', '', ''),
(3, 3, 0, 'Norwegian Bokmal', '', 'pkg_nb-NO', 'package', '', 0, '3.4.5.1', '', 'http://update.joomla.org/language/details3/nb-NO_details.xml', '', ''),
(4, 3, 0, 'Norwegian Nynorsk', '', 'pkg_nn-NO', 'package', '', 0, '3.4.2.1', '', 'http://update.joomla.org/language/details3/nn-NO_details.xml', '', ''),
(5, 1, 700, 'Joomla', '', 'joomla', 'file', '', 0, '3.4.5', '', 'http://update.joomla.org/core/sts/extension_sts.xml', '', ''),
(6, 3, 0, 'Persian', '', 'pkg_fa-IR', 'package', '', 0, '3.4.5.1', '', 'http://update.joomla.org/language/details3/fa-IR_details.xml', '', ''),
(7, 3, 0, 'Polish', '', 'pkg_pl-PL', 'package', '', 0, '3.4.2.1', '', 'http://update.joomla.org/language/details3/pl-PL_details.xml', '', ''),
(8, 3, 0, 'Portuguese', '', 'pkg_pt-PT', 'package', '', 0, '3.4.5.1', '', 'http://update.joomla.org/language/details3/pt-PT_details.xml', '', ''),
(9, 3, 0, 'Russian', '', 'pkg_ru-RU', 'package', '', 0, '3.4.1.3', '', 'http://update.joomla.org/language/details3/ru-RU_details.xml', '', ''),
(10, 3, 0, 'Slovak', '', 'pkg_sk-SK', 'package', '', 0, '3.4.5.1', '', 'http://update.joomla.org/language/details3/sk-SK_details.xml', '', ''),
(11, 3, 0, 'Swedish', '', 'pkg_sv-SE', 'package', '', 0, '3.4.4.1', '', 'http://update.joomla.org/language/details3/sv-SE_details.xml', '', ''),
(12, 3, 0, 'Syriac', '', 'pkg_sy-IQ', 'package', '', 0, '3.4.4.1', '', 'http://update.joomla.org/language/details3/sy-IQ_details.xml', '', ''),
(13, 3, 0, 'Tamil', '', 'pkg_ta-IN', 'package', '', 0, '3.4.5.1', '', 'http://update.joomla.org/language/details3/ta-IN_details.xml', '', ''),
(14, 3, 0, 'Thai', '', 'pkg_th-TH', 'package', '', 0, '3.4.4.1', '', 'http://update.joomla.org/language/details3/th-TH_details.xml', '', ''),
(15, 3, 0, 'Turkish', '', 'pkg_tr-TR', 'package', '', 0, '3.4.5.1', '', 'http://update.joomla.org/language/details3/tr-TR_details.xml', '', ''),
(16, 3, 0, 'Ukrainian', '', 'pkg_uk-UA', 'package', '', 0, '3.4.4.1', '', 'http://update.joomla.org/language/details3/uk-UA_details.xml', '', ''),
(17, 3, 0, 'Uyghur', '', 'pkg_ug-CN', 'package', '', 0, '3.3.0.1', '', 'http://update.joomla.org/language/details3/ug-CN_details.xml', '', ''),
(18, 3, 0, 'Albanian', '', 'pkg_sq-AL', 'package', '', 0, '3.1.1.1', '', 'http://update.joomla.org/language/details3/sq-AL_details.xml', '', ''),
(19, 3, 0, 'Hindi', '', 'pkg_hi-IN', 'package', '', 0, '3.3.6.1', '', 'http://update.joomla.org/language/details3/hi-IN_details.xml', '', ''),
(20, 3, 0, 'Portuguese Brazil', '', 'pkg_pt-BR', 'package', '', 0, '3.4.4.1', '', 'http://update.joomla.org/language/details3/pt-BR_details.xml', '', ''),
(21, 3, 0, 'Serbian Latin', '', 'pkg_sr-YU', 'package', '', 0, '3.4.4.1', '', 'http://update.joomla.org/language/details3/sr-YU_details.xml', '', ''),
(22, 3, 0, 'Spanish', '', 'pkg_es-ES', 'package', '', 0, '3.4.4.1', '', 'http://update.joomla.org/language/details3/es-ES_details.xml', '', ''),
(23, 3, 0, 'Bosnian', '', 'pkg_bs-BA', 'package', '', 0, '3.4.5.1', '', 'http://update.joomla.org/language/details3/bs-BA_details.xml', '', ''),
(24, 3, 0, 'Serbian Cyrillic', '', 'pkg_sr-RS', 'package', '', 0, '3.4.4.1', '', 'http://update.joomla.org/language/details3/sr-RS_details.xml', '', ''),
(25, 3, 0, 'Vietnamese', '', 'pkg_vi-VN', 'package', '', 0, '3.2.1.1', '', 'http://update.joomla.org/language/details3/vi-VN_details.xml', '', ''),
(26, 3, 0, 'Bahasa Indonesia', '', 'pkg_id-ID', 'package', '', 0, '3.3.0.2', '', 'http://update.joomla.org/language/details3/id-ID_details.xml', '', ''),
(27, 3, 0, 'Finnish', '', 'pkg_fi-FI', 'package', '', 0, '3.4.2.1', '', 'http://update.joomla.org/language/details3/fi-FI_details.xml', '', ''),
(28, 3, 0, 'Swahili', '', 'pkg_sw-KE', 'package', '', 0, '3.4.5.1', '', 'http://update.joomla.org/language/details3/sw-KE_details.xml', '', ''),
(29, 3, 0, 'Montenegrin', '', 'pkg_srp-ME', 'package', '', 0, '3.3.1.1', '', 'http://update.joomla.org/language/details3/srp-ME_details.xml', '', ''),
(30, 3, 0, 'EnglishCA', '', 'pkg_en-CA', 'package', '', 0, '3.3.6.1', '', 'http://update.joomla.org/language/details3/en-CA_details.xml', '', ''),
(31, 3, 0, 'FrenchCA', '', 'pkg_fr-CA', 'package', '', 0, '3.4.4.3', '', 'http://update.joomla.org/language/details3/fr-CA_details.xml', '', ''),
(32, 3, 0, 'Welsh', '', 'pkg_cy-GB', 'package', '', 0, '3.3.0.2', '', 'http://update.joomla.org/language/details3/cy-GB_details.xml', '', ''),
(33, 3, 0, 'Sinhala', '', 'pkg_si-LK', 'package', '', 0, '3.3.1.1', '', 'http://update.joomla.org/language/details3/si-LK_details.xml', '', ''),
(34, 3, 0, 'Dari Persian', '', 'pkg_prs-AF', 'package', '', 0, '3.4.4.1', '', 'http://update.joomla.org/language/details3/prs-AF_details.xml', '', ''),
(35, 3, 0, 'Turkmen', '', 'pkg_tk-TM', 'package', '', 0, '3.4.5.1', '', 'http://update.joomla.org/language/details3/tk-TM_details.xml', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `#__update_sites`
--

DROP TABLE IF EXISTS `#__update_sites`;
CREATE TABLE IF NOT EXISTS `#__update_sites` (
  `update_site_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT '',
  `type` varchar(20) DEFAULT '',
  `location` text NOT NULL,
  `enabled` int(11) DEFAULT '0',
  `last_check_timestamp` bigint(20) DEFAULT '0',
  `extra_query` varchar(1000) DEFAULT '',
  PRIMARY KEY (`update_site_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Update Sites' AUTO_INCREMENT=6 ;

--
-- Dumping data for table `#__update_sites`
--

INSERT INTO `#__update_sites` (`update_site_id`, `name`, `type`, `location`, `enabled`, `last_check_timestamp`, `extra_query`) VALUES
(1, 'Joomla! Core', 'collection', 'http://update.joomla.org/core/list.xml', 1, 1448702220, ''),
(2, 'Joomla! Extension Directory', 'collection', 'http://update.joomla.org/jed/list.xml', 1, 1448702220, ''),
(3, 'Accredited Joomla! Translations', 'collection', 'http://update.joomla.org/language/translationlist_3.xml', 1, 1448702218, ''),
(4, 'Joomla! Update Component Update Site', 'extension', 'http://update.joomla.org/core/extensions/com_joomlaupdate.xml', 1, 1448702218, ''),
(5, 'K2 Updates', 'extension', 'http://getk2.org/update.xml', 1, 1448702218, '');

-- --------------------------------------------------------

--
-- Table structure for table `#__update_sites_extensions`
--

DROP TABLE IF EXISTS `#__update_sites_extensions`;
CREATE TABLE IF NOT EXISTS `#__update_sites_extensions` (
  `update_site_id` int(11) NOT NULL DEFAULT '0',
  `extension_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`update_site_id`,`extension_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Links extensions to update sites';

--
-- Dumping data for table `#__update_sites_extensions`
--

INSERT INTO `#__update_sites_extensions` (`update_site_id`, `extension_id`) VALUES
(1, 700),
(2, 700),
(3, 600),
(4, 28),
(5, 10013);

-- --------------------------------------------------------

--
-- Table structure for table `#__usergroups`
--

DROP TABLE IF EXISTS `#__usergroups`;
CREATE TABLE IF NOT EXISTS `#__usergroups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `parent_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Adjacency List Reference Id',
  `lft` int(11) NOT NULL DEFAULT '0' COMMENT 'Nested set lft.',
  `rgt` int(11) NOT NULL DEFAULT '0' COMMENT 'Nested set rgt.',
  `title` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_usergroup_parent_title_lookup` (`parent_id`,`title`),
  KEY `idx_usergroup_title_lookup` (`title`),
  KEY `idx_usergroup_adjacency_lookup` (`parent_id`),
  KEY `idx_usergroup_nested_set_lookup` (`lft`,`rgt`) USING BTREE
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `#__usergroups`
--

INSERT INTO `#__usergroups` (`id`, `parent_id`, `lft`, `rgt`, `title`) VALUES
(1, 0, 1, 18, 'Public'),
(2, 1, 8, 15, 'Registered'),
(3, 2, 9, 14, 'Author'),
(4, 3, 10, 13, 'Editor'),
(5, 4, 11, 12, 'Publisher'),
(6, 1, 4, 7, 'Manager'),
(7, 6, 5, 6, 'Administrator'),
(8, 1, 16, 17, 'Super Users'),
(9, 1, 2, 3, 'Guest');

-- --------------------------------------------------------

--
-- Table structure for table `#__users`
--

DROP TABLE IF EXISTS `#__users`;
CREATE TABLE IF NOT EXISTS `#__users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `username` varchar(150) NOT NULL DEFAULT '',
  `email` varchar(100) NOT NULL DEFAULT '',
  `password` varchar(100) NOT NULL DEFAULT '',
  `block` tinyint(4) NOT NULL DEFAULT '0',
  `sendEmail` tinyint(4) DEFAULT '0',
  `registerDate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastvisitDate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `activation` varchar(100) NOT NULL DEFAULT '',
  `params` text NOT NULL,
  `lastResetTime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'Date of last password reset',
  `resetCount` int(11) NOT NULL DEFAULT '0' COMMENT 'Count of password resets since lastResetTime',
  `otpKey` varchar(1000) NOT NULL DEFAULT '' COMMENT 'Two factor authentication encrypted keys',
  `otep` varchar(1000) NOT NULL DEFAULT '' COMMENT 'One time emergency passwords',
  `requireReset` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'Require user to reset password on next login',
  PRIMARY KEY (`id`),
  KEY `idx_name` (`name`),
  KEY `idx_block` (`block`),
  KEY `username` (`username`),
  KEY `email` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=998 ;

--
-- Dumping data for table `#__users`
--

INSERT INTO `#__users` (`id`, `name`, `username`, `email`, `password`, `block`, `sendEmail`, `registerDate`, `lastvisitDate`, `activation`, `params`, `lastResetTime`, `resetCount`, `otpKey`, `otep`, `requireReset`) VALUES
(851, 'FavThemes Demo User', 'favthemesdemo', 'demo@favthemes.com', '', 0, 1, '2012-11-27 10:15:27', '2013-04-01 21:51:44', '0', '', '0000-00-00 00:00:00', 0, '', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `#__user_keys`
--

DROP TABLE IF EXISTS `#__user_keys`;
CREATE TABLE IF NOT EXISTS `#__user_keys` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` varchar(255) NOT NULL,
  `token` varchar(255) NOT NULL,
  `series` varchar(255) NOT NULL,
  `invalid` tinyint(4) NOT NULL,
  `time` varchar(200) NOT NULL,
  `uastring` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `series` (`series`),
  UNIQUE KEY `series_2` (`series`),
  UNIQUE KEY `series_3` (`series`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `#__user_notes`
--

DROP TABLE IF EXISTS `#__user_notes`;
CREATE TABLE IF NOT EXISTS `#__user_notes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `catid` int(10) unsigned NOT NULL DEFAULT '0',
  `subject` varchar(100) NOT NULL DEFAULT '',
  `body` text NOT NULL,
  `state` tinyint(3) NOT NULL DEFAULT '0',
  `checked_out` int(10) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `created_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_user_id` int(10) unsigned NOT NULL,
  `modified_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `review_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `idx_user_id` (`user_id`),
  KEY `idx_category_id` (`catid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `#__user_profiles`
--

DROP TABLE IF EXISTS `#__user_profiles`;
CREATE TABLE IF NOT EXISTS `#__user_profiles` (
  `user_id` int(11) NOT NULL,
  `profile_key` varchar(100) NOT NULL,
  `profile_value` text NOT NULL,
  `ordering` int(11) NOT NULL DEFAULT '0',
  UNIQUE KEY `idx_user_id_profile_key` (`user_id`,`profile_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Simple user profile storage table';

-- --------------------------------------------------------

--
-- Table structure for table `#__user_usergroup_map`
--

DROP TABLE IF EXISTS `#__user_usergroup_map`;
CREATE TABLE IF NOT EXISTS `#__user_usergroup_map` (
  `user_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Foreign Key to #__users.id',
  `group_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Foreign Key to #__usergroups.id',
  PRIMARY KEY (`user_id`,`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `#__user_usergroup_map`
--

INSERT INTO `#__user_usergroup_map` (`user_id`, `group_id`) VALUES
(851, 3);

-- --------------------------------------------------------

--
-- Table structure for table `#__viewlevels`
--

DROP TABLE IF EXISTS `#__viewlevels`;
CREATE TABLE IF NOT EXISTS `#__viewlevels` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `title` varchar(100) NOT NULL DEFAULT '',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `rules` varchar(5120) NOT NULL COMMENT 'JSON encoded access control.',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_assetgroup_title_lookup` (`title`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `#__viewlevels`
--

INSERT INTO `#__viewlevels` (`id`, `title`, `ordering`, `rules`) VALUES
(1, 'Public', 0, '[1]'),
(2, 'Registered', 2, '[6,2,8]'),
(3, 'Special', 3, '[6,3,8]'),
(5, 'Guest', 1, '[9]'),
(6, 'Super Users', 4, '[8]');

--
-- Dumping data for table `#__modules`
--

INSERT INTO `#__modules` (`id`, `asset_id`, `title`, `note`, `content`, `ordering`, `position`, `checked_out`, `checked_out_time`, `publish_up`, `publish_down`, `published`, `module`, `access`, `showtitle`, `params`, `client_id`, `language`) VALUES
(1, 39, 'Main Menu', '', '', 1, 'nav', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_menu', 1, 0, '{"menutype":"mainmenu","base":"","startLevel":"1","endLevel":"0","showAllChildren":"1","tag_id":"","class_sfx":" nav-pills","window_open":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"itemid","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(2, 40, 'Login', '', '', 1, 'login', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_login', 1, 1, '', 1, '*'),
(3, 41, 'Popular Articles', '', '', 3, 'cpanel', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_popular', 3, 1, '{"count":"5","catid":"","user_id":"0","layout":"_:default","moduleclass_sfx":"","cache":"0","automatic_title":"1"}', 1, '*'),
(4, 42, 'Recently Added Articles', '', '', 4, 'cpanel', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_latest', 3, 1, '{"count":"5","ordering":"c_dsc","catid":"","user_id":"0","layout":"_:default","moduleclass_sfx":"","cache":"0","automatic_title":"1"}', 1, '*'),
(8, 43, 'Toolbar', '', '', 1, 'toolbar', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_toolbar', 3, 1, '', 1, '*'),
(9, 44, 'Quick Icons', '', '', 1, 'icon', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_quickicon', 3, 1, '', 1, '*'),
(10, 45, 'Logged-in Users', '', '', 2, 'cpanel', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_logged', 3, 1, '{"count":"5","name":"1","layout":"_:default","moduleclass_sfx":"","cache":"0","automatic_title":"1"}', 1, '*'),
(12, 46, 'Admin Menu', '', '', 1, 'menu', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_menu', 3, 1, '{"layout":"","moduleclass_sfx":"","shownew":"1","showhelp":"1","cache":"0"}', 1, '*'),
(13, 47, 'Admin Submenu', '', '', 1, 'submenu', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_submenu', 3, 1, '', 1, '*'),
(14, 48, 'User Status', '', '', 2, 'status', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_status', 3, 1, '', 1, '*'),
(15, 49, 'Title', '', '', 1, 'title', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_title', 3, 1, '', 1, '*'),
(16, 50, 'Login Form', '', '', 3, 'mainbottom3', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_login', 1, 1, '{"pretext":"","posttext":"","login":"","logout":"","greeting":"1","name":"0","usesecure":"0","usetext":"0","layout":"_:default","moduleclass_sfx":"","cache":"0","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(17, 51, 'Breadcrumbs', '', '', 1, 'breadcrumbs', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_breadcrumbs', 1, 0, '{"showHere":"0","showHome":"1","homeText":"","showLast":"1","separator":"\\/","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"itemid","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(79, 52, 'Multilanguage status', '', '', 1, 'status', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'mod_multilangstatus', 3, 1, '{"layout":"_:default","moduleclass_sfx":"","cache":"0"}', 1, '*'),
(86, 53, 'Joomla Version', '', '', 1, 'footer', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_version', 3, 1, '{"format":"short","product":"1","layout":"_:default","moduleclass_sfx":"","cache":"0"}', 1, '*'),
(88, 55, 'Site Information', '', '', 3, 'cpanel', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_stats_admin', 3, 1, '{"serverinfo":"1","siteinfo":"1","counter":"0","increase":"0","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 1, '*'),
(89, 56, 'Release News', '', '', 0, 'postinstall', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_feed', 1, 1, '{"rssurl":"http:\\/\\/www.joomla.org\\/announcements\\/release-news.feed","rssrtl":"0","rsstitle":"1","rssdesc":"1","rssimage":"1","rssitems":"3","rssitemdesc":"1","word_count":"0","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 1, '*'),
(90, 57, 'Latest Articles', '', '', 1, 'base2', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_articles_latest', 1, 1, '{"catid":["19"],"count":"5","show_featured":"","ordering":"c_dsc","user_id":"0","layout":"_:default","moduleclass_sfx":"-sfx22","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(93, 60, 'Search', '', '', 1, 'mainbottom3', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_search', 1, 1, '{"label":"","width":"20","text":"","button":"1","button_pos":"right","imagebutton":"0","button_text":"Go","opensearch":"1","opensearch_title":"","set_itemid":"0","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"itemid","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(94, 82, 'K2 Comments', '', '', 1, 'base1', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_k2_comments', 1, 1, '{"moduleclass_sfx":"","module_usage":"0","catfilter":"0","comments_limit":"5","comments_word_limit":"10","commenterName":"1","commentAvatar":"0","commentAvatarWidthSelect":"custom","commentAvatarWidth":"50","commentDate":"1","commentDateFormat":"absolute","commentLink":"1","itemTitle":"1","itemCategory":"1","feed":"0","commenters_limit":"5","commenterNameOrUsername":"1","commenterAvatar":"1","commenterAvatarWidthSelect":"custom","commenterAvatarWidth":"50","commenterLink":"1","commenterCommentsCounter":"1","commenterLatestComment":"1","cache":"1","cache_time":"900","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(95, 83, 'K2 Content Module', '', '', 1, 'mainbottom2', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_k2_content', 1, 1, '{"moduleclass_sfx":"","getTemplate":"Default","source":"specific","catfilter":"1","getChildren":"0","itemCount":"1","itemsOrdering":"","FeaturedItems":"1","popularityRange":"","videosOnly":"0","item":"0","items":["1"],"itemTitle":"0","itemAuthor":"0","itemAuthorAvatar":"0","itemAuthorAvatarWidthSelect":"custom","itemAuthorAvatarWidth":"50","userDescription":"0","itemIntroText":"1","itemIntroTextWordLimit":"","itemImage":"1","itemImgSize":"XLarge","itemVideo":"1","itemVideoCaption":"1","itemVideoCredits":"1","itemAttachments":"1","itemTags":"1","itemCategory":"1","itemDateCreated":"1","itemHits":"1","itemReadMore":"0","itemExtraFields":"0","itemCommentsCounter":"1","feed":"0","itemPreText":"","itemCustomLink":"0","itemCustomLinkTitle":"","itemCustomLinkURL":"http:\\/\\/","itemCustomLinkMenuItem":"101","K2Plugins":"1","JPlugins":"1","cache":"1","cache_time":"900","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(98, 86, 'K2 User', '', '', 2, 'mainbottom3', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_k2_user', 1, 1, '{"moduleclass_sfx":"","pretext":"","posttext":"","userGreetingText":"","name":"1","userAvatar":"1","userAvatarWidthSelect":"custom","userAvatarWidth":"50","menu":"","login":"101","logout":"101","usesecure":"0","cache":"0","cache_time":"900","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(99, 87, 'K2 Quick Icons (admin)', '', '', 0, 'cpanel', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_k2_quickicons', 1, 1, '', 1, '*'),
(100, 88, 'K2 Stats (admin)', '', '', 0, 'cpanel', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_k2_stats', 1, 1, '', 1, '*'),
(102, 96, 'J! Content Menu', '', '', 1, 'sidebar2', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_menu', 1, 1, '{"menutype":"mainmenu","base":"122","startLevel":"3","endLevel":"4","showAllChildren":"1","tag_id":"","class_sfx":"basic","window_open":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"itemid","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(103, 97, 'K2 Content Menu', '', '', 1, 'sidebar2', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_menu', 1, 1, '{"menutype":"mainmenu","base":"176","startLevel":"3","endLevel":"4","showAllChildren":"1","tag_id":"","class_sfx":"basic","window_open":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"itemid","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(104, 100, 'Advert', '', '<p>This is the <code>advert</code> module position. Use this module position for banners, promotions and advertisement space.</p>', 1, 'advert', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 0, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(105, 101, 'Wrapper', '', '', 1, 'mainbottom2', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_wrapper', 1, 1, '{"url":"http:\\/\\/www.favthemes.com\\/","add":"1","scrolling":"no","width":"99%","height":"720","height_auto":"1","frameborder":"1","target":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(106, 102, 'Articles Newsflash ', '', '', 4, 'mainbottom3', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_articles_news', 1, 1, '{"catid":["22"],"image":"1","item_title":"1","link_titles":"0","item_heading":"h4","showLastSeparator":"1","readmore":"1","count":"1","ordering":"a.publish_up","direction":"1","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"itemid","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(107, 103, 'Articles Most Read ', '', '', 4, 'base1', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_articles_popular', 1, 1, '{"catid":["19"],"count":"4","show_front":"1","date_filtering":"off","date_field":"a.created","start_date_range":"","end_date_range":"","relative_date":"30","layout":"_:default","moduleclass_sfx":"-sfx22","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(108, 104, 'Articles Category ', '', '', 1, 'base3', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_articles_category', 1, 1, '{"mode":"normal","show_on_article_page":"1","count":"0","show_front":"show","category_filtering_type":"1","catid":["19"],"show_child_category_articles":"0","levels":"1","author_filtering_type":"1","created_by":[""],"author_alias_filtering_type":"1","created_by_alias":[""],"excluded_articles":"","date_filtering":"off","date_field":"a.created","start_date_range":"","end_date_range":"","relative_date":"30","article_ordering":"a.title","article_ordering_direction":"ASC","article_grouping":"none","article_grouping_direction":"ksort","month_year_format":"F Y","link_titles":"1","show_date":"0","show_date_field":"created","show_date_format":"Y-m-d H:i:s","show_category":"0","show_hits":"0","show_author":"0","show_introtext":"0","introtext_limit":"100","show_readmore":"0","show_readmore_title":"1","readmore_limit":"15","layout":"_:default","moduleclass_sfx":"-sfx22","owncache":"1","cache_time":"900","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(109, 105, 'Articles Categories ', '', '', 1, 'base4', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_articles_categories', 1, 1, '{"parent":"11","show_description":"0","numitems":"0","show_children":"1","count":"0","maxlevel":"0","layout":"_:default","item_heading":"4","moduleclass_sfx":"-sfx22","owncache":"1","cache_time":"900","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(110, 106, 'Footer', '', '', 1, 'footer1', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_footer', 1, 1, '{"layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(111, 107, 'Topbar1', '', '<p>This is the <code>topbar1</code> module position. Use it for module content.</p>', 1, 'topbar1', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":" favdemo","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(112, 108, 'Topbar2', '', '<p>This is the <code>topbar2</code> module position. Use it for module content.</p>', 1, 'topbar2', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":" favdemo","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(113, 109, 'Topbar3', '', '<p>This is the <code>topbar3</code> module position. Use it for module content.</p>', 1, 'topbar3', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":" favdemo","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(114, 110, 'Topbar4', '', '<p>This is the <code>topbar4</code> module position. Use it for module content.</p>', 1, 'topbar4', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":" favdemo","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(115, 111, 'Topbar5', '', '<p>This is the <code>topbar5</code> module position. Use it for module content.</p>', 1, 'topbar5', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":" favdemo","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(116, 112, 'Topbar6', '', '<p>This is the <code>topbar6</code> module position. Use it for module content.</p>', 1, 'topbar6', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":" favdemo","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(117, 113, 'Nav', '', '<p style="padding-top: 14px; margin-bottom: 0px; clear: both;">This is the <code>nav</code> module position. Use this module position for the main navigation of the template.</p>', 2, 'nav', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'mod_custom', 1, 0, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(118, 114, 'Slide', '', '<p>This is the <code>slide</code> module position. Use this module position only for publishing the slideshow module and the slide images. Use the template parameters to set up the maximum width of the slideshow module position.</p>', 1, 'slide', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":" favdemo","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(119, 115, 'Intro1 ', '', '<p>This is the <code>intro1</code> module position. Use it for module content.</p>', 1, 'intro1', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(120, 116, 'Intro2 ', '', '<p>This is the <code>intro2</code> module position. Use it for module content.</p>', 1, 'intro2', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(121, 117, 'Intro3', '', '<p>This is the <code>intro3</code> module position. Use it for module content.</p>', 1, 'intro3', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(122, 118, 'Intro4 ', '', '<p>This is the <code>intro4</code> module position. Use it for module content.</p>', 1, 'intro4', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(123, 119, 'Intro5', '', '<p>This is the <code>intro5</code> module position. Use it for module content.</p>', 1, 'intro5', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(124, 120, 'Intro6', '', '<p>This is the <code>intro6</code> module position. Use it for module content.</p>', 1, 'intro6', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(125, 121, 'Lead1', '', '<p>This is the <code>lead1</code> module position. Use it for module content.</p>', 1, 'lead1', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(126, 122, 'Lead2 ', '', '<p>This is the <code>lead2</code> module position. Use it for module content.</p>', 1, 'lead2', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(127, 123, 'Lead3', '', '<p>This is the <code>lead3</code> module position. Use it for module content.</p>', 1, 'lead3', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(128, 124, 'Lead4', '', '<p>This is the <code>lead4</code> module position. Use it for module content.</p>', 1, 'lead4', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(129, 125, 'Lead5', '', '<p>This is the <code>lead5</code> module position. Use it for module content.</p>', 1, 'lead5', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(130, 126, 'Lead6', '', '<p>This is the <code>lead6</code> module position. Use it for module content.</p>', 1, 'lead6', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(131, 127, 'Promo1', '', '<p>This is the <code>promo1</code> module position. Use it for module content.</p>', 1, 'promo1', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(132, 128, 'Promo2', '', '<p>This is the <code>promo2</code> module position. Use it for module content.</p>', 1, 'promo2', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(133, 129, 'Promo3', '', '<p>This is the <code>promo3</code> module position. Use it for module content.</p>', 1, 'promo3', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(134, 130, 'Promo4', '', '<p>This is the <code>promo4</code> module position. Use it for module content.</p>', 1, 'promo4', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(135, 131, 'Promo5', '', '<p>This is the <code>promo5</code> module position. Use it for module content.</p>', 1, 'promo5', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(136, 132, 'Promo6', '', '<p>This is the <code>promo6</code> module position. Use it for module content.</p>', 1, 'promo6', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(137, 133, 'Prime1', '', '<p>This is the <code>prime1</code> module position. Use it for module content.</p>', 3, 'prime1', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(138, 134, 'Prime2', '', '<p>This is the <code>prime2</code> module position. Use it for module content.</p>', 1, 'prime2', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(139, 135, 'Prime3 ', '', '<p>This is the <code>prime3</code> module position. Use it for module content.</p>', 1, 'prime3', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(140, 136, 'Prime4', '', '<p>This is the <code>prime4</code> module position. Use it for module content.</p>', 3, 'prime4', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(141, 137, 'Prime5', '', '<p>This is the <code>prime5</code> module position. Use it for module content.</p>', 1, 'prime5', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(142, 138, 'Prime6', '', '<p>This is the <code>prime6</code> module position. Use it for module content.</p>', 1, 'prime6', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(143, 139, 'Showcase1', '', '<p>This is the <code>showcase1</code> module position. Use it for module content.</p>', 3, 'showcase1', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(144, 140, 'Showcase2', '', '<p>This is the <code>showcase2</code> module position. Use it for module content.</p>', 1, 'showcase2', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(145, 141, 'Showcase3', '', '<p>This is the <code>showcase3</code> module position. Use it for module content.</p>', 2, 'showcase3', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(146, 142, 'Showcase4', '', '<p>This is the <code>showcase4</code> module position. Use it for module content.</p>', 1, 'showcase4', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(147, 143, 'Showcase5', '', '<p>This is the <code>showcase5</code> module position. Use it for module content.</p>', 1, 'showcase5', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(148, 144, 'Showcase6', '', '<p>This is the <code>showcase6</code> module position. Use it for module content.</p>', 1, 'showcase6', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(149, 145, 'Feature1', '', '<p>This is the <code>feature1</code> module position. Use it for module content.</p>', 1, 'feature1', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(150, 146, 'Feature2', '', '<p>This is the <code>feature2</code> module position. Use it for module content.</p>', 1, 'feature2', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(151, 147, 'Feature3', '', '<p>This is the <code>feature3</code> module position. Use it for module content.</p>', 1, 'feature3', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(152, 148, 'Feature4', '', '<p>This is the <code>feature4</code> module position. Use it for module content.</p>', 1, 'feature4', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(153, 149, 'Feature5', '', '<p>This is the <code>feature5</code> module position. Use it for module content.</p>', 1, 'feature5', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(154, 150, 'Feature6', '', '<p>This is the <code>feature6</code> module position. Use it for module content.</p>', 1, 'feature6', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(155, 151, 'Focus1', '', '<p>This is the <code>focus1</code> module position. Use it for module content.</p>', 1, 'focus1', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(156, 152, 'Focus2', '', '<p>This is the <code>focus2</code> module position. Use it for module content.</p>', 1, 'focus2', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(157, 153, 'Focus3', '', '<p>This is the <code>focus3</code> module position. Use it for module content.</p>', 1, 'focus3', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(158, 154, 'Focus4', '', '<p>This is the <code>focus4</code> module position. Use it for module content.</p>', 1, 'focus4', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(159, 155, 'Focus5', '', '<p>This is the <code>focus5</code> module position. Use it for module content.</p>', 1, 'focus5', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(160, 156, 'Focus6', '', '<p>This is the <code>focus6</code> module position. Use it for module content.</p>', 1, 'focus6', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(161, 157, 'Portfolio1', '', '<p>This is the <code>portfolio1</code> module position. Use it for module content.</p>', 1, 'portfolio1', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(162, 158, 'Portfolio2', '', '<p>This is the <code>portfolio2</code> module position. Use it for module content.</p>', 1, 'portfolio2', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(163, 159, 'Portfolio3', '', '<p>This is the <code>portfolio3</code> module position. Use it for module content.</p>', 1, 'portfolio3', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(164, 160, 'Portfolio4', '', '<p>This is the <code>portfolio4</code> module position. Use it for module content.</p>', 1, 'portfolio4', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(165, 161, 'Portfolio5', '', '<p>This is the <code>portfolio5</code> module position. Use it for module content.</p>', 1, 'portfolio5', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(166, 162, 'Portfolio6', '', '<p>This is the <code>portfolio6</code> module position. Use it for module content.</p>', 1, 'portfolio6', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(167, 163, 'Screen1', '', '<p>This is the <code>screen1</code> module position. Use it for module content.</p>', 1, 'screen1', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(168, 164, 'Screen2', '', '<p>This is the <code>screen2</code> module position. Use it for module content.</p>', 1, 'screen2', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(169, 165, 'Screen3', '', '<p>This is the <code>screen3</code> module position. Use it for module content.</p>', 1, 'screen3', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(170, 166, 'Screen4', '', '<p>This is the <code>screen4</code> module position. Use it for module content.</p>', 1, 'screen4', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(171, 167, 'Screen5', '', '<p>This is the <code>screen5</code> module position. Use it for module content.</p>', 1, 'screen5', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(172, 168, 'Screen6', '', '<p>This is the <code>screen6</code> module position. Use it for module content.</p>', 1, 'screen6', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(173, 169, 'Top1', '', '<p>This is the <code>top1</code> module position. Use it for module content.</p>', 2, 'top1', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(174, 170, 'Top2', '', '<p>This is the <code>top2</code> module position. Use it for module content.</p>', 1, 'top2', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(175, 171, 'Top3', '', '<p>This is the <code>top3</code> module position. Use it for module content.</p>', 1, 'top3', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(176, 172, 'Top4', '', '<p>This is the <code>top4</code> module position. Use it for module content.</p>', 1, 'top4', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(177, 173, 'Top5', '', '<p>This is the <code>top5</code> module position. Use it for module content.</p>', 1, 'top5', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(178, 174, 'Top6', '', '<p>This is the <code>top6</code> module position. Use it for module content.</p>', 1, 'top6', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(179, 175, 'Bottom1', '', '<p>This is the <code>bottom1</code> module position. Use it for module content.</p>', 1, 'bottom1', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(180, 176, 'Bottom2', '', '<p>This is the <code>bottom2</code> module position. Use it for module content.</p>', 1, 'bottom2', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(181, 177, 'Bottom3', '', '<p>This is the <code>bottom3</code> module position. Use it for module content.</p>', 1, 'bottom3', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(182, 178, 'Bottom4', '', '<p>This is the <code>bottom4</code> module position. Use it for module content.</p>', 1, 'bottom4', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(183, 179, 'Bottom5', '', '<p>This is the <code>bottom5</code> module position. Use it for module content.</p>', 1, 'bottom5', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(184, 180, 'Bottom6', '', '<p>This is the <code>bottom6</code> module position. Use it for module content.</p>', 1, 'bottom6', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(185, 181, 'Note1', '', '<p>This is the <code>note1</code> module position. Use it for module content.</p>', 1, 'note1', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(186, 182, 'Note2', '', '<p>This is the <code>note2</code> module position. Use it for module content.</p>', 1, 'note2', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(187, 183, 'Note3', '', '<p>This is the <code>note3</code> module position. Use it for module content.</p>', 1, 'note3', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(188, 184, 'Note4', '', '<p>This is the <code>note4</code> module position. Use it for module content.</p>', 1, 'note4', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(189, 185, 'Note5', '', '<p>This is the <code>note5</code> module position. Use it for module content.</p>', 1, 'note5', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*');
INSERT INTO `#__modules` (`id`, `asset_id`, `title`, `note`, `content`, `ordering`, `position`, `checked_out`, `checked_out_time`, `publish_up`, `publish_down`, `published`, `module`, `access`, `showtitle`, `params`, `client_id`, `language`) VALUES
(190, 186, 'Note6', '', '<p>This is the <code>note6</code> module position. Use it for module content.</p>', 1, 'note6', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(191, 187, 'Base1', '', '<p>This is the <code>base1</code> module position. Use it for module content.</p>', 6, 'base1', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(192, 188, 'Base2', '', '<p>This is the <code>base2</code> module position. Use it for module content.</p>', 1, 'base2', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(193, 189, 'Base3', '', '<p>This is the <code>base3</code> module position. Use it for module content.</p>', 1, 'base3', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(194, 190, 'Base4', '', '<p>This is the <code>base4</code> module position. Use it for module content.</p>', 1, 'base4', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(195, 191, 'Base5', '', '<p>This is the <code>base5</code> module position. Use it for module content.</p>', 1, 'base5', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(196, 192, 'Base6', '', '<p>This is the <code>base6</code> module position. Use it for module content.</p>', 1, 'base6', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(197, 193, 'Block1', '', '<p>This is the <code>block1</code> module position. Use it for module content.</p>', 13, 'block1', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(198, 194, 'Block2', '', '<p>This is the <code>block2</code> module position. Use it for module content.</p>', 1, 'block2', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(199, 195, 'Block3', '', '<p>This is the <code>block3</code> module position. Use it for module content.</p>', 1, 'block3', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(200, 196, 'Block4', '', '<p>This is the <code>block4</code> module position. Use it for module content.</p>', 1, 'block4', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(201, 197, 'Block5', '', '<p>This is the <code>block5</code> module position. Use it for module content.</p>', 1, 'block5', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(202, 198, 'Block6', '', '<p>This is the <code>block6</code> module position. Use it for module content.</p>', 1, 'block6', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(203, 199, 'User1', '', '<p>This is the <code>user1</code> module position. Use it for module content.</p>', 1, 'user1', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(204, 200, 'User2', '', '<p>This is the <code>user2</code> module position. Use it for module content.</p>', 1, 'user2', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(205, 201, 'User3', '', '<p>This is the <code>user3</code> module position. Use it for module content.</p>', 1, 'user3', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(206, 202, 'User4', '', '<p>This is the <code>user4</code> module position. Use it for module content.</p>', 1, 'user4', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(207, 203, 'User5', '', '<p>This is the <code>user5</code> module position. Use it for module content.</p>', 1, 'user5', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(208, 204, 'User6', '', '<p>This is the <code>user6</code> module position. Use it for module content.</p>', 1, 'user6', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(209, 205, 'Footer1', '', '<p>This is the <code>footer1</code> module position. Use it for module content.</p>', 1, 'footer1', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(210, 206, 'Footer2', '', '<p>This is the <code>footer2</code> module position. Use it for module content.</p>', 1, 'footer2', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(211, 207, 'Footer3', '', '<p>This is the <code>footer3</code> module position. Use it for module content.</p>', 1, 'footer3', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(212, 208, 'Footer4', '', '<p>This is the <code>footer4</code> module position. Use it for module content.</p>', 1, 'footer4', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(213, 209, 'Footer5', '', '<p>This is the <code>footer5</code> module position. Use it for module content.</p>', 1, 'footer5', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(214, 210, 'Footer6', '', '<p>This is the <code>footer6</code> module position. Use it for module content.</p>', 1, 'footer6', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(215, 211, 'Maintop1', '', '<p>This is the <code>maintop1</code> module position. Use it for module content.</p>', 1, 'maintop1', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(216, 212, 'Maintop3', '', '<p>This is the <code>maintop3</code> module position. Use it for module content.</p>', 1, 'maintop3', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(217, 213, 'Maintop2', '', '<p>This is the <code>maintop2</code> module position. Use this module position for module content that should be displayed on top of the articles.</p>', 1, 'maintop2', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(218, 214, 'Mainbottom1', '', '<p>This is the <code>mainbottom1</code> module position. Use it for module content.</p>', 1, 'mainbottom1', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(219, 215, 'Mainbottom2', '', '<p>This is the <code>mainbottom2</code> module position. Use this module position for module content that should be displayed at the bottom of the articles.</p>', 1, 'mainbottom2', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(220, 216, 'Mainbottom3', '', '<p>This is the <code>mainbottom3</code> module position. Use it for module content.</p>', 1, 'mainbottom3', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(221, 217, 'Sidebar1', '', '<p>This is the <code>sidebar1</code> module position. Use it for module content next to the articles.</p>', 1, 'sidebar1', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(222, 218, 'Sidebar2', '', '<p>This is the <code>sidebar2</code> module position. Use it for module content next to the articles.</p>', 1, 'sidebar2', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(223, 219, 'Copyright1', '', '<p>This is the <code>copyright1</code> module position. Use it for module content next to the copyright information such as menus or social media links.</p>', 1, 'copyright1', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(224, 220, 'Copyright2', '', '<p>This is the <code>copyright2</code> module position. Use it for module content.</p>', 1, 'copyright2', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(225, 221, 'Debug', '', '<p>This is the <code>debug</code> module position. Use this module position to show debug information under the footer.</p>', 1, 'debug', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(226, 222, 'Breadcrumbs', '', '<p>This is the <code>breadcrumbs</code> module position. Use it as a secondary navigation which shows the page you are on and how to find your way back to the homepage.</p>', 1, 'breadcrumbs', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(227, 223, 'Module Variations Intro', '', '<p class="favintro">Customize the design of your content with 48 module variations! Choose any icon from Font Awesome for the module titles and add custom color styles and fonts to adapt the variations for your project!</p>', 1, 'intro1', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 0, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(228, 224, 'Suffix 1', '', '<p>Use <code>-sfx1</code> as a Module Class Suffix to apply this style. Just insert the suffix in the Advanced Options of the module.</p>', 1, 'promo1', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"-sfx1","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"fa-coffee","style":"0"}', 0, '*'),
(229, 225, 'Suffix 2', '', '<p>Use <code>-sfx2</code> as a Module Class Suffix to apply this style. Just insert the suffix in the Advanced Options of the module.</p>', 1, 'promo2', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"-sfx2","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"fa-cloud","style":"0"}', 0, '*'),
(230, 226, 'Suffix 3', '', '<p>Use <code>-sfx3</code> as a Module Class Suffix to apply this style. Just insert the suffix in the Advanced Options of the module.</p>', 1, 'promo3', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"-sfx3","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"fa-paper-plane","style":"0"}', 0, '*'),
(231, 227, 'Suffix 4', '', '<p>Use <code>-sfx4</code> as a Module Class Suffix to apply this style. Just insert the suffix in the Advanced Options of the module.</p>', 1, 'promo4', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"-sfx4","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"fa-birthday-cake","style":"0"}', 0, '*'),
(232, 228, 'Suffix 5', '', '<p>Use <code>-sfx5</code> as a Module Class Suffix to apply this style. Just insert the suffix in the Advanced Options of the module.</p>', 1, 'promo1', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"-sfx5","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"fa-laptop","style":"0"}', 0, '*'),
(233, 229, 'Suffix 6', '', '<p>Use <code>-sfx6</code> as a Module Class Suffix to apply this style. Just insert the suffix in the Advanced Options of the module.</p>', 1, 'promo2', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"-sfx6","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"fa-pie-chart","style":"0"}', 0, '*'),
(234, 230, 'Suffix 7', '', '<p>Use <code>-sfx7</code> as a Module Class Suffix to apply this style. Just insert the suffix in the Advanced Options of the module.</p>', 1, 'promo3', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"-sfx7","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"fa-code","style":"0"}', 0, '*'),
(235, 231, 'Suffix 8', '', '<p>Use <code>-sfx8</code> as a Module Class Suffix to apply this style. Just insert the suffix in the Advanced Options of the module.</p>', 1, 'promo4', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"-sfx8","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"fa-quote-left","style":"0"}', 0, '*'),
(236, 232, 'Suffix 9', '', '<p>Use <code>-sfx9</code> as a Module Class Suffix to apply this style. Just insert the suffix in the Advanced Options of the module.</p>', 1, 'promo1', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"-sfx9","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"fa-bicycle","style":"0"}', 0, '*'),
(237, 233, 'Suffix 10', '', '<p>Use <code>-sfx10</code> as a Module Class Suffix to apply this style. Just insert the suffix in the Advanced Options of the module.</p>', 1, 'promo2', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"-sfx10","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"fa-puzzle-piece","style":"0"}', 0, '*'),
(238, 234, 'Suffix 11', '', '<p>Use <code>-sfx11</code> as a Module Class Suffix to apply this style. Just insert the suffix in the Advanced Options of the module.</p>', 1, 'promo3', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"-sfx11","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"fa-cogs","style":"0"}', 0, '*'),
(239, 235, 'Suffix 12', '', '<p>Use <code>-sfx12</code> as a Module Class Suffix to apply this style. Just insert the suffix in the Advanced Options of the module.</p>', 1, 'promo4', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"-sfx12","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"fa-comments","style":"0"}', 0, '*'),
(240, 236, 'Suffix 13', '', '<p>Use <code>-sfx13</code> as a Module Class Suffix to apply this style. Just insert the suffix in the Advanced Options of the module.</p>', -1, 'prime1', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"-sfx13","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"fa-coffee","style":"0"}', 0, '*'),
(241, 237, 'Suffix 14', '', '<p>Use <code>-sfx14</code> as a Module Class Suffix to apply this style. Just insert the suffix in the Advanced Options of the module.</p>', 2, 'prime2', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"-sfx14","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"fa-cloud","style":"0"}', 0, '*'),
(242, 238, 'Suffix 15', '', '<p>Use <code>-sfx15</code> as a Module Class Suffix to apply this style. Just insert the suffix in the Advanced Options of the module.</p>', 2, 'prime3', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"-sfx15","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"fa-paper-plane","style":"0"}', 0, '*'),
(243, 239, 'Suffix 22', '', '<p>Use <code>-sfx22</code> as a Module Class Suffix to apply this style. Just insert the suffix in the Advanced Options of the module.</p>', 4, 'prime2', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"-sfx22","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"fa-puzzle-piece","style":"0"}', 0, '*'),
(244, 240, 'Suffix 23', '', '<p>Use <code>-sfx23</code> as a Module Class Suffix to apply this style. Just insert the suffix in the Advanced Options of the module.</p>', 4, 'prime3', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"-sfx23","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"fa-cogs","style":"0"}', 0, '*'),
(245, 241, 'Suffix 24', '', '<p>Use <code>-sfx24</code> as a Module Class Suffix to apply this style. Just insert the suffix in the Advanced Options of the module.</p>', 4, 'prime4', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"-sfx24","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"fa-comments","style":"0"}', 0, '*'),
(246, 242, 'Suffix 31', '', '<p>Use <code>-sfx31</code> as a Module Class Suffix to apply this style. Just insert the suffix in the Advanced Options of the module.</p>', 3, 'showcase3', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"-sfx31","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"fa-code","style":"0"}', 0, '*'),
(247, 243, 'Suffix 32', '', '<p>Use <code>-sfx32</code> as a Module Class Suffix to apply this style. Just insert the suffix in the Advanced Options of the module.</p>', 3, 'showcase4', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"-sfx32","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"fa-quote-left","style":"0"}', 0, '*'),
(248, 244, 'Suffix 33', '', '<p>Use <code>-sfx33</code> as a Module Class Suffix to apply this style. Just insert the suffix in the Advanced Options of the module.</p>', 4, 'showcase1', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"-sfx33","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"fa-bicycle","style":"0"}', 0, '*'),
(249, 245, 'Suffix 40', '', '<p>Use <code>-sfx40</code> as a Module Class Suffix to apply this style. Just insert the suffix in the Advanced Options of the module.</p>', 2, 'focus4', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"-sfx40","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"fa-birthday-cake","style":"0"}', 0, '*'),
(250, 246, 'Suffix 41', '', '<p>Use <code>-sfx41</code> as a Module Class Suffix to apply this style. Just insert the suffix in the Advanced Options of the module.</p>', 3, 'focus1', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"-sfx41","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"fa-laptop","style":"0"}', 0, '*'),
(251, 247, 'Suffix 42', '', '<p>Use <code>-sfx42</code> as a Module Class Suffix to apply this style. Just insert the suffix in the Advanced Options of the module.</p>', 3, 'focus2', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"-sfx42","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"fa-pie-chart","style":"0"}', 0, '*'),
(252, 248, 'Suffix 16', '', '<p>Use <code>-sfx16</code> as a Module Class Suffix to apply this style. Just insert the suffix in the Advanced Options of the module.</p>', 1, 'prime4', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"-sfx16","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"fa-birthday-cake","style":"0"}', 0, '*'),
(253, 249, 'Suffix 17', '', '<p>Use <code>-sx17</code> as a Module Class Suffix to apply this style. Just insert the suffix in the Advanced Options of the module.</p>', 1, 'prime1', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"-sfx17","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"fa-laptop","style":"0"}', 0, '*'),
(254, 250, 'Suffix 18', '', '<p>Use <code>-sfx18</code> as a Module Class Suffix to apply this style. Just insert the suffix in the Advanced Options of the module.</p>', 3, 'prime2', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"-sfx18","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"fa-pie-chart","style":"0"}', 0, '*'),
(255, 251, 'Suffix 19', '', '<p>Use <code>-sfx19</code> as a Module Class Suffix to apply this style. Just insert the suffix in the Advanced Options of the module.</p>', 3, 'prime3', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"-sfx19","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"fa-code","style":"0"}', 0, '*'),
(256, 252, 'Suffix 20', '', '<p>Use <code>-sfx20</code> as a Module Class Suffix to apply this style. Just insert the suffix in the Advanced Options of the module.</p>', 2, 'prime4', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"-sfx20","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"fa-quote-left","style":"0"}', 0, '*'),
(257, 253, 'Suffix 21', '', '<p>Use <code>-sfx21</code> as a Module Class Suffix to apply this style. Just insert the suffix in the Advanced Options of the module.</p>', 2, 'prime1', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"-sfx21","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"fa-bicycle","style":"0"}', 0, '*'),
(258, 254, 'Suffix 25', '', '<p>Use <code>-sfx25</code> as a Module Class Suffix to apply this style. Just insert the suffix in the Advanced Options of the module.</p>', 1, 'showcase1', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"-sfx25","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"fa-coffee","style":"0"}', 0, '*'),
(259, 255, 'Suffix 26', '', '<p>Use <code>-sfx26</code> as a Module Class Suffix to apply this style. Just insert the suffix in the Advanced Options of the module.</p>', 1, 'showcase2', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"-sfx26","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"fa-cloud","style":"0"}', 0, '*'),
(260, 256, 'Suffix 27', '', '<p>Use <code>-sfx27</code> as a Module Class Suffix to apply this style. Just insert the suffix in the Advanced Options of the module.</p>', 1, 'showcase3', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"-sfx27","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"fa-paper-plane","style":"0"}', 0, '*'),
(261, 257, 'Suffix 28', '', '<p>Use <code>-sfx28</code> as a Module Class Suffix to apply this style. Just insert the suffix in the Advanced Options of the module.</p>', 2, 'showcase4', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"-sfx28","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"fa-birthday-cake","style":"0"}', 0, '*'),
(262, 258, 'Suffix 29', '', '<p>Use <code>-sfx29</code> as a Module Class Suffix to apply this style. Just insert the suffix in the Advanced Options of the module.</p>', 2, 'showcase1', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"-sfx29","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"fa-laptop","style":"0"}', 0, '*'),
(263, 259, 'Suffix 30', '', '<p>Use <code>-sfx30</code> as a Module Class Suffix to apply this style. Just insert the suffix in the Advanced Options of the module.</p>', 1, 'showcase2', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"-sfx30","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"fa-pie-chart","style":"0"}', 0, '*'),
(264, 260, 'Suffix 34', '', '<p>Use <code>-sfx34</code> as a Module Class Suffix to apply this style. Just insert the suffix in the Advanced Options of the module.</p>', 1, 'showcase2', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"-sfx34","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"fa-puzzle-piece","style":"0"}', 0, '*'),
(265, 261, 'Suffix 35', '', '<p>Use <code>-sfx35</code> as a Module Class Suffix to apply this style. Just insert the suffix in the Advanced Options of the module.</p>', 4, 'showcase3', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"-sfx35","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"fa-cogs","style":"0"}', 0, '*'),
(266, 262, 'Suffix 36', '', '<p>Use <code>-sfx36</code> as a Module Class Suffix to apply this style. Just insert the suffix in the Advanced Options of the module.</p>', 4, 'showcase4', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"-sfx36","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"fa-comments","style":"0"}', 0, '*'),
(267, 263, 'Suffix 37', '', '<p>Use <code>-sfx37</code> as a Module Class Suffix to apply this style. Just insert the suffix in the Advanced Options of the module.</p>', 2, 'focus1', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"-sfx37","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"fa-coffee","style":"0"}', 0, '*'),
(268, 264, 'Suffix 38', '', '<p>Use <code>-sfx38</code> as a Module Class Suffix to apply this style. Just insert the suffix in the Advanced Options of the module.</p>', 2, 'focus2', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"-sfx38","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"fa-cloud","style":"0"}', 0, '*'),
(269, 265, 'Suffix 39', '', '<p>Use <code>-sfx39</code> as a Module Class Suffix to apply this style. Just insert the suffix in the Advanced Options of the module.</p>', 1, 'focus3', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"-sfx39","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"fa-paper-plane","style":"0"}', 0, '*'),
(270, 266, 'Suffix 43', '', '<p>Use <code>-sfx43</code> as a Module Class Suffix to apply this style. Just insert the suffix in the Advanced Options of the module.</p>', 1, 'focus3', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"-sfx43","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"fa-code","style":"0"}', 0, '*'),
(271, 267, 'Suffix 44', '', '<p>Use <code>-sfx44</code> as a Module Class Suffix to apply this style. Just insert the suffix in the Advanced Options of the module.</p>', 3, 'focus4', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"-sfx44","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"fa-quote-left","style":"0"}', 0, '*'),
(272, 268, 'Suffix 45', '', '<p>Use <code>-sfx45</code> as a Module Class Suffix to apply this style. Just insert the suffix in the Advanced Options of the module.</p>', 4, 'focus1', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"-sfx45","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"fa-bicycle","style":"0"}', 0, '*'),
(273, 269, 'Suffix 46', '', '<p>Use <code>-sfx46</code> as a Module Class Suffix to apply this style. Just insert the suffix in the Advanced Options of the module.</p>', 4, 'focus2', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"-sfx46","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"fa-puzzle-piece","style":"0"}', 0, '*'),
(274, 270, 'Suffix 47', '', '<p>Use <code>-sfx47</code> as a Module Class Suffix to apply this style. Just insert the suffix in the Advanced Options of the module.</p>', 1, 'focus3', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"-sfx47","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"fa-cogs","style":"0"}', 0, '*'),
(275, 271, 'Suffix 48', '', '<p>Use <code>-sfx48</code> as a Module Class Suffix to apply this style. Just insert the suffix in the Advanced Options of the module.</p>', 4, 'focus4', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"-sfx48","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"fa-comments","style":"0"}', 0, '*'),
(276, 272, 'How to use the module variations', '', '<p> </p>\r\n<h4>1. What are the module variations?</h4>\r\n<p>Each theme has 48 module variations that use Joomla''s Module Class Suffix to customize the appearance of the modules. Using the template parameters, the module variations can be further customized with new colors in the <strong>Settings</strong> tab:</p>\r\n<p><img class="img-center img-polaroid" src="images/demo/docs/favthemes-tutorial-variations-settings.jpg" alt="FavThemes tutorial for module variations using the template settings" /></p>\r\n<p> </p>\r\n<h4>2. How to use the module variations</h4>\r\n<p>Choose a module variation from the examples above and insert that <strong>Module Class Suffix</strong> in the Advanced tab of the module. If you do not want to use any Module Variation, then simply leave the Module Class Suffix field blank.</p>\r\n<p><img class="img-center img-polaroid" src="images/demo/docs/favthemes-tutorial-module-variations.jpg" alt="FavThemes tutorial for module variations" /></p>\r\n<p> </p>\r\n<h4>3. How to add icons to the title of the module</h4>\r\n<p>Choose any icon from <a href="http://fontawesome.io/" target="_blank">Font Awesome</a> and insert the name of the icon in the <strong>Header Class</strong> field in the Advanced tab of the module. If you do not want to use any icon for the title of the module, then simply leave the Header Class field blank.</p>\r\n<p><img class="img-center img-polaroid" src="images/demo/docs/favthemes-tutorial-module-title-icons.jpg" alt="FavThemes tutorial for module title icons" /></p>\r\n<p> </p>', 1, 'bottom1', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"-sfx8","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"fa-graduation-cap","style":"0"}', 0, '*'),
(277, 273, 'Menu Basic', '', '', 1, 'promo1', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_menu', 1, 1, '{"menutype":"mainmenu","base":"","startLevel":"1","endLevel":"1","showAllChildren":"0","tag_id":"","class_sfx":"basic","window_open":"","layout":"_:default","moduleclass_sfx":"-sfx4","cache":"1","cache_time":"900","cachemode":"itemid","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(278, 274, 'Menu Arrow', '', '', 1, 'promo2', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_menu', 1, 1, '{"menutype":"mainmenu","base":"","startLevel":"1","endLevel":"1","showAllChildren":"0","tag_id":"","class_sfx":"arrow","window_open":"","layout":"_:default","moduleclass_sfx":"-sfx4","cache":"1","cache_time":"900","cachemode":"itemid","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(279, 275, 'Menu Side', '', '', 1, 'promo3', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_menu', 1, 1, '{"menutype":"mainmenu","base":"","startLevel":"1","endLevel":"1","showAllChildren":"0","tag_id":"","class_sfx":"side","window_open":"","layout":"_:default","moduleclass_sfx":"-sfx4","cache":"1","cache_time":"900","cachemode":"itemid","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(280, 276, 'Menu Arrow Dark', '', '', 1, 'showcase2', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_menu', 1, 1, '{"menutype":"mainmenu","base":"","startLevel":"1","endLevel":"1","showAllChildren":"0","tag_id":"","class_sfx":"arrow menu-dark","window_open":"","layout":"_:default","moduleclass_sfx":"-sfx26","cache":"1","cache_time":"900","cachemode":"itemid","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(281, 277, 'Menu Line', '', '', 1, 'promo4', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_menu', 1, 1, '{"menutype":"mainmenu","base":"","startLevel":"1","endLevel":"1","showAllChildren":"0","tag_id":"","class_sfx":"line","window_open":"","layout":"_:default","moduleclass_sfx":"-sfx4","cache":"1","cache_time":"900","cachemode":"itemid","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(282, 278, 'Menu Basic Color', '', '', 1, 'focus1', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_menu', 1, 1, '{"menutype":"mainmenu","base":"","startLevel":"1","endLevel":"1","showAllChildren":"0","tag_id":"","class_sfx":"basic menu-color","window_open":"","layout":"_:default","moduleclass_sfx":"-sfx38","cache":"1","cache_time":"900","cachemode":"itemid","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(283, 279, 'Menu Side Color', '', '', 1, 'focus3', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_menu', 1, 1, '{"menutype":"mainmenu","base":"","startLevel":"1","endLevel":"1","showAllChildren":"0","tag_id":"","class_sfx":"side menu-color","window_open":"","layout":"_:default","moduleclass_sfx":"-sfx38","cache":"1","cache_time":"900","cachemode":"itemid","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(284, 280, 'Menu Side Clear', '', '', 1, 'prime3', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_menu', 1, 1, '{"menutype":"mainmenu","base":"","startLevel":"1","endLevel":"1","showAllChildren":"0","tag_id":"","class_sfx":"side menu-clear","window_open":"","layout":"_:default","moduleclass_sfx":"-sfx14","cache":"1","cache_time":"900","cachemode":"itemid","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*');
INSERT INTO `#__modules` (`id`, `asset_id`, `title`, `note`, `content`, `ordering`, `position`, `checked_out`, `checked_out_time`, `publish_up`, `publish_down`, `published`, `module`, `access`, `showtitle`, `params`, `client_id`, `language`) VALUES
(285, 281, 'Menu Basic Clear', '', '', 1, 'prime1', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_menu', 1, 1, '{"menutype":"mainmenu","base":"","startLevel":"1","endLevel":"1","showAllChildren":"0","tag_id":"","class_sfx":"basic menu-clear","window_open":"","layout":"_:default","moduleclass_sfx":"-sfx14","cache":"1","cache_time":"900","cachemode":"itemid","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(286, 282, 'Menu Basic Dark', '', '', 1, 'showcase1', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_menu', 1, 1, '{"menutype":"mainmenu","base":"","startLevel":"1","endLevel":"1","showAllChildren":"0","tag_id":"","class_sfx":"basic menu-dark","window_open":"","layout":"_:default","moduleclass_sfx":"-sfx26","cache":"1","cache_time":"900","cachemode":"itemid","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(287, 283, 'Menu Arrow Color', '', '', 1, 'focus2', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_menu', 1, 1, '{"menutype":"mainmenu","base":"","startLevel":"1","endLevel":"1","showAllChildren":"0","tag_id":"","class_sfx":"arrow menu-color","window_open":"","layout":"_:default","moduleclass_sfx":"-sfx38","cache":"1","cache_time":"900","cachemode":"itemid","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(288, 284, 'Menu Arrow Clear', '', '', 1, 'prime2', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_menu', 1, 1, '{"menutype":"mainmenu","base":"","startLevel":"1","endLevel":"1","showAllChildren":"0","tag_id":"","class_sfx":"arrow menu-clear","window_open":"","layout":"_:default","moduleclass_sfx":"-sfx14","cache":"1","cache_time":"900","cachemode":"itemid","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(289, 285, 'Menu Side Dark', '', '', 1, 'showcase3', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_menu', 1, 1, '{"menutype":"mainmenu","base":"","startLevel":"1","endLevel":"1","showAllChildren":"0","tag_id":"","class_sfx":"side menu-dark","window_open":"","layout":"_:default","moduleclass_sfx":"-sfx26","cache":"1","cache_time":"900","cachemode":"itemid","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(290, 286, 'Menu Line Color', '', '', 5, 'focus4', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_menu', 1, 1, '{"menutype":"mainmenu","base":"","startLevel":"1","endLevel":"1","showAllChildren":"0","tag_id":"","class_sfx":"line menu-color","window_open":"","layout":"_:default","moduleclass_sfx":"-sfx38","cache":"1","cache_time":"900","cachemode":"itemid","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(291, 287, 'Menu Line Clear', '', '', 1, 'prime4', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_menu', 1, 1, '{"menutype":"mainmenu","base":"","startLevel":"1","endLevel":"1","showAllChildren":"0","tag_id":"","class_sfx":"line menu-clear","window_open":"","layout":"_:default","moduleclass_sfx":"-sfx14","cache":"1","cache_time":"900","cachemode":"itemid","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(292, 288, 'Menu Line Dark', '', '', 1, 'showcase4', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_menu', 1, 1, '{"menutype":"mainmenu","base":"","startLevel":"1","endLevel":"1","showAllChildren":"0","tag_id":"","class_sfx":"line menu-dark","window_open":"","layout":"_:default","moduleclass_sfx":"-sfx26","cache":"1","cache_time":"900","cachemode":"itemid","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(293, 289, 'Font Awesome Icons', '', '<p><a href="http://fontawesome.io" target="_blank">Font Awesome</a> is the iconic font designed for use with the <a href="http://getbootstrap.com/2.3.2/index.html" target="_blank">Bootstrap framework</a> that comes packed by default with Joomla!3. To insert the icons inside the text of the articles or modules of any of the <a href="http://www.favthemes.com/" target="_blank">FavThemes</a> templates, just type the following code in your text editor. Make sure the code is added in the HTML mode and with the EDITOR NONE setting in Global Configuration.</p>\r\n<pre>&lt;i class="fa fa-name"&gt;&lt;/i&gt;</pre>', 1, 'mainbottom2', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"-sfx4","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(295, 291, 'How to add the icons to the title of the module', '', '<p>Choose any icon from <a href="http://fontawesome.io/" target="_blank">Font Awesome</a> and insert the name of the icon in the HEADER CLASS field in the Advanced tab of the module. If you do not want to use any icon for the title of the module, then simply leave the Header Class field blank.</p>\r\n<p> </p>\r\n<p><img class="img-left img-polaroid" src="images/demo/docs/favthemes-tutorial-module-title-icons.jpg" alt="FavThemes tutorial for the module title icons" /></p>', 1, 'mainbottom2', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"-sfx4","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"fa-graduation-cap","style":"0"}', 0, '*'),
(296, 292, 'Image Styles', '', '<p>Image Styles are designed for the images inside the articles or default HTML Joomla! modules. Use them to create new layout alignments, borders, shadows and shapes such as rounded or circle.</p>\r\n<p>To use one or more image style just type the code from the examples below in your text editor. Make sure the code is added in the HTML mode by pressing the <strong>Toggle Editor</strong> button.</p>\r\n<pre style="display: inline-block; margin-bottom: 0;">&lt;img class="img-left"&gt;</pre>', 1, 'mainbottom2', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"-sfx4","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"fa-image","style":"0"}', 0, '*'),
(297, 293, 'Multiple Image Styles', '', '<p style="margin-bottom: 0;">The preset image styles can be combined in multiple ways to create a more complex style for the same image, for example:</p>', 15, 'block1', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"-sfx4","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(298, 294, 'Image Right', '', '<p><img class="img-right" src="images/demo/img/demo-img-square.jpg" alt="Demo Image" /></p>\r\n<p>Use this style to align the images inside the modules or articles to the right.</p>\r\n<p>To use it just type the following code in your text editor. Make sure the code is added in the HTML mode.</p>\r\n<pre style="display: inline-block;">&lt;img class="img-right"&gt;</pre>', 2, 'bottom2', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(299, 295, 'Image Left', '', '<p><img class="img-left" src="images/demo/img/demo-img-square.jpg" alt="Demo Image" /></p>\r\n<p>Use this style to align the images inside the modules or articles to the left.</p>\r\n<p>To use it just type the following code in your text editor. Make sure the code is added in the HTML mode.</p>\r\n<pre style="display: inline-block;">&lt;img class="img-left"&gt;</pre>', 4, 'bottom1', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(300, 296, 'Image Center', '', '<p><img class="img-center img-polaroid" src="images/demo/img/demo-img-1.jpg" alt="Demo Image" /></p>\r\n<p>Use this style to center the images inside the modules or articles. To use it just type the following code in your text editor and make sure the code is added in the HTML mode by pressing the <strong>Toggle Editor</strong> button:</p>\r\n<pre>&lt;img class="img-center"&gt;</pre>', 14, 'block1', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"-sfx4","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(301, 297, 'Image Circle', '', '<p><img class="img-right img-circle" src="images/demo/img/demo-img-square.jpg" alt="Demo Image" /></p>\r\n<p>Use this style for the images inside the modules or articles.</p>\r\n<p>To use it just type the following code in your text editor. Make sure the code is added in the HTML mode.</p>\r\n<pre style="display: inline-block;">&lt;img class="img-circle"&gt;</pre>', 5, 'bottom2', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(302, 298, 'Image Rounded', '', '<p><img class="img-left img-rounded" src="images/demo/img/demo-img-square.jpg" alt="Demo Image" /></p>\r\n<p>Use this style for the images inside the modules or articles.</p>\r\n<p>To use it just type the following code in your text editor. Make sure the code is added in the HTML mode.</p>\r\n<pre style="display: inline-block;">&lt;img class="img-rounded"&gt;</pre>', 6, 'bottom1', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(303, 299, 'Image Shadow', '', '<p><img class="img-right img-shadow" style="margin-bottom: 26px;" src="images/demo/img/demo-img-square.jpg" alt="Demo Image" /></p>\r\n<p>Use this style for the images inside the modules or articles.</p>\r\n<p>To use it just type the following code in your text editor. Make sure the code is added in the HTML mode.</p>\r\n<pre style="display: inline-block;">&lt;img class="img-shadow"&gt;</pre>', 3, 'bottom2', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(304, 300, 'Image Polaroid', '', '<p><img class="img-left img-polaroid" src="images/demo/img/demo-img-square.jpg" alt="Demo Image" /></p>\r\n<p>Use this style for the images inside the modules or articles.</p>\r\n<p>To use it just type the following code in your text editor. Make sure the code is added in the HTML mode.</p>\r\n<pre style="display: inline-block;">&lt;img class="img-polaroid"&gt;</pre>', 5, 'bottom1', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(305, 301, 'Polaroid Color Style', '', '<p><img class="img-polaroid-color" src="images/demo/img/demo-img-square.jpg" alt="Demo Image" /></p>\r\n<pre>&lt;img class="img-polaroid-color"&gt;</pre>', 1, 'base4', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"-sfx10","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(306, 302, 'Polaroid Dark Style', '', '<p><img class="img-polaroid-dark" src="images/demo/img/demo-img-square.jpg" alt="Demo Image" /></p>\r\n<pre>&lt;img class="img-polaroid-dark"&gt;</pre>', 1, 'base3', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"-sfx22","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(307, 303, 'Polaroid Clear Style', '', '<p><img class="img-polaroid-clear" src="images/demo/img/demo-img-square.jpg" alt="Demo Image" /></p>\r\n<pre>&lt;img class="img-polaroid-clear"&gt;</pre>', 1, 'base2', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"-sfx18","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(308, 304, 'Image Left Polaroid Rounded', '', '<p><img class="img-left img-polaroid img-rounded" src="images/demo/img/demo-img-square.jpg" alt="Demo Image" /></p>\r\n<p> </p>\r\n<p>Use this style to align the images inside the modules or articles to the left, with polaroid style frame and rounded corners.</p>\r\n<p>To use it just type the following code in your text editor. Make sure the code is added in the HTML mode:</p>\r\n<p> </p>\r\n<pre>&lt;img class="img-left img-polaroid img-rounded"&gt;</pre>', 2, 'user1', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(309, 305, 'Image Right Shadow Circle', '', '<p><img class="img-right img-shadow img-circle" src="images/demo/img/demo-img-square.jpg" alt="Demo Image" /></p>\r\n<p> </p>\r\n<p>Use this style to align the images to the right with shadow style frame and circle shape.</p>\r\n<p>To use it just type the following code in your text editor. Make sure the code is added in the HTML mode.</p>\r\n<p style="margin-bottom: 12%;"> </p>\r\n<pre>&lt;img class="img-right img-shadow img-circle"&gt;</pre>', 1, 'user2', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(311, 307, 'Default Button', '', '<p><button class="btn">Button</button></p>\r\n<p>This is the <code>.btn</code>button element.</p>\r\n<p>To use it just type the following code in your text editor. Make sure the code is added in the html mode.</p>\r\n<pre>&lt;button class="btn"&gt;...&lt;/button&gt;</pre>\r\n<p>or if you want to use the button with a link:</p>\r\n<p><a class="btn" href="http://www.favthemes.com/" target="_blank">Button link</a></p>\r\n<pre>&lt;a class="btn" href="#"&gt;...&lt;/a&gt;</pre>', 3, 'bottom1', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"-sfx2","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(312, 308, 'Primary Button', '', '<p><button class="btn btn-primary">Primary</button></p>\r\n<p style="text-align: left;">This is the <code>.btn-primary</code> button element.</p>\r\n<p>To use it just type the following code in your text editor. Make sure the code is added in the html mode.</p>\r\n<pre>&lt;button class="btn btn-primary"&gt;...&lt;/button&gt;</pre>\r\n<p>or if you want to use the button with a link:</p>\r\n<p><a class="btn btn-primary" href="http://www.favthemes.com/" target="_blank">Button link</a></p>\r\n<pre>&lt;a class="btn btn-primary" href="#"&gt;...&lt;/a&gt;</pre>', 4, 'bottom2', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"-sfx2","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(313, 309, 'Button Icons', '', '<p>To use the Font Awesome icons with the Bootstrap buttons, just type the following code in your text editor. Make sure the code is added in the html mode and turn on <b>Editor None</b> in Global Configuration.</p>\r\n<p><a class="btn" href="http://www.favthemes.com/" target="_blank"><i class="fa fa-download"></i>Download</a></p>\r\n<pre>&lt;a class="btn" href="#"&gt; <br />&lt;i class="fa fa-download"&gt;&lt;/i&gt;<br />Download<br />&lt;/a&gt;\r\n</pre>', 1, 'mainbottom2', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"-sfx4","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(314, 310, 'Button Sizes', '', '<p><button class="btn btn-large">Large Button</button></p>\r\n<p style="text-align: left;">This is the large button size. To use it just type the following code in your text editor. Make sure the code is added in the HTML mode.</p>\r\n<pre>&lt;button class="btn btn-large"&gt;...&lt;/button&gt;</pre>\r\n<p><button class="btn">Default Button</button></p>\r\n<p style="text-align: left;">This is the default button size. To use it just type the following code in your text editor. Make sure the code is added in the HTML mode.</p>\r\n<pre>&lt;button class="btn"&gt;...&lt;/button&gt;</pre>\r\n<p><button class="btn btn-small">Small Button</button></p>\r\n<p style="text-align: left;">This is the small button size. To use it just type the following code in your text editor. Make sure the code is added in the HTML mode.</p>\r\n<pre>&lt;button class="btn btn-small"&gt;...&lt;/button&gt;</pre>\r\n<p><button class="btn btn-mini">Mini Button</button></p>\r\n<p style="text-align: left;">This is the mini button size. To use it just type the following code in your text editor. Make sure the code is added in the HTML mode.</p>\r\n<pre>&lt;button class="btn btn-mini"&gt;...&lt;/button&gt;</pre>', 1, 'block1', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"-sfx2","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(315, 311, 'Bootstrap Buttons', '', '<p><button class="btn btn-info">Info</button></p>\r\n<p style="text-align: left;">This is the <code>.btn-info</code> button element. To use it just type the following code in your text editor. Make sure the code is added in the html mode.</p>\r\n<pre>&lt;button class="btn btn-info"&gt;...&lt;/button&gt;</pre>\r\n<p><button class="btn btn-success">Success</button></p>\r\n<p style="text-align: left;">This is the <code>.btn-success</code> button element. To use it just type the following code in your text editor. Make sure the code is added in the html mode.</p>\r\n<pre>&lt;button class="btn btn-success"&gt;...&lt;/button&gt;</pre>\r\n<p><button class="btn btn-warning">Warning</button></p>\r\n<p style="text-align: left;">This is the <code>.btn-warning</code> button element. To use it just type the following code in your text editor. Make sure the code is added in the html mode.</p>\r\n<pre>&lt;button class="btn btn-warning"&gt;...&lt;/button&gt;</pre>\r\n<p><button class="btn btn-danger">Danger</button></p>\r\n<p style="text-align: left;">This is the <code>.btn-danger</code> button element. To use it just type the following code in your text editor. Make sure the code is added in the html mode.</p>\r\n<pre>&lt;button class="btn btn-danger"&gt;...&lt;/button&gt;</pre>\r\n<p><button class="btn btn-inverse">Inverse</button></p>\r\n<p style="text-align: left;">This is the <code>.btn-inverse</code> button element. To use it just type the following code in your text editor. Make sure the code is added in the html mode.</p>\r\n<pre>&lt;button class="btn btn-inverse"&gt;...&lt;/button&gt;</pre>', 1, 'block1', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"-sfx2","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(317, 313, 'Blockquote Dark Style', '', '<blockquote class="blockquote-dark">\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse.</p>\r\n<small>Someone famous</small></blockquote>\r\n<p>This is the <code>.blockquote-dark</code> element designed for the blockquote element when used for dark backgrounds. To use it just type the following code in your text editor. Make sure the code is added in the HTML mode by pressing the <strong>Toggle Editor</strong> button.</p>\r\n<pre>&lt;blockquote class="blockquote-dark"&gt;<br />    &lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit.&lt;/p&gt;<br />&lt;/blockquote&gt;</pre>', 3, 'prime1', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"-sfx34","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(318, 314, 'Blockquote Color Style', '', '<blockquote class="blockquote-color">\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse.</p>\r\n<small>Someone famous</small></blockquote>\r\n<p>This is the <code>.blockquote-color</code> element designed for the blockquote element when used for colored backgrounds. To use it just type the following code in your text editor. Make sure the code is added in the HTML mode by pressing the <strong>Toggle Editor</strong> button.</p>\r\n<pre>&lt;blockquote class="blockquote-color"&gt;<br />    &lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit.&lt;/p&gt;<br />&lt;/blockquote&gt;</pre>', 8, 'prime1', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"-sfx10","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(319, 315, 'Blockquote', '', '<blockquote>\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse.</p>\r\n<small>Someone famous <cite title="Source Title">Source Title</cite></small></blockquote>\r\n<p>This is the <code>&lt;blockquote&gt;</code> element. To use it just type the following code in your text editor. Make sure the code is added in the HTML mode by pressing the <strong>Toggle Editor</strong> button.</p>\r\n<pre>&lt;blockquote&gt;<br />    &lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.&lt;/p&gt;<br />&lt;/blockquote&gt;</pre>\r\n<p>Add <code>small</code> tag for identifying the source. Wrap the name of the source work in <code>cite</code>.</p>\r\n<pre>&lt;blockquote&gt;<br />    &lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.&lt;/p&gt;\r\n    &lt;small&gt;Someone famous &lt;cite title="Source Title"&gt;Source Title&lt;/cite&gt;&lt;/small&gt;<br />&lt;/blockquote&gt;</pre>', 2, 'promo1', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"-sfx4","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(321, 317, 'Square List Style', '', '<div class="span6">\r\n<ul class="list-square">\r\n<li>item</li>\r\n<li>item</li>\r\n<li>item</li>\r\n<li>item</li>\r\n</ul>\r\n</div>\r\n<div class="span6">\r\n<ol class="list-square">\r\n<li>item</li>\r\n<li>item</li>\r\n<li>item</li>\r\n<li>item</li>\r\n</ol>\r\n</div>', 1, 'block1', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"-sfx6","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(322, 318, 'Lead Body Copy', '', '<p class="lead">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus quis lectus metus, at posuere neque. Sed pharetra nibh eget orci convallis at posuere leo convallis.</p>\r\n<p>This is the <code>.lead</code> element from Bootstrap for making a paragraph stand out . To use it just type the following code in your text editor. Make sure the code is added in the HTML mode by pressing the <strong>Toggle Editor</strong> button.</p>\r\n<pre>&lt;p class="lead""&gt;...&lt;/p&gt;</pre>', 2, 'block1', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"-sfx4","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(323, 319, 'Content Boxes', '', '<div class="info-box">This is the info box. To use it just type the following code in your text editor in the HTML mode:<br /> &lt;div class="info-box"&gt; ...Your content goes here... &lt;/div&gt;</div>\r\n<div class="success-box">This is the success box. To use it just type the following code in your text editor in the HTML mode:<br /> &lt;div class="success-box"&gt; ...Your content goes here... &lt;/div&gt;</div>\r\n<div class="warning-box">This is the warning box. To use it just type the following code in your text editor in the HTML mode:<br /> &lt;div class="warning-box"&gt; ...Your content goes here... &lt;/div&gt;</div>\r\n<div class="error-box">This is the error box. To use it just type the following code in your text editor in the HTML mode:<br /> &lt;div class="error-box"&gt; ...Your content goes here... &lt;/div&gt;</div>\r\n<div class="simple-box">This is the simple box. To use it just type the following code in your text editor in the HTML mode:<br /> &lt;div class="simple-box"&gt; ...Your content goes here... &lt;/div&gt;</div>', 8, 'block1', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"-sfx4","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(324, 320, 'Drop Caps', '', '<p class="dropcap">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>\r\n<p>This is the <code>.dropcap</code> element. To use it just type the following code in your text editor. Make sure the code is added in the HTML mode.</p>\r\n<pre>&lt;p class="dropcap"&gt;...&lt;/div&gt;</pre>', 6, 'block1', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"-sfx4","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(325, 321, 'Headings', '', '<p>All HTML headings, <code>&lt;h1&gt;</code> through <code>&lt;h6&gt;</code> are available.</p>\r\n<h1>h1. Heading 1</h1>\r\n<h2>h2. Heading 2</h2>\r\n<h3>h3. Heading 3</h3>\r\n<h4>h4. Heading 4</h4>\r\n<h5>h5. Heading 5</h5>\r\n<h6>h6. Heading 6</h6>\r\n<p>To use the headings just type the following code in your text editor. Make sure the code is added in the HTML mode.</p>\r\n<pre>&lt;h1&gt;...&lt;/h1&gt;</pre>', 7, 'block1', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"-sfx4","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(326, 322, 'How to use the list styles', '', '<p>To use the list styles just type the following code inside your text editor. Make sure the code is added in the HTML mode:</p>\r\n<div class="span6" style="margin-left: 0;">\r\n<pre style="margin-bottom: 0;">&lt;ul class="list-square"&gt;\r\n&lt;li&gt;item&lt;/li&gt;\r\n&lt;li&gt;item&lt;/li&gt;\r\n&lt;li&gt;item&lt;/li&gt;\r\n&lt;li&gt;item&lt;/li&gt;\r\n&lt;/ul&gt;</pre>\r\n</div>\r\n<div class="span6">\r\n<pre style="margin-bottom: 0;">&lt;ol class="list-square"&gt;\r\n&lt;li&gt;item&lt;/li&gt;\r\n&lt;li&gt;item&lt;/li&gt;\r\n&lt;li&gt;item&lt;/li&gt;\r\n&lt;li&gt;item&lt;/li&gt;\r\n&lt;/ol&gt;</pre>\r\n</div>', 1, 'mainbottom2', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"-sfx2","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"fa-graduation-cap","style":"0"}', 0, '*'),
(327, 323, 'Emphasis Classes', '', '<p>Convey meaning through color with a handful of emphasis utility classes. To use it just type the following code in your text editor. Make sure the code is added in the HTML mode.</p>\r\n<p> </p>\r\n<p class="muted">This is the <code>.muted</code> emphasis class.</p>\r\n<pre>&lt;p class="muted"&gt;This is the muted emphasis class.&lt;/p&gt;</pre>\r\n\r\n<p class="text-warning">This is the <code>.text-warning</code> emphasis class.</p>\r\n<pre>&lt;p class="text-warning"&gt;This is the text-warning emphasis class.&lt;/p&gt;</pre>\r\n\r\n<p class="text-error">This is the <code>.text-error</code> emphasis class.</p>\r\n<pre>&lt;p class="text-error"&gt;This is the text-error emphasis class.&lt;/p&gt;</pre>\r\n\r\n<p class="text-info">This is the <code>.text-info</code> emphasis class.</p>\r\n<pre>&lt;p class="text-info"&gt;This is the text-info emphasis class.&lt;/p&gt;</pre>\r\n\r\n<p class="text-success">This is the <code>.text-success</code> emphasis class.</p>\r\n<pre>&lt;p class="text-success"&gt;This is the text-success emphasis class.&lt;/p&gt;</pre>', 10, 'block1', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"-sfx4","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(328, 324, 'Inline Labels', '', '\r\n<p>Lorem ipsum dolor sit amet <span class="label-red">sed do eiusmod tempor incididunt</span> ut labore et dolore magna aliqua.</p>\r\n<p>This is the <code>.label-red</code> label. To use it just type the following code in your text editor. Make sure the code is added in the HTML mode.</p>\r\n<pre>&lt;span class="label-red"&gt;...&lt;/span&gt;</pre>\r\n\r\n<p>Lorem ipsum dolor sit amet <span class="label-blue">sed do eiusmod tempor incididunt</span> ut labore et dolore magna aliqua.</p>\r\n<p>This is the <code>.label-blue</code> label. To use it just type the following code in your text editor. Make sure the code is added in the HTML mode.</p>\r\n<pre>&lt;span class="label-blue"&gt;...&lt;/span&gt;</pre>\r\n\r\n<p>Lorem ipsum dolor sit amet <span class="label-green">sed do eiusmod tempor incididunt</span> ut labore et dolore magna aliqua.</p>\r\n<p>This is the <code>.label-green</code> label. To use it just type the following code in your text editor. Make sure the code is added in the HTML mode.</p>\r\n<pre>&lt;span class="label-green"&gt;...&lt;/span&gt;</pre>\r\n\r\n<p>Lorem ipsum dolor sit amet <span class="label-grey">sed do eiusmod tempor incididunt</span> ut labore et dolore magna aliqua.</p>\r\n<p>This is the <code>.label-grey</code> label. To use it just type the following code in your text editor. Make sure the code is added in the HTML mode.</p>\r\n<pre>&lt;span class="label-grey"&gt;...&lt;/span&gt;</pre>\r\n\r\n<p>Lorem ipsum dolor sit amet <span class="label-orange">sed do eiusmod tempor incididunt</span> ut labore et dolore magna aliqua.</p>\r\n<p>This is the <code>.label-orange</code> label. To use it just type the following code in your text editor. Make sure the code is added in the HTML mode.</p>\r\n<pre>&lt;span class="label-orange"&gt;...&lt;/span&gt;</pre>', 9, 'block1', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"-sfx4","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(329, 325, 'Tables', '', '<p>Add the base class <code>.table</code> to any table.</p>\r\n<table class="table">\r\n<thead>\r\n<tr><th>#</th><th>Table Heading</th><th>Table Heading</th><th>Table Heading</th></tr>\r\n</thead>\r\n<tbody>\r\n<tr>\r\n<td>1</td>\r\n<td>Table Data</td>\r\n<td>Table Data</td>\r\n<td>Table Data</td>\r\n</tr>\r\n<tr>\r\n<td>2</td>\r\n<td>Table Data</td>\r\n<td>Table Data</td>\r\n<td>Table Data</td>\r\n</tr>\r\n<tr>\r\n<td>3</td>\r\n<td>Table Data</td>\r\n<td>Table Data</td>\r\n<td>Table Data</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<p>To use the table just type the following code in your text editor. Make sure the code is added in the HTML mode.</p>\r\n<pre>&lt;table class="table"&gt;\r\n  …\r\n&lt;/table&gt;\r\n</pre>\r\n<p> </p>\r\n<p>Add borders and rounded corners to the table using the <code>.table-bordered</code> class.</p>\r\n<table class="table table-bordered">\r\n<thead>\r\n<tr><th>#</th><th>Table Heading</th><th>Table Heading</th><th>Table Heading</th></tr>\r\n</thead>\r\n<tbody>\r\n<tr>\r\n<td>1</td>\r\n<td>Table Data</td>\r\n<td>Table Data</td>\r\n<td>Table Data</td>\r\n</tr>\r\n<tr>\r\n<td>2</td>\r\n<td>Table Data</td>\r\n<td>Table Data</td>\r\n<td>Table Data</td>\r\n</tr>\r\n<tr>\r\n<td>3</td>\r\n<td>Table Data</td>\r\n<td>Table Data</td>\r\n<td>Table Data</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<p>To use the bordered table just type the following code in your text editor. Make sure the code is added in the HTML mode.</p>\r\n<pre>&lt;table class="table table-bordered"&gt;\r\n  …\r\n&lt;/table&gt;\r\n</pre>', 12, 'block1', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"-sfx4","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(330, 326, 'Code', '', '<p>Wrap inline snippets of code with <code>code</code>. To use it just type the following code in your text editor. Make sure the code is added in the HTML mode.</p>\r\n<pre>&lt;code&gt;...&lt;/code&gt;</pre>\r\n<p>Use <code>pre</code>for multiple lines of code.</p>\r\n<pre>.class { \r\nbackground: #f1f1f1;  \r\n}\r\n</pre>\r\n<p>Be sure to escape any angle brackets in the code for proper rendering. To use it just type the following code in your text editor. Make sure the code is added in the HTML mode.</p>\r\n<pre>&lt;pre&gt;...&lt;/pre&gt;</pre>', 11, 'block1', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"-sfx4","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(331, 327, 'Circle List Style', '', '<div class="span6">\r\n<ul class="list-circle">\r\n<li>item</li>\r\n<li>item</li>\r\n<li>item</li>\r\n<li>item</li>\r\n</ul>\r\n</div>\r\n<div class="span6">\r\n<ol class="list-circle">\r\n<li>item</li>\r\n<li>item</li>\r\n<li>item</li>\r\n<li>item</li>\r\n</ol>\r\n</div>', 1, 'block2', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"-sfx6","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(332, 328, 'Color List Style', '', '<div class="span6">\r\n<ul class="list-color">\r\n<li>item</li>\r\n<li>item</li>\r\n<li>item</li>\r\n<li>item</li>\r\n</ul>\r\n</div>\r\n<div class="span6">\r\n<ol class="list-color">\r\n<li>item</li>\r\n<li>item</li>\r\n<li>item</li>\r\n<li>item</li>\r\n</ol>\r\n</div>', 1, 'block3', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"-sfx10","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(333, 329, 'Dark List Style', '', '<div class="span6">\r\n<ul class="list-dark">\r\n<li>item</li>\r\n<li>item</li>\r\n<li>item</li>\r\n<li>item</li>\r\n</ul>\r\n</div>\r\n<div class="span6">\r\n<ol class="list-dark">\r\n<li>item</li>\r\n<li>item</li>\r\n<li>item</li>\r\n<li>item</li>\r\n</ol>\r\n</div>', 1, 'block4', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"-sfx34","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(373, 369, 'Extensions Intro', '', '<p class="favintro">We customize the extensions for each template with unique styles which can be replicated inside the template by adding the "favstyle" Module Class Suffix inside the Advanced settings of the module.</p>', 1, 'intro1', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 0, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(374, 370, 'Navigations Style Intro', '', '<p class="favintro">Use the navigation styles to customize the way you navigate the content and style the main menu with icons or create horizontal and vertical menus with preset design elements.</p>', 1, 'intro1', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 0, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(397, 393, 'How to apply the circle, color and dark list styles', '', '<p>For the circle, color or dark list styles, just replace <code>list-square</code> with <code>list-circle</code>, <code>list-color</code> or <code>list-dark</code>.</p>', 1, 'bottom1', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"-sfx2","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"fa-graduation-cap","style":"0"}', 0, '*'),
(401, 397, 'Error Page Intro', '', '<p class="favintro">Easy to customize, the new error page lets you edit the content inside a basic Joomla! article with text, images and button links to create custom error pages that match the style of your website!</p>', 1, 'intro1', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 0, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(402, 398, 'Docs Page Intro', '', '<p class="favintro">These tutorials are aimed to help you make the most of our templates and extensions in a short time, so the docs page covers basic topics such as setting up the main navigation or using the template settings.</p>', 1, 'intro1', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 0, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(407, 403, 'Main Navigation Styles', '', '<p>Add any icon from Font Awesome to a main navigation menu item to customize it or use the template settings to change the style and colors of the menu.</p>\r\n<p style="margin-top: 21px;"><img class="img-polaroid" src="images/demo/docs/favthemes-tutorial-navstyle.png" alt="FavThemes tutorial for the main navigation style parameters" /></p>', 2, 'lead1', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"-sfx4","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"fa-magic","style":"0"}', 0, '*'),
(408, 404, 'Vertical Menu Styles', '', '<p style="margin-bottom: 0;">The 4 preset styles for the vertical menus are <code>basic</code>, <code>arrow</code>, <code>side</code> and <code>line</code>. Each of them has different styles for the CLEAR, DARK or COLOR backgrounds:</p>', 3, 'lead1', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"-sfx4","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"fa-th-list","style":"0"}', 0, '*'),
(410, 406, 'Horizontal Menu Styles', '', '<p style="margin-bottom: 0;">To create a secondary horizontal menu, choose any module position and add the Menu Class Suffix inside the settings of the module. Use this menu type only for a secondary menu, not for the main navigation of the template, reserved for the <code>nav</code> module position.</p>', 1, 'lead1', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"-sfx4","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"fa-ellipsis-h","style":"0"}', 0, '*'),
(412, 408, 'How to use the vertical menu styles', '', '<p>To use the vertical menu styles, simply copy the name of the style inside the Menu Class Suffix field inside the Advanced options of the menu module:</p>\r\n<p style="margin-top: 21px; margin-bottom: 21px;"><img class="img-polaroid" src="images/demo/docs/favthemes-tutorial-vertical-menus.png" alt="FavThemes tutorial for the vertical menus" /></p>\r\n<p>For the CLEAR, DARK and COLOR vertical menu styles, add also <code>menu-clear</code>, <code>menu-dark</code> or <code>menu-color</code>:</p>\r\n<p style="margin-top: 21px; margin-bottom: 0;"><img class="img-polaroid" src="images/demo/docs/favthemes-tutorial-vertical-menus-styles.png" alt="FavThemes tutorial for the vertical menus styles" /></p>', 4, 'top1', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"-sfx2","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"fa-graduation-cap","style":"0"}', 0, '*'),
(415, 411, 'How to use the horizontal menu styles', '', '<p>To use the horizontal menu styles, simply copy the name of the style inside the Menu Class Suffix field inside the Advanced Options of the menu module:</p>\r\n<p style="margin-top: 21px; margin-bottom: 21px;"><img class="img-polaroid" src="images/demo/docs/favthemes-tutorial-horizontal-menu.png" alt="FavThemes tutorial for the horizontal menus" /></p>\r\n<p>For the CLEAR, DARK and COLOR horizontal menu styles, add also <code>menu-clear</code>, <code>menu-dark</code> or <code>menu-color</code>:</p>\r\n<p style="margin-top: 21px; margin-bottom: 21px;"><img class="img-polaroid" src="images/demo/docs/favthemes-tutorial-horizontal-menu-color.png" alt="FavThemes tutorial for the horizontal menus color" /></p>\r\n<p>For a horizontal menu that is aligned to the right, add Bootstrap''s <code>pull-right</code> class to <code>horizontal</code>:</p>\r\n<p style="margin-top: 21px; margin-bottom: 0;"><img class="img-polaroid" src="images/demo/docs/favthemes-tutorial-horizontal-menu-right.png" alt="FavThemes tutorial for the horizontal menu right" /></p>', 1, 'bottom1', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"-sfx2","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"fa-graduation-cap","style":"0"}', 0, '*'),
(417, 413, 'Horizontal Menu Clear', '', '', 1, 'prime1', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_menu', 1, 1, '{"menutype":"mainmenu","base":"","startLevel":"1","endLevel":"1","showAllChildren":"1","tag_id":"","class_sfx":"horizontal menu-clear","window_open":"","layout":"_:default","moduleclass_sfx":"-sfx14","cache":"1","cache_time":"900","cachemode":"itemid","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(418, 414, 'Horizontal Menu ', '', '', 3, 'promo1', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_menu', 1, 1, '{"menutype":"mainmenu","base":"","startLevel":"1","endLevel":"1","showAllChildren":"1","tag_id":"","class_sfx":"horizontal","window_open":"","layout":"_:default","moduleclass_sfx":"-sfx4","cache":"1","cache_time":"900","cachemode":"itemid","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*');
INSERT INTO `#__modules` (`id`, `asset_id`, `title`, `note`, `content`, `ordering`, `position`, `checked_out`, `checked_out_time`, `publish_up`, `publish_down`, `published`, `module`, `access`, `showtitle`, `params`, `client_id`, `language`) VALUES
(419, 415, 'Horizontal Menu Dark', '', '', 3, 'showcase1', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_menu', 1, 1, '{"menutype":"mainmenu","base":"","startLevel":"1","endLevel":"1","showAllChildren":"1","tag_id":"","class_sfx":"horizontal menu-dark","window_open":"","layout":"_:default","moduleclass_sfx":"-sfx26","cache":"1","cache_time":"900","cachemode":"itemid","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(420, 416, 'Horizontal Menu Color', '', '', 3, 'focus1', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_menu', 1, 1, '{"menutype":"mainmenu","base":"","startLevel":"1","endLevel":"1","showAllChildren":"1","tag_id":"","class_sfx":"horizontal menu-color","window_open":"","layout":"_:default","moduleclass_sfx":"-sfx38","cache":"1","cache_time":"900","cachemode":"itemid","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(421, 417, 'How to use the 404 error page', '', '<p>The new error page lets you edit the content inside a basic Joomla! article, so you can place your text, images and button links to create custom error pages that match the style of your website:</p>\r\n<p style="margin-top: 21px; margin-bottom: 21px;"><img class="img-polaroid-dark" src="images/demo/docs/favthemes-tutorial-error-page-article.png" alt="FavThemes tutorial for the error page article" /></p>\r\n<p>If the article is not published, then the error page looks like the default Joomla! error page. The template parameters let you control which article is used for the error page by inserting the article ID inside the ERROR PAGE ID setting in the STYLE tab. The default article ID for the error page is 3:</p>\r\n<p style="margin-top: 21px; margin-bottom: 21px;"><img class="img-polaroid-dark" src="images/demo/docs/favthemes-tutorial-error-page-params.png" alt="FavThemes tutorial for the error page template parameters" /></p>', 1, 'footer1', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"-sfx34","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"fa-graduation-cap","style":"0"}', 0, '*'),
(422, 418, 'How to use the custom styles for our extensions', '', '<p>Adding the <code>-favstyle</code> Module Class Suffix will replicate the custom styles for the extensions. To remove the custom styles for the extensions and revert them to their default look, simply remove the <code>-favstyle</code>:</p>\r\n<p style="margin-top: 21px; margin-bottom: 21px;"><img class="img-polaroid" src="images/demo/docs/favthemes-tutorial-extensions-favstyle.png" alt="FavThemes tutorial for vertical menus" /></p>\r\n', 1, 'bottom1', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"-sfx4","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"fa-graduation-cap","style":"0"}', 0, '*'),
(423, 419, 'How to add icons to the main navigation items', '', '<p>Choose any icon from <a href="http://fontawesome.io/" target="_blank">Font Awesome</a> and insert the name of the icon in the LINK CSS STYLE field in the LINK TYPE tab of the menu item. If you do not want to use any icon for the main navigation item, then simply leave this field blank:</p>\r\n<p style="margin-bottom: 21px;"><img class="img-polaroid" src="images/demo/docs/favthemes-tutorial-main-navigation-icons.png" alt="FavThemes tutorial for the main navigation icons" /></p>\r\n\r\n\r\n\r\n\r\n', 2, 'promo1', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"-sfx2","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"fa-graduation-cap","style":"0"}', 0, '*'),
(424, 420, 'How to customize the icons using the template settings', '', '<p>Customize the color and font size of the icons for the main navigation by using the STYLE tab from the template settings:</p>\r\n<p style="margin-bottom: 21px;"><img class="img-polaroid" src="images/demo/docs/favthemes-tutorial-main-navigation-params.png" alt="FavThemes tutorial for the main navigation icons customization in the template settings" /></p>\r\n\r\n\r\n\r\n\r\n', 2, 'promo1', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"-sfx2","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"fa-graduation-cap","style":"0"}', 0, '*'),
(425, 421, 'How to set up the main navigation of the template', '', '<p>Assign your main navigation menu module to the <code>nav</code> module position which is a special module position designed to be used only for the main navigation: </p>\r\n<p style="margin-bottom: 21px;"><img class="img-polaroid" src="images/demo/docs/favthemes-tutorial-nav-module-position.png" alt="FavThemes tutorial for the main navigation module positions" /></p>\r\n<p>Make sure the <code>nav-pills</code> Module Class Suffix is inserted in the Advanced Settings tab of the module:</p>\r\n<p style="margin-bottom: 21px;"><img class="img-polaroid" src="images/demo/docs/favthemes-tutorial-nav-navpills.png" alt="FavThemes tutorial for the main navigation navpills" /></p>\r\n<p>Being responsive, the main navigation adjusts to desktop, tablet and mobile with 3 different layouts. The tablet layout for the main navigation stacks the logo and main navigation on top of each other. The mobile layout for the main navigation uses Bootstrap''s collapsing navbar and has both a dark and a light version:</p>\r\n<p style="margin-bottom: 21px;"><img class="img-polaroid" src="images/demo/docs/favthemes-tutorial-nav-mobile.png" alt="FavThemes tutorial for the main navigation mobile menu" /></p>\r\n         \r\n         \r\n\r\n\r\n\r\n\r\n', 2, 'promo1', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"-sfx4","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"fa-graduation-cap","style":"0"}', 0, '*'),
(426, 422, 'How to use the visible/hidden Bootstrap classes', '', '<p>Use the <a href="http://getbootstrap.com/2.3.2/" target="_blank">Bootstrap</a> classes named <code>visible</code> and <code>hidden</code> to hide/show different modules for desktop, tablet or mobile. These responsive utility classes are: </p>\r\n<p style="margin-top: 21px; margin-bottom: 21px;"><img class="img-polaroid" src="images/demo/docs/favthemes-tutorial-bootstrap-visible-hidden.png" alt="FavThemes tutorial for Bootstrap visible and hidden responsive utility classes" /></p>\r\n<p class="simple-box">The screenshot is taken from the <a href="http://getbootstrap.com/2.3.2/scaffolding.html" target="_blank">Bootstrap 2.3 official documentation</a>, so please see the original source for the responsive utility classes documentation. Bootstrap 2.3 is the version that comes packed by default with Joomla! 3.x. </p>\r\n<p style="margin-top: 21px;">To use the visible and hidden classes, insert the name of the class inside the Module Class Suffix field inside the Advanced settings of the module: </p>\r\n<p style="margin-top: 21px; margin-bottom: 21px;"><img class="img-polaroid" src="images/demo/docs/favthemes-tutorial-bootstrap-module-classes.png" alt="FavThemes tutorial for Bootstrap visible and hidden classes inside the module settings" /></p>\r\n<p class="simple-box">For these classes to work, you need to insert a space before the name of the class (by pressing the spacebar key from your keyboard).</p>\r\n\r\n\r\n         \r\n         \r\n\r\n\r\n\r\n\r\n', 2, 'feature1', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"-sfx4","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"fa-graduation-cap","style":"0"}', 0, '*'),
(427, 423, 'How to use the template''s settings (parameters)', '', '<p>The template settings are designed to help you customize the template for your project as quickly as possible. Find the template you downloaded from FavThemes in the STYLES tab of the Template Manager and access it. The template settings look like this:</p>\r\n<p style="margin-top: 21px; margin-bottom: 21px;"><img class="img-polaroid" src="images/demo/docs/favthemes-tutorial-template-params.png" alt="FavThemes tutorial for the template settings" /></p>\r\n<h4 style="margin-top: 21px; margin-bottom: 7px;">1) Details</h4>\r\n<p>This tab is the presentation tab for the template and has a short description of the template, with links to the demo and documentation and a preview image.</p>\r\n<h4 style="margin-top: 21px; margin-bottom: 7px;">2) Settings</h4>\r\n<p>This tab controls the general settings of the template, the ones that let you customize the main elements such as color styles, the max width, the copyright info, the header, the main navigation, the vertical menus, the horizontal menus, the links, the titles, the articles, the modules, the variations, the buttons, the back to top link, the lists, the error page and the offline page.</p>\r\n<h4 style="margin-top: 21px; margin-bottom: 7px;">3) Layout</h4>\r\n<p>The layout tab targets the main layout elements such as body and module positions blocks: advert, topbar, slide, intro, lead, promo, prime, showcase, feature, focus, portfolio, screen, top, maintop, mainbottom, bottom, note, base, block, user and footer. Each one can be customized with background images and colors for the layout elements such as titles, text or links.</p>\r\n<h4 style="margin-top: 21px; margin-bottom: 7px;">4) Logo</h4>\r\n<p>This tab was created to customize the 4 logo types supported by our templates: default, uploaded, text and retina logo. Each of them can be further customized with additional features such as slogan text, image alt, padding, margin or Google Fonts.</p>\r\n<h4 style="margin-top: 21px; margin-bottom: 7px;">5) Mobile</h4>\r\n<p>Being a responsive product, the mobile behaviour of the template is a key element of the customization process. The tab for the mobile behaviour controls the style of the main navigation for mobile, the mobile images and the mobile font size for an improved reading experience on mobile devices.</p>\r\n<h4 style="margin-top: 21px; margin-bottom: 7px;">6) Analytics</h4>\r\n<p>Insert the Google Analytics tracking code inside this tab. Since SEO is an important element of any website, we''ve created this settings tab to make the Google Analytics setup for your website as easy as possible without any code changes that require editing the template files.</p>', 2, 'top1', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"-sfx4","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"fa-graduation-cap","style":"0"}', 0, '*'),
(428, 424, 'The settings tab', '', '<p>This tab controls the general settings of the template, the ones that let you customize the main elements of the website.</p>\r\n<h4 style="margin-top: 21px; margin-bottom: 7px;">1) Color Styles</h4>\r\n<p style="margin-top: 21px; margin-bottom: 21px;"><img class="img-polaroid" src="images/demo/docs/favthemes-tutorial-params-settings-color.png" alt="FavThemes tutorial for the settings tab color styles" /></p>\r\n<p>Choose between the 10 preset styles to customize the colors and appearance of the template.</p>\r\n<h4 style="margin-top: 21px; margin-bottom: 7px;">2) Maximum Width</h4>\r\n<p style="margin-top: 21px; margin-bottom: 21px;"><img class="img-polaroid" src="images/demo/docs/favthemes-tutorial-params-settings-width.png" alt="FavThemes tutorial for the settings tab maximum width" /></p>\r\n<p>Set the maximum width of the template in pixels, percent or em. A blank field reverts the setting to the default value.</p>\r\n<h4 style="margin-top: 21px; margin-bottom: 7px;">3) Copyright Info</h4>\r\n<p style="margin-top: 21px; margin-bottom: 21px;"><img class="img-polaroid" src="images/demo/docs/favthemes-tutorial-params-settings-copyright.png" alt="FavThemes tutorial for the settings tab copyright info" /></p>\r\n<p>Choose if the copyright text should be displayed or not, insert the text for the copyright field and the link for the copyright field.</p>\r\n<h4 style="margin-top: 21px; margin-bottom: 7px;">4) Main Navigation</h4>\r\n<p style="margin-top: 21px; margin-bottom: 21px;"><img class="img-polaroid" src="images/demo/docs/favthemes-tutorial-params-settings-nav.png" alt="FavThemes tutorial for the settings tab main navigation style" /></p>\r\n<p>Choose the style color, Google Font family, Google Font weight, Google Font style, text transform, font size, padding, icons color and icons font size.</p>\r\n<h4 style="margin-top: 21px; margin-bottom: 7px;">5) Links</h4>\r\n<p style="margin-top: 21px; margin-bottom: 21px;"><img class="img-polaroid" src="images/demo/docs/favthemes-tutorial-params-settings-links.png" alt="FavThemes tutorial for the settings tab links style" /></p>\r\n<p>Choose the colors for the template''s links in the normal and hover states. A blank field reverts the setting to the default value.</p>\r\n<h4 style="margin-top: 21px; margin-bottom: 7px;">6) Titles</h4>\r\n<p style="margin-top: 21px; margin-bottom: 21px;"><img class="img-polaroid" src="images/demo/docs/favthemes-tutorial-params-settings-titles.png" alt="FavThemes tutorial for the settings tab titles style" /></p>\r\n<p>Choose the Google Font family, Google Font weight, Google Font style, text align and text transform for the titles of the articles and modules.</p>\r\n<h4 style="margin-top: 21px; margin-bottom: 7px;">7) Articles</h4>\r\n<p style="margin-top: 21px; margin-bottom: 21px;"><img class="img-polaroid" src="images/demo/docs/favthemes-tutorial-params-settings-articles.png" alt="FavThemes tutorial for the settings tab articles style" /></p>\r\n<p>Choose the title color, title link color, title link hover color, title font size and title line height the articles.</p>\r\n<h4 style="margin-top: 21px; margin-bottom: 7px;">8) Modules</h4>\r\n<p style="margin-top: 21px; margin-bottom: 21px;"><img class="img-polaroid" src="images/demo/docs/favthemes-tutorial-params-settings-modules.png" alt="FavThemes tutorial for the settings tab modules style" /></p>\r\n<p>Choose the color, font size and line height for the title of the modules. Other module settings include the color, font size, padding, border color and border radius for the title icon.</p>\r\n<h4 style="margin-top: 21px; margin-bottom: 7px;">9) Variations</h4>\r\n<p style="margin-top: 21px; margin-bottom: 21px;"><img class="img-polaroid" src="images/demo/docs/favthemes-tutorial-params-settings-variations.png" alt="FavThemes tutorial for the settings tab modules style" /></p>\r\n<p>Choose the color, font size and icon padding for the Module Class Suffix variations style.</p>\r\n<h4 style="margin-top: 21px; margin-bottom: 7px;">10) Buttons</h4>\r\n<p style="margin-top: 21px; margin-bottom: 21px;"><img class="img-polaroid" src="images/demo/docs/favthemes-tutorial-params-settings-buttons.png" alt="FavThemes tutorial for the settings tab buttons style" /></p>\r\n<p>Choose the color, background color, hover color, background hover color, Google Font family, Google Font Weight, Google Font Style and text transform for the button element.</p>\r\n<h4 style="margin-top: 21px; margin-bottom: 7px;">11) Vertical Menus</h4>\r\n<p style="margin-top: 21px; margin-bottom: 21px;"><img class="img-polaroid" src="images/demo/docs/favthemes-tutorial-params-settings-vertical.png" alt="FavThemes tutorial for the settings tab vertical menus style" /></p>\r\n<p>Choose the color and the text transform for the style of the basic, arrow, side and line vertical menus.</p>\r\n<h4 style="margin-top: 21px; margin-bottom: 7px;">12) Horizontal Menus</h4>\r\n<p style="margin-top: 21px; margin-bottom: 21px;"><img class="img-polaroid" src="images/demo/docs/favthemes-tutorial-params-settings-horizontal.png" alt="FavThemes tutorial for the settings tab horizontal menus style" /></p>\r\n<p>Choose the color, Google Font family, Google Font weight, Google Font style and text transform for the horizontal menus.</p>\r\n<h4 style="margin-top: 21px; margin-bottom: 7px;">13) Lists</h4>\r\n<p style="margin-top: 21px; margin-bottom: 21px;"><img class="img-polaroid" src="images/demo/docs/favthemes-tutorial-params-settings-lists.png" alt="FavThemes tutorial for the settings tab lists style" /></p>\r\n<p>Choose the color for the ordered and unordered square, circle, color and dark lists types. A blank field reverts the setting to the default value.</p>\r\n<h4 style="margin-top: 21px; margin-bottom: 7px;">14) Back to Top</h4>\r\n<p style="margin-top: 21px; margin-bottom: 21px;"><img class="img-polaroid" src="images/demo/docs/favthemes-tutorial-params-settings-backtop.png" alt="FavThemes tutorial for the settings tab back to top button" /></p>\r\n<p>Choose if the button arrow for the back to top link should be displayed or not.</p>\r\n<h4 style="margin-top: 21px; margin-bottom: 7px;">15) Error Page</h4>\r\n<p style="margin-top: 21px; margin-bottom: 21px;"><img class="img-polaroid" src="images/demo/docs/favthemes-tutorial-params-settings-error.png" alt="FavThemes tutorial for the settings tab error page" /></p>\r\n<p>Insert the ID of the article used for the custom 404 error page. A blank field reverts the setting to the default value.</p>\r\n<h4 style="margin-top: 21px; margin-bottom: 7px;">16) Offline Page</h4>\r\n<p style="margin-top: 21px; margin-bottom: 21px;"><img class="img-polaroid" src="images/demo/docs/favthemes-tutorial-params-settings-offline.png" alt="FavThemes tutorial for the settings tab offline page" /></p>\r\n<p>Upload the background image for the site when in offline mode and choose the style for the background image for the site when in offline mode.</p>\r\n<h4 style="margin-top: 21px; margin-bottom: 7px;">17) Responsive K2 Images</h4>\r\n<p style="margin-top: 21px; margin-bottom: 21px;"><img class="img-polaroid" src="images/demo/docs/favthemes-tutorial-params-settings-k2img.png" alt="FavThemes tutorial for the settings tab k2 images" /></p>\r\n<p>Choose if the K2 images should be responsive or not.</p>', 2, 'top1', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"-sfx2","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"fa-cogs","style":"0"}', 0, '*'),
(429, 425, 'The layout tab', '', '<p>The layout tab targets the main layout elements such as body and module positions blocks which can be customized with background images and colors. </p>\r\n<h4 style="margin-top: 21px; margin-bottom: 7px;">1) Body</h4>\r\n<p style="margin-top: 21px; margin-bottom: 21px;"><img class="img-polaroid" src="images/demo/docs/favthemes-tutorial-layout-body.png" alt="FavThemes tutorial for the layout tab body element" /></p>\r\n<p>The body element serves as a container for the entire content of the website. Use it to change the design of the entire template, not just a section of the template like the module position blocks. To customize the body element, choose a background color, text color, title color, link color and link hover color. The body background image, background image style and background image overlay controls the top/bottom image stripe which by default is has a wood pattern element.</p>\r\n<h4 style="margin-top: 21px; margin-bottom: 7px;">2) Advert</h4>\r\n<p style="margin-top: 21px; margin-bottom: 21px;"><img class="img-polaroid" src="images/demo/docs/favthemes-tutorial-layout-advert.png" alt="FavThemes tutorial for the layout tab advert element" /></p>\r\n<p>The advert element is designed for banners, promotions and advertisement space. To customize the advert element, choose if the advert close button should be displayed or not, choose a background color, text color, title color, link color and link hover color. </p>\r\n<h4 style="margin-top: 21px; margin-bottom: 7px;">3) Slide</h4>\r\n<p style="margin-top: 21px; margin-bottom: 21px;"><img class="img-polaroid" src="images/demo/docs/favthemes-tutorial-layout-slide\r\n.png" alt="FavThemes tutorial for the layout tab slide element" /></p>\r\n<p>The slide element is designed for publishing the slideshow module and the slide images. The most important slide parameter is setting up the maximum width of this module position up to full width (100%). To customize the slide element, choose a background image, a background image style, a background image overlay, a background color, text color, title color, link color and link hover color. </p>\r\n<h4 style="margin-top: 21px; margin-bottom: 7px;">4) Module Position Blocks</h4>\r\n<p style="margin-top: 21px; margin-bottom: 21px;"><img class="img-polaroid" src="images/demo/docs/favthemes-tutorial-layout-top\r\n.png" alt="FavThemes tutorial for the layout tab module position blocks" /></p>\r\n<p>The module positions blocks are sections of the template and each one can be customized. If the advert, copyright and slide are special module position blocks with very custom functions, the rest of the module positions can be used for any type of content. Each of these module positions (topbar, intro, lead, promo, prime, showcase, feature, focus, portfolio, screen, top, maintop, mainbottom, bottom, note, base, block, user and footer) have the same set of parameters. </p>\r\n<p>To customize the module positions blocks, choose for each of them a background image, a background image style, a background image overlay, a background color, text color, title color, link color and link hover color. </p>\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n', 2, 'top1', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"-sfx2","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"fa-th","style":"0"}', 0, '*'),
(430, 426, 'The logo tab', '', '<p>The logo tab was created to customize the 4 logo types supported by our templates: default, uploaded, text and retina logo.</p>\r\n<h4 style="margin-top: 21px; margin-bottom: 7px;">1) Default logo</h4>\r\n<p style="margin-top: 21px; margin-bottom: 21px;"><img class="img-polaroid" src="images/demo/docs/favthemes-tutorial-logo-default.png" alt="FavThemes tutorial for the logo tab default" /></p>\r\n<p>The default logo is the logo that comes by default with the template and can be changed from the files and folders of the template inside the Template Manager. To customize the default logo, choose if it should be displayed or not, the logo image, the logo image alt tag, the padding and the margin.</p>\r\n<h4 style="margin-top: 21px; margin-bottom: 7px;">2) Uploaded logo</h4>\r\n<p style="margin-top: 21px; margin-bottom: 21px;"><img class="img-polaroid" src="images/demo/docs/favthemes-tutorial-logo-uploaded.png" alt="FavThemes tutorial for the logo tab uploaded" /></p>\r\n<p>The uploaded logo type is designed to let you upload any image for your logo without editing the template files or using FTP. To customize the uploaded logo, choose if it should be displayed or not, upload the the logo image or set the logo image alt tag, the padding and the margin.</p>\r\n<h4 style="margin-top: 21px; margin-bottom: 7px;">3) Text logo</h4>\r\n<p style="margin-top: 21px; margin-bottom: 21px;"><img class="img-polaroid" src="images/demo/docs/favthemes-tutorial-logo-text.png" alt="FavThemes tutorial for the logo tab text" /></p>\r\n<p>The text logo type is designed to be used instead of the logo image and can be styled with Google Fonts.  To customize the text logo, choose if it should be displayed or not, the logo text, the color, the font size, the Google Font family, the Google Font weight, the Google Font style, the line height, the padding and the margin.</p>\r\n<h4 style="margin-top: 21px; margin-bottom: 7px;">4) Slogan</h4>\r\n<p style="margin-top: 21px; margin-bottom: 21px;"><img class="img-polaroid" src="images/demo/docs/favthemes-tutorial-logo-slogan.png" alt="FavThemes tutorial for the logo tab slogan" /></p>\r\n<p>The slogan is designed to be used with the default, uploaded, retina or text logo types. To customize the slogan, choose if it should be displayed or not, the slogan text, the color, the font size, the line height, the padding and the margin.</p>\r\n<h4 style="margin-top: 21px; margin-bottom: 7px;">5) Retina logo</h4>\r\n<p style="margin-top: 21px; margin-bottom: 21px;"><img class="img-polaroid" src="images/demo/docs/favthemes-tutorial-logo-retina.png" alt="FavThemes tutorial for the logo tab retina" /></p>\r\n<p>The retina logo is designed to be used as the logo image for devices with Retina Display and only for those devices, being a Retina version of the normal logo (either default, uploaded or text logo types). Both the normal logo and the Retina logo can be used at the same time, the normal logo will be visible on the normal display, while the Retina logo will be visible only on the Retina Display. To customize the Retina logo, choose if it should be displayed or not, the logo image, the height, the width, the image alt tag, the padding and the margin.</p>\r\n\r\n\r\n\r\n', 2, 'top1', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"-sfx2","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"fa-globe","style":"0"}', 0, '*'),
(431, 427, 'The mobile tab', '', '<p>Being a responsive product, the mobile behaviour of the template is a key element of the customization process. The tab for the mobile behaviour controls the style of the main navigation for mobile, the mobile images and the mobile font size for an improved reading experience on mobile devices.</p>\r\n<h4 style="margin-top: 21px; margin-bottom: 7px;">1) Mobile Navigation</h4>\r\n<p style="margin-top: 21px; margin-bottom: 21px;"><img class="img-polaroid" src="images/demo/docs/favthemes-tutorial-mobile-navigation.png" alt="FavThemes tutorial for the mobile tab navigation" /></p>\r\n<p>Choose the light or dark version for the mobile navigation, choose if the mobile submenu items and mobile menu text should be displayed or not and insert the text for the mobile menu.</p>\r\n<h4 style="margin-top: 21px; margin-bottom: 7px;">2) Mobile Images</h4>\r\n<p style="margin-top: 21px; margin-bottom: 21px;"><img class="img-polaroid" src="images/demo/docs/favthemes-tutorial-mobile-images.png" alt="FavThemes tutorial for the mobile tab images" /></p>\r\n<p>Choose if the images should be displayed or not on mobile devices.</p>\r\n<h4 style="margin-top: 21px; margin-bottom: 7px;">3) Mobile Font Size</h4>\r\n<p style="margin-top: 21px; margin-bottom: 21px;"><img class="img-polaroid" src="images/demo/docs/favthemes-tutorial-mobile-font-size.png" alt="FavThemes tutorial for the mobile tab font size" /></p>\r\n<p>For an improved reading experience on mobile devices, choose the font size on mobile for the paragraph text, for the article and for the module title.</p>\r\n', 2, 'top1', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"-sfx2","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"fa-tablet","style":"0"}', 0, '*'),
(432, 428, 'The analytics tab', '', '<p>Insert the Google Analytics tracking code inside this tab. Since SEO is an important element of any website, we''ve created this settings tab to make the Google Analytics setup for your website as easy as possible without any code changes that require editing the template files.</p>\r\n<p style="margin-top: 21px; margin-bottom: 21px;"><img class="img-polaroid" src="images/demo/docs/favthemes-tutorial-analytics-tab.png" alt="FavThemes tutorial for the mobile tab navigation" /></p>\r\n\r\n\r\n', 2, 'top1', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"-sfx2","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"fa-bar-chart ","style":"0"}', 0, '*'),
(471, 479, 'How to use the offline page', '', '<p>To use the offline page, simply set the SITE OFFLINE setting to YES inside the Joomla! administrator interface by accessing the SYSTEM/GLOBAL CONFIGURATION/SITE tab.</p>\r\n<p style="margin-top: 21px; margin-bottom: 21px;"><img class="img-polaroid" src="images/demo/docs/favthemes-tutorial-offline-page.png" alt="FavThemes tutorial for the offline page" /></p>\r\n<p>Using the template parameters, the offline page can be further customized by uploading a background image and setting its style:</p>\r\n<p style="margin-top: 21px; margin-bottom: 21px;"><img class="img-polaroid" src="images/demo/docs/favthemes-tutorial-offline-page-params.png" alt="FavThemes tutorial for the offline page template parameters" /></p>', 1, 'promo1', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"-sfx2","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"fa-graduation-cap","style":"0"}', 0, '*'),
(472, 480, 'Offline Page Intro', '', '<p class="favintro">While you are creating or changing your website, the offline page is a key element of your awesome project. So we redesigned the offline page, making it responsive and customizable.</p>', 1, 'intro1', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 0, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(474, 482, 'FavEffects', '', '', 1, 'bottom1', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_faveffects', 1, 0, '{"jquery_load":"1","layout_effect":"layout-effect1","icon_width":"96%","icon_border_radius":"4px","icon_border_type":"solid","icon_border_width":"1px","title_google_font":"Roboto","title_font_size":"21px","show_icon1":"1","icon_effect1":"effect4","icon_name1":"fa-laptop","icon_color1":"0099FF","icon_bg_color1":"FFFFFF","show_icon_link1":"1","icon_link1":"http:\\/\\/getbootstrap.com\\/2.3.2\\/","icon_link_target1":"blank","icon_font_size1":"4em","icon_border_color1":"D7D7D7","title_text1":"Bootstrap ","title_color1":"444444","show_icon2":"1","icon_effect2":"effect4","icon_name2":"fa-font","icon_color2":"0099FF","icon_bg_color2":"FFFFFF","show_icon_link2":"1","icon_link2":"https:\\/\\/www.google.com\\/fonts\\/","icon_link_target2":"blank","icon_font_size2":"4em","icon_border_color2":"D7D7D7","title_text2":"Google Fonts ","title_color2":"444444","show_icon3":"1","icon_effect3":"effect4","icon_name3":"fa-flask","icon_color3":"0099FF","icon_bg_color3":"FFFFFF","show_icon_link3":"1","icon_link3":"index.php\\/features\\/404","icon_link_target3":"self","icon_font_size3":"4em","icon_border_color3":"D7D7D7","title_text3":"404 page","title_color3":"444444","show_icon4":"1","icon_effect4":"effect4","icon_name4":"fa-paint-brush","icon_color4":"0099FF","icon_bg_color4":"FFFFFF","show_icon_link4":"1","icon_link4":"index.php","icon_link_target4":"self","icon_font_size4":"4em","icon_border_color4":"D7D7D7","title_text4":"10 color styles ","title_color4":"444444","show_icon5":"1","icon_effect5":"effect4","icon_name5":"fa-paper-plane-o","icon_color5":"0099FF","icon_bg_color5":"FFFFFF","show_icon_link5":"1","icon_link5":"index.php\\/features\\/offline-page","icon_link_target5":"self","icon_font_size5":"4em","icon_border_color5":"D7D7D7","title_text5":"Offline Page ","title_color5":"444444","show_icon6":"1","icon_effect6":"effect4","icon_name6":"fa-clone","icon_color6":"0099FF","icon_bg_color6":"FFFFFF","show_icon_link6":"1","icon_link6":"index.php\\/features\\/module-variations","icon_link_target6":"self","icon_font_size6":"4em","icon_border_color6":"D7D7D7","title_text6":"Module Icons ","title_color6":"444444","moduleclass_sfx":" favstyle","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(475, 483, 'FavGlyph', '', '', 1, 'lead1', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_favglyph', 1, 0, '{"jquery_load":"1","layout_effect":"layout-effect1","icon_width":"60%","icon_border_radius":"50%","icon_border_type":"solid","icon_border_width":"1px","title_google_font":"Roboto","title_font_size":"24px","title_margin":"24px 0px 14px","description_font_size":"14px","show_icon1":"1","icon_layout1":"center","icon_name1":"fa-magic","icon_color1":"FFFFFF","icon_bg_color1":"0099FF","icon_border_color1":"0099FF","icon_link1":"index.php","icon_link_target1":"self","icon_font_size1":"4em","title_text1":"CUSTOMIZE","title_color1":"444444","description_layout1":"0","description_text1":"the template easy & fast <br\\/>\\r\\nusing over 200 parameters","description_text_color1":"444444","show_icon2":"1","icon_layout2":"center","icon_name2":"fa-gift","icon_color2":"FFFFFF","icon_bg_color2":"0099FF","icon_border_color2":"0099FF","icon_link2":"index.php","icon_link_target2":"self","icon_font_size2":"4.6em","title_text2":"AWESOMENESS","title_color2":"444444","description_layout2":"0","description_text2":"with Google Fonts and <br\\/>\\r\\nFont Awesome Icons ","description_text_color2":"444444","show_icon3":"1","icon_layout3":"center","icon_name3":"fa-laptop","icon_color3":"FFFFFF","icon_bg_color3":"0099FF","icon_border_color3":"0099FF","icon_link3":"index.php","icon_link_target3":"self","icon_font_size3":"4.4em","title_text3":"RESPONSIVE","title_color3":"444444","description_layout3":"0","description_text3":"made with Bootstrap, <br\\/>\\r\\nHTML5 and CSS3 ","description_text_color3":"444444","show_icon4":"1","icon_layout4":"center","icon_name4":"fa-cogs","icon_color4":"FFFFFF","icon_bg_color4":"0099FF","icon_border_color4":"0099FF","icon_link4":"index.php\\/k2\\/k2-content","icon_link_target4":"self","icon_font_size4":"4em","title_text4":"K2 SUPPORT ","title_color4":"444444","description_layout4":"0","description_text4":"powerful content extension for <br\\/>\\r\\nJoomla! integration ","description_text_color4":"444444","show_icon5":"0","icon_layout5":"center","icon_name5":"fa-tablet","icon_color5":"673AB7","icon_bg_color5":"FFFFFF","icon_border_color5":"CCCCCC","icon_link5":"http:\\/\\/www.favthemes.com","icon_link_target5":"blank","icon_font_size5":"6em","title_text5":"Title Text","title_color5":"444444","description_layout5":"0","description_text5":"Lorem ipsum dolor sit amet, dolore magna aliqua.","description_text_color5":"444444","show_icon6":"0","icon_layout6":"center","icon_name6":"fa-puzzle-piece","icon_color6":"00BCD4","icon_bg_color6":"FFFFFF","icon_border_color6":"CCCCCC","icon_link6":"http:\\/\\/www.favthemes.com","icon_link_target6":"blank","icon_font_size6":"6em","title_text6":"Title Text","title_color6":"444444","description_layout6":"0","description_text6":"Lorem ipsum dolor sit amet, dolore magna aliqua.","description_text_color6":"444444","moduleclass_sfx":" favstyle","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(477, 485, 'FavPromote', '', '', 1, 'prime1', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_favpromote', 1, 0, '{"jquery_load":"1","layout_effect":"layout-effect1","title_google_font":"Roboto","title_padding":"10px 20px","title_font_size":"21px","title_line_height":"1.4em","title_text_align":"left","title_icon_font_size":"18px","title_icon_vertical_align":"baseline","description_text_font_size":"14px","description_text_line_height":"1.5em","description_text_align":"justify","show_column1":"1","column_border_color1":"FFFFFF","column_border_radius1":"4px","upload_image1":"images\\/demo\\/img\\/favpromote-img-1.jpg","show_image_link1":"1","image_link1":"index.php\\/documentation","image_target1":"self","image_alt1":"","title_text1":" Logo Options ","title_color1":"FFFFFF","title_bg_color1":"444444","show_title_link1":"1","title_link1":"index.php\\/documentation","title_target1":"self","title_icon1":"fa-bookmark-o","description_text1":"Upload your own logo image or use a text logo that supports Google Fonts. Need a slogan under your logo? No problem! ","description_text_color1":"444444","show_column2":"1","column_border_color2":"FFFFFF","column_border_radius2":"4px","upload_image2":"images\\/demo\\/img\\/favpromote-img-2.jpg","show_image_link2":"1","image_link2":"index.php\\/typography\\/icon-styles","image_target2":"self","image_alt2":"","title_text2":"Font Awesome ","title_color2":"FFFFFF","title_bg_color2":"444444","show_title_link2":"1","title_link2":"index.php\\/typography\\/icon-styles","title_target2":"self","title_icon2":"fa-flag-o","description_text2":"Use this amazing icon font with many elements of the template such as module titles, menus, buttons or lists. ","description_text_color2":"444444","show_column3":"1","column_border_color3":"FFFFFF","column_border_radius3":"4px","upload_image3":"images\\/demo\\/img\\/favpromote-img-3.jpg","show_image_link3":"1","image_link3":"index.php\\/features\\/navigation-styles\\/main-navigation-styles","image_target3":"self","image_alt3":"","title_text3":"Menu Styles ","title_color3":"FFFFFF","title_bg_color3":"444444","show_title_link3":"1","title_link3":"index.php\\/features\\/navigation-styles\\/main-navigation-styles","title_target3":"self","title_icon3":"fa-ellipsis-h","description_text3":"Customize the way you navigate the content and style the main navigation, the horizontal and vertical menus. ","description_text_color3":"444444","show_column4":"1","column_border_color4":"FFFFFF","column_border_radius4":"4px","upload_image4":"images\\/demo\\/img\\/favpromote-img-4.jpg","show_image_link4":"1","image_link4":"index.php\\/extensions","image_target4":"self","image_alt4":"","title_text4":"Extensions ","title_color4":"FFFFFF","title_bg_color4":"444444","show_title_link4":"1","title_link4":"index.php\\/extensions","title_target4":"self","title_icon4":"fa-puzzle-piece","description_text4":"We customize the extensions for each template with unique styles which can be enabled by adding the \\"favstyle\\" Module Class Suffix. ","description_text_color4":"444444","show_column5":"0","column_border_color5":"DDDDDD","column_border_radius5":"4px","upload_image5":"","show_image_link5":"1","image_link5":"http:\\/\\/www.favthemes.com","image_target5":"blank","image_alt5":"","title_text5":"Title Text","title_color5":"FFFFFF","title_bg_color5":"D63E52","show_title_link5":"1","title_link5":"http:\\/\\/www.favthemes.com","title_target5":"blank","title_icon5":"fa-quote-right","description_text5":"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor.","description_text_color5":"444444","show_column6":"0","column_border_color6":"DDDDDD","column_border_radius6":"4px","upload_image6":"","show_image_link6":"1","image_link6":"http:\\/\\/www.favthemes.com","image_target6":"blank","image_alt6":"","title_text6":"Title Text","title_color6":"FFFFFF","title_bg_color6":"D63E52","show_title_link6":"1","title_link6":"http:\\/\\/www.favthemes.com","title_target6":"blank","title_icon6":"fa-recycle","description_text6":"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor.","description_text_color6":"444444","moduleclass_sfx":" favstyle","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(478, 486, 'FavSlider Responsive Slideshow', '', '', 1, 'slide', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_favslider', 1, 0, '{"sliderType":"basic","jqueryLoad":"1","animationEffect":"fade","slideHeight":"","thumbHeight":"","linkTarget":"self","arrowNav":"1","arrowNavStyle":"dark-arrows","controlNav":"1","slideshow":"0","randomize":"0","animationLoop":"1","pauseOnHover":"1","keyboardNav":"1","mousewheel":"0","slideshowSpeed":"7000","captionHide":"hidden-phone","layoutEffect":"layout-effect3","captionTextAlign":"favalign-left","captionStyle":"favstyle-default","captionBgStyle":"favstyle-bg-dark","captionWidth":"","captionHeight":"","captionTitleGoogleFont":"Roboto","captionTitleFontSize":"30px","captionTitleTextTransform":"none","captionTitlePadding":"","captionTitleMargin":"","captionDescriptionGoogleFont":"Roboto","captionDescriptionFontSize":"","captionReadMoreColor":"","captionReadMoreBgColor":"","captionReadMoreGoogleFont":"Roboto","captionReadMorePadding":"","captionReadMoreMargin":"","file1active":"1","file1type":"image","file1":"images\\/demo\\/slide\\/slide1.jpg","file1alt":"","file1link":"","file1favtitle":"Hey, I’m a free & responsive Joomla! template!","file1favdescription":"","file1favreadmore":"","file2active":"1","file2type":"image","file2":"images\\/demo\\/slide\\/slide2.jpg","file2alt":"","file2link":"index.php\\/documentation","file2favtitle":"Amazing customization with 200+ template settings","file2favdescription":"","file2favreadmore":"","file3active":"0","file3type":"image","file3":"media\\/favslider\\/demo\\/slide3.jpg","file3alt":"","file3link":"","file3favtitle":"","file3favdescription":"","file3favreadmore":"","file4active":"0","file4type":"image","file4":"media\\/favslider\\/demo\\/slide4.jpg","file4alt":"","file4link":"","file4favtitle":"lorem ipsum","file4favdescription":"lorem ipsum dolor sit amet","file4favreadmore":"","file5active":"0","file5type":"image","file5":"media\\/favslider\\/demo\\/slide5.jpg","file5alt":"","file5link":"","file5favtitle":"lorem ipsum","file5favdescription":"lorem ipsum dolor sit amet","file5favreadmore":"","file6active":"0","file6type":"image","file6":"","file6alt":"","file6link":"","file6favtitle":"","file6favdescription":"","file6favreadmore":"","file7active":"0","file7type":"image","file7":"","file7alt":"","file7link":"","file7favtitle":"","file7favdescription":"","file7favreadmore":"","file8active":"0","file8type":"image","file8":"","file8alt":"","file8link":"","file8favtitle":"","file8favdescription":"","file8favreadmore":"","file9active":"0","file9type":"image","file9":"","file9alt":"","file9link":"","file9favtitle":"","file9favdescription":"","file9favreadmore":"","file10active":"0","file10type":"image","file10":"","file10alt":"","file10link":"","file10favtitle":"","file10favdescription":"","file10favreadmore":"","moduleclass_sfx":" favstyle","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(479, 487, 'FavSocial', '', '', 1, 'copyright1', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_favsocial', 1, 0, '{"module_align":"favsocial-right","show_icon1":"1","icon_name1":"fa-twitter","icon_color1":"FFFFFF","icon_bg_color1":"AAAAAA","icon_font_size1":"18px","icon_link1":"https:\\/\\/twitter.com\\/","icon_link_target1":"blank","icon_padding1":"0.2em","icon_border_radius1":"4px","show_icon2":"1","icon_name2":"fa-facebook","icon_color2":"FFFFFF","icon_bg_color2":"AAAAAA","icon_font_size2":"18px","icon_link2":"https:\\/\\/www.facebook.com\\/","icon_link_target2":"blank","icon_padding2":"0.2em","icon_border_radius2":"4px","show_icon3":"1","icon_name3":"fa-dribbble","icon_color3":"FFFFFF","icon_bg_color3":"AAAAAA","icon_font_size3":"18px","icon_link3":"https:\\/\\/dribbble.com\\/","icon_link_target3":"blank","icon_padding3":"0.2em","icon_border_radius3":"4px","show_icon4":"0","icon_name4":"","icon_color4":"","icon_bg_color4":"","icon_font_size4":"","icon_link4":"","icon_link_target4":"blank","icon_padding4":"","icon_border_radius4":"","show_icon5":"0","icon_name5":"","icon_color5":"","icon_bg_color5":"","icon_font_size5":"","icon_link5":"","icon_link_target5":"blank","icon_padding5":"","icon_border_radius5":"","show_icon6":"0","icon_name6":"","icon_color6":"","icon_bg_color6":"","icon_font_size6":"","icon_link6":"","icon_link_target6":"blank","icon_padding6":"","icon_border_radius6":"","show_icon7":"0","icon_name7":"","icon_color7":"","icon_bg_color7":"","icon_font_size7":"","icon_link7":"","icon_link_target7":"blank","icon_padding7":"","icon_border_radius7":"","show_icon8":"0","icon_name8":"","icon_color8":"","icon_bg_color8":"","icon_font_size8":"","icon_link8":"","icon_link_target8":"blank","icon_padding8":"","icon_border_radius8":"","show_icon9":"0","icon_name9":"","icon_color9":"","icon_bg_color9":"","icon_font_size9":"","icon_link9":"","icon_link_target9":"blank","icon_padding9":"","icon_border_radius9":"","show_icon10":"0","icon_name10":"","icon_color10":"","icon_bg_color10":"","icon_font_size10":"","icon_link10":"","icon_link_target10":"blank","icon_padding10":"","icon_border_radius10":"","moduleclass_sfx":"pull-right favstyle","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(482, 490, 'Menu Home', '', '', 1, 'sidebar2', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_menu', 1, 1, '{"menutype":"mainmenu","base":"","startLevel":"1","endLevel":"1","showAllChildren":"0","tag_id":"","class_sfx":"basic","window_open":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"itemid","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(483, 504, 'How to use the pull-left/pull-right Bootstrap classes', '', '<p>Use the <a href="http://getbootstrap.com/2.3.2/" target="_blank">Bootstrap</a> classes named <code>pull-left</code> and <code>pull-right</code> to position horizontally 2 or more modules using the same module position. <br/> To use them, insert the name of the class inside the Module Class Suffix field inside the Advanced settings of the module: </p>\r\n<p style="margin-top: 21px; margin-bottom: 21px;"><img class="img-polaroid" src="images/demo/docs/favthemes-tutorial-bootstrap-pull-left-right.png" alt="FavThemes tutorial for Bootstrap pull-left and pull-right classes inside the module settings" /></p>\r\n<p class="simple-box">For these classes to work, you need to insert a space before the name of the class (by pressing the spacebar key from your keyboard). <br/> For the classes to work, don''t use both of them at the same time for the same module, use either <code>pull-left</code> or <code>pull-right</code>.</p>\r\n\r\n\r\n         \r\n         \r\n\r\n\r\n\r\n\r\n', 2, 'feature1', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"-sfx4","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"fa-graduation-cap","style":"0"}', 0, '*'),
(484, 505, 'Download theme', '', '<div id="fav-download">\r\n<p id="fav-download-message">Use this theme for your next project</p>\r\n<p id="fav-download-btn"><a class="btn" href="http://www.favthemes.com/themes/product/favourite-free-responsive-joomla-3-template.html" target="_blank"> <i class="fa fa-download"></i>\r\nDOWNLOAD</a></p>\r\n</div>', 1, 'showcase1', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 0, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*');
INSERT INTO `#__modules` (`id`, `asset_id`, `title`, `note`, `content`, `ordering`, `position`, `checked_out`, `checked_out_time`, `publish_up`, `publish_down`, `published`, `module`, `access`, `showtitle`, `params`, `client_id`, `language`) VALUES
(485, 506, 'Offline Page', '', '<p style="margin-top: 21px; margin-bottom: 0;"><img class="img-polaroid" src="images/demo/docs/docs-offline-image.jpg" alt="FavThemes tutorial for the offline page" /></p>', 1, 'lead1', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 0, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(486, 507, 'Typography Intro', '', '<p class="favintro">Typography styles are a great way to customize the appearance of content elements such as Font Awesome icons, menus, images, buttons, Bootstrap elements, all with different color styles.</p>', 1, 'intro1', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 0, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(487, 508, 'FavSlider Responsive Slideshow', '', '', 1, 'lead1', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_favslider', 1, 1, '{"sliderType":"basic","jqueryLoad":"1","animationEffect":"fade","slideHeight":"","thumbHeight":"","linkTarget":"self","arrowNav":"1","arrowNavStyle":"dark-arrows","controlNav":"1","slideshow":"0","randomize":"0","animationLoop":"1","pauseOnHover":"1","keyboardNav":"1","mousewheel":"0","slideshowSpeed":"7000","captionHide":"hidden-phone","layoutEffect":"layout-effect3","captionTextAlign":"favalign-left","captionStyle":"favstyle-default","captionBgStyle":"favstyle-bg-dark","captionWidth":"","captionHeight":"","captionTitleGoogleFont":"Roboto","captionTitleFontSize":"30px","captionTitleTextTransform":"none","captionTitlePadding":"","captionTitleMargin":"","captionDescriptionGoogleFont":"Roboto","captionDescriptionFontSize":"","captionReadMoreColor":"","captionReadMoreBgColor":"","captionReadMoreGoogleFont":"Roboto","captionReadMorePadding":"","captionReadMoreMargin":"","file1active":"1","file1type":"image","file1":"images\\/demo\\/slide\\/slide1.jpg","file1alt":"","file1link":"","file1favtitle":"Hey, I’m a free & responsive Joomla! template!","file1favdescription":"","file1favreadmore":"","file2active":"1","file2type":"image","file2":"images\\/demo\\/slide\\/slide2.jpg","file2alt":"","file2link":"index.php\\/documentation","file2favtitle":"Amazing customization with 200+ template settings","file2favdescription":"","file2favreadmore":"","file3active":"0","file3type":"image","file3":"media\\/favslider\\/demo\\/slide3.jpg","file3alt":"","file3link":"","file3favtitle":"","file3favdescription":"","file3favreadmore":"","file4active":"0","file4type":"image","file4":"media\\/favslider\\/demo\\/slide4.jpg","file4alt":"","file4link":"","file4favtitle":"lorem ipsum","file4favdescription":"lorem ipsum dolor sit amet","file4favreadmore":"","file5active":"0","file5type":"image","file5":"media\\/favslider\\/demo\\/slide5.jpg","file5alt":"","file5link":"","file5favtitle":"lorem ipsum","file5favdescription":"lorem ipsum dolor sit amet","file5favreadmore":"","file6active":"0","file6type":"image","file6":"","file6alt":"","file6link":"","file6favtitle":"","file6favdescription":"","file6favreadmore":"","file7active":"0","file7type":"image","file7":"","file7alt":"","file7link":"","file7favtitle":"","file7favdescription":"","file7favreadmore":"","file8active":"0","file8type":"image","file8":"","file8alt":"","file8link":"","file8favtitle":"","file8favdescription":"","file8favreadmore":"","file9active":"0","file9type":"image","file9":"","file9alt":"","file9link":"","file9favtitle":"","file9favdescription":"","file9favreadmore":"","file10active":"0","file10type":"image","file10":"","file10alt":"","file10link":"","file10favtitle":"","file10favdescription":"","file10favreadmore":"","moduleclass_sfx":"-sfx4 favstyle","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(488, 509, 'FavGlyph', '', '', 1, 'promo1', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_favglyph', 1, 1, '{"jquery_load":"1","layout_effect":"layout-effect1","icon_width":"60%","icon_border_radius":"50%","icon_border_type":"solid","icon_border_width":"1px","title_google_font":"Roboto","title_font_size":"24px","title_margin":"24px 0px 14px","description_font_size":"14px","show_icon1":"1","icon_layout1":"center","icon_name1":"fa-magic","icon_color1":"FFFFFF","icon_bg_color1":"0099FF","icon_border_color1":"0099FF","icon_link1":"index.php","icon_link_target1":"self","icon_font_size1":"4em","title_text1":"CUSTOMIZE","title_color1":"444444","description_layout1":"0","description_text1":"the template easy & fast <br\\/>\\r\\nusing over 200 parameters","description_text_color1":"444444","show_icon2":"1","icon_layout2":"center","icon_name2":"fa-gift","icon_color2":"FFFFFF","icon_bg_color2":"0099FF","icon_border_color2":"0099FF","icon_link2":"index.php","icon_link_target2":"self","icon_font_size2":"4.6em","title_text2":"AWESOMENESS","title_color2":"444444","description_layout2":"0","description_text2":"with Google Fonts and <br\\/>\\r\\nFont Awesome Icons ","description_text_color2":"444444","show_icon3":"1","icon_layout3":"center","icon_name3":"fa-laptop","icon_color3":"FFFFFF","icon_bg_color3":"0099FF","icon_border_color3":"0099FF","icon_link3":"index.php","icon_link_target3":"self","icon_font_size3":"4.4em","title_text3":"RESPONSIVE","title_color3":"444444","description_layout3":"0","description_text3":"made with Bootstrap, <br\\/>\\r\\nHTML5 and CSS3 ","description_text_color3":"444444","show_icon4":"1","icon_layout4":"center","icon_name4":"fa-cogs","icon_color4":"FFFFFF","icon_bg_color4":"0099FF","icon_border_color4":"0099FF","icon_link4":"index.php\\/k2\\/k2-content","icon_link_target4":"self","icon_font_size4":"4em","title_text4":"K2 SUPPORT ","title_color4":"444444","description_layout4":"0","description_text4":"powerful content extension for <br\\/>\\r\\nJoomla! integration ","description_text_color4":"444444","show_icon5":"0","icon_layout5":"center","icon_name5":"fa-tablet","icon_color5":"673AB7","icon_bg_color5":"FFFFFF","icon_border_color5":"CCCCCC","icon_link5":"http:\\/\\/www.favthemes.com","icon_link_target5":"blank","icon_font_size5":"6em","title_text5":"Title Text","title_color5":"444444","description_layout5":"0","description_text5":"Lorem ipsum dolor sit amet, dolore magna aliqua.","description_text_color5":"444444","show_icon6":"0","icon_layout6":"center","icon_name6":"fa-puzzle-piece","icon_color6":"00BCD4","icon_bg_color6":"FFFFFF","icon_border_color6":"CCCCCC","icon_link6":"http:\\/\\/www.favthemes.com","icon_link_target6":"blank","icon_font_size6":"6em","title_text6":"Title Text","title_color6":"444444","description_layout6":"0","description_text6":"Lorem ipsum dolor sit amet, dolore magna aliqua.","description_text_color6":"444444","moduleclass_sfx":"-sfx4 favstyle","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(489, 510, 'FavPromote', '', '', 1, 'prime1', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_favpromote', 1, 1, '{"jquery_load":"1","layout_effect":"layout-effect1","title_google_font":"Roboto","title_padding":"10px 20px","title_font_size":"21px","title_line_height":"1.4em","title_text_align":"left","title_icon_font_size":"18px","title_icon_vertical_align":"baseline","description_text_font_size":"14px","description_text_line_height":"1.5em","description_text_align":"justify","show_column1":"1","column_border_color1":"FFFFFF","column_border_radius1":"4px","upload_image1":"images\\/demo\\/img\\/favpromote-img-1.jpg","show_image_link1":"1","image_link1":"index.php\\/documentation","image_target1":"self","image_alt1":"","title_text1":" Logo Options ","title_color1":"FFFFFF","title_bg_color1":"444444","show_title_link1":"1","title_link1":"index.php\\/documentation","title_target1":"self","title_icon1":"fa-bookmark-o","description_text1":"Upload your own logo image or use a text logo that supports Google Fonts. Need a slogan under your logo? No problem! ","description_text_color1":"444444","show_column2":"1","column_border_color2":"FFFFFF","column_border_radius2":"4px","upload_image2":"images\\/demo\\/img\\/favpromote-img-2.jpg","show_image_link2":"1","image_link2":"index.php\\/typography\\/icon-styles","image_target2":"self","image_alt2":"","title_text2":"Font Awesome ","title_color2":"FFFFFF","title_bg_color2":"444444","show_title_link2":"1","title_link2":"index.php\\/typography\\/icon-styles","title_target2":"self","title_icon2":"fa-flag-o","description_text2":"Use this amazing icon font with many elements of the template such as module titles, menus, buttons or lists. ","description_text_color2":"444444","show_column3":"1","column_border_color3":"FFFFFF","column_border_radius3":"4px","upload_image3":"images\\/demo\\/img\\/favpromote-img-3.jpg","show_image_link3":"1","image_link3":"index.php\\/features\\/navigation-styles\\/main-navigation-styles","image_target3":"self","image_alt3":"","title_text3":"Menu Styles ","title_color3":"FFFFFF","title_bg_color3":"444444","show_title_link3":"1","title_link3":"index.php\\/features\\/navigation-styles\\/main-navigation-styles","title_target3":"self","title_icon3":"fa-ellipsis-h","description_text3":"Customize the way you navigate the content and style the main navigation, the horizontal and vertical menus. ","description_text_color3":"444444","show_column4":"1","column_border_color4":"FFFFFF","column_border_radius4":"4px","upload_image4":"images\\/demo\\/img\\/favpromote-img-4.jpg","show_image_link4":"1","image_link4":"index.php\\/extensions","image_target4":"self","image_alt4":"","title_text4":"Extensions ","title_color4":"FFFFFF","title_bg_color4":"444444","show_title_link4":"1","title_link4":"index.php\\/extensions","title_target4":"self","title_icon4":"fa-puzzle-piece","description_text4":"We customize the extensions for each template with unique styles which can be enabled by adding the \\"favstyle\\" Module Class Suffix. ","description_text_color4":"444444","show_column5":"0","column_border_color5":"DDDDDD","column_border_radius5":"4px","upload_image5":"","show_image_link5":"1","image_link5":"http:\\/\\/www.favthemes.com","image_target5":"blank","image_alt5":"","title_text5":"Title Text","title_color5":"FFFFFF","title_bg_color5":"D63E52","show_title_link5":"1","title_link5":"http:\\/\\/www.favthemes.com","title_target5":"blank","title_icon5":"fa-quote-right","description_text5":"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor.","description_text_color5":"444444","show_column6":"0","column_border_color6":"DDDDDD","column_border_radius6":"4px","upload_image6":"","show_image_link6":"1","image_link6":"http:\\/\\/www.favthemes.com","image_target6":"blank","image_alt6":"","title_text6":"Title Text","title_color6":"FFFFFF","title_bg_color6":"D63E52","show_title_link6":"1","title_link6":"http:\\/\\/www.favthemes.com","title_target6":"blank","title_icon6":"fa-recycle","description_text6":"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor.","description_text_color6":"444444","moduleclass_sfx":"-sfx16 favstyle","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(490, 511, 'FavEffects', '', '', 1, 'top1', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_faveffects', 1, 1, '{"jquery_load":"1","layout_effect":"layout-effect1","icon_width":"96%","icon_border_radius":"4px","icon_border_type":"solid","icon_border_width":"1px","title_google_font":"Roboto","title_font_size":"21px","show_icon1":"1","icon_effect1":"effect4","icon_name1":"fa-laptop","icon_color1":"0099FF","icon_bg_color1":"FFFFFF","show_icon_link1":"1","icon_link1":"http:\\/\\/getbootstrap.com\\/2.3.2\\/","icon_link_target1":"blank","icon_font_size1":"4em","icon_border_color1":"D7D7D7","title_text1":"Bootstrap ","title_color1":"444444","show_icon2":"1","icon_effect2":"effect4","icon_name2":"fa-font","icon_color2":"0099FF","icon_bg_color2":"FFFFFF","show_icon_link2":"1","icon_link2":"https:\\/\\/www.google.com\\/fonts\\/","icon_link_target2":"blank","icon_font_size2":"4em","icon_border_color2":"D7D7D7","title_text2":"Google Fonts ","title_color2":"444444","show_icon3":"1","icon_effect3":"effect4","icon_name3":"fa-flask","icon_color3":"0099FF","icon_bg_color3":"FFFFFF","show_icon_link3":"1","icon_link3":"index.php\\/features\\/404","icon_link_target3":"self","icon_font_size3":"4em","icon_border_color3":"D7D7D7","title_text3":"404 page","title_color3":"444444","show_icon4":"1","icon_effect4":"effect4","icon_name4":"fa-paint-brush","icon_color4":"0099FF","icon_bg_color4":"FFFFFF","show_icon_link4":"1","icon_link4":"index.php","icon_link_target4":"self","icon_font_size4":"4em","icon_border_color4":"D7D7D7","title_text4":"10 color styles ","title_color4":"444444","show_icon5":"1","icon_effect5":"effect4","icon_name5":"fa-paper-plane-o","icon_color5":"0099FF","icon_bg_color5":"FFFFFF","show_icon_link5":"1","icon_link5":"index.php\\/features\\/offline-page","icon_link_target5":"self","icon_font_size5":"4em","icon_border_color5":"D7D7D7","title_text5":"Offline Page ","title_color5":"444444","show_icon6":"1","icon_effect6":"effect4","icon_name6":"fa-clone","icon_color6":"0099FF","icon_bg_color6":"FFFFFF","show_icon_link6":"1","icon_link6":"index.php\\/features\\/module-variations","icon_link_target6":"self","icon_font_size6":"4em","icon_border_color6":"D7D7D7","title_text6":"Module Icons ","title_color6":"444444","moduleclass_sfx":"-sfx4 favstyle","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*');

--
-- Dumping data for table `#__modules_menu`
--

INSERT INTO `#__modules_menu` (`moduleid`, `menuid`) VALUES
(0, 0),
(1, 0),
(2, 0),
(3, 0),
(4, 0),
(6, 0),
(7, 0),
(8, 0),
(9, 0),
(10, 0),
(12, 0),
(13, 0),
(14, 0),
(15, 0),
(16, 149),
(17, -217),
(17, -152),
(17, -150),
(17, -114),
(17, -113),
(17, -112),
(17, -107),
(17, -106),
(17, -104),
(17, -103),
(17, -102),
(17, -101),
(79, 0),
(86, 0),
(88, 0),
(89, 0),
(90, 149),
(93, 149),
(94, 182),
(95, 182),
(98, 182),
(99, 0),
(100, 0),
(102, 121),
(102, 122),
(102, 123),
(102, 124),
(102, 125),
(102, 126),
(102, 127),
(102, 128),
(102, 129),
(102, 130),
(102, 131),
(102, 132),
(102, 133),
(102, 134),
(102, 135),
(102, 136),
(102, 137),
(102, 138),
(102, 139),
(102, 140),
(102, 141),
(102, 142),
(102, 143),
(102, 144),
(102, 145),
(102, 146),
(102, 147),
(102, 148),
(103, 175),
(103, 176),
(103, 177),
(103, 178),
(103, 179),
(103, 180),
(103, 181),
(104, 113),
(105, 149),
(106, 149),
(107, 149),
(108, 149),
(109, 149),
(110, 149),
(111, 113),
(112, 113),
(113, 113),
(114, 113),
(115, 113),
(116, 113),
(117, 113),
(118, 113),
(119, 113),
(120, 113),
(121, 113),
(122, 113),
(123, 113),
(124, 113),
(125, 113),
(126, 113),
(127, 113),
(128, 113),
(129, 113),
(130, 113),
(131, 113),
(132, 113),
(133, 113),
(134, 113),
(135, 113),
(136, 113),
(137, 113),
(138, 113),
(139, 113),
(140, 113),
(141, 113),
(142, 113),
(143, 113),
(144, 113),
(145, 113),
(146, 113),
(147, 113),
(148, 113),
(149, 113),
(150, 113),
(151, 113),
(152, 113),
(153, 113),
(154, 113),
(155, 113),
(156, 113),
(157, 113),
(158, 113),
(159, 113),
(160, 113),
(161, 113),
(162, 113),
(163, 113),
(164, 113),
(165, 113),
(166, 113),
(167, 113),
(168, 113),
(169, 113),
(170, 113),
(171, 113),
(172, 113),
(173, 113),
(174, 113),
(175, 113),
(176, 113),
(177, 113),
(178, 113),
(179, 113),
(180, 113),
(181, 113),
(182, 113),
(183, 113),
(184, 113),
(185, 113),
(186, 113),
(187, 113),
(188, 113),
(189, 113),
(190, 113),
(191, 113),
(192, 113),
(193, 113),
(194, 113),
(195, 113),
(196, 113),
(197, 113),
(198, 113),
(199, 113),
(200, 113),
(201, 113),
(202, 113),
(203, 113),
(204, 113),
(205, 113),
(206, 113),
(207, 113),
(208, 113),
(209, 113),
(210, 113),
(211, 113),
(212, 113),
(213, 113),
(214, 113),
(215, 113),
(216, 113),
(217, 113),
(218, 113),
(219, 113),
(220, 113),
(221, 113),
(222, 113),
(223, 113),
(224, 113),
(225, 113),
(226, 113),
(227, 114),
(228, 114),
(229, 114),
(230, 114),
(231, 114),
(232, 114),
(233, 114),
(234, 114),
(235, 114),
(236, 114),
(237, 114),
(238, 114),
(239, 114),
(240, 114),
(241, 114),
(242, 114),
(243, 114),
(244, 114),
(245, 114),
(246, 114),
(247, 114),
(248, 114),
(249, 114),
(250, 114),
(251, 114),
(252, 114),
(253, 114),
(254, 114),
(255, 114),
(256, 114),
(257, 114),
(258, 114),
(259, 114),
(260, 114),
(261, 114),
(262, 114),
(263, 114),
(264, 114),
(265, 114),
(266, 114),
(267, 114),
(268, 114),
(269, 114),
(270, 114),
(271, 114),
(272, 114),
(273, 114),
(274, 114),
(275, 114),
(276, 114),
(277, 188),
(278, 188),
(279, 188),
(280, 188),
(281, 188),
(282, 188),
(283, 188),
(284, 188),
(285, 188),
(286, 188),
(287, 188),
(288, 188),
(289, 188),
(290, 188),
(291, 188),
(292, 188),
(293, 117),
(295, 117),
(296, 118),
(297, 118),
(298, 118),
(299, 118),
(300, 118),
(301, 118),
(302, 118),
(303, 118),
(304, 118),
(305, 118),
(306, 118),
(307, 118),
(308, 118),
(309, 118),
(311, 119),
(312, 119),
(313, 119),
(314, 119),
(315, 119),
(317, 120),
(318, 120),
(319, 120),
(321, 186),
(322, 120),
(323, 120),
(324, 120),
(325, 120),
(326, 186),
(327, 120),
(328, 120),
(329, 120),
(330, 120),
(331, 186),
(332, 186),
(333, 186),
(373, 151),
(374, 115),
(374, 187),
(374, 188),
(374, 189),
(397, 186),
(401, 150),
(402, 152),
(407, 187),
(408, 188),
(410, 189),
(412, 188),
(415, 189),
(417, 189),
(418, 189),
(419, 189),
(420, 189),
(421, 150),
(422, 151),
(423, 187),
(424, 187),
(425, 152),
(426, 152),
(427, 152),
(428, 152),
(429, 152),
(430, 152),
(431, 152),
(432, 152),
(471, 217),
(472, 217),
(474, 101),
(474, 125),
(474, 126),
(474, 127),
(474, 132),
(474, 133),
(474, 134),
(474, 179),
(475, 101),
(477, 101),
(478, 101),
(479, -113),
(482, 101),
(483, 152),
(484, 101),
(485, 217),
(486, 117),
(486, 118),
(486, 119),
(486, 120),
(486, 186),
(487, 151),
(488, 151),
(489, 151),
(490, 151);

--
-- Dumping data for table `#__assets`
--

INSERT INTO `#__assets` (`id`, `parent_id`, `lft`, `rgt`, `level`, `name`, `title`, `rules`) VALUES
(1, 0, 0, 717, 0, 'root.1', 'Root Asset', '{"core.login.site":{"6":1,"2":1},"core.login.admin":{"6":1},"core.login.offline":{"6":1},"core.admin":{"8":1},"core.manage":{"7":1},"core.create":{"6":1,"3":1},"core.delete":{"6":1},"core.edit":{"6":1,"4":1},"core.edit.state":{"6":1,"5":1},"core.edit.own":{"6":1,"3":1}}'),
(2, 1, 1, 2, 1, 'com_admin', 'com_admin', '{}'),
(3, 1, 3, 6, 1, 'com_banners', 'com_banners', '{"core.admin":{"7":1},"core.manage":{"6":1},"core.create":[],"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(4, 1, 7, 8, 1, 'com_cache', 'com_cache', '{"core.admin":{"7":1},"core.manage":{"7":1}}'),
(5, 1, 9, 10, 1, 'com_checkin', 'com_checkin', '{"core.admin":{"7":1},"core.manage":{"7":1}}'),
(6, 1, 11, 12, 1, 'com_config', 'com_config', '{}'),
(7, 1, 13, 16, 1, 'com_contact', 'com_contact', '{"core.admin":{"7":1},"core.manage":{"6":1},"core.create":[],"core.delete":[],"core.edit":[],"core.edit.state":[],"core.edit.own":[]}'),
(8, 1, 17, 72, 1, 'com_content', 'com_content', '{"core.admin":{"7":1},"core.options":[],"core.manage":{"6":1},"core.create":{"3":1},"core.delete":[],"core.edit":{"4":1},"core.edit.state":{"5":1},"core.edit.own":[]}'),
(9, 1, 73, 74, 1, 'com_cpanel', 'com_cpanel', '{}'),
(10, 1, 75, 76, 1, 'com_installer', 'com_installer', '{"core.admin":[],"core.manage":{"7":0},"core.delete":{"7":0},"core.edit.state":{"7":0}}'),
(11, 1, 77, 78, 1, 'com_languages', 'com_languages', '{"core.admin":{"7":1},"core.manage":[],"core.create":[],"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(12, 1, 79, 80, 1, 'com_login', 'com_login', '{}'),
(13, 1, 81, 82, 1, 'com_mailto', 'com_mailto', '{}'),
(14, 1, 83, 84, 1, 'com_massmail', 'com_massmail', '{}'),
(15, 1, 85, 86, 1, 'com_media', 'com_media', '{"core.admin":{"7":1},"core.manage":{"6":1},"core.create":{"3":1},"core.delete":{"5":1}}'),
(16, 1, 87, 88, 1, 'com_menus', 'com_menus', '{"core.admin":{"7":1},"core.manage":[],"core.create":[],"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(17, 1, 89, 90, 1, 'com_messages', 'com_messages', '{"core.admin":{"7":1},"core.manage":{"7":1}}'),
(18, 1, 91, 680, 1, 'com_modules', 'com_modules', '{"core.admin":{"7":1},"core.manage":[],"core.create":[],"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(19, 1, 681, 684, 1, 'com_newsfeeds', 'com_newsfeeds', '{"core.admin":{"7":1},"core.manage":{"6":1},"core.create":[],"core.delete":[],"core.edit":[],"core.edit.state":[],"core.edit.own":[]}'),
(20, 1, 685, 686, 1, 'com_plugins', 'com_plugins', '{"core.admin":{"7":1},"core.manage":[],"core.edit":[],"core.edit.state":[]}'),
(21, 1, 687, 688, 1, 'com_redirect', 'com_redirect', '{"core.admin":{"7":1},"core.manage":[]}'),
(22, 1, 689, 690, 1, 'com_search', 'com_search', '{"core.admin":{"7":1},"core.manage":{"6":1}}'),
(23, 1, 691, 692, 1, 'com_templates', 'com_templates', '{"core.admin":{"7":1},"core.manage":[],"core.create":[],"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(24, 1, 693, 696, 1, 'com_users', 'com_users', '{"core.admin":{"7":1},"core.options":[],"core.manage":[],"core.create":[],"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(26, 1, 697, 698, 1, 'com_wrapper', 'com_wrapper', '{}'),
(27, 8, 18, 19, 2, 'com_content.category.2', 'Uncategorised', '{"core.create":[],"core.delete":[],"core.edit":[],"core.edit.state":[],"core.edit.own":[]}'),
(28, 3, 4, 5, 2, 'com_banners.category.3', 'Uncategorised', '{"core.create":[],"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(29, 7, 14, 15, 2, 'com_contact.category.4', 'Uncategorised', '{"core.create":[],"core.delete":[],"core.edit":[],"core.edit.state":[],"core.edit.own":[]}'),
(30, 19, 682, 683, 2, 'com_newsfeeds.category.5', 'Uncategorised', '{"core.create":[],"core.delete":[],"core.edit":[],"core.edit.state":[],"core.edit.own":[]}'),
(32, 24, 694, 695, 1, 'com_users.category.7', 'Uncategorised', '{"core.create":[],"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(33, 1, 699, 700, 1, 'com_finder', 'com_finder', '{"core.admin":{"7":1},"core.manage":{"6":1},"core.create":[],"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(34, 1, 701, 702, 1, 'com_joomlaupdate', 'com_joomlaupdate', '{"core.admin":[],"core.manage":[],"core.delete":[],"core.edit.state":[]}'),
(35, 1, 703, 704, 1, 'com_tags', 'com_tags', '{"core.admin":[],"core.manage":[],"core.manage":[],"core.delete":[],"core.edit.state":[]}'),
(36, 1, 705, 706, 1, 'com_contenthistory', 'com_contenthistory', '{}'),
(37, 1, 707, 708, 1, 'com_ajax', 'com_ajax', '{}'),
(38, 1, 709, 710, 1, 'com_postinstall', 'com_postinstall', '{}'),
(39, 18, 92, 93, 2, 'com_modules.module.1', 'Main Menu', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(40, 18, 94, 95, 2, 'com_modules.module.2', 'Login', '{"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(41, 18, 96, 97, 2, 'com_modules.module.3', 'Popular Articles', '{"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(42, 18, 98, 99, 2, 'com_modules.module.4', 'Recently Added Articles', '{"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(43, 18, 100, 101, 2, 'com_modules.module.8', 'Toolbar', '{"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(44, 18, 102, 103, 2, 'com_modules.module.9', 'Quick Icons', '{"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(45, 18, 104, 105, 2, 'com_modules.module.10', 'Logged-in Users', '{"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(46, 18, 106, 107, 2, 'com_modules.module.12', 'Admin Menu', '{"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(47, 18, 108, 109, 2, 'com_modules.module.13', 'Admin Submenu', '{"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(48, 18, 110, 111, 2, 'com_modules.module.14', 'User Status', '{"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(49, 18, 112, 113, 2, 'com_modules.module.15', 'Title', '{"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(50, 18, 114, 115, 2, 'com_modules.module.16', 'Login Form', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(51, 18, 116, 117, 2, 'com_modules.module.17', 'Breadcrumbs', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(52, 18, 118, 119, 2, 'com_modules.module.79', 'Multilanguage status', '{"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(53, 18, 120, 121, 2, 'com_modules.module.86', 'Joomla Version', '{"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(55, 18, 122, 123, 2, 'com_modules.module.88', 'Site Information', '{"core.delete":{"6":1},"core.edit":{"6":1,"4":1},"core.edit.state":{"6":1,"5":1}}'),
(56, 18, 124, 125, 2, 'com_modules.module.89', 'Release News', '{"core.delete":{"6":1},"core.edit":{"6":1,"4":1},"core.edit.state":{"6":1,"5":1}}'),
(57, 18, 126, 127, 2, 'com_modules.module.90', 'Latest Articles', '{"core.delete":{"6":1},"core.edit":{"6":1,"4":1},"core.edit.state":{"6":1,"5":1},"module.edit.frontend":[]}'),
(60, 18, 128, 129, 2, 'com_modules.module.93', 'Search', '{"core.delete":{"6":1},"core.edit":{"6":1,"4":1},"core.edit.state":{"6":1,"5":1},"module.edit.frontend":[]}'),
(63, 8, 20, 23, 2, 'com_content.category.8', 'Home', '{"core.create":{"6":1,"3":1},"core.delete":{"6":1},"core.edit":{"6":1,"4":1},"core.edit.state":{"6":1,"5":1},"core.edit.own":{"6":1,"3":1}}'),
(64, 8, 24, 25, 2, 'com_content.category.9', 'Store', '{"core.create":{"6":1,"3":1},"core.delete":{"6":1},"core.edit":{"6":1,"4":1},"core.edit.state":{"6":1,"5":1},"core.edit.own":{"6":1,"3":1}}'),
(65, 8, 26, 43, 2, 'com_content.category.10', 'Features', '{"core.create":{"6":1,"3":1},"core.delete":{"6":1},"core.edit":{"6":1,"4":1},"core.edit.state":{"6":1,"5":1},"core.edit.own":{"6":1,"3":1}}'),
(66, 8, 44, 63, 2, 'com_content.category.11', 'Joomla', '{"core.create":{"6":1,"3":1},"core.delete":{"6":1},"core.edit":{"6":1,"4":1},"core.edit.state":{"6":1,"5":1},"core.edit.own":{"6":1,"3":1}}'),
(67, 8, 64, 67, 2, 'com_content.category.12', 'K2 ', '{"core.create":{"6":1,"3":1},"core.delete":{"6":1},"core.edit":{"6":1,"4":1},"core.edit.state":{"6":1,"5":1},"core.edit.own":{"6":1,"3":1}}'),
(68, 8, 68, 69, 2, 'com_content.category.13', 'Extensions', '{"core.create":{"6":1,"3":1},"core.delete":{"6":1},"core.edit":{"6":1,"4":1},"core.edit.state":{"6":1,"5":1},"core.edit.own":{"6":1,"3":1}}'),
(69, 8, 70, 71, 2, 'com_content.category.14', 'Docs', '{"core.create":{"6":1,"3":1},"core.delete":{"6":1},"core.edit":{"6":1,"4":1},"core.edit.state":{"6":1,"5":1},"core.edit.own":{"6":1,"3":1}}'),
(70, 65, 27, 30, 3, 'com_content.category.15', 'Module Positions', '{"core.create":{"6":1,"3":1},"core.delete":{"6":1},"core.edit":{"6":1,"4":1},"core.edit.state":{"6":1,"5":1},"core.edit.own":{"6":1,"3":1}}'),
(71, 65, 31, 32, 3, 'com_content.category.16', 'Module Variations', '{"core.create":{"6":1,"3":1},"core.delete":{"6":1},"core.edit":{"6":1,"4":1},"core.edit.state":{"6":1,"5":1},"core.edit.own":{"6":1,"3":1}}'),
(72, 65, 33, 34, 3, 'com_content.category.17', 'Navigation Styles', '{"core.create":{"6":1,"3":1},"core.delete":{"6":1},"core.edit":{"6":1,"4":1},"core.edit.state":{"6":1,"5":1},"core.edit.own":{"6":1,"3":1}}'),
(73, 65, 35, 36, 3, 'com_content.category.18', 'Typography', '{"core.create":{"6":1,"3":1},"core.delete":{"6":1},"core.edit":{"6":1,"4":1},"core.edit.state":{"6":1,"5":1},"core.edit.own":{"6":1,"3":1}}'),
(74, 66, 45, 56, 3, 'com_content.category.19', 'Joomla Content', '{"core.create":{"6":1,"3":1},"core.delete":{"6":1},"core.edit":{"6":1,"4":1},"core.edit.state":{"6":1,"5":1},"core.edit.own":{"6":1,"3":1}}'),
(75, 66, 57, 58, 3, 'com_content.category.20', 'Joomla Modules', '{"core.create":{"6":1,"3":1},"core.delete":{"6":1},"core.edit":{"6":1,"4":1},"core.edit.state":{"6":1,"5":1},"core.edit.own":{"6":1,"3":1}}'),
(76, 65, 39, 42, 3, 'com_content.category.21', '404 Page', '{"core.create":{"6":1,"3":1},"core.delete":{"6":1},"core.edit":{"6":1,"4":1},"core.edit.state":{"6":1,"5":1},"core.edit.own":{"6":1,"3":1}}'),
(77, 66, 59, 62, 3, 'com_content.category.22', 'Newsflash', '{"core.create":{"6":1,"3":1},"core.delete":{"6":1},"core.edit":{"6":1,"4":1},"core.edit.state":{"6":1,"5":1},"core.edit.own":{"6":1,"3":1}}'),
(78, 67, 65, 66, 3, 'com_content.category.23', 'K2 Modules', '{"core.create":{"6":1,"3":1},"core.delete":{"6":1},"core.edit":{"6":1,"4":1},"core.edit.state":{"6":1,"5":1},"core.edit.own":{"6":1,"3":1}}'),
(79, 74, 54, 55, 4, 'com_content.article.2', 'Say hello to Favourite!  ', '{"core.delete":[],"core.edit":{"4":1},"core.edit.state":{"5":1}}'),
(80, 76, 40, 41, 4, 'com_content.article.3', '404', '{"core.delete":[],"core.edit":{"4":1},"core.edit.state":{"5":1}}'),
(81, 1, 711, 712, 1, 'com_k2', 'COM_K2', '{"core.admin":[],"core.manage":[],"core.create":[],"core.delete":[],"core.edit":[],"core.edit.state":[],"core.edit.own":[]}'),
(82, 18, 130, 131, 2, 'com_modules.module.94', 'K2 Comments', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(83, 18, 132, 133, 2, 'com_modules.module.95', 'K2 Content Module', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(86, 18, 134, 135, 2, 'com_modules.module.98', 'K2 User', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(87, 18, 136, 137, 2, 'com_modules.module.99', 'K2 Quick Icons (admin)', ''),
(88, 18, 138, 139, 2, 'com_modules.module.100', 'K2 Stats (admin)', ''),
(89, 74, 46, 47, 4, 'com_content.article.4', 'Customize everything', '{"core.delete":[],"core.edit":{"4":1},"core.edit.state":{"5":1}}'),
(90, 74, 48, 49, 4, 'com_content.article.5', 'Responsive theme', '{"core.delete":[],"core.edit":{"4":1},"core.edit.state":{"5":1}}'),
(91, 74, 50, 51, 4, 'com_content.article.6', 'Ready for mobile devices', '{"core.delete":[],"core.edit":{"4":1},"core.edit.state":{"5":1}}'),
(92, 77, 60, 61, 4, 'com_content.article.7', 'A brand new store', '{"core.delete":[],"core.edit":{"4":1},"core.edit.state":{"5":1}}'),
(93, 70, 28, 29, 4, 'com_content.article.8', 'Article Title', '{"core.delete":[],"core.edit":{"4":1},"core.edit.state":{"5":1}}'),
(94, 74, 52, 53, 4, 'com_content.article.9', 'Archived Article', '{"core.delete":[],"core.edit":{"4":1},"core.edit.state":{"5":1}}'),
(96, 18, 140, 141, 2, 'com_modules.module.102', 'J! Content Menu', '{"core.delete":{"6":1},"core.edit":{"6":1,"4":1},"core.edit.state":{"6":1,"5":1},"module.edit.frontend":[]}'),
(97, 18, 142, 143, 2, 'com_modules.module.103', 'K2 Content Menu', '{"core.delete":{"6":1},"core.edit":{"6":1,"4":1},"core.edit.state":{"6":1,"5":1},"module.edit.frontend":[]}'),
(98, 1, 713, 714, 1, '#__ucm_content.2', '#__ucm_content.2', '[]'),
(99, 1, 715, 716, 1, '#__ucm_content.3', '#__ucm_content.3', '[]'),
(100, 18, 144, 145, 2, 'com_modules.module.104', 'Advert', '{"core.delete":{"6":1},"core.edit":{"6":1,"4":1},"core.edit.state":{"6":1,"5":1},"module.edit.frontend":[]}'),
(101, 18, 146, 147, 2, 'com_modules.module.105', 'Wrapper', '{"core.delete":{"6":1},"core.edit":{"6":1,"4":1},"core.edit.state":{"6":1,"5":1},"module.edit.frontend":[]}'),
(102, 18, 148, 149, 2, 'com_modules.module.106', 'Articles Newsflash ', '{"core.delete":{"6":1},"core.edit":{"6":1,"4":1},"core.edit.state":{"6":1,"5":1},"module.edit.frontend":[]}'),
(103, 18, 150, 151, 2, 'com_modules.module.107', 'Articles Most Read ', '{"core.delete":{"6":1},"core.edit":{"6":1,"4":1},"core.edit.state":{"6":1,"5":1},"module.edit.frontend":[]}'),
(104, 18, 152, 153, 2, 'com_modules.module.108', 'Articles Category ', '{"core.delete":{"6":1},"core.edit":{"6":1,"4":1},"core.edit.state":{"6":1,"5":1},"module.edit.frontend":[]}'),
(105, 18, 154, 155, 2, 'com_modules.module.109', 'Articles Categories ', '{"core.delete":{"6":1},"core.edit":{"6":1,"4":1},"core.edit.state":{"6":1,"5":1},"module.edit.frontend":[]}'),
(106, 18, 156, 157, 2, 'com_modules.module.110', 'Footer', '{"core.delete":{"6":1},"core.edit":{"6":1,"4":1},"core.edit.state":{"6":1,"5":1},"module.edit.frontend":[]}'),
(107, 18, 158, 159, 2, 'com_modules.module.111', 'Topbar1', '{"core.delete":{"6":1},"core.edit":{"6":1,"4":1},"core.edit.state":{"6":1,"5":1},"module.edit.frontend":[]}'),
(108, 18, 160, 161, 2, 'com_modules.module.112', 'Topbar2', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(109, 18, 162, 163, 2, 'com_modules.module.113', 'Topbar3', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(110, 18, 164, 165, 2, 'com_modules.module.114', 'Topbar4', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(111, 18, 166, 167, 2, 'com_modules.module.115', 'Topbar5', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(112, 18, 168, 169, 2, 'com_modules.module.116', 'Topbar6', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(113, 18, 170, 171, 2, 'com_modules.module.117', 'Nav', '{"core.delete":{"6":1},"core.edit":{"6":1,"4":1},"core.edit.state":{"6":1,"5":1},"module.edit.frontend":[]}'),
(114, 18, 172, 173, 2, 'com_modules.module.118', 'Slide', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(115, 18, 174, 175, 2, 'com_modules.module.119', 'Intro1 ', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(116, 18, 176, 177, 2, 'com_modules.module.120', 'Intro2 ', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(117, 18, 178, 179, 2, 'com_modules.module.121', 'Intro3', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(118, 18, 180, 181, 2, 'com_modules.module.122', 'Intro4 ', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(119, 18, 182, 183, 2, 'com_modules.module.123', 'Intro5', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(120, 18, 184, 185, 2, 'com_modules.module.124', 'Intro6', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(121, 18, 186, 187, 2, 'com_modules.module.125', 'Lead1', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(122, 18, 188, 189, 2, 'com_modules.module.126', 'Lead2 ', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(123, 18, 190, 191, 2, 'com_modules.module.127', 'Lead3', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(124, 18, 192, 193, 2, 'com_modules.module.128', 'Lead4', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(125, 18, 194, 195, 2, 'com_modules.module.129', 'Lead5', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(126, 18, 196, 197, 2, 'com_modules.module.130', 'Lead6', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(127, 18, 198, 199, 2, 'com_modules.module.131', 'Promo1', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(128, 18, 200, 201, 2, 'com_modules.module.132', 'Promo2', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(129, 18, 202, 203, 2, 'com_modules.module.133', 'Promo3', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(130, 18, 204, 205, 2, 'com_modules.module.134', 'Promo4', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(131, 18, 206, 207, 2, 'com_modules.module.135', 'Promo5', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(132, 18, 208, 209, 2, 'com_modules.module.136', 'Promo6', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(133, 18, 210, 211, 2, 'com_modules.module.137', 'Prime1', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(134, 18, 212, 213, 2, 'com_modules.module.138', 'Prime2', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(135, 18, 214, 215, 2, 'com_modules.module.139', 'Prime3 ', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(136, 18, 216, 217, 2, 'com_modules.module.140', 'Prime4', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(137, 18, 218, 219, 2, 'com_modules.module.141', 'Prime5', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(138, 18, 220, 221, 2, 'com_modules.module.142', 'Prime6', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(139, 18, 222, 223, 2, 'com_modules.module.143', 'Showcase1', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(140, 18, 224, 225, 2, 'com_modules.module.144', 'Showcase2', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(141, 18, 226, 227, 2, 'com_modules.module.145', 'Showcase3', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(142, 18, 228, 229, 2, 'com_modules.module.146', 'Showcase4', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(143, 18, 230, 231, 2, 'com_modules.module.147', 'Showcase5', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(144, 18, 232, 233, 2, 'com_modules.module.148', 'Showcase6', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(145, 18, 234, 235, 2, 'com_modules.module.149', 'Feature1', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(146, 18, 236, 237, 2, 'com_modules.module.150', 'Feature2', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(147, 18, 238, 239, 2, 'com_modules.module.151', 'Feature3', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(148, 18, 240, 241, 2, 'com_modules.module.152', 'Feature4', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(149, 18, 242, 243, 2, 'com_modules.module.153', 'Feature5', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(150, 18, 244, 245, 2, 'com_modules.module.154', 'Feature6', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(151, 18, 246, 247, 2, 'com_modules.module.155', 'Focus1', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(152, 18, 248, 249, 2, 'com_modules.module.156', 'Focus2', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(153, 18, 250, 251, 2, 'com_modules.module.157', 'Focus3', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(154, 18, 252, 253, 2, 'com_modules.module.158', 'Focus4', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(155, 18, 254, 255, 2, 'com_modules.module.159', 'Focus5', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(156, 18, 256, 257, 2, 'com_modules.module.160', 'Focus6', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(157, 18, 258, 259, 2, 'com_modules.module.161', 'Portfolio1', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(158, 18, 260, 261, 2, 'com_modules.module.162', 'Portfolio2', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(159, 18, 262, 263, 2, 'com_modules.module.163', 'Portfolio3', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(160, 18, 264, 265, 2, 'com_modules.module.164', 'Portfolio4', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(161, 18, 266, 267, 2, 'com_modules.module.165', 'Portfolio5', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(162, 18, 268, 269, 2, 'com_modules.module.166', 'Portfolio6', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(163, 18, 270, 271, 2, 'com_modules.module.167', 'Screen1', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(164, 18, 272, 273, 2, 'com_modules.module.168', 'Screen2', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(165, 18, 274, 275, 2, 'com_modules.module.169', 'Screen3', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(166, 18, 276, 277, 2, 'com_modules.module.170', 'Screen4', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(167, 18, 278, 279, 2, 'com_modules.module.171', 'Screen5', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(168, 18, 280, 281, 2, 'com_modules.module.172', 'Screen6', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(169, 18, 282, 283, 2, 'com_modules.module.173', 'Top1', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(170, 18, 284, 285, 2, 'com_modules.module.174', 'Top2', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(171, 18, 286, 287, 2, 'com_modules.module.175', 'Top3', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(172, 18, 288, 289, 2, 'com_modules.module.176', 'Top4', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(173, 18, 290, 291, 2, 'com_modules.module.177', 'Top5', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(174, 18, 292, 293, 2, 'com_modules.module.178', 'Top6', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(175, 18, 294, 295, 2, 'com_modules.module.179', 'Bottom1', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(176, 18, 296, 297, 2, 'com_modules.module.180', 'Bottom2', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(177, 18, 298, 299, 2, 'com_modules.module.181', 'Bottom3', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(178, 18, 300, 301, 2, 'com_modules.module.182', 'Bottom4', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(179, 18, 302, 303, 2, 'com_modules.module.183', 'Bottom5', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(180, 18, 304, 305, 2, 'com_modules.module.184', 'Bottom6', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(181, 18, 306, 307, 2, 'com_modules.module.185', 'Note1', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(182, 18, 308, 309, 2, 'com_modules.module.186', 'Note2', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(183, 18, 310, 311, 2, 'com_modules.module.187', 'Note3', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(184, 18, 312, 313, 2, 'com_modules.module.188', 'Note4', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(185, 18, 314, 315, 2, 'com_modules.module.189', 'Note5', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(186, 18, 316, 317, 2, 'com_modules.module.190', 'Note6', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(187, 18, 318, 319, 2, 'com_modules.module.191', 'Base1', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(188, 18, 320, 321, 2, 'com_modules.module.192', 'Base2', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(189, 18, 322, 323, 2, 'com_modules.module.193', 'Base3', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(190, 18, 324, 325, 2, 'com_modules.module.194', 'Base4', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(191, 18, 326, 327, 2, 'com_modules.module.195', 'Base5', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(192, 18, 328, 329, 2, 'com_modules.module.196', 'Base6', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(193, 18, 330, 331, 2, 'com_modules.module.197', 'Block1', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(194, 18, 332, 333, 2, 'com_modules.module.198', 'Block2', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(195, 18, 334, 335, 2, 'com_modules.module.199', 'Block3', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(196, 18, 336, 337, 2, 'com_modules.module.200', 'Block4', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(197, 18, 338, 339, 2, 'com_modules.module.201', 'Block5', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(198, 18, 340, 341, 2, 'com_modules.module.202', 'Block6', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(199, 18, 342, 343, 2, 'com_modules.module.203', 'User1', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(200, 18, 344, 345, 2, 'com_modules.module.204', 'User2', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(201, 18, 346, 347, 2, 'com_modules.module.205', 'User3', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(202, 18, 348, 349, 2, 'com_modules.module.206', 'User4', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(203, 18, 350, 351, 2, 'com_modules.module.207', 'User5', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(204, 18, 352, 353, 2, 'com_modules.module.208', 'User6', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(205, 18, 354, 355, 2, 'com_modules.module.209', 'Footer1', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(206, 18, 356, 357, 2, 'com_modules.module.210', 'Footer2', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(207, 18, 358, 359, 2, 'com_modules.module.211', 'Footer3', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(208, 18, 360, 361, 2, 'com_modules.module.212', 'Footer4', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(209, 18, 362, 363, 2, 'com_modules.module.213', 'Footer5', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(210, 18, 364, 365, 2, 'com_modules.module.214', 'Footer6', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(211, 18, 366, 367, 2, 'com_modules.module.215', 'Maintop1', '{"core.delete":{"6":1},"core.edit":{"6":1,"4":1},"core.edit.state":{"6":1,"5":1},"module.edit.frontend":[]}'),
(212, 18, 368, 369, 2, 'com_modules.module.216', 'Maintop3', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(213, 18, 370, 371, 2, 'com_modules.module.217', 'Maintop2', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(214, 18, 372, 373, 2, 'com_modules.module.218', 'Mainbottom1', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(215, 18, 374, 375, 2, 'com_modules.module.219', 'Mainbottom2', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(216, 18, 376, 377, 2, 'com_modules.module.220', 'Mainbottom3', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(217, 18, 378, 379, 2, 'com_modules.module.221', 'Sidebar1', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(218, 18, 380, 381, 2, 'com_modules.module.222', 'Sidebar2', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(219, 18, 382, 383, 2, 'com_modules.module.223', 'Copyright1', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(220, 18, 384, 385, 2, 'com_modules.module.224', 'Copyright2', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(221, 18, 386, 387, 2, 'com_modules.module.225', 'Debug', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(222, 18, 388, 389, 2, 'com_modules.module.226', 'Breadcrumbs', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(223, 18, 390, 391, 2, 'com_modules.module.227', 'Module Variations Intro', '{"core.delete":{"6":1},"core.edit":{"6":1,"4":1},"core.edit.state":{"6":1,"5":1},"module.edit.frontend":[]}'),
(224, 18, 392, 393, 2, 'com_modules.module.228', 'Suffix 1', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(225, 18, 394, 395, 2, 'com_modules.module.229', 'Suffix 2', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(226, 18, 396, 397, 2, 'com_modules.module.230', 'Suffix 3', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(227, 18, 398, 399, 2, 'com_modules.module.231', 'Suffix 4', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(228, 18, 400, 401, 2, 'com_modules.module.232', 'Suffix 5', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(229, 18, 402, 403, 2, 'com_modules.module.233', 'Suffix 6', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(230, 18, 404, 405, 2, 'com_modules.module.234', 'Suffix 7', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(231, 18, 406, 407, 2, 'com_modules.module.235', 'Suffix 8', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(232, 18, 408, 409, 2, 'com_modules.module.236', 'Suffix 9', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(233, 18, 410, 411, 2, 'com_modules.module.237', 'Suffix 10', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(234, 18, 412, 413, 2, 'com_modules.module.238', 'Suffix 11', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(235, 18, 414, 415, 2, 'com_modules.module.239', 'Suffix 12', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(236, 18, 416, 417, 2, 'com_modules.module.240', 'Suffix 13', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(237, 18, 418, 419, 2, 'com_modules.module.241', 'Suffix 14', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(238, 18, 420, 421, 2, 'com_modules.module.242', 'Suffix 15', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(239, 18, 422, 423, 2, 'com_modules.module.243', 'Suffix 22', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(240, 18, 424, 425, 2, 'com_modules.module.244', 'Suffix 23', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(241, 18, 426, 427, 2, 'com_modules.module.245', 'Suffix 24', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(242, 18, 428, 429, 2, 'com_modules.module.246', 'Suffix 31', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(243, 18, 430, 431, 2, 'com_modules.module.247', 'Suffix 32', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(244, 18, 432, 433, 2, 'com_modules.module.248', 'Suffix 33', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(245, 18, 434, 435, 2, 'com_modules.module.249', 'Suffix 40', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(246, 18, 436, 437, 2, 'com_modules.module.250', 'Suffix 41', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(247, 18, 438, 439, 2, 'com_modules.module.251', 'Suffix 42', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(248, 18, 440, 441, 2, 'com_modules.module.252', 'Suffix 16', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(249, 18, 442, 443, 2, 'com_modules.module.253', 'Suffix 17', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(250, 18, 444, 445, 2, 'com_modules.module.254', 'Suffix 18', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(251, 18, 446, 447, 2, 'com_modules.module.255', 'Suffix 19', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(252, 18, 448, 449, 2, 'com_modules.module.256', 'Suffix 20', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(253, 18, 450, 451, 2, 'com_modules.module.257', 'Suffix 21', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(254, 18, 452, 453, 2, 'com_modules.module.258', 'Suffix 25', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(255, 18, 454, 455, 2, 'com_modules.module.259', 'Suffix 26', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(256, 18, 456, 457, 2, 'com_modules.module.260', 'Suffix 27', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(257, 18, 458, 459, 2, 'com_modules.module.261', 'Suffix 28', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(258, 18, 460, 461, 2, 'com_modules.module.262', 'Suffix 29', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(259, 18, 462, 463, 2, 'com_modules.module.263', 'Suffix 30', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(260, 18, 464, 465, 2, 'com_modules.module.264', 'Suffix 34', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(261, 18, 466, 467, 2, 'com_modules.module.265', 'Suffix 35', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(262, 18, 468, 469, 2, 'com_modules.module.266', 'Suffix 36', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(263, 18, 470, 471, 2, 'com_modules.module.267', 'Suffix 37', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(264, 18, 472, 473, 2, 'com_modules.module.268', 'Suffix 38', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(265, 18, 474, 475, 2, 'com_modules.module.269', 'Suffix 39', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(266, 18, 476, 477, 2, 'com_modules.module.270', 'Suffix 43', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(267, 18, 478, 479, 2, 'com_modules.module.271', 'Suffix 44', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(268, 18, 480, 481, 2, 'com_modules.module.272', 'Suffix 45', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(269, 18, 482, 483, 2, 'com_modules.module.273', 'Suffix 46', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(270, 18, 484, 485, 2, 'com_modules.module.274', 'Suffix 47', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(271, 18, 486, 487, 2, 'com_modules.module.275', 'Suffix 48', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(272, 18, 488, 489, 2, 'com_modules.module.276', 'How to use the module variations', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(273, 18, 490, 491, 2, 'com_modules.module.277', 'Menu Basic', '{"core.delete":{"6":1},"core.edit":{"6":1,"4":1},"core.edit.state":{"6":1,"5":1},"module.edit.frontend":[]}'),
(274, 18, 492, 493, 2, 'com_modules.module.278', 'Menu Arrow', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(275, 18, 494, 495, 2, 'com_modules.module.279', 'Menu Side', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(276, 18, 496, 497, 2, 'com_modules.module.280', 'Menu Arrow Dark', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(277, 18, 498, 499, 2, 'com_modules.module.281', 'Menu Line', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(278, 18, 500, 501, 2, 'com_modules.module.282', 'Menu Basic Color', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(279, 18, 502, 503, 2, 'com_modules.module.283', 'Menu Side Color', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(280, 18, 504, 505, 2, 'com_modules.module.284', 'Menu Side Clear', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(281, 18, 506, 507, 2, 'com_modules.module.285', 'Menu Basic Clear', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(282, 18, 508, 509, 2, 'com_modules.module.286', 'Menu Basic Dark', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(283, 18, 510, 511, 2, 'com_modules.module.287', 'Menu Arrow Color', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(284, 18, 512, 513, 2, 'com_modules.module.288', 'Menu Arrow Clear', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(285, 18, 514, 515, 2, 'com_modules.module.289', 'Menu Side Dark', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(286, 18, 516, 517, 2, 'com_modules.module.290', 'Menu Line Color', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(287, 18, 518, 519, 2, 'com_modules.module.291', 'Menu Line Clear', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(288, 18, 520, 521, 2, 'com_modules.module.292', 'Menu Line Dark', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(289, 18, 522, 523, 2, 'com_modules.module.293', 'Font Awesome Icons', '{"core.delete":{"6":1},"core.edit":{"6":1,"4":1},"core.edit.state":{"6":1,"5":1},"module.edit.frontend":[]}'),
(291, 18, 524, 525, 2, 'com_modules.module.295', 'How to add the icons to the title of the module', '{"core.delete":{"6":1},"core.edit":{"6":1,"4":1},"core.edit.state":{"6":1,"5":1},"module.edit.frontend":[]}'),
(292, 18, 526, 527, 2, 'com_modules.module.296', 'Image Styles', '{"core.delete":{"6":1},"core.edit":{"6":1,"4":1},"core.edit.state":{"6":1,"5":1},"module.edit.frontend":[]}'),
(293, 18, 528, 529, 2, 'com_modules.module.297', 'Multiple Image Styles', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(294, 18, 530, 531, 2, 'com_modules.module.298', 'Image Right', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(295, 18, 532, 533, 2, 'com_modules.module.299', 'Image Left', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(296, 18, 534, 262, 2, 'com_modules.module.300', 'Image Center', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(297, 18, 536, 537, 2, 'com_modules.module.301', 'Image Circle', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(298, 18, 538, 539, 2, 'com_modules.module.302', 'Image Rounded', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(299, 18, 540, 541, 2, 'com_modules.module.303', 'Image Shadow', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(300, 18, 542, 543, 2, 'com_modules.module.304', 'Image Polaroid', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(301, 18, 544, 545, 2, 'com_modules.module.305', 'Polaroid Color Style', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(302, 18, 546, 547, 2, 'com_modules.module.306', 'Polaroid Dark Style', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(303, 18, 548, 549, 2, 'com_modules.module.307', 'Polaroid Clear Style', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(304, 18, 550, 551, 2, 'com_modules.module.308', 'Image Left Polaroid Rounded', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(305, 18, 552, 553, 2, 'com_modules.module.309', 'Image Right Shadow Circle', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(307, 18, 554, 555, 2, 'com_modules.module.311', 'Default Button', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(308, 18, 556, 557, 2, 'com_modules.module.312', 'Primary Button', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(309, 18, 558, 559, 2, 'com_modules.module.313', 'Button Icons', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(310, 18, 560, 561, 2, 'com_modules.module.314', 'Button Sizes', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(311, 18, 562, 563, 2, 'com_modules.module.315', 'Bootstrap Buttons', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(313, 18, 564, 565, 2, 'com_modules.module.317', 'Blockquote Dark Style', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(314, 18, 566, 567, 2, 'com_modules.module.318', 'Blockquote Color Style', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(315, 18, 568, 569, 2, 'com_modules.module.319', 'Blockquote', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(317, 18, 570, 571, 2, 'com_modules.module.321', 'Square List Style', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(318, 18, 572, 573, 2, 'com_modules.module.322', 'Lead Body Copy', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(319, 18, 574, 575, 2, 'com_modules.module.323', 'Content Boxes', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(320, 18, 576, 577, 2, 'com_modules.module.324', 'Drop Caps', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(321, 18, 578, 579, 2, 'com_modules.module.325', 'Headings', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(322, 18, 580, 581, 2, 'com_modules.module.326', 'How to use the list styles', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(323, 18, 582, 583, 2, 'com_modules.module.327', 'Emphasis Classes', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(324, 18, 584, 585, 2, 'com_modules.module.328', 'Inline Labels', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(325, 18, 586, 587, 2, 'com_modules.module.329', 'Tables', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(326, 18, 588, 589, 2, 'com_modules.module.330', 'Code', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(327, 18, 590, 591, 2, 'com_modules.module.331', 'Circle List Style', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(328, 18, 592, 593, 2, 'com_modules.module.332', 'Color List Style', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(329, 18, 594, 595, 2, 'com_modules.module.333', 'Dark List Style', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(369, 18, 596, 597, 2, 'com_modules.module.373', 'Extensions Intro', '{"core.delete":{"6":1},"core.edit":{"6":1,"4":1},"core.edit.state":{"6":1,"5":1},"module.edit.frontend":[]}'),
(370, 18, 598, 599, 2, 'com_modules.module.374', 'Navigations Style Intro', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(393, 18, 600, 601, 2, 'com_modules.module.397', 'How to apply the circle, color and dark list styles', '{"core.delete":{"6":1},"core.edit":{"6":1,"4":1},"core.edit.state":{"6":1,"5":1},"module.edit.frontend":[]}'),
(397, 18, 602, 603, 2, 'com_modules.module.401', 'Error Page Intro', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(398, 18, 604, 605, 2, 'com_modules.module.402', 'Docs Page Intro', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(403, 18, 606, 607, 2, 'com_modules.module.407', 'Main Navigation Styles', '{"core.delete":{"6":1},"core.edit":{"6":1,"4":1},"core.edit.state":{"6":1,"5":1},"module.edit.frontend":[]}'),
(404, 18, 608, 609, 2, 'com_modules.module.408', 'Vertical Menu Styles', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(406, 18, 610, 611, 2, 'com_modules.module.410', 'Horizontal Menu Styles', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(408, 18, 612, 613, 2, 'com_modules.module.412', 'How to use the vertical menu styles', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(411, 18, 614, 615, 2, 'com_modules.module.415', 'How to use the horizontal menu styles', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(413, 18, 616, 617, 2, 'com_modules.module.417', 'Horizontal Menu Clear', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(414, 18, 618, 619, 2, 'com_modules.module.418', 'Horizontal Menu ', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(415, 18, 620, 621, 2, 'com_modules.module.419', 'Horizontal Menu Dark', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(416, 18, 622, 623, 2, 'com_modules.module.420', 'Horizontal Menu Color', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(417, 18, 624, 625, 2, 'com_modules.module.421', 'How to use the 404 error page', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(418, 18, 626, 627, 2, 'com_modules.module.422', 'How to use the custom styles for our extensions', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(419, 18, 628, 629, 2, 'com_modules.module.423', 'How to add icons to the main navigation items', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(420, 18, 630, 631, 2, 'com_modules.module.424', 'How to customize the icons using the template settings', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(421, 18, 632, 633, 2, 'com_modules.module.425', 'How to set up the main navigation of the template', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(422, 18, 634, 635, 2, 'com_modules.module.426', 'How to use the visible/hidden Bootstrap classes', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(423, 18, 636, 637, 2, 'com_modules.module.427', 'How to use the template''s settings (parameters)', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(424, 18, 638, 639, 2, 'com_modules.module.428', 'The settings tab', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(425, 18, 640, 641, 2, 'com_modules.module.429', 'The layout tab', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(426, 18, 642, 643, 2, 'com_modules.module.430', 'The logo tab', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}');
INSERT INTO `#__assets` (`id`, `parent_id`, `lft`, `rgt`, `level`, `name`, `title`, `rules`) VALUES
(427, 18, 644, 645, 2, 'com_modules.module.431', 'The mobile tab', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(428, 18, 646, 647, 2, 'com_modules.module.432', 'The analytics tab', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(478, 65, 37, 38, 3, 'com_content.category.24', 'Offline Page', '{"core.create":{"6":1,"3":1},"core.delete":{"6":1},"core.edit":{"6":1,"4":1},"core.edit.state":{"6":1,"5":1},"core.edit.own":{"6":1,"3":1}}'),
(479, 18, 648, 649, 2, 'com_modules.module.471', 'How to use the offline page', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(480, 18, 650, 651, 2, 'com_modules.module.472', 'Offline Page Intro', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(482, 18, 652, 653, 2, 'com_modules.module.474', 'FavEffects', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(483, 18, 654, 655, 2, 'com_modules.module.475', 'FavGlyph', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(485, 18, 656, 657, 2, 'com_modules.module.477', 'FavPromote', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(486, 18, 658, 659, 2, 'com_modules.module.478', 'FavSlider Responsive Slideshow', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(487, 18, 660, 661, 2, 'com_modules.module.479', 'FavSocial', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(490, 18, 662, 663, 2, 'com_modules.module.482', 'Menu Home', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(491, 63, 21, 22, 3, 'com_content.article.10', 'Say hello to Favourite! ', '{"core.delete":[],"core.edit":{"4":1},"core.edit.state":{"5":1}}'),
(504, 18, 664, 665, 2, 'com_modules.module.483', 'How to use the pull-left/pull-right Bootstrap classes', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(505, 18, 666, 667, 2, 'com_modules.module.484', 'Download theme', '{"core.delete":{"6":1},"core.edit":{"6":1,"4":1},"core.edit.state":{"6":1,"5":1},"module.edit.frontend":[]}'),
(506, 18, 668, 669, 2, 'com_modules.module.485', 'Offline Page', '{"core.delete":{"6":1},"core.edit":{"6":1,"4":1},"core.edit.state":{"6":1,"5":1},"module.edit.frontend":[]}'),
(507, 18, 670, 671, 2, 'com_modules.module.486', 'Typography Intro', '{"core.delete":{"6":1},"core.edit":{"6":1,"4":1},"core.edit.state":{"6":1,"5":1},"module.edit.frontend":[]}'),
(508, 18, 672, 673, 2, 'com_modules.module.487', 'FavSlider Responsive Slideshow', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(509, 18, 674, 675, 2, 'com_modules.module.488', 'FavGlyph', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(510, 18, 676, 677, 2, 'com_modules.module.489', 'FavPromote', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(511, 18, 678, 679, 2, 'com_modules.module.490', 'FavEffects', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}');

